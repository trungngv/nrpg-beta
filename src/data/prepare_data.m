function [params,xtest,ytest] = prepare_data(alg,dataset,test_prediction,num_modes,...
  standardise_x,log_scale)
%PREPARE_DATA prepare_data(alg,dataset,test_prediction,num_modes,...
%  standardise_x,func_hypinit,log_scale)
%
% Prepare data for one of the available models (gprn-mf or gprn-npv) and a
% dataset. Some input parameters may only be applicable to specific models.
% For these input, simply pass the empty value ([]).
% 
% INPUT:
%   - alg: 1 for gprn-mf and 2 for gprn-npv
%   - dataset: name of the data set
%   
%
% OUTPUT:
%   - params : the initialized model with all necessary parameters
%   - xtest : the pre-processed xtest
%   - ytest : the pre-processed ytest
%
% Trung V. Nguyen
% 19/11/12
%
GPRNMF = 1;
GPRNNPV = 2;

if alg == GPRNMF
  switch dataset
  case 'synthetic'
    Ntrain = 30; Ntest = 20;
    num_latents = 3; num_outputs = 3; D = 2; num_modes = 1;
    sigma_f = 0.000; sigma_y = 0.1;
    theta_f = log(rand(D+1,1));
    theta_w = log(rand(D+1,1));
    theta_f(end) = 0.5+theta_f(end);
    theta_w(end) = 0.5+theta_w(end);
    a = ones(num_latents,2)*theta_f(end);
    [params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs,...
        num_modes, theta_f, theta_w, a, sigma_f, sigma_y,true);
    % prediction on new/test data or missing data
    if test_prediction
      ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
      params.Y(ind_n,1) = nan;
    else
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end
      
  case 'jura1' % jura with standard gp -- 1 output
    num_latents = 2; num_outputs = 1;
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
        'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    Y = Y(:,1); ytest = ytest(:,1);
    if ~test_prediction
      X = [X; xtest]; Y = [Y; ytest];
    end
    params.X = X; params.Y = Y;
    
  case 'jura'
    num_latents = 2; num_outputs = 3; % jura
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
        'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    X = [X; xtest];
    if test_prediction
      Y = [Y; [nan(size(ytest,1),1), ytest(:,2:3)]];
    else
      Y = [Y; ytest];
    end
    params.X = X; params.Y = Y;
    
  case {'concrete','concrete3'}
    num_latents = 1;
    [X, Y, xtest, ytest] = load_data('data/concreteslump', dataset);
    num_outputs = size(Y,2);
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    features = [3:5];
    X = X(:,features); xtest = xtest(:,features);
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    params.X = X; params.Y = Y;
    
  case 'sarcos'
    Ntrain = 100; Ntest = 100;
    num_latents = 1; num_outputs = 1;
    related_outputs = [2]; %[2,3,4,7]
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/SARCOS', 'sarcos');
    X = X(1:Ntrain,:); Y = Y(1:Ntrain,related_outputs);
    xtest = xtest(1:Ntest,:); ytest = ytest(1:Ntest,related_outputs);
    params.X = X; params.Y = Y;
    
  otherwise
    if ~isempty(strfind(dataset, 'geneExpression')) % for gene data set
          [X, Y, xtest, ytest] = load_data('data/gene', dataset);
      num_latents = 1; num_outputs = size(Y,2);
      params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
          'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
      % pre-process y
      Y = Y(:,1:num_outputs);
      ytest = ytest(:,1:num_outputs);
      params.X = X; params.Y = Y;
    end
  end  
elseif alg == GPRNNPV
  switch (dataset)
  case 'synthetic'
    Ntrain = 100; Ntest = 100;
    num_latents = 2; num_outputs = 3; D = 2;
    [X,Y,xtest,ytest] = load_data('data/synthetic', 'synthetic');
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,...
        [],[],[],[],[],false);
    % prediction on new/test data or missing data
    if ~test_prediction
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end
    params.X = X; params.Y = Y;
    
  case 'jura1' % jura with standard gp -- 1 output
    Ntrain = 259; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 1;
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if ~test_prediction
      X = [X; xtest];
      Y = [Y; ytest];
    end
    params.X = X(1:Ntrain,:); params.Y = Y(1:Ntrain,:);
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    
  case 'jura'
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    X = [X; xtest];
    [Ntrain,D] = size(X);
    [Ntest,num_outputs] = size(ytest);
    num_latents = 2;
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    if (test_prediction)
      Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
    else
      Y = [Y; ytest];
    end
    params.X = X; params.Y = Y;

  case {'concrete3','concrete'}
    [X, Y, xtest, ytest] = load_data('data/concreteslump', dataset);
    features = [3:5];
    X = X(:,features); xtest = xtest(:,features);
    num_outputs = size(Y,2); num_latents = 2;
    [Ntrain, D] = size(X);
    Ntest = size(xtest,1);
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    params.X = X; params.Y = Y;
    
  case 'sarcos'
    Ntrain = 100; Ntest = 100; D = 21;
    num_latents = 1; num_outputs = 1;
    [X, Y, xtest, ytest] = load_data('data/SARCOS', 'sarcos');
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    related_outputs = [3];
    X = X(1:Ntrain,:); Y = Y(1:Ntrain,related_outputs);
    xtest = xtest(1:Ntest,:); ytest = ytest(1:Ntest,related_outputs);
    params.X = X; params.Y = Y;
  end
end

% pre-process x and y
if standardise_x
  [params.X, Xmean, Xstd] = standardize(params.X,1,[],[]);
  xtest = standardize(xtest,1,Xmean,Xstd);
  %TODO: how to normalize xtest for Jura case when xtest is in X?
end
if log_scale
  [params.Y, Ymean, Ystd] = standardize(log(params.Y),1,[],[]);
else
  [params.Y, Ymean, Ystd] = standardize(params.Y,1,[],[]);
end

params.Ymean = Ymean;
params.Ystd = Ystd;
if isempty(log_scale)
  params.logscale = false;
else
  params.logscale = log_scale;
end
save([dataset '.mat'], 'Ymean', 'Ystd');

% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);
  
end


