% function x = read_mvlens_items_features(fitems)
% 
% Read item features from the movielens item features file.
%
% author: trung (trung.ngvan@gmail.com)
% last update: 28 june 2012

function x = read_mvlens_items_features(fitems)
f = importdata(fitems, '|');
% matlab conveniently reads the categorical feature into a matrix
% all other features (id, title, release dates, url) are ignored
x = f.data;
return;

