% function F = read_mvlens_ratings(fratings, total_users, total_items, nusers, nitems)
%
% Read the orders of items by all users from the given file. Each line is
% the order of items that a user prefers. Smaller order means more
% preferred. This can be think of as a 'soft utility' that user gives the
% items. 
% 
% Currently only reads the orders for 10 most preffered items.
%
% RETURN
%     a N x M matrix where M is the number of users and N is the number of
%     items; each column is the ratings of an user.
%
% author: trung nguyen (trung.ngvan@gmail.com)
% last update: 26 june 2012
%

function F = read_mvlens_ratings(fratings, total_users, total_items)
rng(1110, 'twister');
ratings = importdata(fratings, '\t');
ratings = ratings(:, [1 2 3]); % ignore the timestamp column
F = zeros(total_items, total_users);
for i = 1 : size(ratings, 1)
  F(ratings(i, 2), ratings(i, 1)) = ratings(i, 3);
end

return;
