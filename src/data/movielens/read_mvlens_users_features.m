% function x = read_mvlens_users_features(fusers, use_age, use_gender,
%   user_occuptation, use_zip)
% 
% Read users' features from the user features file for the movielens data.
% User demographic information contains: age, gender, occupation, zip code.
% This function converts all demographic features into the 1-of-M vector
% representation.
% 
% trung v. nguyen (trung.ngvan@gmail.com)
% last updated: 26 june 2012
%
function t = read_mvlens_users_features(fusers, use_age, use_gender, ...
  use_occupation, use_zip)
fid = fopen(fusers);
% id, age, gender, occupation, zip code
users = textscan(fid, '%d%d%s%s%s', 'Delimiter', '|');
fclose(fid);
nusers = size(users{1}, 1);
% only need the first 2 characters of zip code (bigger region)
for i = 1 : nusers
  str = char(users{5}(i));
  users{5}{i} = str(1:2);
end

min_age = double(min(users{2}));
max_age = double(max(users{2}));
age_categories = 5; % 4 categories
age_ranges = linspace(min_age * 1.0, max_age, age_categories);
%age_to_category(age_ranges, 50)

occupation_map = unique(users{4});
zip_map = unique(users{5});
% find_key(occupation_map, 'executive')
% find_key(zip_map, '23')
Ioccupation = eye(length(occupation_map));
Izip = eye(length(zip_map));
Iage = eye(age_categories - 1);

t = cell(nusers, 1);
for i = 1:nusers
  t{i} = [];
  if use_age
    t{i} = Iage(age_to_category(age_ranges, users{2}(i)), :);
  end
  if use_gender
    gender = users{3}(i);
    t{i} = [t{i}, isequal('M', gender{1})];
  end
  if use_occupation
    t{i} = [t{i}, Ioccupation(find_key(occupation_map, users{4}(i)), :)];
  end
  if use_zip
    t{i} = [t{i}, Izip(find_key(zip_map, users{5}(i)), :)];
  end
end
t = cell2mat(t);

return;

% map the age of an user to a category
function cat = age_to_category(ranges, age)
  for cat = length(ranges):-1:1
    if age > ranges(cat)
      return;
    end
  end
cat = 1;

return;

% find the key of a value in a map
function key = find_key(map, value)
  for key = 1 : length(map)
    if isequal(map{key}, value{1})
      return;
    end
  end
  
return;
  
