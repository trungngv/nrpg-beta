function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] ...
  = generate_mvlens_data()
% function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ytrue_test] =  ...
%     generate_mvlens_data()
%
% Read the movielens data and convert them into the structured required by the gppe package
%
% OUTPUT:
%   - idx_pairs: The matrix of all pairwise item comparisons:
%           idx_pairs(i,1) > idx_pairs(i,2)
%   - train_pairs: Cell array of M elements. Each element is a O_m x 2 matrix 
%       where O_m is the number of preferences observed for the corresponding
%       user. Each row all_pairs{m} contains a preference relation 
%       of the form train_pairs{m}(1) > train_pairs{m}(2)     
%   - train_t: Matrix of training users' features 
%   - x: Matrix of items' features 
%   - test_t: vector of test user's features 
%   - test_idx: Index of test users 
%   - ytrue_test: Binary vector indicating if the corresponding 
%       item comparisons hold for the test users
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 26/06/2012
%% Read data from files
fusers = 'u.user'; fitems = 'u.item'; fratings = 'u.data';
total_users = 943; total_items = 1642;
use_age = 1; use_gender = 1; use_occupation = 1; use_zip = 1;
all_t = read_mvlens_users_features(fusers, use_age, use_gender, use_occupation, use_zip);
all_x = read_mvlens_items_features(fitems);
all_F = read_mvlens_ratings(fratings, total_users, total_items);
N = 10; M = 40;
[t, x, F] = get_subset(all_t, all_x, all_F, M, N);
t = t + rand(size(t)) / 10000;
x = x + rand(size(x)) / 10000;

%% Set-up for training data
Mtrain = 20;
idx_pairs = combnk(1:N, 2); % all possible pairs of items
% convert ratings into pairwise preference
Y = ratings_to_preferences(F, idx_pairs);
train_idx = randperm(size(t, 1), Mtrain);
Ytrain = Y(:, train_idx);

% For each training user we create a cell that contains the ordered pairs
all_pairs = cell(1, Mtrain);
fprintf('Preferences for users:\n');
for j = 1 : Mtrain
  tmp_pairs = idx_pairs;
  % flip the indices so that idx_1 in a pair is the prefered item
  idx_0 = find(Ytrain(:, j) == 0); % indices in reverse order
  tmp_pairs(idx_0,:) = fliplr(idx_pairs(idx_0,:));
  tmp_pairs(Ytrain(:, j) == -1, :) = [];
  all_pairs{j} = tmp_pairs;
  fprintf('%d: %d,  ', j, length(tmp_pairs));
  if mod(j, 10) == 0
    fprintf('\n');
  end  
end
    
    
%% Assigning training and testing data (take the last user as test user)
test_idx = train_idx; % test unseen data
%test_idx = setdiff(1:M, train_idx); % test remaning users
Ntrain_ratio = 0.5; % How many training pairs to use
train_pairs = get_training_pairs(all_pairs, Mtrain, Ntrain_ratio);
train_t = t(train_idx, :);
test_t = t(test_idx, :);
ytrue_test = Y(:, test_idx);
ftrue_test = F(:, test_idx);

%% saving data for C++ code and for elicitation queries on sushidata
DataTr = convert_pairs_to_matrix(train_pairs);
save('sushidata.mat', 'F', 'train_pairs', 'train_t', 'x', ...
    'test_t',  'DataTr', 'test_idx', 'idx_pairs', 'all_pairs');
    
return;

%% convert ratings to preferences (with missing indicated by -1)
function Y = ratings_to_preferences(F, idx_pairs)
Y = zeros(length(idx_pairs), size(F, 2));
for p = 1 : length(idx_pairs)
  for j = 1 : size(F, 2)
    Y(p, j) = pref2(F, j, idx_pairs(p, 1), idx_pairs(p, 2));
  end
end

return;

%% return the preference of a user for a pair of movies
% 1 means movie1 is prefered to movie2, 0 means the contrary, and -1 means
% undecided (missing pref)
function p = pref(F, user, movie1, movie2)
if F(movie1, user) > 0 && F(movie2, user) > 0
  if F(movie1, user) ~= F(movie2, user)
    p = (F(movie1, user) - F(movie2, user)) > 0;
    return;
  end
  % determine pref for 2 movies with same ratings
  % this is heuristic only can should be used with care
  same_users_1 = sum(F(movie1, :) == F(movie1, user));
  same_users_2 = sum(F(movie2, :) == F(movie2, user));
  if same_users_1 ~= same_users_2
    p = (same_users_1 - same_users_2) > 0;
    return;
  end
end
p = -1;

return;

%% return the preference of a user for a pair of movies
% This simply assigns pref(user, movie1, movie2) = F(movie1, user) >=
% F(movie2, user)
function p = pref2(F, user, movie1, movie2)
if F(movie1, user) > 0 && F(movie2, user) > 0
  p = F(movie1, user) >= F(movie2, user);
  return;
end
p = -1;

return;

%% get training pairs 
function train_pairs = get_training_pairs(all_pairs, Mtrain, ratio)
all_pairs = all_pairs(1 : Mtrain);
train_pairs = cell(Mtrain, 1);
for j = 1 : Mtrain
    pairs = all_pairs{j};
    % random Ntrains pairs for training
    idx   = randperm(size(pairs, 1));
    Npairs = floor(size(pairs, 1) * ratio);
    idx   = idx(1:Npairs);
    train_pairs{j} = pairs(idx, :);
end

return;

%% get a subset of the movielens data for gppe
function [t, x, F] = get_subset(all_t, all_x, all_F, M, N)
% only take movies rated by > 100 users
valid_movies = find(sum(all_F ~= 0, 2) > 300);
rnd_ind = randperm(length(valid_movies), N);
movie_ind = valid_movies(rnd_ind);

% only take users rated > 50 movies
valid_users = find(sum(all_F ~= 0, 1) > 100);
rnd_ind = randperm(length(valid_users), M);
user_ind = valid_users(rnd_ind);

F = all_F(movie_ind, user_ind);
t = all_t(user_ind, :);
x = all_x(movie_ind, :);

return;