function [trainX, trainY, testX, testY] = load_data_gpr(basedir, dataset, ntrains)
% LOAD_DATA_GPR [X, y, Xtest, ytest] = load_data_grp(basedir, dataset)
% 
% Loads training and test data from one of the available data set used for
% standard GP regression. Both inputs and outputs are standardized to have
% mean 0 and variance 1.
%
% Currently available data sets are:
% 1. SYNTH2 : dataset = 'SYNTH2'
% 2. SYNTH8 : dataset = 'SYNTH8'
% 3. CHEM : dataset = 'CHEM'
% 4. SARCO : dataset = 'SARCO'
% 
% See Chalupka, Williams, and Murray (2012) for detailed information about
% the data sets.
%
% INPUT
%   - basedir : path to the directory containing the datasets
%   - dataset : one of the dataset names
%   - ntrains : number of trainings (and test) to use; this is so that we
%   can fit the K^{-1} matrix into the memory
%
% OUTPUT
%    - trainX, trainY : training inputs and outputs (size N x d) and (N x p) where N
%    is the number of points, d = input dimension, p = output dimension (1
%    for most data sets)
%    - testX, testY : similar to X, y but for test points
%
% Code by Krzysztof Chalupka, University of Edinburgh 
% and California Institute of Technology, 2012
%
% Adapted by Trung V. Nguyen
% Last updated: 20/07/12

%----------------------------------------
% MODIFY THIS TO
% ADD YOUR OWN DATASET PREPROCESSING.
%----------------------------------------
if strcmp(dataset, 'SYNTH2')
    %----------------------------------------
    % SYNTH2 
    %----------------------------------------
    load('SYNTH/T02');
    ids=randperm(size(x,1)); 
    n=ceil(length(ids)/2);
    D=2;
    trainX=x(ids(1:n),:);
    trainY=y(ids(1:n),:);
    testX=x(ids(end-n+1:end),:);
    testY=y(ids(end-n+1:end),:);

elseif strcmp(dataset, 'SYNTH8')
    %----------------------------------------
    % SYNTH8
    %----------------------------------------
    load('SYNTH/T08');
    load('SYNTH/T00'); % Loads Gaussian noise.
    ids=randperm(size(x,1));
    n = ceil(length(ids)/2);
    D=8;
    trainX=x(ids(1:n),:);
    trainY=y(ids(1:n),:);
    testX=x(ids(end-n+1:end),:);
    testY=y(ids(end-n+1:end),:);
    trainY = trainY + sqrt(0.001)*noise(ids(1:n)); % Add noise to the data. 
    testY = testY + sqrt(0.001)*noise(ids(end-n+1:end));
    
elseif strcmp(dataset, 'CHEM')
    %----------------------------------------
    % CHEM 
    %----------------------------------------
    load('CHEM/vinylBromide.txt');
    x=vinylBromide(:,1:15);
    y=vinylBromide(:,16);
    rand('seed', 0);
    ids=randperm(size(x,1));
    n=ceil(length(ids)/2); 
    D=15;
    trainX=x(ids(1:n),:);
    trainY=y(ids(1:n),:);
    testX=x(ids(end-n+1:end),:);
    testY=y(ids(end-n+1:end),:);

elseif strcmp(dataset,'SARCOS')
    %----------------------------------------
    % SARCOS
    %----------------------------------------
    train=load('SARCOS/sarcos_inv.mat')
    train=train.sarcos_inv;
    test=load('SARCOS/sarcos_inv_test.mat')
    test=test.sarcos_inv_test;
    n=size(train,1);
    ids = randperm(n);
    D=21;
    trainX = train(1:n,1:D); trainY = train(1:n,D+1);
    testX  = test(:,1:D); testY = test(:,D+1);
else
    error(['Data set' dataset ' not found in the directory "' basedir '"']);
    return;
end

%------------------------------------------------------------------------
% DON'T MODIFY THIS.
% Limit the number of training and testing to use (to fit in memory)
% Normalize data to zero mean, variance one. 
%------------------------------------------------------------------------
if isempty(ntrains)
  ntrains = n;
end
ind = randperm(size(trainX,1), ntrains);
trainX = trainX(ind,:);
trainY = trainY(ind,:);
ind = randperm(size(testX,1),ntrains);
testX = testX(ind,:);
testY = testY(ind,:);
n = ntrains;
meanMatrix = repmat(mean(trainX), n, 1);
trainYMean = mean(trainY);
trainYStd  = std(trainY);
stdMatrix  = repmat(std(trainX), n, 1);
trainX = (trainX - meanMatrix);
trainX = trainX./stdMatrix;
trainY = (trainY - trainYMean)./trainYStd;

testX  = (testX-repmat(meanMatrix(1,:), size(testX,1),1))./repmat(stdMatrix(1,:), size(testX,1),1);
testY  = (testY - trainYMean)./trainYStd;

% Reseed the rng.
rand('seed', 100*sum(clock));
clear x n D
