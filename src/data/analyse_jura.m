% analyse the jura data set
[X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
X = [X; xtest];
Y = [Y; ytest];
input_dist = sq_dist(X', X');
N = size(X,1);
radius = 0.0001;
in_small_ball = input_dist < radius;
diff = 0;
cnt = 0;
for i=1:N-1
  for j=i+1:N
    if (in_small_ball(i,j))
      %disp([X(i,:), X(j,:), abs(Y(i,1)-Y(j,1))])
      diff = diff + abs(Y(i,1) - Y(j,1));
      cnt = cnt + 1;
    end
  end
end

fprintf('total points in a very small ball of radius %.5f\n', radius);
disp(cnt)
disp('avg difference in output 1 of these points')
disp(diff/cnt);
disp('mean of output 1')
disp(mean(Y(:,1)))

figure;
plot3(X(:,1),X(:,2),Y(:,1), 'o');
title('Jura Cd - output 1');

figure;
plot3(X(:,1),X(:,2),Y(:,2),'o');
title('Jura Cd - output 2');

figure;
plot3(X(:,1),X(:,2),Y(:,3),'o');
title('Jura Cd - output 3');
