data = 'data/gene/drosExpression1000Fold';
folds = 10; N = 12; P = 1000;
for i = 1:folds
  load([data num2str(i) '.mat']);
  % convert X,XTest,y,yTest to matrix
  myX = (0:11)'; myXTest = (0:11)';
  myY = zeros(N,P); myYtest = zeros(N,P);
  for p=1:P
    myY(:,p) = y{p};
    myYtest(:,p) = yTest{p};
  end
  save_data(myX,myY,myXTest, myYtest, 'data/gene', ['geneExpression' num2str(i)]);
end

