function [X, Y, Xtest, Ytest] = read_jura_data(metal)
%READ_JURA_DATA [X, Y, Xtest, Ytest] = read_jura_data()
% 
% Use the existing function in the cgp package to read the Jura data set
% and return the data in the standard format for our procedure.
%
% INPUT
%   - metal : one of Cd, Cu, or Zn
%
% OUTPUT
%   - X : N x D (training samples x input dimension)
%   - Y : N x p (training samples x #outputs)
%   - Xtest : Ntest x D
%   - Ytest : Ntest x p
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 06 Jul 2012
%
% XTemp{1},YTemp{1} : training points for the first output (missing 100 points), 
% X/YTemp{2}, X/YTemp{3}: full trainingp points for 2 remaning outputs
[XTemp, yTemp, XTestTemp, yTestTemp] = mapLoadData(['juraData' metal]);
X = XTemp{1};
[N ~] = size(X);
Xtest = XTestTemp{1};
Y = [yTemp{1}(1:N), yTemp{2}(1:N), yTemp{3}(1:N)];
Ytest = [yTestTemp{1}, yTestTemp{2}, yTestTemp{3}];
end

