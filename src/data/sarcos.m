load('data/SARCOS/sarcos_inv');
load('data/SARCOS/sarcos_inv');
rng(1110, 'twister');
perm = randperm(size(sarcos_inv,1),size(sarcos_inv,1));
sarcos_inv = sarcos_inv(perm,:);
perm = randperm(size(sarcos_inv_test,1),size(sarcos_inv_test,1));
sarcos_inv_test = sarcos_inv_test(perm,:);
Ntrain = 5000;
Ntest = 1000;
trainIdx = randperm(size(sarcos_inv,1), Ntrain);
testIdx = randperm(size(sarcos_inv_test,1), Ntest);
x = sarcos_inv(trainIdx, 1:21);
y = sarcos_inv(trainIdx, 22:end);
xtest = sarcos_inv_test(testIdx, 1:21);
ytest = sarcos_inv_test(testIdx, 22:end);
save_data(x, y, xtest, ytest, 'data/SARCOS', 'sarcos');

