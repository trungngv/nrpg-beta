% function F = read_users_orders(forders)
%
% Read the orders of items by all users from the given file. Each line is
% the order of items that a user prefers. Smaller order means more
% preferred. This can be think of as a 'soft utility' that user gives the
% items. 
% 
% Currently only reads the orders for 10 most preffered items.
%
% RETURN
%     a N x M matrix where M is the number of users and N is the number of
%     items; each column is the order of an user.
%
% author: trung nguyen (trung.ngvan@gmail.com)
% last update: 26 june 2012
%

function F = read_users_orders(forders)
orders = importdata(forders, ' ', 1);
F = orders.data;
F(:, [1 2]) = []; % remove first 2 columns
F = F';
return;
