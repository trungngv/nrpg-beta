% function x = read_items_features(fusers)
% 
% Read users' features from the given file fusers.
%
% The sushi3.udata file contains features of 5000 users whose features are
% stored in the line corresponding to their ids.
% 1.  the user ID
% 2.  gender   0:male 1:female
% 3.  age      0:15-19  1:20-29   2:30-39   3:40-49   4:50-59    5:60-
% 4.  the total time need to fill questionnaire form
% 5.  prefecture ID at which you have been the most longly lived until 15 years old
% 6.  region ID at which you have been the most longly lived until 15 years old
% 7.  east/west ID at which you have been the most longly lived until 15 years old
% 8.  prefecture ID at which you currently live
% 9.  region ID at which you currently live
% 10. east/west ID at which you currently live
% 11. 0 if features 5 and 8 are equal; 1 otherwise
% 
% This function converts the categorical features 3 (age), 5 - 10 to 1-of-M vector
% and ignores feature 1 (user id), 4 (time), and 11.
% 
% trung nguyen (trung.ngvan@gmail.com)
% last updated: 26 june 2012
%
function t = read_users_features(fusers)
fid = fopen(fusers);
users = textscan(fid, '%d%d%d%d%d%d%d%d%d%d%d');
fclose(fid);
D = 1 + 6 + (48 + 12 + 1) * 2;
%D = 1 + 6 + 12;
N = size(users{1}, 1);
t = zeros(N, D);
% use this identity matrix to replace a categorial value with a 1-of-M vector
I6 = eye(6); % for age 
I48 = eye(48); % for prefecture no
I12 = eye(12); % for region no
for i = 1:N
  % not using the prefecture no (might be too detailed) and also cheaper
  % computation. however this somehow causes error ??
% t(i, :) = [users{2}(i), I6(users{3}(i) + 1, :), I12(users{9}(i) + 1, :)];
  t(i, :) = [users{2}(i), I6(users{3}(i) + 1, :), I48(users{5}(i) + 1, :), ...
    I12(users{6}(i) + 1, :), users{7}(i), I48(users{8}(i) + 1, :), I12(users{9}(i) + 1, :), users{10}(i)];
end

return;

