% function x = read_items_features(fitems)
% 
% Read item features from the given file
% The sushi3.idata file contains features of 100 items but ids of items in
% set A are inconsistent with theirs in set B. Can try 2 things
% 1) Use ids in set A as true ids (0 -> 9)
% 2) Find ids for items in set A in the file by their names
%
% author: trung (trung.ngvan@gmail.com)
% last update: 26 june 2012

function x = read_items_features(fitems)
fid = fopen(fitems);
sushis = textscan(fid, '%f%s%f%f%f%f%f%f%f');
fclose(fid);
D = 18;
N = size(sushis{1}, 1);
x = zeros(N, D, 'double');
% use this identity matrix to replace a categorial value with a 1-of-M vector
I12 = eye(12);
for i = 1:N
  x(i, :) = [sushis{3}(i), sushis{4}(i), I12(sushis{5}(i) + 1, :), ...
    sushis{6}(i), sushis{7}(i), sushis{8}(i), sushis{9}(i)];
end

% standardize the last 4 features (15 -> 18) to be N(0,1)
% (this has immediate effect on learning as the negative likelihood
% decreases after this step)
for col = 15:18
  xcol = x(:, col);
  x(:, col) = (xcol - mean(xcol)) / std(xcol);
end

return;

