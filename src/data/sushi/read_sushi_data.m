% Deprecated.
%
% Read the sushi data and create set of training data and test data
function [X, y, Xtest, ytest] = read_sushi_data(fsushi, fscore, Nusers, Nitems)
% fsushi : the file containing features of sushi
% fscore : the file containing scores of sushis given by users
% Nusers : number of users to use
% Nitems: number of items to use

fid = fopen(fsushi);
allSushis = textscan(fid, '%d%s%d%d%d%f%f%f%f');
fclose(fid);
% first try the 10 most common sushis (line 1-10)
% can random sushis in later tests

items = cell(Nitems, 1);
% use this identity matrix to replace a categorial value with a 1-of-M vector
I12 = eye(12);
for i = 1:Nitems
  items{i} = [I12(allSushis{5}(i) + 1, :) allSushis{6}(i) allSushis{7}(i) allSushis{8}(i) allSushis{9}(i)];
end
D = length(items{1}); % dimension of feature vector for each input (item)

ratingMatrix = load(fscore);
ratingMatrix = ratingMatrix(:, 1:Nitems);
itemsRatedPerUser = sum(ratingMatrix >= 0, 2);
[~, topUserId] = sort(itemsRatedPerUser, 'descend');
% Select Nusers that have most items rated
% later can select random users to test the cold-start probelm

X = cell(1, Nusers);
y = cell(1, Nusers);
Xtest = cell(1, Nusers);
ytest = cell(1, Nusers);
for i = 1:Nusers
  % get all items rated and their ratings (output points) for this task
  rated = find(ratingMatrix(topUserId(i), :) >= 0);
  ratings = ratingMatrix(topUserId(i), rated);
  
  % reserve 2 points for validation/test and remaining for training
  validationPoints = 2;
  p = randperm(length(rated));
  p = p(1:validationPoints);
  
  % store inputs and outputs of training and testing data for each task/user
  X{i} = [];
  Xtest{i} = [];
  y{i} = [];
  ytest{i} = [];
  for idx = 1:length(rated)
    if ~ismember(idx, p)
      X{i} = [X{i}; items{rated(idx)}]; % training input
      y{i} = [y{i}; ratings(idx)]; % corresponding training output
    else
      Xtest{i} = [Xtest{i}; items{rated(idx)}]; % test input
      ytest{i} = [ytest{i}; ratings(idx)]; % corresponding test output
    end
  end
end

end