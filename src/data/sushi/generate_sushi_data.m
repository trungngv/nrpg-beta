function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] ...
  = generate_sushi_data(fusers, fitems, forders, Ntrain, Mtrain)
% function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ytrue_test] =  ...
%     generate_sushi_data(fusers, fitems, forders, Ntrain, Mtrain)
%
% Read the sushi data and convert them into the structured required by the gppe package
%
% INPUT:
%   - fusers: the file containing user features
%   - fitems:  the file containing item features
%   - forders: the file containing orders of items
%   - Ntrain, Mtrain: number of items and users for training
%
% OUTPUT:
%   - idx_pairs: The matrix of all pairwise item comparisons:
%           idx_pairs(i,1) > idx_pairs(i,2)
%   - train_pairs: Cell array of M elements. Each element is a O_m x 2 matrix 
%       where O_m is the number of preferences observed for the corresponding
%       user. Each row all_pairs{m} contains a preference relation 
%       of the form train_pairs{m}(1) > train_pairs{m}(2)     
%   - train_t: Matrix of training users' features 
%   - x: Matrix of items' features 
%   - test_t: vector of test user's features 
%   - test_idx: Index of test users 
%   - ytrue_test: Binary vector indicating if the corresponding 
%       item comparisons hold for the test users
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 26/06/2012

%% Read data from files
ftrue_test = [];
t = read_users_features(fusers);
x = read_items_features(fitems);
t = t + rand(size(t)) / 1e+6;
x = x + rand(size(x)) / 1e+6;

%sushi_seta_ids = [1, 2, 3, 4, 5, 16, 8, 9, 27, 30]; % correct ids of the sushi in set A
%sushi_seta_ids = randperm(10, 10);
% so far using the default id works best!
%x = x(sushi_seta_ids, :);
F = read_users_orders(forders);
x = x(1:Ntrain, :);
idx_pairs = combnk(1:Ntrain, 2); % all possible pairs of items
% convert preferences into pairwise preference
Y = (F(idx_pairs(:,1),:) < F(idx_pairs(:,2),:));
train_idx = randperm(size(t, 1), Mtrain);
Ytrain = Y(:, train_idx);

% For each user we create a cell that contains the ordered pairs
all_pairs = cell(1, Mtrain);
for j = 1 : Mtrain
    tmp_pairs = idx_pairs;
    % flip the indices so that idx_1 in a pair is the prefered item
    idx_0 = find(Ytrain(:,j) == 0); % indices in reverse order
    tmp_pairs(idx_0,:) = fliplr(idx_pairs(idx_0,:));
    all_pairs{j} = tmp_pairs;
end
    
%% Assigning training and testing data
test_idx = train_idx; % test unseen data
%test_idx = randperm(size(t, 1), 20); % test 20 new user
train_t = t(train_idx, :);
test_t = t(test_idx, :);
ytrue_test = Y(:, test_idx);
Ntrain_ratio = 0.3; % How many training pairs to use
Npairs_train = floor(size(idx_pairs,1)*Ntrain_ratio);
[train_pairs, ytrue_test] = get_training_pairs(all_pairs, Mtrain, Npairs_train, ytrue_test);
%test_pairs  = all_pairs{test_idx};
%% saving data for C++ code and for elicitation queries on sushidata
% DataTr = convert_pairs_to_matrix(train_pairs);
% save('sushidata.mat', 'F', 'train_pairs', 'test_pairs', 'train_t', 'x', ...
%     'test_t',  'DataTr', 'test_idx', 'idx_pairs', 'all_pairs');
    
end

%% get training pairs (only works when 1:Mtrain (where Mtrain = M - 1) are training users
% and M as the test user
function [train_pairs, ytest] = get_training_pairs(all_pairs, Mtrain, Ntrain, ytest)
ytest = ytest * 1.0;
all_pairs = all_pairs(1:Mtrain);
train_pairs = cell(Mtrain,1);
for j = 1 : Mtrain
    pairs = all_pairs{j};
    % random Ntrain pairs for training
    idx   = randperm(size(pairs,1),Ntrain);
    train_pairs{j} = pairs(idx, :);
     % mark the training as -1 so that they are not used for testing
    ytest(idx,j) = -1;
end

end

