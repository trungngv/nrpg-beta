% Mixtures of Gaussian Processes
% Generate samples from mixtures of Gaussian Processes

% Author: Trung Nguyen
% Date: May 2012

% use 2 GP components
rng(30, 'twister')

% exp 1: seed 30
% exp 2: seed 100
exno = 4;
basedir = 'data';
basefile = ['experiment' num2str(exno)];

mean = [0, 0, 0, 0];
scale = [0.6, 0.3, 0.2, 0.75];
X = (-2:0.005:2)';
nsamples = length(X);
covfunc_x = 'covSEard';

% sample four functions fi ~ GP(0, kxi)
%k = @(s,x,y) exp(-0.5*((x-y)/s)^2);
kx = cell(1, length(mean));
f = cell(1, length(mean));
for i = 1:length(mean)
  kx{i} = feval(covfunc_x, [log(scale(i)); 0], X);
  f{i} = gsamp(repmat(mean(i), nsamples, 1), kx{i}, 1);
end
F1 = 0.7*f{1} + 0.3*f{2};
F2 = 0.7*f{1} + 0.3*f{2};
%F = [F1; F2];
F3 = f{4};
F = [F1; F2;F3];
testIdx = 1:5:length(X);
idx = setdiff(1:length(X), testIdx);
saveData(X(idx), F(:, idx), X(testIdx), F(:, testIdx), basedir, basefile);

% plot the functions
color = ['m', 'g', 'b', 'r'];
figure(11); hold on;
for i = 1:length(mean)
  plot(X, f{i}, color(i));
end
axis([-2, 2, -3, 3]);
legend('f0', 'f1', 'f2', 'f3');
figure(12); hold on;
plot(X, F1, 'm');
plot(X, F2, 'r');
plot(X, F3, 'b');
axis([-2, 2, -3, 3]);
%legend('F1 = 0.7f0 + 0.3f1', 'F2 = 0.7f0 + 0.3f2');
legend('F1 = 0.7f0 + 0.3f1', 'F2 = 0.7f0 + 0.3f2', 'F3 = f3');
title('Mixtures of 2 GPs');
