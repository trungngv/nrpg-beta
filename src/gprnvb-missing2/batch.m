clear;
%rng(13, 'twister');
iters = 20;
num_outputs=3;
modes = 2;
for num_modes=modes
mae = zeros(iters,num_outputs);
Ymean = zeros(iters,num_outputs);
cnt = 1;
while cnt ~= iters
  try
    [mae(cnt,:) Ymean(cnt,:)] = gprn_npv_run_naive(num_modes);
    %[mae(cnt,:) Ymean(cnt,:)] = gprn_npv_run_vanilla();
    cnt = cnt + 1;
    disp(mae)
  catch err
    disp(err)
  end
end

mae = mae(:,1); ymean = Ymean(:,1);
ymean = ymean(mae > 0); mae = mae(mae > 0); 
disp('mae      ymean')
disp([mae ymean])
fprintf('avg: %.4f, std: %.4f\n', mean(mae), std(mae));
% save(['batch' num2str(num_modes) '.mat'], 'mae');
end
