function check_grad_nlog_joint()
%CHECK_GRAD_NLOG_JOINT check_grad_nlog_joint()
%
% Check gradient for the log joint probability (complete likelihood) component
% of the evidence lower bound: df/dmu_n = dlogp(mu_n,y) / dmu_n. This
% constitutes the approximation to the expectation of the log joint.
%

[N, params] = test_data();
[Nd D] = size(params.X);
sigma_y = 0.1;
theta = gprn_put_parameters(log(rand(D+1,1)), log(rand(D+1,1)), 0.001, sigma_y);
[~, ~, Lf, Lw] = sample_prior(params.X, theta, params, [], []);
for n=1:N
  mu_n = params.Mu(:,n); % mu_n is dx1
  [gradient, delta] = gradchek(mu_n', @my_f, @my_grad, sigma_y, Lf, Lw, params);
  myhesschek(mu_n',@my_f,@my_hess, sigma_y, Lf, Lw, params);
end  

end

function f = my_f(x, sigma_y, Lf, Lw, params)
f = nlog_joint(x', sigma_y, Lf, Lw, params);
end

function g = my_grad(x, sigma_y, Lf, Lw, params)
[dummy g] = nlog_joint(x', sigma_y, Lf, Lw, params);
g = g';
end

function h = my_hess(x, sigma_y, Lf, Lw, params)
[dum dum2 g2] = nlog_joint(x', sigma_y, Lf, Lw, params);
h = g2';
end
