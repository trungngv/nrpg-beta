function test_mvnpdf_iso()
%TEST_MVNPDF_ISO function test_mvnpdf_iso()
%
N=2; d=3;
x = rand(N,d);
mu = rand(1,d);
sigma = 0.5;
p = mvnpdf_iso(x, mu, sigma);
p2=[];
for i=1:N
  p2(i) = mvnpdf_iso(mu, x(i,:), sigma);
end
p2
p_expected = mvnpdf(x, mu, sigma^2 * eye(d));
fprintf('ftrue = %10.10f\n', p_expected);
fprintf('ftest = %10.10f\n', p);
assert(all(abs(p-p_expected) < 1.e-10));
fprintf('test_mvnpdf_iso() passed\n');
end

