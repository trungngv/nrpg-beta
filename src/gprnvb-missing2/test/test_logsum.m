function test_logsum()
%TEST_LOGSUM test_logsum()
% Test for the logsum function
%
% Trung V. Nguyen
% 15/08/12
a = rand/1000; b = rand/1000; c = rand/1000;
expected = log(a + b + c);
test = logsum([log(a), log(b), log(c)]);
assert(abs(expected-test) < 1e-10);
fprintf('test_logsum() passed: difference = %.10f \n', expected-test);
end

