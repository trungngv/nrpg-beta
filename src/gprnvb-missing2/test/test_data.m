function [num_modes, params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
  theta_f,theta_w,a,sf,sy)
%TEST_DATA [num_modes, params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
%  theta_f,theta_w,a,sf,sy)
% 
% Generate some data for tests.
% 
% Trung V. Nguyen
% 08/08/12
%rng(3, 'twister'); % order of decreasing performnace 110,3 > 1 > 1100
%rng(23,'twister');

params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard'); 

% provide default test to be compatible with previous version
if nargin < 1
num_modes = 3;
params.num_modes = num_modes;
params.num_latents = 5;
params.num_outputs = 3;
Ntrain = 5; D = 2;
params.Mu = rand(Ntrain*params.num_latents*(params.num_outputs+1),params.num_modes); % Nq + Npq
params.sn2 = rand(params.num_modes,1);
params.X = rand(Ntrain,D);
params.Y = rand(Ntrain,params.num_outputs);

return;
end

params.num_modes = num_modes;
params.num_latents = num_latents;
params.num_outputs = num_outputs;
theta_ff = repmat(theta_f', num_latents, 1);
if ~isempty(a)
  for j=1:params.num_latents
    theta_ff(j,D+1) = a(j);
  end
end  
X = rand(Ntrain + Ntest,D);
Ynoise = rand(Ntrain+Ntest,params.num_outputs);
[~, ~, Ynoise, ~, ~] = gprn_utility_funcs(theta_ff, theta_w,...
  sf, sy, X, num_outputs, true);
params.X = X(1:Ntrain, :);
params.Y = Ynoise(1:Ntrain, :);
xtest = X(Ntrain+1 : Ntrain+Ntest, :);
ytest = Ynoise(Ntrain+1 : Ntrain+Ntest, :);
end

