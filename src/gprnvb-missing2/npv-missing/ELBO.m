function F = ELBO(theta,nlogpdf,h,s2min,params)
    
    % Calculate evidence lower bound (2nd-order approximation, L2).
    % For checking covergence
    
    [N K] = size(theta); D = K - 1;
    nF = lower_bound_MoG(theta,N,D,s2min,[],params);
    fprintf('\t lower_bound_MoG \t%.4f\n', nF);
    s2 = exp(theta(:,end)) + s2min;
    F = -nF;
    for n = 1:N
        nf = nlogpdf(theta(n,1:end-1));
        fprintf('\t nlog_joint \t\t%.4f\n', nf);
        F = F - nf/N - 0.5*s2(n)*h(n)/N;
        fprintf('\t hessian term \t\t%.4f\n', h(n));
    end