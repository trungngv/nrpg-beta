function [nfu ndu ndu2 dFu1] = nlog_joint(u,lsy,Lf,Lw,params)
%NLOG_JOINT [nfu ndu ndu2] = nlog_joint(u,lsy,Lf,Lw,params)
%   
% Returns the negative log joint probability (complete likelihood).
% This implementation is for the case when all latent functions share the
% same signal variance.
%
% Gradients:
% dFu/du evaluated at u given the fixed hyper-parameters:
% f(u) = log p(u, y) = log p(y|u)p(u) = log(likelihood) + log(prior)
% 
% INPUT
%   - u
%   - sigma_y
%   - Lf : the cholesky decomposition (chol(Kf)') of the cov matrix of
%   latent functions
%   - Lw : the cholesky decomposition (chol(Kw)') of the cov matrix of
%   weight functiosn
%
% OUTPUT
%
% Trung V. Nguyen
% 18/09/12
P = params.num_outputs; Q = params.num_latents; Nd = size(params.X,1);
X = params.X; Y = params.Y;
[fhat,w] = u_to_fhat_w(u, params);
w(params.w_isnan) = 0; % discard missing data
Fhat = reshape(fhat,Nd,Q); W = reshape(w, Nd*P,Q);
nfu = -(log_likelihood(W, Fhat, lsy, params) + log_prior_missing(fhat,w,Lf,Lw,params));
ind_Wxn = Nd*(0:P-1)';
sy2 = exp(2*lsy);
Kw = Lw*Lw';

% first derivatives
if nargout == 2
  dFf = zeros(Nd,Q); dFw = zeros(Nd*P,Q);
  for n=1:Nd
    Wxn = W(ind_Wxn+n,:);
    yd = Y(n,:)' - Wxn*Fhat(n,:)'; % y(xn) - W(xn)fhat(xn)
    yd(isnan(yd)) = 0; % for missing data
    dFf(n,:) = yd'*Wxn; % dp(y|u)/d fhat(xn)
    dFw(ind_Wxn+n,:) = yd*Fhat(n,:); % dp(y|u)/dW(xn)
  end
  dFu1 = [dFf(:); dFw(:)]; % dp(y|u)/du
  Wcell = mat2cell(W,Nd*ones(1,P),ones(1,Q));
  dFu2 = zeros(size(dFu1)); last_ind = 1;

  for j=1:Q
    alpha = solve_chol(Lf', Fhat(:,j));
    %alpha = Kfinv*Fhat(:,j);
    dFu2(last_ind : last_ind + numel(alpha) - 1) = alpha;
    last_ind = last_ind + numel(alpha);
  end

  for j=1:Q
    for i=1:P
      alpha = solve_chol(Lw', Wcell{i,j});
      %alpha = Kwinv*Wcell{i,j};
      dFu2(last_ind : last_ind + numel(alpha) - 1) = alpha;
      last_ind = last_ind + numel(alpha);
    end
  end
  ndu = -dFu1/sy2 + dFu2; % df/du
  ndu(Nd*Q+find(params.w_isnan)) = 0; % because du = [df; dw]
end

% Hessian (diagonal only)
if nargout >= 3
  ndu = [];
  dFf = zeros(Nd,Q); dFw = zeros(Nd*P,Q);
  for n=1:Nd
    Wxn = W(ind_Wxn+n,:);
    dFf(n,:) = -sum(Wxn.^2); % d^2 p(y|u)/d fhat(xn)^2
    dFw(ind_Wxn+n,:) = -repmat(Fhat(n,:).^2,P,1); % dp(y|u)/dW(xn)
  end
  dFu1 = [dFf(:); dFw(:)]; % d2p(y|u)/du2
  dFu1(Nd*Q+find(params.w_isnan)) = 0;
  Kfinv = Lf'\(Lf\eye(Nd));
  Kwinv = Lw'\(Lw\eye(Nd));
  dFu2 = -[repmat(diag(Kfinv),Q,1); repmat(diag(Kwinv),P*Q,1)];
%   dFu2 = -repmat(diag(Kfinv),Q,1);
%   for j=1:Q
%     for i=1:P
%       if sum(params.Wcell_isnan{i,j}) > 0 % missing data
%         valid_ind = ~params.Wcell_isnan{i,j};
%         Lwij = jit_chol(Kw(valid_ind,valid_ind))';
%         tmp = zeros(Nd,1);
%         tmp(valid_ind) = -diag(Lwij'\(Lwij\eye(size(Lwij,1))));
%         dFu2 = [dFu2; tmp];
%       else
%         dFu2 = [dFu2; -diag(Kwinv)];
%       end  
%     end
%   end
  %dFu2 = -diag(Kinv); % d2p(u)/du2
  ndu2 = -(dFu1/sy2 + dFu2); % d2f/du2
  %ndu2 = -dFu1/sy2;
  ndu2(Nd*Q+find(params.w_isnan)) = 0; % because du = [df; dw]
end

end

% compute the likelihood p(y|W, Fhat)
function logl = log_likelihood(W,Fhat,lsy,params)
Y = params.Y; N = size(Y, 1);
P = params.num_outputs;
sy2 = exp(2*lsy);

%vectorised version
Wxn_ind = N*(0:P-1)';
W_ind = bsxfun(@plus,Wxn_ind,1:N);
Wblk = W(W_ind(:),:);
tt = repmat(1:N,P,1);
Fblk = Fhat(tt(:),:);
Ymean = reshape(sum(Wblk.*Fblk,2),P,N)';
Ydiff = (Y-Ymean).^2;
Ydiff(params.Y_isnan) = 0; % missing data part
logl = -0.5*sum(sum(Ydiff))/sy2;
logl = logl - 0.5*(N*P-sum(sum(params.Y_isnan)))*log(2*pi*sy2); % due to missing data
end

