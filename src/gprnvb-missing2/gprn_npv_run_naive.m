function [mae Ymean] = gprn_npv_run_naive(num_modes)
% Demonstration of GPRN with nonparametric variational inference
% Version that learn length-scales of covariance functions.
% All latent functions share the same hyperparameters.

Ntrain = 359; Ntest = 100;
%Ntrain = 50; Ntest = 50; D = 2;
num_latents = 2; num_outputs = 3; D= 2; % jura
if nargin < 1
  num_modes = 1;
end
sigma_f = 0.000; sigma_y = 0.01;
theta_f = log(rand(D+1,1));
a = ones(num_latents,2)*theta_f(D+1);
theta_w = log(rand(D+1,1));
% make f and w (hence y) bigger and less susceptible to noise
theta_f(end) = 0.5+theta_f(end);
theta_w(end) = 0.5+theta_w(end);
[num_modes, params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
    theta_f, theta_w, a, sigma_f, sigma_y);
  
%% synthetic data   
% %training data as test
% xtest = params.X(1:Ntest,:);
% ytest = params.Y(1:Ntest,:);
% [params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
% save('temp2.mat', 'Ymean', 'Ystd');
% %missing data
% ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
% %params.Y(ind_n,1) = nan;

%% jura data set
[X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
X = [X; xtest];

%missing data
Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
Ymean = zeros(1,3); Ystd = zeros(1,3);
[Y(1:Ntrain-Ntest,1), Ymean(1), Ystd(1)] = standardize(log(Y(1:Ntrain-Ntest,1)),1,[],[]);
[Y(:,2:3), Ymean(2:3), Ystd(2:3)] = standardize(log(Y(:,2:3)),1,[],[]);

% training data as test points
% Y = [Y; ytest];
% [Y Ymean Ystd] = standardize(log(Y),1,[],[]);

params.X = X(1:Ntrain,:);
params.Y = Y(1:Ntrain,:);
save('temp2.mat', 'Ymean', 'Ystd');

%% Standard GP : one output
% [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
% X = [X; xtest]; Y = [Y; ytest]; % aggregate all data
% xtest = X(1:Ntest,:);
% ytest = Y(1:Ntest,1);
% X = X(1:Ntrain,:);
% Y = Y(1:Ntrain,1);
% [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
% params.X = X;
% params.Y = Y;
% Ymean = Ymean(1); Ystd = Ystd(1);
% save('temp2.mat', 'Ymean', 'Ystd');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
D = Ntrain * num_latents *(num_outputs+1);
theta0 = randn(num_modes, D+1);         
theta0(:,D+1) = 1;

thetaf0 = log(rand(numel(theta_f),1));
thetaf0(end) = 0.5+rand;
thetaw0 = log(rand(numel(theta_w),1));
thetaw0(end) = 0.5+rand;
hyp0 = [thetaf0; thetaw0; log(0.5*rand)];
[F mu s2 hyp] = my_npv_run(theta0,hyp0,sigma_f,params);      % run NPV

disp('learned hyp vs. init hyp');
disp([exp(hyp(:)), exp(hyp0(:))])
disp('s2')
disp(s2)
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

%% prediction
mu(1:num_modes,1:10)
theta = hyp(1:end-1);
mae = gprn_prediction(xtest,ytest,mu,theta,params);

end

function [F mu s2 hyp] = my_npv_run(theta,hyp,sigma_f,params)
    nIter = 20; tol = 0.00001;
    s2min = 0.0000001;      % enforce minimium bandwidth to avoid numerical problems
    [K D] = size(theta); D = D - 1;
    opts = struct('Display','off','Method','lbfgs','MaxIter',200,'MaxFunEvals',200,'DerivativeCheck','off');  % optimization options

    % pre-compute Lf, Lw 
    X = params.X; Dx = size(X,2); Q = params.num_latents;
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+2:end-1), X);
    lsy = hyp(end);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
    nlogpdf = @(x) nlog_joint(x,lsy,Lf,Lw,params);

    tbegin = tic;
    for iter = 1:nIter
        disp(['iteration: ',num2str(iter)]);
        
        tstart = tic;
        for k = 1:K
            func = @(x) nELBO1(x,@(x) nlogpdf(x),K,D,s2min,theta,k,params);
            %theta(n,1:end-1) = minimize(theta(n,1:D)',func,20);
            theta(k,1:end-1) = minFunc(func,theta(k,1:D)',opts);
        end
        disp(['optimising mu takes ', num2str(toc(tstart))])
        
        % second-order approximation (L2): optimize s2
        mu = theta(:,1:D); h = zeros(K,1);
        tstart = tic;
        for k = 1:K
          [dum1 dum2 diagH] = nlogpdf(mu(k,:)');
          h(k) = sum(diagH);
        end
        func = @(theta) nELBO2(theta,K,D,mu,s2min,h,params);
        theta = minFunc(func, theta(:,D+1),opts);
        %theta = minimize(theta(:,D+1),func,20);
        theta = [mu reshape(theta,K,1)];
        disp(['optimising sigma takes ', num2str(toc(tstart))])

        %M-step: optimise hyperparameters
        %if (iter < nIter) % so that mu and sigma are updated using best hyp
          tstart = tic;
          func = @(x) nlog_joint_hyp_noise(x,theta(:,1:D),theta(:,D+1),log(sigma_f),params);
          hyp = minFunc(func, hyp, opts);
          disp(['optimising log hyp takes ', num2str(toc(tstart))])
          disp(['hyp = ', num2str(exp(hyp'))])

          % use updated hyp to get ELBO
          Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
          Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
          Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
          lsy = hyp(end);
          nlogpdf = @(x) nlog_joint(x,lsy,Lf,Lw,params);
          for k = 1:K
            [dum1 dum2 diagH] = nlogpdf(mu(k,:)');
            h(k) = sum(diagH);
          end
        %end
        
        F(iter,1) = ELBO(theta,nlogpdf,h,s2min,params);
        fprintf('ELBO %.5f\n', F(iter,1));
        if iter > 1 && abs(F(iter)-F(iter-1))<tol; break; end % check for convergence
    end
    fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
    mu = theta(:,1:D);
    s2 = exp(theta(:,end)) + s2min;
end    