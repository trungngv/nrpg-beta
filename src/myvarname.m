function out = myvarname(varargin)
%MYVARNAME out = myvarname(var)
%   Returns the string representing name of the variable
out = {inputname(1), inputname(2)};
end

