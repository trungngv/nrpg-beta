function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] ...
  = gsynthetic(Ntrain)
% function [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ytrue_test] =  ...
%     gsynthetic_data(Ntrain, Mtrain)
%
% Generate synthetic data to demonstrate that non-stationary kernel
% outperforms stationary kernel
%
% INPUT:
%   - Ntrain, Mtrain: number of items and users for training
%
% OUTPUT:
%   - idx_pairs: The matrix of all pairwise item comparisons:
%           idx_pairs(i,1) > idx_pairs(i,2)
%   - train_pairs: Cell array of M elements. Each element is a O_m x 2 matrix 
%       where O_m is the number of preferences observed for the corresponding
%       user. Each row all_pairs{m} contains a preference relation 
%       of the form train_pairs{m}(1) > train_pairs{m}(2)     
%   - train_t: Matrix of training users' features 
%   - x: Matrix of items' features 
%   - test_t: vector of test user's features 
%   - test_idx: Index of test users 
%   - ytrue_test: Binary vector indicating if the corresponding 
%       item comparisons hold for the test users
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 30/07/2012

% TODO(trung): generate using functions with no specific formula
% to make the data harder to learn
ftrue_test = [];
% use 3 users, 2 input dimensions
Mtrain=3; D = 2;
t = zeros(Mtrain, D);

% user1: like dim1, hate dim2
%i.e., utility high when dim1 high & dim2 low
f1 = @(x1,x2) 0.8.*(x1-x2);
t(1,:) = [1 -1];

% user2: like both dim1 and dim2
%i.e., utility high when either dim1 or dim2 is high
f2 = @(x1,x2) 0.6.*(x1+x2);
t(2,:) = [0.9 0.9];

% user3: hate dim1, like dim2
%i.e., utility high when dim1 low and dim2 high
f3 = @(x1,x2) 0.7.*(-x1+x2);
t(3,:) = [-0.7 0.7];

x = rand(Ntrain, D);
F = zeros(Ntrain, Mtrain);
F(:,1) = f1(x(:,1),x(:,2)) + 0.01*randn(Ntrain,1);
F(:,2) = f2(x(:,1),x(:,2)) + 0.01*randn(Ntrain,1);
F(:,3) = f3(x(:,1),x(:,2)) + 0.01*randn(Ntrain,1);
disp(F(1:5,:))
idx_pairs = combnk(1:Ntrain, 2); % all possible pairs of items
% convert preferences into pairwise preference
Y = (F(idx_pairs(:,1),:) > F(idx_pairs(:,2),:));
train_idx = randperm(size(t, 1), Mtrain);
Ytrain = Y(:, train_idx);

% For each user we create a cell that contains the ordered pairs
all_pairs = cell(1, Mtrain);
for j = 1 : Mtrain
    tmp_pairs = idx_pairs;
    % flip the indices so that idx_1 in a pair is the prefered item
    idx_0 = find(Ytrain(:,j) == 0); % indices in reverse order
    tmp_pairs(idx_0,:) = fliplr(idx_pairs(idx_0,:));
    all_pairs{j} = tmp_pairs;
end
    
%% Assigning training and testing data
test_idx = train_idx; % test unseen data
%test_idx = randperm(size(t, 1), 20); % test 20 new user
train_t = t(train_idx, :);
test_t = t(test_idx, :);
ytrue_test = Y(:, test_idx);
Ntrain_ratio = 0.5; % How many training pairs to use
Npairs_train = floor(size(idx_pairs,1)*Ntrain_ratio);
[train_pairs, ytrue_test] = get_training_pairs(all_pairs, Mtrain, Npairs_train, ytrue_test);
%test_pairs  = all_pairs{test_idx};
    
end

%% get training pairs (only works when 1:Mtrain (where Mtrain = M - 1) are training users
% and M as the test user
function [train_pairs, ytest] = get_training_pairs(all_pairs, Mtrain, Ntrain, ytest)
ytest = ytest * 1.0;
all_pairs = all_pairs(1:Mtrain);
train_pairs = cell(Mtrain,1);
for j = 1 : Mtrain
    pairs = all_pairs{j};
    % random Ntrain pairs for training
    idx   = randperm(size(pairs,1),Ntrain);
    train_pairs{j} = pairs(idx, :);
     % mark the training as -1 so that they are not used for testing
    ytest(idx,j) = -1;
end

end

