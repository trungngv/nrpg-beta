function gppe_prediction_synthetic()
% A toy example demonstrating the use of the gppe package to verify the
% effect of using non-stationary utility function with the sushi dataset.
% The most important blocks in the code are elicit_gppe which carries out
% the elicitation process and the blocks corresponding to hyper-parameter
% learning.
%
% For a new dataset it is necessary to create the corresponding data
% structures (see elicit_gppe) and, more importantly, create new functions 
% for the pointers ptr_query_func and ptr_loss_func. These functions make 
% a query on a test user and compute the corresponding loss of making a
% particular recommendation respectively. See make_query_toydata.m and
% loss_query_toydata.m for more details on these functions.
% 

% Trung V. Nguyen (trung.ngvan@gmail.com)
% 30/07/2012
%
p = genpath('external'); addpath(p);
p = genpath('scripts');  addpath(p);
p = genpath('utils');  addpath(p);

dataset = 'synthetic';
cov_t = 'covSEard';
cov_x = 'covSEard';
covfunc_t      = check_covariance(cov_t);
covfunc_x      = check_covariance(cov_x);
Ntrain = 10; Mtrain = 3;

filename = ['output/gppe/' dataset '-' cov_t '-' cov_x '-N' num2str(Ntrain) '-M' num2str(Mtrain) '.txt3'];
fid = fopen(filename, 'w+');
fprintf(fid, 'Covariance functions: users=%s, items=%s\n', cov_t, cov_x);
fprintf(fid, 'Ntrain = %d, Mtrain = %d\n', Ntrain, Mtrain);
fprintf(fid, 'Train pairs / All pairs = %.2f\n', 0.1);
fprintf(fid, 'Evaluation: topk = 0.5\n');

%rngs = [1, 12, 239, 200, 100, 199, 166, 799, 3533, 5333, 779313];
rngs = 1:10;
niters = 10;
iter_errors = zeros(niters,1);
for i=1:niters
rng(rngs(i), 'twister');
fprintf(fid, '==============================================\n');
fprintf(fid, 'RUN %d\n', i);
fprintf(fid, '==============================================\n');

fprintf('running iteration %d\n', i);
[idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] = ...
    gsynthetic(Ntrain);

% init model
N = size(x, 1);
[covfunc_t, covfunc_x, theta] = init_data(train_t, x, covfunc_t, covfunc_x);
M           = length(train_pairs); % Update number of users to training users
[idx_global_1, idx_global_2] = compute_global_index(train_pairs, N);
idx_global    = unique([idx_global_1; idx_global_2]);
[ind_x ind_t] = ind2sub([N M], idx_global); % idx of seen points and tasks

% learn hyp
tic;
theta0 = [log(0.5*rand(size(theta,1)-1,1)); 0]; % initial noise=1
try
theta_learned = learn_gppe_with_minimize(covfunc_t, covfunc_x, ...
      theta0, train_t, x, train_pairs, idx_global, ...
      idx_global_1, idx_global_2, ind_t, ind_x, M, N);
disp('learned theta:')
disp(theta_learned')

% make prediction after learning hyp
[mean_error errors] = gppe_make_prediction_users(covfunc_t, covfunc_x, ...
  theta_learned, train_t, x, train_pairs, idx_global, idx_global_1, idx_global_2, ...
  ind_t, ind_x, test_t, idx_pairs, ftrue_test, ytrue_test);
iter_errors(i) = mean_error;  
fprintf(fid, 'mean error: %.4f\n', mean_error);
fprintf(fid, 'errors for each test user:\n');
fprintf(fid, '%.4f ', errors);
fprintf(fid, '\n');

fprintf('\nmean error: %.4f\n', mean_error);
fprintf('execution time %.2f (secs)\n', toc);
catch err
  % ignore error to continue running the next experiment
  disp(err.message)
end

end

iter_errors = nonzeros(iter_errors);
fprintf(fid, '\nIterations without error: %d', length(iter_errors));
fprintf(fid, '\nERROR: AVG = %.4f, STD = %.4f\n', mean(iter_errors), std(iter_errors));
fclose(fid);

return;

%% Init data for the model
function [covfunc_t, covfunc_x, theta] = init_data(t, x, covfunc_t, covfunc_x)
%theta is [hyp(covfunc_t); hyp(covfunc_x); sigma]
D = size(t,2);   ntheta_t = eval(feval(covfunc_t{:}));
D = size(x,2);   ntheta_x = eval(feval(covfunc_x{:}));
theta = zeros(ntheta_t + ntheta_x + 1, 1);

return;

