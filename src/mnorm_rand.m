% function x = mnorm_rand(Mu, Sigma)
%
% Samples a vector x from N(Mu, Sigma) where Mu is the mean vector and Sigma
% is the covariance vector. The returned sample is a vector x of size D x
% 1.
function x = mnorm_rand(Mu, Sigma)
  n = size(Mu, 1);
  Mu = reshape(Mu, n, 1);
  if ~isequal(size(Sigma), [n n])
    error('Sigma must be a square matrix.');
  end
  
  u = randn(n, 1); % u ~ N(0, I)
  A = chol(Sigma); % Sigma = A'A
  % Au + mean(X) ~ N(mean(X), AIA') = N(mean(X), Sigma)
  x = Mu + A' * u;
  % Note: if Mu is row vector, x = Mu + uA where u = randn(1,n)
end