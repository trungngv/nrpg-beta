function [nf ndhyp] = nlog_joint_cov_numeric(hyp,Mu,sn2,params)
%NLOG_JOINT_HYP [nf ndhyp] = nlog_joint_cov(hyp,Mu,sn2,params)
%   
% Returns the negative log joint probability, excluding the constant terms
% wrt hyperparameters. Hence this can be used as the objective function
% for optimisation wrt the log hyperparameters.
% 
% NOTE that this is the exact lower bound as a function of
% hyper-parameters.
%
% INPUT
%   - hyp : log hyperparameters, should be log[theta_f, theta_w]
%   - Mu : KxD matrix
%
% OUTPUT
%
% Trung V. Nguyen
% 03/10/12
nf = mynlog_joint(hyp,Mu,sn2,params);
ndhyp = numeric_grad3(@mynlog_joint,hyp, Mu,sn2,params);
end

function nf = mynlog_joint(hyp,Mu,sn2,params)
P = params.num_outputs; Q = params.num_latents; K = params.num_modes;
X = params.X;
Nd = size(X,1);
sn2 = exp(sn2) + 1e-10;
ltheta_f = hyp(1:size(X,2)+1);
ltheta_w = hyp(numel(ltheta_f)+1:end);
Kf = feval(params.covfunc_f, ltheta_f, X);% + (exp(lsf)^2)*eye(Nd);
Kw = feval(params.covfunc_w, ltheta_w, X);
[Lf ~] = jit_chol(Kf); [Lw ~]= jit_chol(Kw);
nf = 0;
for k=1:K
  [f, w] = u_to_fhat_w(Mu(k,:),params);
  w(params.w_isnan) = 0;
  nf = nf - log_prior_missing(f,w,Lf',Lw',params);
end
  
Kfinv = Lf\(Lf'\eye(Nd));   Kwinv_full = Lw\(Lw'\eye(Nd));
nf = nf + 0.5*Q*sum(sn2)*trace(Kfinv); % + 0.5 \sum_k\sum_j trace(K_f^{-1|)
for i=1:P
  valid_ind = ~params.Y_isnan(:,i);
  if sum(valid_ind) ~= Nd
    [Lw ~] = jit_chol(Kw(valid_ind,valid_ind));
    Kwinv = Lw\(Lw'\eye(size(Lw,1)));
  else
    Kwinv = Kwinv_full;
  end
  nf = nf + 0.5*Q*sum(sn2)*trace(Kwinv); % + 0.5 \sum_k \sum_i \sum_j trace(K_wij^{-1})
end
nf = nf/K;
end

