function [mae smse] = gprn_prediction(xtest,ytest,mu,theta,params)
%GPRN_PREDICTION [mae smse] = gprn_prediction(xtest,ytest,mu,theta,params)
% Make prediction
%
% INPUT
%   - xtest : input locations
%   - ytest : outputs
%
% OUTPUT
%   - mu : KxD means matrix
%
P = params.num_outputs;
Q = params.num_latents;
K = params.num_modes;
[Ntest D] = size(xtest);
X = params.X;
Ntrain = size(X,1);
ltheta_f = theta(1:D+1);
ltheta_w = theta(D+2:end);

Kf = feval(params.covfunc_f, ltheta_f, params.X);% + (sf^2) * eye(N);
Kw = feval(params.covfunc_w, ltheta_w, params.X);
Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
Lf_blk = [];
Lw_blk = [];
for j=1:Q
  Lf_blk = blkdiag(Lf_blk, Lf);
  for i=1:P
    Lw_blk = blkdiag(Lw_blk, Lw);
  end
end

% Step 1: Compute the mean of f and w for the missing data
for k=1:K
  [ft wt] = u_to_fhat_w(mu(k,:), params);
  ft = ft(:);
  if sum(params.w_isnan) == 0
    break;
  end
  wt(params.w_isnan) = 0;
  W = mat2cell(reshape(wt(:), Ntrain*P,Q), Ntrain*ones(1,P), ones(1,Q));
  for p=1:P
    for q=1:Q
      if (sum(params.Wcell_isnan{p,q})) > 0
        obs_ind = ~params.Wcell_isnan{p,q};
        Kw = feval(params.covfunc_w, ltheta_w, X(obs_ind,:), X(obs_ind,:));
        Lw_obs = jit_chol(Kw)';
        kw_s = feval(params.covfunc_w, ltheta_w, X(obs_ind,:), X(params.Wcell_isnan{p,q},:));
        W{p,q}(params.Wcell_isnan{p,q}) = (Lw_obs\kw_s)' * (Lw_obs\W{p,q}(obs_ind));
      end  
    end
  end
  wt = cell2mat(W);
  mu(k,:) = [ft; wt(:)];
end

% Step 2: Prediction
ystar = zeros(P,Ntest);
for k=1:K
  [ft wt] = u_to_fhat_w(mu(k,:), params);
  ft = ft(:); wt = wt(:);
  fstar = zeros(Q,1);   Wstar = zeros(P,Q);
  for i=1:Ntest
    kf_s = feval(params.covfunc_f, ltheta_f, X, xtest(i,:));
    kw_s = feval(params.covfunc_w, ltheta_w, X, xtest(i,:));
    alphaLf = (Lf\kf_s)'; alphaLw = (Lw\kw_s)';
    idx_f = 1; idx_w = 1;
    for q=1:Q
      fstar(q) = alphaLf * (Lf\ft(idx_f:idx_f+Ntrain-1));
      idx_f = idx_f + Ntrain;
      for p=1:P
        Wstar(p,q) = alphaLw * (Lw\wt(idx_w:idx_w+Ntrain-1));
        idx_w = idx_w + Ntrain;
      end
    end
    ystar(:,i) = ystar(:,i) + Wstar * fstar;
  end
end

Ymean = repmat(params.Ymean,Ntest,1);
Ystd = repmat(params.Ystd,Ntest,1);
if params.logscale
  ystar = exp((ystar'/K).*Ystd + Ymean);
else  
  ystar = (ystar'/K).*Ystd + Ymean;
end  

disp('ystar      ytest')
disp([ystar ytest])

mae = mean(abs(ystar-ytest));
smse = mean((ystar-ytest).^2)./(var(ytest) + mean(ytest).^2);
disp('mae = ');
fprintf('%.4f\t', mae);
fprintf('\n');
disp('smse = ');
fprintf('%.4f\t', smse);
fprintf('\n');

end

