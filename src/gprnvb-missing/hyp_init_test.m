function hyp = hyp_init_test(D)
%HYP_INIT_FINE_TUNE hyp = hyp_init_best(D)
%
thetaf = log([0.1; 0.1; rand]);
thetaw = log([2; 2; rand]);
% thetaf = log([0.2; 0.2; rand]);
% thetaw = log([2; 2; rand]);
hyp = [thetaf; thetaw; log(1)];
end

