function hyp = hyp_init_standard(D)
%HYP_INIT_TOTAL_RANDOM hyp = hyp_init_standard()
% 
% thetaf = log(rand(D+1,1));
% thetaw = [log(rand(D,1)); log(1)];
thetaf = rand(D+1,1);
thetaw = [rand(D,1); 0];
hyp = [thetaf; thetaw; log(rand)];
end
