function hyp = hyp_init_best(D)
%HYP_INIT_FINE_TUNE hyp = hyp_init_best(D)
%
thetaf = log([0.07; 0.01; 1]);
thetaw = log([1.38; 1.95; 1.90]);
hyp = [thetaf; thetaw; log(1)];
end

