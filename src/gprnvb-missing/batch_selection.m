clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'jura';
num_outputs = 3;
predict_test = true;
standardise_x = false;
enforce_bound = false;
optimise_sf = false;
optimise_sw = true;
executor = @npv_run_separation;
func_hypinit = 'hyp_init_fine';
modes = 1;
for modes =1:2
filename = ['output/npvgprn/' func_hypinit  '/' func2str(executor) ...
    '_modes' num2str(modes) '_' num2str(tic) '.txt'];
logfilename = ['output/npvgprn/' func_hypinit  '/' func2str(executor) ...
    '_modes' num2str(modes) '_' num2str(tic) '.log'];
maxItersHypOpt = 50; maxIters = 50;

% model selection for hyper-parameters
cnt = 1; iters = 10;
elbos = zeros(iters,1);
learned_hyp = zeros(7,iters);
fid = fopen(logfilename, 'w');
mae = zeros(iters,num_outputs);
mae_pseudo = mae;
while cnt <= iters
 try
    [mae(cnt,:), ~, hyp, elbo, mae_pseudo(cnt,:), hyp0] = executor(dataset, modes, predict_test,...
      standardise_x, maxItersHypOpt, func_hypinit,enforce_bound,optimise_sf,optimise_sw);
    elbo = elbo(elbo ~= 0);
    fprintf(fid, '====================\nRun %d:\n',cnt);
    fprintf(fid, 'Init hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp0));
    fprintf(fid, '\nOptimised hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp));
    fprintf(fid, '\n\tELBO = ');
    fprintf(fid, '\n\t\t%.4f', elbo);
    fprintf(fid, '\n\tmae \t\t\t\t =');
    fprintf(fid, '%.5f\t', mae(cnt,:));
    fprintf(fid, '\n\tmae (pseudo) =');
    fprintf(fid, '%.5f\t', mae_pseudo(cnt,:));
    fprintf(fid, '\n');
    elbos(cnt) = elbo(end);
    learned_hyp(:,cnt) = hyp(:);
    cnt = cnt + 1;
 catch err
    disp(err)
  end
end  
fclose(fid);
[~, idx_max] = max(elbos);
func_hypinit = @(D) (learned_hyp(:,idx_max));

% record settings
fid = fopen(filename, 'w');
fprintf(fid, 'Experiment: model selection for hyperparameters\n');
fprintf(fid, 'Runner: %s\n', func2str(executor));
fprintf(fid, 'Date time: %s\n', datestr(now));
fprintf(fid, 'Rngno:\t %d\n', rngno);
fprintf(fid, 'Dataset:\t %s\n', dataset);
fprintf(fid, 'Num outputs:\t %d\n', num_outputs);
fprintf(fid, 'Num modes:\t %d\n', modes);
fprintf(fid, 'Max iters: %d\n', maxIters);
fprintf(fid, 'Standardised X:\t %d\n', standardise_x);
fprintf(fid, 'Predict on new data:\t %d\n', predict_test);
fprintf(fid, 'Enforce lower bound improvement: %d\n', enforce_bound);
fprintf(fid, 'Optimise latent signal variance: %d\n', optimise_sf);
fprintf(fid, 'Optimise weight signal variance: %d\n', optimise_sw);

cnt = 1; iters = 10;
mae = zeros(iters,num_outputs);
mae_pseudo = mae;
ymean = zeros(iters,num_outputs);
while cnt <= iters
  try
    [mae(cnt,:) ymean(cnt,:) hyp lowerbound mae_pseudo(cnt,:) hyp0] = executor(dataset, modes,...
      predict_test,standardise_x,maxIters,func_hypinit,enforce_bound,optimise_sf,optimise_sw);
    fprintf(fid, '====================\nRun %d:\n',cnt);
    fprintf(fid, 'Init hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp0));
    fprintf(fid, '\nOptimised hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp));
    fprintf(fid, '\n\tELBO = ');
    fprintf(fid, '\n\t\t%.4f', lowerbound);
    fprintf(fid, '\n\tmae \t\t\t\t =');
    fprintf(fid, '%.5f\t', mae(cnt,:));
    fprintf(fid, '\n\tmae (pseudo) =');
    fprintf(fid, '%.5f\t', mae_pseudo(cnt,:));
    fprintf(fid, '\n');
    cnt = cnt + 1;
    disp(mae)
  catch err
    disp(err)
  end
end  

mae = mae(:,1); ymean=ymean(:,1); mae_pseudo = mae_pseudo(:,1);
disp('mae      ymean')
disp([mae ymean])
fprintf('avg: %.4f, std: %.4f\n', mean(mae), std(mae));
fprintf(fid, '\nMean absolute error:\n');
fprintf(fid, '%.5f\n', mae);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));

fprintf(fid, '\nMean absolute error with pseudo_w:\n');
fprintf(fid, '%.5f\n', mae_pseudo);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae_pseudo), std(mae_pseudo));

fclose(fid);
disp(['output written to ' filename])
copyfile(filename, '/home/trung/Dropbox/experimentslog/');
end
