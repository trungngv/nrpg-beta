%%
% standard GP
clear;
rng(13, 'twister');
D = 2; log_transform = true;
covfunc = {@covSEard}; likfunc = @likGauss; infFunc = @infExact;
[X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');

%transforming X
[X xmean xstd] = standardize(X,1,[],[]);
xtest = standardize(xtest,1,xmean,xstd);

% pre-processing
output = 1;
Y = Y(:,output);
ytest  = ytest(:,output);
if log_transform
  [Y ymean ystd] = standardize(log(Y),1,[],[]);
else
  [Y ymean ystd] = standardize(Y,1,[],[]);
end  

iters = 10;
cnt = 1;
mae = zeros(iters,1);
while cnt <= iters
  hyp.cov = [log(0.1*rand(D,1)); 0]; % SEard
  hyp.lik = log(rand);
 try
    hyp0 = [hyp.cov(:); hyp.lik(:)];
    hyp = minimize(hyp, @gp, 5000, infFunc, [], covfunc, likfunc, X, Y);
    [ystaaar s2 ystar] = gp(hyp, infFunc, [], covfunc, likfunc, X, Y, xtest);
    if log_transform
      ystar = exp(ystar .* ystd + ymean);
    else
      ystar = ystar .* ystd + ymean;
    end  
    mae(cnt) = mean(abs(ystar - ytest));
    disp([ystar, ytest]);
    disp('test mae = ')
    fprintf('%.5f\n', mae(cnt));
    smse = mean((ystar-ytest).^2)/(var(ytest) + mean(ytest).^2);
    fprintf('test smse = %.5f\n', smse);
    cnt = cnt + 1;
  catch err
    disp(err)
  end
end  

fprintf('avg: %.5f, std: %.5f\n', mean(mae), std(mae));
if log_transform
  save('output/npvgprn/jura/logigp.mat', 'mae', 'smse');
else
  save('output/npvgprn/jura/igp.mat', 'mae', 'smse');
end