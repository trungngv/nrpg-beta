function [nf ndhyp] = nlowerbound_hyp(hyp,Mu,sn2,lsf,params)
%NLOWERBOUND_HYP [nf ndhyp] = nlowerbound_hyp(hyp, Mu, sn2, lsf, params)
%   
% Returns the EXACT negative lower bound excluding the constant terms
% wrt hyperparameters
%
% INPUT
%   - hyp : log hyperparameters, should be log[theta_f, theta_w, sigma_y]
%   - Mu : KxD matrix
%
% OUTPUT
%
% Trung V. Nguyen
% 03/10/12
P = params.num_outputs; Q = params.num_latents; K = params.num_modes;
X = params.X;
Nd = size(X,1);
sn2 = exp(sn2) + 1e-10;
ltheta_f = hyp(1:size(X,2)+1);
ltheta_w = hyp(numel(ltheta_f)+1:end-1);
Kf = feval(params.covfunc_f, ltheta_f, X);
Kw = feval(params.covfunc_w, ltheta_w, X);
[Lf ~] = jit_chol(Kf); [Lw ~] = jit_chol(Kw);
Kfinv = Lf\(Lf'\eye(Nd));

nf = K*Q*sum(log(diag(Lf))) + 0.5*Q*sum(sn2)*trace(Kfinv);
for k=1:K
  [f,w] = u_to_fhat_w(Mu(k,:), params);
  Fhat = reshape(f,Nd,Q); Wmat=reshape(w,Nd*P,Q);
  W = mat2cell(Wmat,Nd*ones(1,P),ones(1,Q));
  for j=1:Q
    nf = nf + 0.5*Fhat(:,j)'*Kfinv*Fhat(:,j);
    for i=1:P
      if sum(params.Wcell_isnan{i,j}) > 0 % missing data
        valid_ind = ~params.Wcell_isnan{i,j};
        Lwij = jit_chol(Kw(valid_ind, valid_ind))';
      else
        Lwij = Lw';
      end
      alpha = Lwij\W{i,j}(valid_ind);
      % ln |K_wij| + w_ij^T K_{wij}^{-1} w_ij^T
      nf = nf + sum(log(diag(Lwij))) + 0.5*(alpha')*alpha;
    end
  end
end
nf = nf/K;

if nargout == 2
  ndf = zeros(size(ltheta_f)); ndw = zeros(size(ltheta_w));
  for idx=1:numel(ltheta_f)
    dK = feval(params.covfunc_f, ltheta_f, X, [], idx);
    alpha = Kfinv*dK; % alpha = Lf\(Lf'\dK)
    % using tr(AB) = sum(sum(A.*B'))
    ndf(idx) = -sum(sn2)*sum(sum(Kfinv.*alpha')) + Q*K*trace(alpha); % Lf\(Lf'\alpha);
    for k=1:K
      [f,w] = u_to_fhat_w(Mu(k,:), params);
      Fhat = reshape(f,Nd,Q); Wmat=reshape(w,Nd*P,Q);
      W = mat2cell(Wmat,Nd*ones(1,P),ones(1,Q));
      for j=1:Q
        ndf(idx) = ndf(idx) - trace(Fhat(:,j)*(Fhat(:,j)*alpha));
      end
    end
  end
end
ndhyp = [ndf; ndw]*0.5/K;
end
