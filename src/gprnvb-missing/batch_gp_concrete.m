%% 
clear;
rng(1110, 'twister');
[x, y, xtest, ytest] = load_data('data/concreteslump', 'concrete3');
features = [1,3:5];
x = x(:,features); xtest = xtest(:,features);

covfunc = {@covSEard}; D = size(x,2);
likfunc = @likGauss; infFunc = @infExact;

%GP for each output
Xtrain = x; Xtest = xtest;
num_outputs = 3;
iters = 10;
obj_all = zeros(iters,num_outputs);
hyp_all = cell(num_outputs,iters);
mae_all = zeros(iters,num_outputs);
smse_all = mae_all;
% model selection: hyper with best objective value
for i=1:num_outputs
  Ytrain = y(:,i);
  %[Ytrain, Ymean, Ystd] = standardize(log(Ytrain),1,[],[]);
  [Ytrain, Ymean, Ystd] = standardize(Ytrain,1,[],[]);
  Ytest = ytest(:,i);
  [Xtrain,Xmean,Xstd] = standardize(Xtrain,1,[],[]);
  Xtest = standardize(Xtest,1,[],[]);
  hyp_all{i} = zeros(iters,D+2);
  for iter =1:iters
    hyp.cov(1:D+1) = log(rand(D+1,1));
    hyp.lik = log(rand);
    [hyp_all{i,iter},fx,~] = minimize(hyp, @gp, 5000, infFunc, [], covfunc, likfunc, Xtrain, Ytrain);
    obj_all(iter,i) = fx(end);
    [~, s2, ystar] = gp(hyp, infFunc, [], covfunc, likfunc, Xtrain, Ytrain, Xtest);
    ystar = ystar .* Ystd + Ymean;
    mae = mean(abs(ystar - Ytest));
    smse = mean((ystar-Ytest).^2)/(var(Ytest) + mean(Ytest).^2);
    mae_all(iter,i) = mae;
    smse_all(iter,i) = smse;
  end
end
save('output/npvgprn/concrete3/ft1345igp.mat', 'mae_all', 'smse_all');

%% after model selection
mae_all = zeros(iters,num_outputs);
smse_all = mae_all;
for i=1:num_outputs
  Ytrain = y(:,i);
  [Ytrain, Ymean, Ystd] = standardize(Ytrain,1,[],[]);
  Ytest = ytest(:,i);
  [Xtrain,Xmean,Xstd] = standardize(Xtrain,1,[],[]);
  Xtest = standardize(Xtest,1,[],[]);
  [~, best_idx] = min(obj_all(:,i));
  hyp = hyp_all{i,best_idx};
  for iter=1:iters
    % error on testing data
    [~, s2, ystar] = gp(hyp, infFunc, [], covfunc, likfunc, Xtrain, Ytrain, Xtest);
    ystar = ystar .* Ystd + Ymean;
    disp([ystar, Ytest])
    %scatter(ystar, Ytest);
    mae = mean(abs(ystar - Ytest));
    fprintf('test mae = %.5f\n', mae);
    smse = mean((ystar-Ytest).^2)/(var(Ytest) + mean(Ytest).^2);
    fprintf('test smse = %.5f\n', smse);
    mae_all(iter,i) = mae;
    smse_all(iter,i) = smse;
  end
end
save('output/npvgprn/concrete3/ft1345igp_selection.mat', 'mae_all', 'smse_all');

