function chgrad_exact_nlogjoint()
%CHGRAD_EXACT_NLOGJOINT chgrad_exact_nlogjoint()
%
% Check gradient for the expected log joint.
%
rng(23, 'twister');
params = test_data();
[N D] = size(params.X);
lsy = 0.1; lsk = 0.01;
ltheta = rand(2*(D+1),1);
theta = gprn_put_parameters(log(rand(D+1,1)), log(rand(D+1,1)), 0.001, lsy);
[~, ~, Lf, Lw] = sample_prior(params.X, theta, params, [], []);
K = params.num_modes;
for k=1:K
  mu_k = params.Mu(:,k); % mu_n is dx1
  [gradient, delta] = gradchek(mu_k',@fmu, @gmu,lsk,ltheta,lsy,Lf,Lw,params);
  [gradient, delta] = gradchek(lsk,@flsk,@glsk,mu_k,ltheta,lsy,Lf,Lw,params);
  [gradient, delta] = gradchek(lsy,@flsy,@glsy,mu_k,lsk,ltheta,Lf,Lw,params);
  [gradient, delta] = gradchek(ltheta',@fltheta,@gltheta,mu_k,lsk,lsy,[],[],params);
end  

end

function f = fmu(x,lsk,ltheta,lsy,Lf,Lw,params)
f = exact_nlogjoint(x',lsk,ltheta,lsy,Lf,Lw,params,1);
end

function g = gmu(x,lsk,ltheta,lsy,Lf,Lw,params)
[dummy g] = exact_nlogjoint(x',lsk,ltheta,lsy,Lf,Lw,params,1);
g = g';
end

function f = flsk(x,mu,ltheta,lsy,Lf,Lw,params)
f = exact_nlogjoint(mu,x,ltheta,lsy,Lf,Lw,params,2);
end

function g = glsk(x,mu,ltheta,lsy,Lf,Lw,params)
[~, ~, g] = exact_nlogjoint(mu,x,ltheta,lsy,Lf,Lw,params,2);
g = g';
end

function f = fltheta(x,mu,lsk,lsy,Lf,Lw,params)
[~, D] = size(params.X);
ltheta_f = x(1:D+1);
ltheta_w = x(D+2:end);
Kf = feval(params.covfunc_f, ltheta_f, params.X);
Kw = feval(params.covfunc_w, ltheta_w, params.X);
Lf = jit_chol(Kf)';
Lw = jit_chol(Kw)';
f = exact_nlogjoint(mu,lsk,x',lsy,Lf,Lw,params,3,true);
end

function g = gltheta(x,mu,lsk,lsy,Lf,Lw,params)
[~, D] = size(params.X);
ltheta_f = x(1:D+1);
ltheta_w = x(D+2:end);
Kf = feval(params.covfunc_f, ltheta_f, params.X);
Kw = feval(params.covfunc_w, ltheta_w, params.X);
Lf = jit_chol(Kf)';
Lw = jit_chol(Kw)';
[~, ~, ~, g] = exact_nlogjoint(mu,lsk,x',lsy,Lf,Lw,params,3,true);
g = g';
end

function f = flsy(x,mu,lsk,ltheta,Lf,Lw,params)
f = exact_nlogjoint(mu,lsk,ltheta,x,Lf,Lw,params,4);
end

function g = glsy(x,mu,lsk,ltheta,Lf,Lw,params)
[~, ~, ~, ~, g] = exact_nlogjoint(mu,lsk,ltheta,x,Lf,Lw,params,4);
g = g';
end
