function chgrad_nlog_joint_cov()
%CHECK_GRAD_NLOG_JOINT_COV check_grad_nlog_joint_cov()
% 
% Test for the grad_nlog_joint_hyp() function.
%rng(201, 'twister');
params = test_data();
num_modes = params.num_modes;
D = size(params.X,2);
Q = params.num_latents;
hyp = 2*rand(1,2*(D+1));
lsn2 = log(rand(num_modes,1));
mu = params.Mu';
optim = true;
[grad delta] = gradchek(hyp, @my_f, @my_grad,mu,lsn2,params,optim);
disp('grad')
fprintf('%.5f\n', grad);
disp('delta')
fprintf('%.5f\n', delta);
end

function f = my_f(x,Mu,sn2,params,optim)
f = nlog_joint_cov(x',Mu,sn2,params,optim);
end

function g = my_grad(x,Mu,sn2,params,optim)
[dummy g] = nlog_joint_cov(x',Mu,sn2,params,optim);
g = g';
end

