function chgrad_exact_nELBO()
%CHGRAD_EXACT_NELBO chgrad_exact_nELBO()
%
% Check gradient for the lower bound.
%
rng(123, 'twister');
params = test_data();
[N D] = size(params.X);
K = params.num_modes;
lsy = 0.1; lsk = rand(K,1)*0.1
theta = gprn_put_parameters(log(rand(D+1,1)), log(rand(D+1,1)), 0.001, lsy);
[~, ~, Lf, Lw] = sample_prior(params.X, theta, params, [], []);

Mu = params.Mu;
for k=1:K
  mu_k = Mu(:,k); % mu_n is dx1
%   [gradient, delta] = gradchek(mu_k',@fmu, @gmu,k,Mu',lsk,lsy,Lf,Lw,params);
%   fprintf('%.6f\n', delta);
end  
[gradient, delta] = gradchek(lsk',@flsk,@glsk,Mu',lsy,Lf,Lw,params);
fprintf('%.6f\n', delta);
end

function f = fmu(x,k,Mu,lsk,lsy,Lf,Lw,params)
f = exact_nELBO(Mu,lsk,lsy,Lf,Lw,params,1,k,x);
end

function g = gmu(x,k,Mu,lsk,lsy,Lf,Lw,params)
[~, g] = exact_nELBO(Mu,lsk,lsy,Lf,Lw,params,1,k,x);
g = g';
end

function f = flsk(x,Mu,lsy,Lf,Lw,params)
f = exact_nELBO(Mu,x',lsy,Lf,Lw,params,2);
end

function g = glsk(x,Mu,lsy,Lf,Lw,params)
[~, g] = exact_nELBO(Mu,x',lsy,Lf,Lw,params,2);
g = g';
end
