function check_grad_nlog_joint_hyp()
%CHECK_GRAD_NLOG_JOINT2 check_grad_nlog_joint_hyp()
% 
% Test for the grad_nlog_joint_hyp() function.
%rng(20, 'twister');
params = test_data();
num_modes = params.num_modes;
D = size(params.X,2);
Q = params.num_latents;
lsf = log(0.001);
hyp = rand(1,2*(D+1)+1);
lsn2 = log(rand(num_modes,1));
mu = params.Mu';

[grad delta] = gradchek(hyp, @my_f, @my_grad,mu,lsn2,lsf,params);
disp('grad')
fprintf('%.5f\n', grad);
disp('delta')
fprintf('%.5f\n', delta);
end

function f = my_f(x,Mu,sn2,lsf,params)
f = nlog_joint_hyp(x',Mu,sn2,lsf,params);
end

function g = my_grad(x,Mu,sn2,lsf,params)
[dummy g] = nlog_joint_hyp(x',Mu,sn2,lsf,params);
g = g';
end

