function test_entropy()
%TEST_ENTROPY Test for the entropy function
% 
P = 3; Q = 2; N = 10;
CovF = cell(Q,1);
CovW = cell(P,Q);
params = struct('num_latents', Q, 'num_outputs', P);
params.CovF = CovF; params.CovW = CovW;
expected = 0;
for j=1:Q
  A = rand(N,N);
  params.CovF{j} = A*A';
  expected = expected + 0.5*log(det(params.CovF{j}));
  for i=1:P
    A = rand(N,N);
    params.CovW{i,j} = A*A';
    expected = expected + 0.5*log(det(params.CovW{i,j}));
  end
end

result = entropy(params);
assert(abs(expected - result) < 1e20, 'test_entropy failed');
disp('test_entropy() passed');
end

