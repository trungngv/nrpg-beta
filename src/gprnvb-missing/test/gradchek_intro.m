% learn and test usage of the gradchek function
function gradchek_intro()
[~, params] = test_data();
d = size(params.Mu,1);
mn = params.Mu(:,1); mj = params.Mu(:,1);
x = params.sigma(1);
[gradient, delta] = gradchek(x, @f, @grad, mn, mj, d, params);
fprintf('difference: %12.12f\n', delta);
end

% function f = x'Ax
function val = f(sn, mn, mj, d, params)
sj = params.sigma(1);
s=sn^2+sj^2;
val = mvnpdf_iso(mn, mj, sqrt(s));
fprintf('val\n');
fprintf('sn = %.10f\n', sn);
fprintf('sj = %.10f\n', sj);

end

function g = grad(sn, mn, mj, d, params)
sj = params.sigma(1);
fprintf('grad\n');
fprintf('sn = %.10f\n', sn);
fprintf('sj = %.10f\n', sj);
s = sn^2+sj^2;
g = mvnpdf_iso(mn, mj, sqrt(s))*sn*((norm(mn-mj)/s)^2 - d/s);
end
