N = 1000;
A = rand(N,N);
B = rand(N,N);
C = rand(N,N);
tic;
x1 = trace(A*B*C);
toc
tic;
x2 = 0;
for i=1:N
  x2 = x2 + A(i,:)*B*C(:,i);
end
toc
fprintf('\ndelta = %.5f', x1 - x2);
