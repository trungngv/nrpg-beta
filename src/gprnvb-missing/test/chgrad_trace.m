function check_grad_trace()
%CHECK_GRAD_TRACE check_grad_trace()
%
% Gradient check for the function Tr(K^{-1}B) where B is square matrix.
% d Tr(K^{-1}B)/dtheta_i = Tr( dK^{-1}/dtheta_i B)
%                        = -Tr(K^{-1} dK/dtheta_i K^{-1}B)
N = 20; D = 2;
X = rand(N, D);
theta = log(rand(D+1,1)); theta(end) = 1;
B = rand(N,N);
[gradient, delta] = gradchek(theta',@f,@grad,X,B);
fprintf('difference: %12.12f\n', delta);
end

function val = f(theta,X,B)
K = covSEard(theta',X);
val = trace(K\B);
end

function g = grad(theta,X,B)
K = covSEard(theta,X);
g = zeros(size(theta));
for i=1:numel(theta)
  g(i) = -trace((K\covSEard(theta, X, [], i)) * (K\B));
end
end
