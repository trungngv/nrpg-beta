function chgrad_nlog_joint_noise()
%CHGRAD_NLOG_JOINT_NOISE chgrad_nlog_joint_noise()
% 
% Test for the grad_nlog_joint_noise() function.
%rng(20, 'twister');
params = test_data();
num_modes = params.num_modes;
D = size(params.X,2);
Q = params.num_latents;
lsf = log(0.001);
lsn2 = log(rand(num_modes,1));
mu = params.Mu';
K = feval('covSEard', log(rand(D+1,1)), params.X);
Lf = chol(K)';
K = feval('covSEard', log(rand(D+1,1)), params.X);
Lw = chol(K)';

[grad delta] = gradchek(log(rand), @my_f, @my_grad,mu,lsn2,Lf,Lw,lsf,params);
disp('grad')
fprintf('%.5f\n', grad);
disp('delta')
fprintf('%.5f\n', delta);
end

function f = my_f(x,Mu,sn2,Lf,Lw,lsf,params)
f = nlog_joint_noise(x',Mu,sn2,Lf,Lw,lsf,params);
end

function g = my_grad(x,Mu,sn2,Lf,Lw,lsf,params)
[dummy g] = nlog_joint_noise(x',Mu,sn2,Lf,Lw,lsf,params);
g = g';
end

