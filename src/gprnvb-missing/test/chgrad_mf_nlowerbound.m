function chgrad_mf_nlowerbound()
%CHGRAD_MF_NLOWERBOUND chgrad_mf_nlowerbound()
% 
% Gradient check for the mf_nlowerbound() function.
%rng(23, 'twister');
model = test_data();
P = model.num_outputs;
Q = model.num_latents;
[N D] = size(model.X);
lsy = 0.1;
ltheta = rand(2*(D+1),1);
theta = gprn_put_parameters(log(rand(D+1,1)), log(rand(D+1,1)), 0.001, lsy);
[~, ~, Lf, Lw] = sample_prior(model.X, theta, model, [], []);
CovF = cell(Q,1);
CovW = cell(P,Q);
model.CovF = CovF; model.CovW = CovW;
for j=1:Q
  A = rand(N,N);
  CovF{j} = tril(A)*tril(A)';
  for i=1:P
    A = rand(N-sum(model.Y_isnan(:,i)),N-sum(model.Y_isnan(:,i)));
    CovW{i,j} = tril(A)*tril(A)';
  end
end
% ensure var is the diagonal of corresponding covariance matrices
var = [];
for j=1:Q
  var = [var; diag(CovF{j})];
end
for j=1:Q
  for i=1:P
    wij = zeros(N,1);
    wij(~model.Y_isnan(:,i)) = diag(CovW{i,j});
    var = [var; wij];
  end
end
model.CovF = CovF;
model.CovW = CovW;

mu = model.Mu(:,1); % mu_n is dx1
[gradient, delta] = gradchek(lsy,@flsy,@glsy,mu,var,ltheta,Lf,Lw,model);
fprintf('delta = %.10f\n', delta);
[gradient, delta] = gradchek(ltheta',@fltheta,@gltheta,mu,var,lsy,[],[],model);
fprintf('delta = %.10f\n', delta);

[nf,~,~,mu,var] = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,0);
end

function f = fltheta(x,mu,var,lsy,Lf,Lw,model)
[~, D] = size(model.X);
ltheta_f = x(1:D+1);
ltheta_w = x(D+2:end);
Kf = feval(model.covfunc_f, ltheta_f, model.X);
Kw = feval(model.covfunc_w, ltheta_w, model.X);
Lf = jit_chol(Kf)';
Lw = jit_chol(Kw)';
f = mf_nlowerbound(mu,var,x',lsy,Lf,Lw,model,1,true,true);
end

function g = gltheta(x,mu,var,lsy,Lf,Lw,model)
[~, D] = size(model.X);
ltheta_f = x(1:D+1);
ltheta_w = x(D+2:end);
Kf = feval(model.covfunc_f, ltheta_f, model.X);
Kw = feval(model.covfunc_w, ltheta_w, model.X);
Lf = jit_chol(Kf)';
Lw = jit_chol(Kw)';
[~, g] = mf_nlowerbound(mu,var,x',lsy,Lf,Lw,model,1,true,true);
g = g';
end

function f = flsy(x,mu,var,ltheta,Lf,Lw,model)
f = mf_nlowerbound(mu,var,ltheta,x,Lf,Lw,model,2);
end

function g = glsy(x,mu,var,ltheta,Lf,Lw,model)
[~, ~, g] = mf_nlowerbound(mu,var,ltheta,x,Lf,Lw,model,2);
g = g';
end
