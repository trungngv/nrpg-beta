N = 4; D = 3;
rng(1110, 'twister')
mu = rand(N, D); mu_mean = rand(N,D);
% sq_dist takes the matrix where each column is the vector to take distance
S1 = sq_dist(mu',mu_mean');
S3 = sq_dist(mu'-mu_mean');
S2 = zeros(N, N);
for i=1:N
  for j=1:N
    S2(i,j) = (mu(i,:)-mu_mean(j,:))*(mu(i,:)-mu_mean(j,:))';
  end
end
disp(S1-S2)
disp(S1-S3)