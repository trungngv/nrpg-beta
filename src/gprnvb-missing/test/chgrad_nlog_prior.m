function check_grad_nlog_prior()
%CHECK_GRAD_NLOG_PRIOR check_grad_nlog_prior()
% Test for the grad_nlog_prior() function.
%
% 17/08/12
N = 10; D = 3;
%rng(1, 'twister');
x = rand(N,D);
covfunc = 'covSEard';
y = rand(N,1);
theta = rand(D+1,1);
gradchek(theta', @my_f, @my_grad, covfunc, x, y);
end

function f = my_f(theta, covfunc, x, y)
[n D] = size(x);
K = feval(covfunc, theta, x);
L = chol(K);
alpha = solve_chol(L,y);
f = 0.5*y'*alpha + sum(log(diag(L))) + 0.5*n*log(2*pi);
end

function g = my_grad(theta, covfunc, x, y)
g = grad_nlog_prior(theta', covfunc, x, y);
g = g';
end

