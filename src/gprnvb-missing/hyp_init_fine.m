function hyp = hyp_init_fine(D)
%HYP_INIT_FINE hyp = hyp_init_fine(D)
%
choose = floor(5*rand);
switch choose
  case 0
    hyp = hyp_init_smallf(D);
  case 1
    hyp = hyp_init_test(D);
  case 2
    hyp = hyp_init_total_random(D);
  case 3
    hyp = hyp_init_smallf_new(D);
  case 4
    thetaf = log(0.1*rand(D+1,1));
    thetaw = log([rand(D,1); 1]);
    hyp = [thetaf; thetaw; log(1)];
end
end

