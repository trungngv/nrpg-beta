clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 101;
rng(rngno, 'twister');
dataset = 'jura';
num_outputs = 3;
max_iters = 3;
predict_test = true;
standardise_x = false;
hypinits = {'hyp_init_best'};
enforce_bound = false;
optimise_sf = true;
optimise_sw = true;
executor = @npv_run_separation;
modes =[1];
for hypinit=hypinits
  func_hypinit = hypinit{1};
for num_modes=modes
  % record settings
  filename = ['output/npvgprn/' func_hypinit  '/' func2str(executor) ...
    '_modes' num2str(num_modes) '_' num2str(tic) '.txt'];
  fid = fopen(filename, 'w');
  fprintf(fid, 'Runner: %s\n', func2str(executor));
  fprintf(fid, 'Date time: %s\n', datestr(now));
  fprintf(fid, 'Rngno:\t %d\n', rngno);
  fprintf(fid, 'Dataset:\t %s\n', dataset);
  fprintf(fid, 'Num outputs:\t %d\n', num_outputs);
  fprintf(fid, 'Num modes:\t %d\n', num_modes);
  fprintf(fid, 'Max iters: %d\n', max_iters);
  fprintf(fid, 'Hyp init function: %s\n', func_hypinit);
  fprintf(fid, 'Standardised X:\t %d\n', standardise_x);
  fprintf(fid, 'Predict on new data:\t %d\n', predict_test);
  fprintf(fid, 'Enforce lower bound improvement: %d\n', enforce_bound);
  fprintf(fid, 'Optimise latent signal variance: %d\n', optimise_sf);
  fprintf(fid, 'Optimise weight signal variance: %d\n', optimise_sw);

  cnt = 1; iters = 10;
  mae = zeros(iters,num_outputs);
  mae_pseudo = mae;
  ymean = zeros(iters,num_outputs);
  while cnt <= iters
    try
      [mae(cnt,:) ymean(cnt,:) hyp lowerbound mae_pseudo(cnt,:) hyp0] = executor(dataset, num_modes,...
        predict_test,standardise_x,max_iters,func_hypinit,enforce_bound,optimise_sf,optimise_sw);
      fprintf(fid, '====================\nRun %d:\n',cnt);
      fprintf(fid, 'Init hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp0));
      fprintf(fid, '\nOptimised hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp));
      fprintf(fid, '\n\tELBO = ');
      fprintf(fid, '\n\t\t%.4f', lowerbound);
      fprintf(fid, '\n\tmae \t\t\t\t =');
      fprintf(fid, '%.5f\t', mae(cnt,:));
      fprintf(fid, '\n\tmae (pseudo) =');
      fprintf(fid, '%.5f\t', mae_pseudo(cnt,:));
      fprintf(fid, '\n');
      cnt = cnt + 1;
      disp(mae)
   catch err
      disp(err)
    end
  end  

  mae = mae(:,1); ymean=ymean(:,1); mae_pseudo = mae_pseudo(:,1);
  disp('mae      ymean')
  disp([mae ymean])
  fprintf('avg: %.4f, std: %.4f\n', mean(mae), std(mae));

  fprintf(fid, '\nMean absolute error:\n');
  fprintf(fid, '%.5f\n', mae);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));

  fprintf(fid, '\nMean absolute error with pseudo_w:\n');
  fprintf(fid, '%.5f\n', mae_pseudo);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae_pseudo), std(mae_pseudo));

  fclose(fid);
  disp(['output written to ' filename])
  copyfile(filename, '/home/trung/Dropbox/experimentslog/');
end
end
