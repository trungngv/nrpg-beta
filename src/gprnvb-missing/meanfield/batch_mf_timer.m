clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'concrete3';
num_outputs = 3;
maxIters = 200;
predict_test = true;
standardise_x = true;
log_transform_y = false;
hypinits = {'hyp_init_standard'};
optimise_sf = false;
optimise_sw = true;
executor = @gprn_mf_run_timer;
for hypinit=hypinits
  func_hypinit = hypinit{1};
  cnt = 1; iters = 5;
  while cnt <= iters
    try
      executor(dataset,predict_test, standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,true,log_transform_y);
      cnt = cnt + 1;
   catch err
      disp(err)
    end
  end  

end

