function f = entropy(params)
%ENTROPY f = entropy(params)
% 
% Entropy of the posterior approximation.
%
f = 0;
params.num_latents;
for j=1:params.num_latents
  L = jit_chol(params.CovF{j});
  f = f + sum(log(diag(L)));
  for i=1:params.num_outputs
    L = jit_chol(params.CovW{i,j});
    f = f + sum(log(diag(L)));
  end
end
end

