function [mae smse hyp F hyp0] = gprn_mf_run(dataset, test_prediction,...
  standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,optimise_hyp,log_scale)
%gprn_mf_run(dataset, test_prediction,standardise_x, maxIters, func_hypinit,
%optimise_sf,optimise_sw,optimise_hyp,log_scale)
%
% Trung V. Nguyen
% 19/10/12

% read data and init model
[params, xtest, ytest] = prepare_data(1, dataset,test_prediction, [],...
  standardise_x,log_scale);
[Ntrain, D] = size(params.X);
num_latents = params.num_latents;
num_outputs = params.num_outputs;

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
hyp0 = feval(func_hypinit, D);
D = Ntrain*num_latents*(num_outputs+1);
mu = randn(D,1);
var = rand(D,1);

% run
[F mu var hyp lsy] = mf_run(mu,var,hyp0(1:end-1),hyp0(end),params,maxIters,...
  optimise_sf,optimise_sw,optimise_hyp);

disp('learned hyp vs. init hyp');
disp([exp([hyp(:); lsy]), exp(hyp0(:))])
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

%% prediction
mu(1:10)
[mae smse]= mf_prediction(xtest,ytest,mu,hyp,params);
hyp = [hyp; lsy];
end

function [F mu var hyp lsy] = mf_run(mu,var,hyp,lsy,model,nIter,optimise_sf,...
  optimise_sw,optimise_hyp)
tol = 0.00001; F = zeros(nIter,1);
opts = struct('Display','off','Method','lbfgs','MaxIter',200,...
  'MaxFunEvals',200,'DerivativeCheck','off'); 

% pre-compute Lf, Lw 
X = model.X; Dx = size(X,2); Q = model.num_latents;
tbegin = tic;
for iter = 1:nIter
  disp(['iteration: ',num2str(iter)]);
  if iter == 1 || optimise_hyp
    if ~optimise_sf,  hyp(Dx+1) = log(1);  end
    if ~optimise_sw,  hyp(end) = log(1);   end
    disp(['hyp = ', num2str(exp([hyp' lsy]))])
    Kf = feval(model.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(model.covfunc_w, hyp(Dx+2:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
  end  
  if iter > 1,
    F(iter) = -mf_nlowerbound(mu,var,[],lsy,Lf,Lw,model,-1);
%     elbo_after = F(iter);
%     fprintf('\tafter - before = %.10f\n', elbo_after - elbo_before);
  end
  fprintf('ELBO %.5f\n', F(iter));
  if iter > 1 && abs(F(iter)-F(iter-1)) < tol; break; end
  
  tstart = tic;
  % optimise mu and var
  [~,~,~,mu,var,model] = mf_nlowerbound(mu,var,[],lsy,Lf,Lw,model,0);
  disp(['optimising mu and var takes ', num2str(toc(tstart))])
        
  % optimise noise
  tstart = tic;
  func = @(x) nbound_lsy(x,mu,var,[],Lf,Lw,model);
  lsy = minFunc(func,lsy,opts);
  disp(['optimising noise takes ', num2str(toc(tstart))]);
  
  % optimise hyp of covariance function
  if optimise_hyp
    tstart = tic;
    func = @(x) nbound_ltheta(x,mu,var,lsy,model,optimise_sf,optimise_sw);
    hyp = minFunc(func, hyp, opts);
    disp(['optimising log hyp takes ', num2str(toc(tstart))])
  end  
end
fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
end

function [nf ng] = nbound_ltheta(ltheta,mu,var,lsy,model,optimise_sf,optimise_sw)
X = model.X; D = size(X,2);
%Must 'synchronise' Var(Wij) and CovW{i,j} when Kw changes!
if ~optimise_sf,   ltheta(D+1) = log(1); end
if ~optimise_sw,   ltheta(end) = log(1); end
Kf = feval(model.covfunc_f, ltheta(1:D+1), X);
Kw = feval(model.covfunc_w, ltheta(D+2:end), X);
[Lf, ~] = jit_chol(Kf); [Lw, ~] = jit_chol(Kw);
Lf = Lf'; Lw = Lw';
if nargout > 1
  [nf, ng] = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,1,optimise_sf,optimise_sw);
else
  nf = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,1);
end
end

function [nf ng] = nbound_lsy(lsy,mu,var,ltheta,Lf,Lw,model)
if nargout > 1
  [nf, ~, ng] = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,2);
else
  nf = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,2);
end
end

