clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'geneExpression1';
num_outputs = 1000;
maxIters = 100;
predict_test = true;
standardise_x = false;
log_scale = false;
hypinits = {'hyp_init_standard'};
optimise_sf = false;
optimise_sw = true;
executor = @gprn_mf_run;
for hypinit=hypinits
  func_hypinit = hypinit{1};
  % record settings
  filename = ['output/npvgprn/' dataset  '/' func2str(executor) '_' ...
    func_hypinit '_' num2str(tic) '.txt'];
  mkdir(['output/npvgprn/' dataset]); 
  fid = fopen(filename, 'w');
  runtime = now;
  log_to_file(fid, executor, runtime, rngno, dataset, num_outputs, maxIters,...
    func_hypinit, standardise_x, log_scale, predict_test, optimise_sf, optimise_sw);
  cnt = 1; iters = 5;
  mae = zeros(iters,num_outputs);
  smse = mae;
  while cnt <= iters
%     try
      [mae(cnt,:) smse(cnt,:) hyp lowerbound hyp0] = executor(dataset,predict_test,...
        standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,true,log_scale);
      fprintf(fid, '\n====================\nRun %d:\n',cnt);
      fprintf(fid, 'Init hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp0));
      fprintf(fid, '\nOptimised hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp));
      fprintf(fid, '\n\tELBO = ');
      fprintf(fid, '\n\t\t%.4f', lowerbound(lowerbound~=0));
      fprintf(fid, '\n\tmae \t\t\t\t =');
      fprintf(fid, '%.5f\t', mae(cnt,:));
      fprintf(fid, '\n\tsmse \t\t\t\t =');
      fprintf(fid, '%.5f\t', smse(cnt,:));
      fprintf(fid, '\n');
      cnt = cnt + 1;
%    catch err
%       disp(err)
%     end
  end  

  % save prediction error to file
  save([filename '.mat'], 'mae', 'smse');
  % for the first output
  mae = mae(:,1); smse = smse(:,1);
  
  fprintf(fid, '\nMAE:\n');
  fprintf(fid, '%.5f\n', mae);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));

  fprintf(fid, '\nSMSE:\n');
  fprintf(fid, '%.5f\n', smse);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(smse), std(smse));

  fclose(fid);
  disp(['output written to ' filename])
  copyfile(filename, '/home/trung/Dropbox/experimentslog/');
end

