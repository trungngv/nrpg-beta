function gprn_mf_run_timer(dataset, test_prediction,...
  standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,optimise_hyp,log_transform)
%MF_RUN_V1 [mae Ymean hyp F mae_pseudo hyp0] = mf_run_v1(dataset, test_prediction,...
%  standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,optimise_hyp,log_transform)
%
% Trung V. Nguyen
% 19/10/12
%% init data set
switch (dataset)
  case 'synthetic'
    Ntrain = 30; Ntest = 20;
    num_latents = 3; num_outputs = 3; D = 2; num_modes = 1;
    sigma_f = 0.000; sigma_y = 0.1;
    theta_f = log(rand(D+1,1));
    theta_w = log(rand(D+1,1));
    theta_f(end) = 0.5+theta_f(end);
    theta_w(end) = 0.5+theta_w(end);
    a = ones(num_latents,2)*theta_f(end);
    [params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs,...
      num_modes, theta_f, theta_w, a, sigma_f, sigma_y,true);
    [params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    % prediction on new/test data or missing data
    if test_prediction
      ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
      params.Y(ind_n,1) = nan;
    else
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end
  case 'jura1' % jura with standard gp -- 1 output
    Ntrain = 259; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 1;
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if (test_prediction)
      [Y, Ymean, Ystd] = standardize(log(Y),1,[],[]);
    else
      Y = [Y; ytest];
      [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
    end
    params.X = X(1:Ntrain,:);
    params.Y = Y(1:Ntrain,:);
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    
  case 'jura'
    Ntrain = 359; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 3; % jura
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    % pre-process x
    X = [X; xtest];
    if (standardise_x)
      X = standardize(X,1,[],[]);
      xtest = X(Ntrain-Ntest+1:end,:);
    end
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if (test_prediction)
      Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
      Ymean = zeros(1,3); Ystd = zeros(1,3);
      [Y(1:Ntrain-Ntest,1), Ymean(1), Ystd(1)] = standardize(log(Y(1:Ntrain-Ntest,1)),1,[],[]);
      [Y(:,2:3), Ymean(2:3), Ystd(2:3)] = standardize(log(Y(:,2:3)),1,[],[]);
    else
      Y = [Y; ytest];
      [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
    end
    params.X = X(1:Ntrain,:);
    params.Y = Y(1:Ntrain,:);
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    
  case {'concrete','concrete3'}
    num_latents = 1;
    [X, Y, xtest, ytest] = load_data('data/concreteslump', dataset);
    num_outputs = size(Y,2);
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    features = [1,3:5];
    X = X(:,features); xtest = xtest(:,features);
    [Ntrain, D] = size(X);
    % pre-process x
    if (standardise_x)
      [X, Xmean, Xstd] = standardize(X,1,[],[]);
      xtest = standardize(xtest,1,Xmean,Xstd);
    end
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    [Y Ymean Ystd] = standardize(Y,1,[],[]);
    params.X = X; params.Y = Y;
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
  case 'sarcos'
    Ntrain = 100; Ntest = 100;
    D = 21; num_latents = 1; num_outputs = 1;
    related_outputs = [2]; %[2,3,4,7]
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [X, Y, xtest, ytest] = load_data('data/SARCOS', 'sarcos');
    X = X(1:Ntrain,:); Y = Y(1:Ntrain,related_outputs);
    xtest = xtest(1:Ntest,:); ytest = ytest(1:Ntest,related_outputs);
    % pre-process x
    if (standardise_x)
      [X, Xmean, Xstd] = standardize(X,1,[],[]);
      xtest = standardize(xtest,1,Xmean,Xstd);
    end
    % pre-process y
    Ymean = zeros(1,num_outputs); Ystd = zeros(1,num_outputs);
    for i=1:num_outputs
      if log_transform
        Ymean(i) = mean(log(Y(:,i)));
        Ystd(i) = std(log(Y(:,i)));
        Y(:,i) = (log(Y(:,i)) - Ymean(i))/Ystd(i);
      else
        Ymean(i) = mean(Y(:,i));
        Ystd(i) = std(Y(:,i));
        Y(:,i) = (Y(:,i) - Ymean(i))/Ystd(i);
      end
    end
    params.X = X; params.Y = Y;
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    
  otherwise
    if ~isempty(strfind(dataset, 'geneExpression')) % for gene data set
          [X, Y, xtest, ytest] = load_data('data/gene', dataset);
    num_latents = 1; num_outputs = size(Y,2);
    params = struct('num_latents', num_latents, 'num_outputs', num_outputs,...
      'covfunc_f', 'covSEard', 'covfunc_w', 'covSEard');
    [Ntrain, D] = size(X);
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    [Y Ymean Ystd] = standardize(Y,1,[],[]);
    params.X = X; params.Y = Y;
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    end
end

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
hyp0 = feval(func_hypinit, D);
D = Ntrain*num_latents*(num_outputs+1);
mu = randn(D,1);
var = rand(D,1);

% run
mf_run(mu,var,hyp0(1:end-1),hyp0(end),params,maxIters,optimise_sf,optimise_sw,optimise_hyp);

end

function mf_run(mu,var,hyp,lsy,model,nIter,optimise_sf,optimise_sw,optimise_hyp)
fid = fopen(['time' num2str(tic) '_mf.txt'],'w');
tol = 0.00001; F = zeros(nIter,1);
opts = struct('Display','off','Method','lbfgs','MaxIter',200,...
  'MaxFunEvals',200,'DerivativeCheck','off'); 

% pre-compute Lf, Lw 
X = model.X; Dx = size(X,2); Q = model.num_latents;
tbegin = tic;
for iter = 1:nIter
  if iter == 1 || optimise_hyp
    if ~optimise_sf,  hyp(Dx+1) = log(1);  end
    if ~optimise_sw,  hyp(end) = log(1);   end
    Kf = feval(model.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(model.covfunc_w, hyp(Dx+2:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
  end  
  if iter > 1,
    F(iter) = -mf_nlowerbound(mu,var,[],lsy,Lf,Lw,model,-1);
  end
  if iter > 1 && abs(F(iter)-F(iter-1)) < tol; break; end
  [~,~,~,mu,var,model] = mf_nlowerbound(mu,var,[],lsy,Lf,Lw,model,0);
  func = @(x) nbound_lsy(x,mu,var,[],Lf,Lw,model);
  lsy = minFunc(func,lsy,opts);
  if optimise_hyp
    func = @(x) nbound_ltheta(x,mu,var,lsy,model,optimise_sf,optimise_sw);
    hyp = minFunc(func, hyp, opts);
  end  
end
elapse = toc(tbegin);
fprintf(fid,'%s\n', 'jura');
fprintf(fid,'\n\ntotal training time %.2f(s)\n', elapse);
fprintf(fid,'\niterations %d\n', iter);
fprintf(fid,'\n average training per iterations: %.2f(s)\n', elapse/iter);
fclose(fid);    
end

function [nf ng] = nbound_ltheta(ltheta,mu,var,lsy,model,optimise_sf,optimise_sw)
X = model.X; D = size(X,2);
%Must 'synchronise' Var(Wij) and CovW{i,j} when Kw changes!
if ~optimise_sf,   ltheta(D+1) = log(1); end
if ~optimise_sw,   ltheta(end) = log(1); end
Kf = feval(model.covfunc_f, ltheta(1:D+1), X);
Kw = feval(model.covfunc_w, ltheta(D+2:end), X);
[Lf, ~] = jit_chol(Kf); [Lw, ~] = jit_chol(Kw);
Lf = Lf'; Lw = Lw';
if nargout > 1
  [nf, ng] = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,1,optimise_sf,optimise_sw);
else
  nf = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,1);
end
end

function [nf ng] = nbound_lsy(lsy,mu,var,ltheta,Lf,Lw,model)
if nargout > 1
  [nf, ~, ng] = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,2);
else
  nf = mf_nlowerbound(mu,var,ltheta,lsy,Lf,Lw,model,2);
end
end

