function [mae smse] = mf_prediction(xtest,ytest,mu,hyp,params)
%MF_PREDICTION [mae smse] = mf_prediction(xtest,ytest,mu,hyp,params)
% Make prediction
%
% INPUT
%   - xtest : input locations
%   - ytest : outputs
%
% OUTPUT
%   - error
%
P = params.num_outputs;
Q = params.num_latents;
[Ntest D] = size(xtest);
X = params.X;
Ntrain = size(X,1);
ltheta_f = hyp(1:D+1);
ltheta_w = hyp(D+2:end);

Kf = feval(params.covfunc_f, ltheta_f, params.X);% + (sf^2) * eye(N);
Kw = feval(params.covfunc_w, ltheta_w, params.X);
Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';

[ft wt] = u_to_fhat_w(mu, params);
% Step 1: Compute expected value of w for the missing data
if sum(params.w_isnan) > 0
wt(params.w_isnan) = 0;
W = mat2cell(reshape(wt(:), Ntrain*P,Q), Ntrain*ones(1,P), ones(1,Q));
for p=1:P
  for q=1:Q
    if (sum(params.Wcell_isnan{p,q})) > 0
      obs_ind = ~params.Wcell_isnan{p,q};
      Kw = feval(params.covfunc_w, ltheta_w, X(obs_ind,:), X(obs_ind,:));
      Lw_obs = jit_chol(Kw)';
      kw_s = feval(params.covfunc_w, ltheta_w, X(obs_ind,:), X(params.Wcell_isnan{p,q},:));
      W{p,q}(params.Wcell_isnan{p,q}) = (Lw_obs\kw_s)' * (Lw_obs\W{p,q}(obs_ind));
    end  
  end
end
wt = cell2mat(W);
mu = [ft(:); wt(:)];
end

% Step 2: Prediction
ystar = zeros(P,Ntest);
[ft wt] = u_to_fhat_w(mu, params);
ft = ft(:); wt = wt(:);
wt(params.w_isnan) = 0;
%wt = wt(~params.w_isnan); % discard missing data
for i=1:Ntest
  kf_s = feval(params.covfunc_f, ltheta_f, X, xtest(i,:));
  kw_s = feval(params.covfunc_w, ltheta_w, X, xtest(i,:));
  alphaLf = (Lf\kf_s)'; alphaLw = (Lw\kw_s)';
  idx_f = 1; idx_w = 1;
  Wstar = zeros(P,Q); fstar = zeros(Q,1);
  for q=1:Q
    fstar(q) = alphaLf * (Lf\ft(idx_f:idx_f+Ntrain-1));
    idx_f = idx_f + Ntrain;
    for p=1:P
      Wstar(p,q) = alphaLw * (Lw\wt(idx_w:idx_w+Ntrain-1));
      idx_w = idx_w + Ntrain;
    end
  end
  ystar(:,i) = ystar(:,i) + Wstar * fstar;
end

Ymean = repmat(params.Ymean,Ntest,1);
Ystd = repmat(params.Ystd,Ntest,1);
if params.logscale
  ystar = exp(ystar'.*Ystd + Ymean);
else
  ystar = ystar'.*Ystd + Ymean;
end

mae = mean(abs(ystar-ytest));
smse = mysmse(ystar, ytest);
if P < 10
  disp('ystar      ytest')
  disp([ystar ytest])
  
  disp('mae = ');
  fprintf('%.4f\t', mae);
  fprintf('\n');
  disp('smse = ');
  fprintf('%.4f\t', smse);
  fprintf('\n');
else
  disp('ystar      ytest (first 5)')
  disp([ystar(:,1:5) ytest(:,1:5)])

  disp('mean-absolute error (first 10 outputs) = ')
  fprintf('%.4f\t', mae(1:10));
  fprintf('\n');
  disp('avg mean-absolute-error')
  fprintf('%.4f\n', mean(mae));
  disp('standardized-mean-squared-error (first 10 outputs)')
  fprintf('%.4f\t', smse(1:10));
  fprintf('\n');
  disp('avg smse')
  fprintf('%.4f\n', mean(smse));
end
end

