clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'concrete3';
num_outputs = 3;
predict_test = true;
standardise_x = true;
optimise_sf = false;
optimise_sw = true;
log_scale = false;
executor = @gprn_mf_run;
func_hypinit = 'hyp_init_standard';
filename = ['output/npvgprn/' dataset  '/selection' func2str(executor) '_' ...
    func_hypinit '_' num2str(tic) '.txt'];
maxItersHypOpt = 500;
maxIters = 500;

% model selection for hyper-parameters
cnt = 1; iters = 5;
elbos = zeros(iters,1);
learned_hyp = [];
while cnt <= iters
  try
    [~,~, hyp, elbo, ~] = executor(dataset, predict_test, ...
      standardise_x, maxItersHypOpt, func_hypinit,optimise_sf,optimise_sw,true,log_scale);
    elbo = elbo(elbo ~= 0);
    elbos(cnt) = elbo(end);
    if isempty(learned_hyp)
      learned_hyp = zeros(numel(hyp), iters);
    end  
    learned_hyp(:,cnt) = hyp(:);
    cnt = cnt + 1;
  catch err
    disp(err)
  end
end  
[~, idx_max] = max(elbos);
func_hypinit = @(D) (learned_hyp(:,idx_max));

% record settings
fid = fopen(filename, 'w');
runtime = now;
log_to_file(fid, executor, runtime, rngno, dataset, num_outputs, maxItersHypOpt,...
  maxIters, standardise_x, log_scale, predict_test, optimise_sf, optimise_sw);
cnt = 1; iters = 10;
mae = zeros(iters,num_outputs);
mae_pseudo = mae; smse = mae; smse_pseudo = mae;
ymean = zeros(iters,num_outputs);
while cnt <= iters
%   try
    [mae(cnt,:) smse(cnt,:) hyp lowerbound hyp0] = executor(dataset,...
      predict_test, standardise_x, maxIters,func_hypinit,optimise_sf,optimise_sw,true,log_scale);
    fprintf(fid, '====================\nRun %d:\n',cnt);
    fprintf(fid, 'Init hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp0));
    fprintf(fid, '\nOptimised hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp));
    fprintf(fid, '\n\tELBO = ');
    fprintf(fid, '\n\t\t%.4f', lowerbound(lowerbound ~=0));
    fprintf(fid, '\n\tmae \t\t\t\t =');
    fprintf(fid, '%.5f\t', mae(cnt,:));
    fprintf(fid, '\n\tsmse \t\t\t\t =');
    fprintf(fid, '%.5f\t', smse(cnt,:));
    fprintf(fid, '\n');
    cnt = cnt + 1;
    disp(mae)
%   catch err
%     disp(err)
%   end
end  

% save prediction error to file
save([filename '.mat'], 'mae', 'smse');
% for the first output
mae = mae(:,1); ymean=ymean(:,1); mae_pseudo = mae_pseudo(:,1);
smse = smse(:,1); smse_pseudo = smse_pseudo(:,1);
 
fprintf(fid, '\nMAE:\n');
fprintf(fid, '%.5f\n', mae);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));

fprintf(fid, '\nSMSE:\n');
fprintf(fid, '%.5f\n', smse);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(smse), std(smse));

fclose(fid);
disp(['output written to ' filename])
copyfile(filename, '/home/trung/Dropbox/experimentslog/');
