function [nf ndy] = nlog_joint_noise(lsy,Mu,sn2,Lf,Lw,lsf,params)
%NLOG_JOINT_HYP [nf ndhyp] = nlog_joint_noise(lsy,Mu,sn2,Lf,Lw,lsf,params)
%   
% Returns the negative log joint probability, excluding the constant terms
% wrt the noise \log \sigma_y. 
%
% INPUT
%   - hyp : log hyperparameters, should be log[sigma_y]
%   - Mu : KxD matrix
%   - Lf : jit_chol(Kf)'
%   - Lw : jit_chol(Kw)'
%
% OUTPUT
%
% Trung V. Nguyen
% 24/08/12
P = params.num_outputs; Q = params.num_latents; K = params.num_modes;
X = params.X;
Nd = size(X,1);
sn2 = exp(sn2) + 1e-10;
sy2 = exp(2*lsy);
nf = 0;
dtraceH = zeros(size(Mu,2),size(Mu,1)); %DxK
ntraceH = zeros(K,1);
for k=1:K
  [nfk dum ndiagH dtraceH(:,k)] = nlog_joint(Mu(k,:),lsy,Lf,Lw,params);
  ntraceH(k) = sum(ndiagH);
  nf = nf + nfk + 0.5*sn2(k)*ntraceH(k);
end
nf = nf/K;

if nargout == 2
  ndy = 0;
  for k=1:K
    [f, w] = u_to_fhat_w(Mu(k,:),params);
    Fhat = reshape(f,Nd,Q); Wmat=reshape(w,Nd*P,Q);
    ndy = ndy + ndkdy(params.Y,Fhat,Wmat,sy2,params) + sum(dtraceH(:,k))*sn2(k)/sy2;
  end
  ndy = ndy/K + Nd*P-sum(sum(params.Y_isnan)); %missing data
end
end

% - d log p(y|mu_k) / d log sigma_y
function dy = ndkdy(Y,Fhat,W,sy2,params)
[N,P] = size(Y);
Wxn_ind = N*(0:P-1)';
W_ind = bsxfun(@plus,Wxn_ind,1:N);
Wblk = W(W_ind(:),:);
tt = repmat(1:N,P,1);
Fblk = Fhat(tt(:),:);
Ymean = reshape(sum(Wblk.*Fblk,2),P,N)';
Ydiff = (Y-Ymean).^2;
Ydiff(params.Y_isnan) = 0; % missing data part
dy = -sum(sum(Ydiff))/sy2;
end

