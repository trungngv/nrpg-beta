function [nf ndhyp] = nlog_joint_hyp(hyp,Mu,sn2,lsf,params)
%NLOG_JOINT_HYP [nf ndhyp] = nlog_joint_hyp(hyp, Mu, sn2, lsf, params)
%   
% Returns the negative log joint probability, excluding the constant terms
% wrt hyperparameters (including hyp of covariance function and the noise).
% Hence this can be used as the objective function for optimisation wrt the log hyperparameters.
%
% This implementation is for the case when all latent functions share the
% same hyperparameters. It also learns the output noise sigma_y.
% 
% INPUT
%   - hyp : log hyperparameters, should be log[theta_f, theta_w, sigma_y]
%   - Mu : KxD matrix
%
% OUTPUT
%
% Trung V. Nguyen
% 24/08/12
P = params.num_outputs; Q = params.num_latents; K = params.num_modes;
X = params.X;
Nd = size(X,1);
sn2 = exp(sn2) + 1e-10;
ltheta_f = hyp(1:size(X,2)+1);
ltheta_w = hyp(numel(ltheta_f)+1:end-1);
lsy = hyp(end); sy2 = exp(2*lsy);
Kf = feval(params.covfunc_f, ltheta_f, X);% + (exp(lsf)^2)*eye(Nd);
Kw = feval(params.covfunc_w, ltheta_w, X);
[Lf ~] = jit_chol(Kf); [Lw ~] = jit_chol(Kw);
nf = 0;
dtraceH = zeros(size(Mu,2),size(Mu,1)); %DxK
ntraceH = zeros(K,1);
for k=1:K
  [nfk dum ndiagH dtraceH(:,k)] = nlog_joint(Mu(k,:),lsy,Lf',Lw',params);
  ntraceH(k) = sum(ndiagH);
  nf = nf + nfk + 0.5*sn2(k)*ntraceH(k);
end
nf = nf/K;

if nargout == 2
  dtheta_f = zeros(size(ltheta_f)); dtheta_w = zeros(size(ltheta_w));
  ndy = 0;
  for k=1:K
    [f, w] = u_to_fhat_w(Mu(k,:),params);
    Fhat = reshape(f,Nd,Q); Wmat=reshape(w,Nd*P,Q);
    W = mat2cell(Wmat,Nd*ones(1,P),ones(1,Q));
    for j=1:Q
      dtheta_f = dtheta_f + grad_nlog_prior(ltheta_f,params.covfunc_f,X,Fhat(:,j));
      for i=1:P
        dtheta_w = dtheta_w + grad_nlog_prior(ltheta_w,params.covfunc_w,...
          X(~params.Y_isnan(:,i),:),W{i,j}(~params.Y_isnan(:,i)));
      end
    end
    ndy = ndy + ndkdy(params.Y,Fhat,Wmat,sy2,params) + sum(dtraceH(:,k))*sn2(k)/sy2;
  end
  
  % second-order approximation
  dtheta2_f = zeros(size(ltheta_f)); dtheta2_w = zeros(size(ltheta_w));
  Kfinv = Lf\(Lf'\eye(Nd));%   Kwinv = Lw\(Lw'\eye(Nd));
  for idx=1:numel(ltheta_f)
    dK = feval(params.covfunc_f, ltheta_f, X, [], idx);
    dtheta2_f(idx) = Q*trace(Kfinv*dK*Kfinv);
    for i=1:P
      valid_ind = ~params.Y_isnan(:,i);
      dK = feval(params.covfunc_w, ltheta_w, X(valid_ind,:), [], idx);
      [Lw ~] = jit_chol(Kw(valid_ind,valid_ind));
      Kwinv = Lw\(Lw'\eye(size(Lw,1)));
      dtheta2_w(idx) = dtheta2_w(idx) + Q*trace(Kwinv*dK*Kwinv);
    end
  end
  ndhyp = [dtheta_f; dtheta_w]/K - 0.5*sum(sn2)*[dtheta2_f; dtheta2_w]/K;
  %ndhyp = [dtheta_f; dtheta_w]/K; %1st-order
  ndy = ndy/K + Nd*P-sum(sum(params.Y_isnan)); %missing data
  ndhyp = [ndhyp; ndy];
end
end

% - d log p(y|mu_k) / d log sigma_y
function dy = ndkdy(Y,Fhat,W,sy2,params)
[N,P] = size(Y);
Wxn_ind = N*(0:P-1)';
W_ind = bsxfun(@plus,Wxn_ind,1:N);
Wblk = W(W_ind(:),:);
tt = repmat(1:N,P,1);
Fblk = Fhat(tt(:),:);
Ymean = reshape(sum(Wblk.*Fblk,2),P,N)';
Ydiff = (Y-Ymean).^2;
Ydiff(params.Y_isnan) = 0; % missing data part
dy = -sum(sum(Ydiff))/sy2;
end

