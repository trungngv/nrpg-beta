function hyp = hyp_init_smallf(D)
%HYP_INIT_SMALLF hyp = hyp_init_smallf(D)
%
thetaf = log(0.1*rand(D+1,1));
thetaw = log(1+rand(D+1,1));
hyp = [thetaf; thetaw; log(1)];
end

