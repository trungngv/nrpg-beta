function [s q] = sum_logqn_arg_sn2(sn2, params)
%SUM_LOGQN_ARG_SN2 val = sum_logqn_arg_sn2(sn2, params)
% 
% Returns \sum log(q_n), which is the second term in L_1[q].
%
% This is a wrapper that treats sigma^2 as variable of the function, which is
% helpful for optimization.
%
N = params.num_modes;
q = zeros(N,1);
for n=1:N
  [q(n) ~] = qn_arg_sn2(sn2, n, params);
end
s = sum(log(q));

end

