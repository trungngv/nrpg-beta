function check_grad_sum_logqn_mu()
%CHECK_GRAD_SUM_LOGQN_MU check_grad_sum_logqn_mu()
%
% Check gradient for \sum logqn(), which is the second term in L_1[q], wrt
% mu_n
%
% Trung V. Nguyen
% 08/08/12
%

[N, params] = test_data();
for n=1:N
  mu_n = params.Mu(:,n); % mu_n is dx1
  [gradient, delta] = gradchek(mu_n', @my_f, @my_grad, n, params);
end  

end

function l = my_f(x, n, params)
l = sum_logqn_arg_mu(x', n, params);
end

function g = my_grad(x, n, params)
g = grad_sum_logqn_mu(x', n, params);
g = g';
end