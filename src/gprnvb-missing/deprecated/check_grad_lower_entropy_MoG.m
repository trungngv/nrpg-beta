function check_grad_lower_entropy_MoG()
%CHECK_GRAD_LOWER_ENTROPY_MOG Summary of this function goes here
%   Detailed explanation goes here
[~, params] = test_data();
num_modes = params.num_modes;
for n=1:num_modes
  theta = [params.Mu(:,n); params.sn2];
  [gradient, delta] = gradchek(theta', @my_f, @my_grad, n, params);
end

% compare with the implementation in lower_bound_MoG
D = size(params.Mu,1);
s2min = 0;
for n=1:num_modes
  theta = [params.Mu' log(params.sn2)];
  [f dmu] = lower_bound_MoG(theta,num_modes,D,s2min,n);
  [f dummy ds] = lower_bound_MoG(theta,num_modes,D,s2min,n);
  [mf mdmu mds] = lower_entropy_MoG(n, params);
  disp('    f       my f')
  disp([f mf])
  disp('    dmu     my dmu')
  disp([dmu; mdmu'])
  disp('    ds      my ds') % taking different derivatives
  disp([ds; mds'])
end
end

function f = my_f(x, n, params)
d = size(params.Mu, 1);
params.Mu(:,n) = x(1:d);
params.sn2 = x(d+1:end);
f = lower_entropy_MoG(n, params);
end

function g = my_grad(x, n, params)
[dummy dmu ds] = lower_entropy_MoG(n, params);
g = [dmu' ds'];
end

