function [q q_nj] = qn_arg_sn2(sn2, n, params)
%QN_ARG_SN2 [q q_nj] = qn_arg_sn2(sn2, n, params)
% 
% Compute q_n = (1/N)\sum N(mu_n; mu_j, (sigma_n^2 + sigma_j^2)I).
%
% This is a wrapper that treats sigma^2 as variable of the function, which is
% helpful for optimization.
%
% Trung V. Nguyen
% 08/08/12

N = params.num_modes; % N = number of components
% Mu = [mu_1 mu_2 ... mu_N]
Mu = params.Mu; % dxN
q_nj = zeros(N,1);
for j=1:N
  q_nj(j) = mvnpdf_iso(Mu(:,n), Mu(:,j), sqrt(sn2(n) + sn2(j)));
end
q = mean(q_nj);

end

