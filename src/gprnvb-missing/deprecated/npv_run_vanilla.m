function [mae Ymean] = gprn_npv_run_vanilla()
% Demonstration of GPRN with nonparametric variational inference
% Version without learning hyperparameters
clear
%rng(23,'twister');
Ntrain = 20; Ntest = 20;
D = 2; num_latents = 1; num_outputs = 2;
num_modes = 1;
sigma_f = 0.0; sigma_y = 0.01;
theta_f = log(rand(D+1,1)); theta_w = log(rand(D+1,1));
a = ones(num_latents,1)*theta_f(D+1);
theta_f(end) = log(0.5+exp(theta_f(end)));
theta_w(end) = log(0.5+exp(theta_w(end)));
[num_modes, params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
  theta_f, theta_w, a,sigma_f, sigma_y);
% training points as test points
xtest = params.X(1:Ntest,:);
ytest = params.Y(1:Ntest,:);
[params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
save('temp.mat', 'Ymean', 'Ystd');
% missing data only occurs when have multiple outputs
if num_outputs > 1
  ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
  ind_p = randi(num_outputs,ceil(0.1*Ntrain),1);
  ind_p = ones(ceil(0.1*Ntrain),1);
  %params.Y(ind_n,ind_p) = nan;
end  

Kf = feval(params.covfunc_f, theta_f, params.X);
Lf = jit_chol(Kf)';
Kw = feval(params.covfunc_w, theta_w, params.X);
Lw = jit_chol(Kw)';

%for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%initialization
D = Ntrain*params.num_latents*(params.num_outputs+1);
theta0 = randn(num_modes, D+1);
theta0(:,D+1) = 1;
nlogpdf = @(x) nlog_joint(x,log(sigma_y),Lf,Lw,params);
[F mu s2] = my_npv_run(nlogpdf,theta0,params);      % run NPV
disp('s2')
disp(s2)
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

% prediction
theta = [theta_f; theta_w];
mae = gprn_prediction(xtest,ytest,mu,theta,params);
end

function [F mu s2] = my_npv_run(nlogpdf,theta,params)
    nIter = 20; tol = 0.000001;
    s2min = 0.0000001;      % enforce minimium bandwidth to avoid numerical problems
    [N D] = size(theta); D = D - 1;
    opts = struct('Display','off','Method','lbfgs','MaxIter',200,'MaxFunEvals',200,'DerivativeCheck','off');  % optimization options
    tbegin = tic;
    for iter = 1:nIter
        disp(['iteration: ',num2str(iter)]);
        
        % first-order approximation (L1): optimize mu, one component at a time
        tstart = tic;
        for n = 1:N
            func = @(x) nELBO1(x,@(x) nlogpdf(x),N,D,s2min,theta,n,params);
            %theta(n,1:end-1) = minimize(theta(n,1:D)',func,20);
            % minFunc is better (but less robust)
            theta(n,1:end-1) = minFunc(func,theta(n,1:D)',opts);
        end
        disp(['optimising mu takes ', num2str(toc(tstart))])
        
        % second-order approximation (L2): optimize s2
        mu = theta(:,1:D); h = zeros(N,1);
        tstart = tic;
        for n = 1:N
          [dum1 dum2 diago] = nlogpdf(mu(n,:)');
          h(n) = sum(diago);
        end
        func = @(theta) nELBO2(theta,N,D,mu,s2min,h,params);
        theta = minFunc(func, theta(:,D+1),opts);
        %theta = minimize(theta(:,D+1),func,20);
        theta = [mu reshape(theta,N,1)];
        disp(['optimising sigma takes ', num2str(toc(tstart))])
        
        F(iter,1) = ELBO(theta,nlogpdf,h,s2min,params);
        fprintf('ELBO %.5f\n', F(iter,1));
        if iter > 1 && abs(F(iter)-F(iter-1))<tol; break; end % check for convergence
    end
    fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
    mu = theta(:,1:D);
    s2 = exp(theta(:,end)) + s2min;
end    