function gprn_npv_run_poisonous()
% Demonstration of GPRN with nonparametric variational inference
% Version that learn length-scales of covariance functions.
% Each latent function is associated with a different signal variance.
clear
%rng(1110, 'twister');
Ntrain = 259; Ntest = 100; D = 2;
%Ntrain = 10; Ntest = 5; D = 2;
num_latents = 2;
num_outputs = 3;
num_modes = 2;
sigma_f = 0.0001; sigma_y = 0.1;
theta_f = log(rand(D,1));
a = log(rand(num_latents,1));
theta_f = [theta_f; a(1)];
theta_w = log(rand(D+1,1));
[num_modes, params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
   theta_f, theta_w, a, sigma_f, sigma_y);
 
% jura data set
% PREPROCESSING and INIT
% std(log(Y))
% std(X)
% theta as log(rand) or rand(): log(rand) appears to be better
% sigma_f, sigma_y
% ways of optimising hyp
[X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
[Y, Ymean, Ystd] = standardize(log(Y),1,[],[]); % their paper
[X, Xmean, Xstd]=standardize(X,1,[],[]);
xtest = standardize(xtest,1,Xmean,Xstd);
ytest = standardize(log(ytest),1,Ymean,Ystd);
params.X = X(1:Ntrain,:);
params.Y = Y(1:Ntrain,:);
xtest = xtest(1:Ntest,:);
ytest = ytest(1:Ntest,:);

% parameters
D = Ntrain * num_latents *(num_outputs+1);
% initial parameters -- can do better by taking the empirical mean somehow
theta0 = randn(num_modes, D+1);         
theta0(:,D+1) = 1;
hyp0 = rand(numel(theta_f)-1+num_latents+numel(theta_w),1);
disp('initial hyp vs. true hyp');
disp([exp(hyp0(:)), exp([theta_f(1:end-1); a; theta_w])])
[F mu s2 hyp] = my_npv_run(theta0,hyp0,sigma_f,sigma_y,params);      % run NPV

disp('learned hyp vs. true hyp');
disp([exp(hyp(:)), exp([theta_f(1:end-1); a; theta_w])])
disp('s2')
disp(s2)
figure;
plot(F,'-o','LineWidth',2);
xlabel('Iteration');
ylabel('ELBO');

% prediction
mu(1:num_modes,1:10)
theta = [theta_f(1:end-1); a; theta_w];
gprn_prediction(xtest,ytest,mu,theta,params);

end

function [F mu s2 hyp] = my_npv_run(theta,hyp,sigma_f,sigma_y,params)
    nIter = 10; tol = 0.000001;
    s2min = 0.0000001;      % enforce minimium bandwidth to avoid numerical problems
    [N D] = size(theta); D = D - 1;
    opts = struct('Display','off','Method','lbfgs','MaxIter',200,'MaxFunEvals',200,'DerivativeCheck','off');  % optimization options

    % pre-compute Lf, Lw 
    X = params.X; Dx = size(X,2); Q = params.num_latents;
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+Q+1:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
    a = hyp(Dx+1:Dx+Q);
    nlogpdf = @(x) nlog_joint_multif(x,log(sigma_y),a,Lf,Lw,params);

    tbegin = tic;
    for iter = 1:nIter
        disp(['iteration: ',num2str(iter)]);
        
        % first-order approximation (L1): optimize mu, one component at a time
        tstart = tic;
        for n = 1:N
            func = @(x) nELBO1(x,@(x) nlogpdf(x),N,D,s2min,theta,n);
            %theta(n,1:end-1) = minimize(theta(n,1:D)',func,20);
            theta(n,1:end-1) = minFunc(func,theta(n,1:D)',opts);
        end
        disp(['optimising mu takes ', num2str(toc(tstart))])
        
        % second-order approximation (L2): optimize s2
        mu = theta(:,1:D); h = zeros(N,1);
        tstart = tic;
        for n = 1:N
          [dum1 dum2 diagH] = nlogpdf(mu(n,:)');
          h(n) = sum(diagH);
        end
        func = @(theta) nELBO2(theta,N,D,mu,s2min,h);
        theta = minFunc(func, theta(:,D+1),opts);
        %theta = minimize(theta(:,D+1),func,20);
        theta = [mu reshape(theta,N,1)];
        disp(['optimising sigma takes ', num2str(toc(tstart))])

        %M-step: optimise hyperparameters
        tstart = tic;
        func = @(x) nlog_joint_hyp_multif(x,theta(:,1:D),theta(:,D+1),log(sigma_f),log(sigma_y),params);
        hyp = minFunc(func, hyp, opts);
        disp(['optimising log hyp takes ', num2str(toc(tstart))])
        disp(['hyp = ', num2str(exp(hyp'))])

        % use updated hyp to get ELBO
        Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
        Kw = feval(params.covfunc_w, hyp(Dx+Q+1:end), X);
        Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
        a = hyp(Dx+1:Dx+Q);
        nlogpdf = @(x) nlog_joint_multif(x,log(sigma_y),a,Lf,Lw,params);
        for n = 1:N
          [dum1 dum2 diagH] = nlogpdf(mu(n,:)');
          h(n) = sum(diagH);
        end
        
        F(iter,1) = ELBO(theta,nlogpdf,h,s2min);              % calculate the approximate ELBO (L2)
        fprintf('ELBO %.5f\n', F(iter,1));
        if iter > 1 && abs(F(iter)-F(iter-1))<tol; break; end % check for convergence
    end
    fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
    mu = theta(:,1:D);
    s2 = exp(theta(:,end)) + s2min;
end    