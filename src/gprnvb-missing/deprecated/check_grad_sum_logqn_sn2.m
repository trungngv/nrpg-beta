function check_grad_sum_logqn_sn2()
%CHECK_GRAD_SUM_LOGQN_SN2 check_grad_sum_logqn_sn2()
%
% Check gradient for \sum logqn(), which is the second term in L_1[q], wrt
% sigma^2
%
% Trung V. Nguyen
% 09/08/12
%
[N, params] = test_data();
sn2 = rand(size(params.sn2));
[gradient, delta] = gradchek(sn2', @my_f, @my_grad, params);

end

function l = my_f(x, params)
l = sum_logqn_arg_sn2(x', params);
end

function g = my_grad(x, params)
g = grad_sum_logqn_sn2(x', params);
g = g';
end

