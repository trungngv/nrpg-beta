function grad = grad_logqn_mu(mu_n, n, params)
%GRADIENT_LOGQN grad = grad_logqn_mu(mu_n, n, params)
%   
% Computes dlogq_n / dmu_n evaluated at mu_n
%
% INPUT
%   - n : the component for taking gradient
%   - params
%
% OUTPUT
%   - dlog_qn/dmu_n
%
N = params.num_modes;
sn2 = params.sn2;
Mu = params.Mu;
[q, q_nj] = qn_arg_mu(mu_n, n, params);
grad = zeros(size(Mu,1),1); % same size as mu
for j=1:N
  grad = grad + q_nj(j)*(Mu(:,j)-mu_n)/(sn2(n)+sn2(j));
end
grad = grad/(N*q);
end

