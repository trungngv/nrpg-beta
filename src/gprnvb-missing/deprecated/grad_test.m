function grad = grad_test(mu_n, j, n, params)
%GRADIENT_LOGQN grad = grad_test(mu_n, j, n, params)
%   
% Computes dlogq_j / dmu_n
%
% INPUT
%   - n : the component for taking gradient
%   - params
%
% OUTPUT
%   - dlog_qn/dmu_n
%
N = params.num_modes;
sigma = params.sigma;
Mu = params.Mu;
[~, q_nj] = qn_arg_mu(mu_n, n, params);
%params.Mu(:,n) = mu_n;
qj = qn(j, params);
grad = q_nj(j)*(Mu(:,j)-mu_n)/(sigma(n)^2+sigma(j)^2)/N/qj;
end

