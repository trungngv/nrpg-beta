function check_grad_logqn_sn2()
%CHEK_GRAD_LOGQN_SN2 chek_grad_logqn_sn2()
%  
% Gradient check for dlog(q_n)/dsigma_n^2
%
[N, params] = test_data();
sn2 = rand(size(params.sn2));
for n=1:N
  [gradient, delta] = gradchek(sn2', @my_f, @my_grad, n, params);
end  

end

function l = my_f(x, n, params)
  % x is scalar in this case
  l = log(qn_arg_sn2(x', n, params));
end

function g = my_grad(x, n, params)
  g = grad_logqn_sn2(x', n, params);
  g = g';
end

