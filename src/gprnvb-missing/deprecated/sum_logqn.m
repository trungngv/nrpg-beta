function [s q] = sum_logqn(params)
%SUM_LOGQN [s q] = sum_logqn(params)
% 
% Returns \sum log(q_n), which is the second term in L_1[q]
%
N = params.num_modes;
q = zeros(N,1);
for n=1:N
  [q(n) ~] = qn(n, params);
end
s = sum(log(q));
end

