function [q q_nj] = qn_arg_mu(mu_n, n, params)
%QN [q q_nj] = qn_arg_mu(mu_n,n,params)
% 
% Compute q_n = (1/N)\sum N(mu_n; mu_j, (sigma_n^2 + sigma_j^2)I).
%
% This is a wrapper that treats mu_n as variable of the function, which is
% helpful for optimization.
%
% Trung V. Nguyen
% 08/08/12

tmp_params = params;
tmp_params.Mu(:,n) = mu_n;
[q q_nj] = qn(n, tmp_params);

end

