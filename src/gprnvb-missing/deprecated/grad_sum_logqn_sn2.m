function grad = grad_sum_logqn_sn2(sn2, params)
%GRADIENT_SUM_LOGQN_SN2 grad = grad_sum_logqn_sn2(sn2, params)
%   
% Returns the gradient of \sum log(q_n), which is the second term in
% L_1[q], evaluated at sigma
%
% NOTE: this computes \sum log (q_n) only; it must be divided by N for the
% real objective and gradient.
%
% Trung V. Nguyen
% 09/08/12
N = params.num_modes;
grad = zeros(size(sn2));
for n=1:N
  grad = grad + grad_logqn_sn2(sn2, n, params);
end
end

