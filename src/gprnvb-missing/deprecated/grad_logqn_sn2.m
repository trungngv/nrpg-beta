function grad = grad_logqn_sn2(sn2, n, params)
%GRADIENT_LOGQN grad = grad_logqn_sigma(sigma, n, params)
%   
% Computes dlogq_n / dsigma_n evaluated at sigma
%
% INPUT
%   - sigma
%   - n : the component for taking gradient
%   - params
%
% OUTPUT
%   - dlog_qn/dmu_n
%
N = params.num_modes;
Mu = params.Mu;
[q, q_nj] = qn_arg_sn2(sn2, n, params);
d = size(Mu,1);
grad = zeros(N,1); % same size as mu
for nn=1:N
  if (nn == n) % dlog(q_n)/dsigma_n
    for j=1:N
      s2 = sn2(n) + sn2(j);
      dif = Mu(:,n)-Mu(:,j);
      grad(nn) = grad(nn) + 0.5*q_nj(j)*((norm(dif)/s2)^2 - d/s2);
      % add one more time for the case when j==n
      if (j == n)
        grad(nn) = grad(nn) + 0.5*q_nj(j)*((norm(dif)/s2)^2 - d/s2);
      end
    end
  else % dlog(q_n)/dsigma_nn2 (nn != n)
    s2 = sn2(n) + sn2(nn);
    dif = Mu(:,n)-Mu(:,nn);
    grad(nn) = 0.5*q_nj(nn)*((norm(dif)/s2)^2 - d/s2);
  end
end
grad = grad/(N*q);
end

