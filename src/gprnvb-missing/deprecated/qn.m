function [q q_nj] = qn(n, params)
%QN [q q_nj] = qn(n,params)
%
% Compute q_n = (1/N)\sum N(mu_n; mu_j, (sigma_n^2 + sigma_j^2)I)
%
% Trung V. Nguyen
% 08/08/12

num_modes = params.num_modes; % N = number of components
% Mu = [mu_1 mu_2 ... mu_N]
Mu = params.Mu; % dxN
sn2 = params.sn2;
q_nj = zeros(num_modes,1);
for j=1:num_modes
  q_nj(j) = mvnpdf_iso(Mu(:,n), Mu(:,j), sqrt(sn2(n) + sn2(j)));
end
q = mean(q_nj);
end

