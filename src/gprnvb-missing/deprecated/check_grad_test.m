function check_grad_test()
%CHEK_GRAD_LOGQN chek_grad_test()
%  
% Gradient check for dlog(q_j)/dmu_n
%
%rng(11, 'twister');
[num_modes, params] = test_data();
j=1;
for n=1:num_modes
  mu_n = params.Mu(:,n); % mu_n is dx1
  if n ~= j
    [gradient, delta] = gradchek(mu_n', @my_f, @my_grad, n, j, params);
  end  
end  
end

function l = my_f(x, n, j, params)
  params.Mu(:,n) = x';
  l = log(qn(j, params)); % this was the key
end

function g = my_grad(x, n, j, params)
  g = grad_test(x', j, n, params);
  g = g';
end