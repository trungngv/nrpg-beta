function [s q] = sum_logqn_arg_mu(mu_n, n, params)
%SUM_LOGQN val = sum_logqn_arg_mu(mu_n, n, params)
% 
% Returns \sum log(q_n), which is the second term in L_1[q].
%
% This is a wrapper that treats mu_n as variable of the function, which is
% helpful for optimization.
%
tmp_params = params;
tmp_params.Mu(:,n) = mu_n;
[s q] = sum_logqn(tmp_params);
end

