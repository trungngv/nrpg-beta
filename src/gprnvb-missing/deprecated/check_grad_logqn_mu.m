function check_grad_logqn_mu()
%CHEK_GRAD_LOGQN chek_grad_logqn_mu()
%  
% Gradient check for dlog(q_n)/dmu_n
%
[N, params] = test_data();

for n=1:N
  mu_n = params.Mu(:,n); % mu_n is dx1
  [gradient, delta] = gradchek(mu_n', @my_f, @my_grad, n, params);
end  
end

function l = my_f(x, n, params)
  l = log(qn_arg_mu(x', n, params));
end

function g = my_grad(x, n, params)
  g = grad_logqn_mu(x', n, params);
  g = g';
end

