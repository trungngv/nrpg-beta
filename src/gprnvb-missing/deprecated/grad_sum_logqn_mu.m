function grad = grad_sum_logqn_mu(mu_n, n, params)
%GRAD_SUM_LOGQN_MU grad = grad_sum_logqn_mu(mu_n, n, params)
%   
% Returns the gradient of \sum log(q_n), which is the second term in
% L_1[q], evaluated at mu_n
%
% NOTE: this computes sum log (q_n) only; it must be divided by N for the
% real objective and gradient.
%
% Trung V. Nguyen
% 08/08/12
N = params.num_modes;
sn2 = params.sn2;
Mu = params.Mu;
[q, q_nj] = qn_arg_mu(mu_n, n, params);
grad1 = zeros(size(Mu,1),1); % same size as mu
grad2 = zeros(size(Mu,1),1);
for j=1:N
  grad1 = grad1 + q_nj(j)*(Mu(:,j)-mu_n)/(sn2(n)+sn2(j));
  if (j ~= n)
    qj = qn(j, params);
    grad2 = grad2 + q_nj(j)/qj*(Mu(:,j)-mu_n)/(sn2(n)+sn2(j));
    %grad2 = grad2 + grad_test(mu_n, j, n, params);
  end  
end
grad = grad1/(N*q) + grad2/N;
%grad = grad1/(N*q) + grad2;
end

