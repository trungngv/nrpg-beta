function [f dmu ds2] = lower_entropy_MoG(n, params)
%ENTROPY_MOG [f dmu ds2] = lower_entropy_MoG(n, params)
%   
% The entropy for mixture of gaussian term in L[q].
%
% TODO: represent the variables explicitly rather than boxing in params!
% TODO: cf lower_bound_MoG
%
% 09/08/12
%
% theta = reshape(theta, num_modes, D+1);
% Mu = theta(:,D)';
% sn2 = theta(:,end);
num_modes = params.num_modes;
Mu = params.Mu; sn2 = params.sn2;
f = sum_logqn(params)/num_modes;
if nargout > 1 % derivates for mu_n
  dmu = grad_sum_logqn_mu(Mu(:,n), n, params)/num_modes;
end
if nargout == 3 % derivates for sigma
  ds2 = grad_sum_logqn_sn2(sn2, params)/num_modes;
end
end

