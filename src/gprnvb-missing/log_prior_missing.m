function log_p = log_prior_missing(f, w, Lf, Lw, params)
%LOG_PRIOR log_p = log_prior_missing(f, w, Lf, Lw, params)
% 
% Computes the log prior probability ln p(u | theta) for an input point.
%
% INPUT
%   - f : the latent functions associated with the input
%   - w : the weight functions associated with the input
%   - Lf : the Cholesky decomposition (chol(Kf)') of the covariance matrix of latent functions
%   - Lw : the Cholesky decomposition (chol(Kw)') of the covariance matrix of weight functions
%   - params
%
% OUTPUT
%   - log_p : p(f, w | theta)
%
% Author : Trung V. Nguyen (trung.ngvan@gmail.com)
% 18/08/2012
%
N = size(params.X,1); P = params.num_outputs; Q = params.num_latents;
F = reshape(f,N,Q); W = mat2cell(reshape(w,N*P,Q), N*ones(1,P), ones(1,Q));
log_p = -0.5*N*Q*log(2*pi) -0.5*Q*(N*P-sum(sum(params.Y_isnan)))*log(2*pi) -Q*sum(log(diag(Lf)));
Kw = Lw*Lw'; % TODO pass Kw
for j=1:Q
  %log_p = log_p - 0.5*F(:,j)'*Kf_inv*F(:,j);
  alpha = Lf\F(:,j);
  log_p = log_p - 0.5*(alpha')*alpha;
  for i=1:P
    %log_p = log_p - 0.5*W{i,j}'*Kw_inv*W{i,j};
    if sum(params.Wcell_isnan{i,j}) > 0 % missing data
      valid_ind = ~params.Wcell_isnan{i,j};
      Lwij = jit_chol(Kw(valid_ind, valid_ind))';
      alpha = Lwij\W{i,j}(valid_ind);
      log_p = log_p - 0.5*(alpha')*alpha - sum(log(diag(Lwij)));
    else
      alpha = Lw\W{i,j};
      log_p = log_p - 0.5*(alpha')*alpha - sum(log(diag(Lw)));
    end  
  end
end

end

