function [mae Ymean hyp F] = gprn_npv_run_naive(dataset, num_modes, test_prediction, ...
  standardise_x, maxIters, use_numerical_hyp, func_hypinit,pseudo_w,enforce_bound_improve)
% Demonstration of GPRN with nonparametric variational inference
% Version that learn length-scales of covariance functions.
% All latent functions share the same hyperparameters.

%% init data set
switch (dataset)
  case 'synthetic'
    Ntrain = 50; Ntest = 50;
    num_latents = 2; num_outputs = 3; D = 2;
    sigma_f = 0.000; sigma_y = 0.1;
    theta_f = log(rand(D+1,1));
    theta_w = log(rand(D+1,1));
    theta_f(end) = 0.5+theta_f(end);
    theta_w(end) = 0.5+theta_w(end);
    a = ones(num_latents,2)*theta_f(end);
    [params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
        theta_f, theta_w, a, sigma_f, sigma_y,true);
    [params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
    save('temp.mat', 'Ymean', 'Ystd');
    % prediction on new/test data or missing data
    if test_prediction
      ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
      params.Y(ind_n,1) = nan;
    else
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end
  case 'jura' % jura with standard gp -- 1 output
    Ntrain = 359; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 3; % jura
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    % pre-process x
    X = [X; xtest];
    if (standardise_x)
      X = standardize(X,1,[],[]);
      xtest = X(Ntrain-Ntest+1:end,:);
    end
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if (test_prediction)
      Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
      Ymean = zeros(1,3); Ystd = zeros(1,3);
      [Y(1:Ntrain-Ntest,1), Ymean(1), Ystd(1)] = standardize(log(Y(1:Ntrain-Ntest,1)),1,[],[]);
      [Y(:,2:3), Ymean(2:3), Ystd(2:3)] = standardize(log(Y(:,2:3)),1,[],[]);
    else
      Y = [Y; ytest];
      [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
    end
    params.X = X(1:Ntrain,:);
    params.Y = Y(1:Ntrain,:);
    save('temp.mat', 'Ymean', 'Ystd'); % need this for prediction
end

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
hyp0 = feval(func_hypinit, D);

D = Ntrain*num_latents*(num_outputs+1);
theta0 = randn(num_modes, D+1);
theta0(:,D+1) = 1;

sigma_f = 0;
[F mu s2 hyp] = my_npv_run(theta0,hyp0,sigma_f,params,maxIters,use_numerical_hyp,enforce_bound_improve);

disp('learned hyp vs. init hyp');
disp([exp(hyp(:)), exp(hyp0(:))])
disp('s2')
disp(s2)
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

%% prediction
mu(1:num_modes,1:10)
theta = hyp(1:end-1);
mae = gprn_prediction(xtest,ytest,mu,theta,params,pseudo_w);

end

function [F mu s2 hyp] = my_npv_run(theta,hyp,sigma_f,params,nIter,use_numerical_hyp,enforce_bound_improve)
    tol = 0.00001; s2min = 0.0000001; F = [];
    [K D] = size(theta); D = D - 1;
    opts = struct('Display','off','Method','lbfgs','MaxIter',200,'MaxFunEvals',200,'DerivativeCheck','on'); 

    % pre-compute Lf, Lw 
    X = params.X; Dx = size(X,2); Q = params.num_latents;
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+2:end-1), X);
    lsy = hyp(end);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
    nlogpdf = @(x) nlog_joint(x,lsy,Lf,Lw,params);

    tbegin = tic;
    for iter = 1:nIter
        disp(['iteration: ',num2str(iter)]);
        old_theta = theta;
        tstart = tic;
        for k = 1:K
            func = @(x) nELBO1(x,@(x) nlogpdf(x),K,D,s2min,theta,k,params);
            %theta(n,1:end-1) = minimize(theta(n,1:D)',func,20);
            theta(k,1:end-1) = minFunc(func,theta(k,1:D)',opts);
        end
        disp(['optimising mu takes ', num2str(toc(tstart))])
        
        % second-order approximation (L2): optimize s2
        mu = theta(:,1:D); h = zeros(K,1);
        tstart = tic;
        for k = 1:K
          [dum1 dum2 diagH] = nlogpdf(mu(k,:)');
          h(k) = sum(diagH);
        end
        func = @(x) nELBO2(x,K,D,mu,s2min,h,params);
        theta = minFunc(func, theta(:,D+1),opts);
        %theta = minimize(theta(:,D+1),func,20);
        theta = [mu reshape(theta,K,1)];
        disp(['optimising sigma takes ', num2str(toc(tstart))])
        
        tstart = tic;
        if use_numerical_hyp
          func = @(x) nlog_joint_hyp_numeric(x,theta(:,1:D),theta(:,D+1),log(sigma_f),params);
        else
          func = @(x) nlog_joint_hyp(x,theta(:,1:D),theta(:,D+1),log(sigma_f),params);
        end
        old_hyp = hyp;
        hyp = minFunc(func, hyp, opts);
        disp(['optimising log hyp takes ', num2str(toc(tstart))])

        % use updated hyp to get ELBO
        Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
        Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
        Lf = jit_chol(Kf)';
        Lw = jit_chol(Kw)';
        nlogpdf = @(x) nlog_joint(x,hyp(end),Lf,Lw,params);
        for k = 1:K
          [dum1 dum2 diagH] = nlogpdf(mu(k,:)');
          h(k) = sum(diagH);
        end
        
        elbo_after_hyp = ELBO(theta,nlogpdf,h,s2min,params);
        if iter > 1 && elbo_after_hyp < F(end) && enforce_bound_improve
          hyp = old_hyp; theta = old_theta;
          break;
        else
          F = [F elbo_after_hyp];
        end
%         if iter > 1 && elbo_after_hyp < F(end) % reject hyp that makes ELBO decrease
%           hyp = old_hyp; Lf = old_Lf; Lw = old_Lw;
%           nlogpdf = @(x) nlog_joint(x,hyp(end),Lf,Lw,params);
%         else
%           F = [F elbo_after_hyp];
%         end
        disp(['hyp = ', num2str(exp(hyp'))])
        fprintf('ELBO %.5f\n', F(end));
        if iter > 1 && abs(F(end)-F(end-1))<tol; break; end
    end
    fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
    mu = theta(:,1:D);
    s2 = exp(theta(:,end)) + s2min;
end

