function [nf ndhyp] = nlog_joint_cov(hyp,Mu,sn2,params,optimise_sf,optimise_sw)
%NLOG_JOINT_HYP [nf ndhyp] = nlog_joint_cov(hyp,Mu,sn2,params,optimise_sf,optimise_sw)
%   
% Returns the negative log joint probability, excluding the constant terms
% wrt hyperparameters. Hence this can be used as the objective function
% for optimisation wrt the log hyperparameters.
% 
% NOTE that this is the exact lower bound as a function of
% hyper-parameters.
%
% INPUT
%   - hyp : log hyperparameters, should be log[theta_f, theta_w]
%   - Mu : KxD matrix
%   - sn2
%   - params
%   - optimise_sf,optimise_sw: true to optimise signal variance of latent function
%       and weight function respectively
%
% OUTPUT
%
% Trung V. Nguyen
% 06/11/12
P = params.num_outputs; Q = params.num_latents; K = params.num_modes;
X = params.X; D = size(X,2);
Nd = size(X,1);
sn2 = exp(sn2) + 1e-10;
if ~optimise_sf,      hyp(D+1) = log(1); end
if ~optimise_sw,      hyp(end) = log(1); end  
ltheta_f = hyp(1:D+1);
ltheta_w = hyp(D+2:end);

Kf = feval(params.covfunc_f, ltheta_f, X);
Kw = feval(params.covfunc_w, ltheta_w, X);
[Lf ~] = jit_chol(Kf); [Lw_full ~]= jit_chol(Kw);
Kfinv = Lf\(Lf'\eye(Nd));   Kwinv_full = Lw_full\(Lw_full'\eye(Nd));

nf = 0;
dtheta_f = zeros(size(ltheta_f)); dtheta_w = zeros(size(ltheta_w));
for k=1:K
  [f, w] = u_to_fhat_w(Mu(k,:),params);
  nf = nf - log_prior_missing(f,w,Lf',Lw_full',params);
  if nargout == 2
    Fhat = reshape(f,Nd,Q); Wmat=reshape(w,Nd*P,Q);
    W = mat2cell(Wmat,Nd*ones(1,P),ones(1,Q));
    for j=1:Q
      dtheta_f = dtheta_f + grad_nlog_prior(ltheta_f,params.covfunc_f,X,Fhat(:,j));
      for i=1:P
        dtheta_w = dtheta_w + grad_nlog_prior(ltheta_w,params.covfunc_w,...
          X(~params.Y_isnan(:,i),:),W{i,j}(~params.Y_isnan(:,i)));
      end
    end
  end
end
nf = nf + 0.5*Q*sum(sn2)*trace(Kfinv); % + 0.5 \sum_k sn2(k) \sum_j trace(K_f^{-1|)
dtheta2_f = zeros(size(ltheta_f));
if nargout == 2
  for idx=1:D
    dK = feval(params.covfunc_f, ltheta_f, X, [], idx);
    dtheta2_f(idx) = Q*trace(Kfinv*dK*Kfinv);
  end
  if optimise_sf
    dtheta2_f(D+1) = 2*Q*trace(Kfinv);
  end
end

dtheta2_w = zeros(size(ltheta_w));
for i=1:P
  valid_ind = ~params.Y_isnan(:,i);
  if sum(valid_ind) ~= Nd
    [Lw ~] = jit_chol(Kw(valid_ind,valid_ind));
    Kwinv = Lw\(Lw'\eye(size(Lw,1)));
  else
    Kwinv = Kwinv_full;
  end
  if nargout == 2
    for idx=1:D
      dK = feval(params.covfunc_w, ltheta_w, X(valid_ind,:), [], idx);
      dtheta2_w(idx) = dtheta2_w(idx) + Q*trace(Kwinv*dK*Kwinv);
    end
    if optimise_sw
      dtheta2_w(D+1) = dtheta2_w(D+1) + 2*Q*trace(Kwinv);
    end
  end
  nf = nf + 0.5*Q*sum(sn2)*trace(Kwinv); % 0.5 \sum_k sn2(k) \sum_i \sum_j trace(K_wij^{-1})
end

nf = nf/K;
if nargout == 2
  ndhyp = [dtheta_f; dtheta_w]/K - 0.5*sum(sn2)*[dtheta2_f; dtheta2_w]/K;
  if ~optimise_sf,   ndhyp(D+1)=0;    end
  if ~optimise_sw,   ndhyp(end) = 0;   end
end
end

