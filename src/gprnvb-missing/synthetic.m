rng(1110, 'twister');
Ntrain = 100; Ntest = 100;
num_latents = 2; num_outputs = 3; D = 2;
sigma_f = 0.000; sigma_y = 0.05;
theta_f = [0.3; 0.1; 1.];
theta_w = [2; 2; 1];
a = ones(num_latents,2)*theta_f(end);
theta_ff = repmat(theta_f', num_latents, 1);
for j=1:num_latents
  theta_ff(j,D+1) = a(j);
end

X = 5.*randn(Ntrain + Ntest,D);
[~,~,Y,~,~] = gprn_utility_funcs(theta_ff,theta_w,sigma_f,sigma_y,X,num_outputs,true);
xtest = X(Ntrain+1:Ntrain+Ntest, :);
ytest = Y(Ntrain+1:Ntrain+Ntest, :);
X = X(1:Ntrain,:);
Y = Y(1:Ntrain,:);
save_data(X,Y,xtest,ytest,'data/synthetic','synthetic');
hyp = [theta_f; theta_w; log(sigma_y)];
save('data/synthetic/hyp.mat', 'hyp');

disp('corr y')
corr(Y)
disp('corr ytest')
corr(ytest)
