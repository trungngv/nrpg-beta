%% Plotting of features vs. outputs for concrete data set
clear;
[x, y, xtest, ytest] = load_data('data/SARCOS', 'sarcos');
Ntrain = 100; Ntest = 100;
x = x(1:Ntrain,:); y = y(1:Ntrain,:);
xtest = xtest(1:Ntest,:); ytest = ytest(1:Ntest,:);

% for i=1:5
%   for j=1:3
%     subplot(5,3,(i-1)*3+j);
%     plot(x(:,i),y(:,j),'o');
%     title(['feature ' num2str(i) ' vs. output', num2str(j)]);
%   end
% end

%covfunc = {@covSEard}; D = size(x,2);
covfunc = {@covSEard}; D = 21;
likfunc = @likGauss; infFunc = @infExact;

%GP for each output
iters = 1;
smse = zeros(iters,size(y,2));
for iter=1:iters
for i=3:3
  Xtrain = x; Ytrain = y(:,i);
  Xtest = xtest; Ytest = ytest(:,i);
  [Xtrain,Xmean,Xstd] = standardize(Xtrain,1,[],[]);
  [Ytrain, Ymean, Ystd] = standardize(Ytrain,1,[],[]);
  Xtest = standardize(Xtest,1,Xmean,Xstd);
  
  % init hyper
  hyp.cov(1:D+1) = log(rand(D+1,1));
  hyp.lik = log(rand);
  hyp0 = [hyp.cov(:); hyp.lik(:)];
  
  % learn and predict
  hyp = minimize(hyp, @gp, 5000, infFunc, [], covfunc, likfunc, Xtrain, Ytrain);
  hyp_learned = [hyp.cov(:); hyp.lik(:)];
  disp('learned log hyp vs. hyp0')
  disp([hyp_learned, hyp0])
  
  % error on training data
  [~, s2, ystar] = gp(hyp, infFunc, [], covfunc, likfunc, Xtrain, Ytrain, Xtrain);
  mae = mean(abs(ystar - Ytrain));
  fprintf('train mae = %.5f\n', mae);
  serror = mean((ystar-Ytrain).^2)/(var(Ytrain) + mean(Ytrain)^2);
  fprintf('train smse = %.5f\n', serror);
  
  % error on testing data
  [~, s2, ystar] = gp(hyp, infFunc, [], covfunc, likfunc, Xtrain, Ytrain, Xtest);
  ystar = ystar .* Ystd + Ymean;
  disp([ystar, Ytest])
  scatter(ystar, Ytest);
  mae = mean(abs(ystar - Ytest));
  fprintf('test mae = %.5f\n', mae);
  serror = mean((ystar-Ytest).^2)/(var(Ytest) + mean(Ytest)^2);
  fprintf('test smse = %.5f\n', serror);
  smse(iter,i) = serror;
  disp(smse)
  
end
end
