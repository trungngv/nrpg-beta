function [nf ndhyp] = nlog_joint_hyp_numeric(hyp,Mu,sn2,lsf,params)
%NLOG_JOINT_HYP_NUMERIC [nf ndhyp] = nlog_joint_hyp_numeric(hyp, Mu, sn2, lsf, params)
%   
% Returns the negative log joint probability, excluding the constant terms
% wrt hyperparameters. Hence this can be used as the objective function
% for optimisation wrt the log hyperparameters.
%
% This implementation uses numerical differention to for gradients of the
% hyper-parameters.
% 
% INPUT
%   - hyp : log hyperparameters, should be log[theta_f, theta_w, sigma_y]
%   - Mu : KxD matrix
%
% OUTPUT
%
% Trung V. Nguyen
% 24/09/12
nf = mynlog_joint(hyp,Mu,sn2,params);
ndhyp = numeric_grad3(@mynlog_joint,hyp, Mu,sn2,params);
end

function nf = mynlog_joint(hyp,Mu,sn2,params)
K = params.num_modes;
X = params.X;
sn2 = exp(sn2) + 1e-10;
ltheta_f = hyp(1:size(X,2)+1);
ltheta_w = hyp(numel(ltheta_f)+1:end-1);
lsy = hyp(end);
Kf = feval(params.covfunc_f, ltheta_f, X);
Kw = feval(params.covfunc_w, ltheta_w, X);
[Lf ~] = jit_chol(Kf); [Lw ~] = jit_chol(Kw);
nf = 0;
ntraceH = zeros(K,1);
for k=1:K
  [nfk dum ndiagH] = nlog_joint(Mu(k,:),lsy,Lf',Lw',params);
  ntraceH(k) = sum(ndiagH);
  nf = nf + nfk + 0.5*sn2(k)*ntraceH(k);
end
nf = nf/K;
end
