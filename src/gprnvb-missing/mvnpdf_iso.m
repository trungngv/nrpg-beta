function p = mvnpdf_iso(X, mu, sigma)
%MVNPDF_ISO p = mvnpdf_iso(X, mu, sigma)
% 
% Computes the probability density of the multivariate normal distribution
% with mean mu and isotrophic covariance sigma^2 I_n, evaluated at each row
% of X.
%
% INPUT
%   - X : Nxd matrix
%   - mu : 1xd or dx1 matrix
%   - sigma: scalar
% OUTPUT
%   - p : N x 1 matrix each row being probability density of evaluated at
%   the corresponding row of X
%
% Trung V. Nguyen
% 08/08/12
d = length(mu); mu = reshape(mu,1,d);
X = reshape(X,numel(X)/d,d);
[N,d] = size(X);
p = zeros(N,1);
z = (2*pi*sigma^2)^(-d/2);
for i=1:N
  p(i)= z * exp(-0.5*(X(i,:)-mu)*(X(i,:)-mu)'/sigma^2);
end
end

