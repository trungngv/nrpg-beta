%% plot for jura
%npv1
figure;
mae = load('output/npvgprn/jura/npv_run_exact_hyp_init_selection_modes1_1352805236638291.txt.mat','mae_pseudo');
mae = mae.mae_pseudo(:,1);
npv1_mean = mean(mae);
npv1_std = 2*std(mae)/sqrt(numel(mae));
%npv2
mae = load('output/npvgprn/jura/npv_run_exact_hyp_init_standard_modes2_1352851230506754.txt.mat','mae_pseudo');
mae = mae.mae_pseudo(:,1);
npv2_mean = mean(mae);
npv2_std = 2*std(mae)/sqrt(numel(mae));
%npv3
mae = load('output/npvgprn/jura/npv_run_exact_hyp_init_standard_modes3_1352850207714378.txt.mat','mae_pseudo');
mae = mae.mae_pseudo(:,1);
npv3_mean = mean(mae);
npv3_std = 2*std(mae)/sqrt(numel(mae));
%mf
mae = load('output/npvgprn/jura/gprn_mf_run_hyp_init_smallf_1352774140556451.txt.mat','mae_pseudo');
mae = mae.mae_pseudo(:,1);
mf_mean = mean(mae);
mf_std = 2*std(mae)/sqrt(numel(mae));
%igp
mae = load('output/npvgprn/jura/logigp.mat', 'mae');
mae = mae.mae;
igp_mean = mean(mae);
igp_std = 2*std(mae)/sqrt(numel(mae));

% plot
barvalues = [igp_mean(:),mf_mean(:),npv1_mean(:),npv2_mean(:),npv3_mean(:)];
errors = [igp_std(:),mf_std(:),npv1_std(:),npv2_std(:),npv3_std(:)];
[hb,~] = mybarweb(barvalues,errors,[],[],[],[]);
legend(hb, 'IGP','MF','NPV1','NPV2','NPV3', 'Location','NorthEast','Orientation','Vertical');
set(gca, 'Box', 'Off', 'FontSize', 17);
legend boxoff
ylim([0.2,0.58]);

%% plot for concrete
figure;
[~,~,~,ytest] = load_data('data/concreteslump', 'concrete3');
old_normalization = (var(ytest) + mean(ytest).^2);
old_normalization = repmat(old_normalization,10,1);
normalization = repmat(var(ytest,1,1),10,1);

%npv1
smse = load('output/npvgprn/concrete3/gprn_npv_run_regulariser_modes1_1352674223825747.txt.mat','smse');
npv1 = smse.smse;
npv1 = (npv1 .* old_normalization) ./ normalization;
npv1_mean = mean(npv1);
npv1_std = 2*std(npv1)/sqrt(10);
%npv2
smse = load('output/npvgprn/concrete3/gprn_npv_run_regulariser_modes2_1352686072688063.txt.mat','smse');
npv2 = smse.smse;
npv2 = (npv2 .* old_normalization) ./ normalization;
npv2_mean = mean(npv2);
npv2_std = 2*std(npv2)/sqrt(10);
%npv3
smse = load('output/npvgprn/concrete3/gprn_npv_run_regulariser_modes3_1352675966153635.txt.mat','smse');
npv3 = smse.smse;
npv3 = (npv3 .* old_normalization) ./ normalization;
npv3_mean = mean(npv3);
npv3_std = 2*std(npv3)/sqrt(10);
%meanfield
smse = load('output/npvgprn/concrete3/gprn_mf_run_hyp_init_standard_1352675353247383.txt.mat','smse');
mf = smse.smse;
mf = (mf .* old_normalization) ./ normalization;
mf_mean = mean(mf);
mf_std = 2*std(mf)/sqrt(10);
%igp
smse = load('output/npvgprn/concrete3/igp.mat','smse_all');
igp = smse.smse_all;
igp = (igp .* old_normalization) ./ normalization;
igp_mean = mean(igp);
igp_std = 2*std(igp)/sqrt(10);

% plot
barvalues = [igp_mean(:),mf_mean(:),npv1_mean(:),npv2_mean(:),npv3_mean(:)];
errors = [igp_std(:),mf_std(:),npv1_std(:),npv2_std(:),npv3_std(:)];
groupnames = {'Slump', 'Flow', 'Compressive Strength'};
[hb,~] = mybarweb(barvalues,errors,[],groupnames,[],[]);
legend(hb, 'IGP','MF','NPV1','NPV2','NPV3','Location','Best','Orientation','Vertical');
set(gca, 'Box', 'Off', 'FontSize', 17);

% average across all outputs
npv1_all = npv1(:);
npv2_all = npv2(:);
npv3_all = npv3(:);
mf_all = mf(:);
disp('average smse')
fprintf('npv1_all: %5f\n', mean(npv1_all));
fprintf('npv2_all: %5f\n', mean(npv2_all));
fprintf('npv3_all: %5f\n', mean(npv3_all));
fprintf('mf_all: %5f\n', mean(mf_all));

%%evidence lower bound for modes = 1,2,3
elbos3 = [-1811.6709,-2487.4542,-1694.2040,-1114.9253,-2599.2553,-2345.9764,-2560.8586,-2301.3584,-2274.2218,-2542.2599];
elbos2 = [-2771.6685,-2639.7909,-2054.2088,-2429.9009,-2359.8267,-2672.7290,-2610.9024,-2201.3718,-2087.4363,-2131.6009];
elbos1 = [-1837.7667,-2480.9949,-2700.9538,-2179.7133,-2812.2395,-2179.6207,-2001.0304,-2300.9746,-2369.0834,-2174.9983];

