function hyp = hyp_init_smallf_new(D)
%HYP_INIT_SMALLF hyp = hyp_init_smallf(D)
%
thetaf = log([rand(D,1); rand]);
thetaw = log(1.5*rand(D+1,1));
hyp = [thetaf; thetaw; log(1)];
end

