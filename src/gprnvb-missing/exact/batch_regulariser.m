clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'concrete3';
num_outputs = 3;
maxIters = 100;
predict_test = true;
standardise_x = true;
log_scale = false;
optimise_sf = false;
optimise_sw = true;
lambda = 0.0;
executor = @gprn_npv_run_regulariser;
func_hypinit = 'hyp_init_standard';
modes = [1];
for num_modes=modes
  % record settings
  filename = ['output/npvgprn/' dataset '/' func2str(executor) '_modes'...
    num2str(num_modes) '_' num2str(tic) '.txt'];
  fid = fopen(filename, 'w');
  runtime = now;
  log_to_file(fid, executor, runtime, rngno, dataset, num_outputs, num_modes, ...
      maxIters, func_hypinit, standardise_x, log_scale, predict_test, optimise_sf, optimise_sw, lambda);
  cnt = 1; iters = 1;
  mae = zeros(iters,num_outputs);
  mae_pseudo = mae; smse = mae; smse_pseudo = mae;
  ymean = zeros(iters,num_outputs);
  while cnt <= iters
%     try
      [mae(cnt,:) smse(cnt,:) hyp lowerbound hyp0] = executor(dataset,num_modes,...
          predict_test, standardise_x, log_scale, maxIters, func_hypinit,optimise_sf,optimise_sw,lambda,true);
      lowerbound = lowerbound(lowerbound ~= 0);
      fprintf(fid, '\n====================\nRun %d:\n',cnt);
      fprintf(fid, 'Init hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp0));
      fprintf(fid, '\nOptimised hyp: ');
      fprintf(fid, '%.4f\t', exp(hyp));
      fprintf(fid, '\n\tELBO = ');
      fprintf(fid, '\n\t\t%.4f', lowerbound);
      fprintf(fid, '\n\tmae \t\t\t\t =');
      fprintf(fid, '%.5f\t', mae(cnt,:));
      fprintf(fid, '\n\tsmse \t\t\t\t =');
      fprintf(fid, '%.5f\t', smse(cnt,:));
      fprintf(fid, '\n');
      assert(lowerbound(end) < -500);
      cnt = cnt + 1;
      disp(mae)
%    catch err
%       disp(err)
%     end
  end  
  
  % save prediction error to file
  save([filename '.mat'], 'mae', 'smse');
  % for the first output
  mae = mae(:,1); ymean=ymean(:,1); mae_pseudo = mae_pseudo(:,1);
  smse = smse(:,1); smse_pseudo = smse_pseudo(:,1);
  
  fprintf(fid, '\nMAE:\n');
  fprintf(fid, '%.5f\n', mae);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));
  
  fprintf(fid, '\nSMSE:\n');
  fprintf(fid, '%.5f\n', smse);
  fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(smse), std(smse));

  fclose(fid);
  disp(['output written to ' filename])
  copyfile(filename, '/home/trung/Dropbox/experimentslog/');

end

