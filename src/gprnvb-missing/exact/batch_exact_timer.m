clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'jura';
num_outputs = 3;
maxIters = 100;
predict_test = true;
standardise_x = true;
optimise_sf = false;
optimise_sw = true;
log_transform_y = true;
executor = @npv_run_exact_timer;
hypinits = {'hyp_init_standard'};
modes =[2,3];
for hypinit=hypinits
  func_hypinit = hypinit{1};
for num_modes=modes
  cnt = 1; iters = 5;
  while cnt <= iters
    try
      executor(dataset,num_modes,predict_test,standardise_x,maxIters,func_hypinit,optimise_sf,optimise_sw,true,log_transform_y);
      cnt = cnt + 1;
   catch err
      disp(err)
    end
  end  
end
end
