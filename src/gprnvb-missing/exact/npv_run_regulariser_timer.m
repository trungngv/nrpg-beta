function npv_run_regulariser_timer(dataset, num_modes, test_prediction, ...
  standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,lambda,optimise_hyp)

%% init data set
switch (dataset)
  case 'synthetic'
    Ntrain = 30; Ntest = 20;
    num_latents = 3; num_outputs = 3; D = 2;
    sigma_f = 0.000; sigma_y = 0.1;
    theta_f = log(rand(D+1,1));
    theta_w = log(rand(D+1,1));
    theta_f(end) = 0.5+theta_f(end);
    theta_w(end) = 0.5+theta_w(end);
    a = ones(num_latents,2)*theta_f(end);
    [params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
        theta_f, theta_w, a, sigma_f, sigma_y,true);
    [params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
    % prediction on new/test data or missing data
    if test_prediction
      ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
      params.Y(ind_n,1) = nan;
    else
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end

  case 'concrete3'
    [X, Y, xtest, ytest] = load_data('data/concreteslump', 'concrete3');
    features = [3:5];
    X = X(:,features); xtest = xtest(:,features);
    num_outputs = size(Y,2); num_latents = 2;
    [Ntrain, D] = size(X);
    Ntest = size(xtest,1);
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    
  case 'concrete'
    [X, Y, xtest, ytest] = load_data('data/concreteslump', 'concrete');
    features = [3:5];
    X = X(:,features); xtest = xtest(:,features);
    num_outputs = size(Y,2); num_latents = 1;
    [Ntrain, D] = size(X);
    Ntest = size(xtest,1);
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);

  case 'jura' % jura with standard gp -- 1 output
    Ntrain = 359; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 3; % jura
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    % pre-process x
    X = [X; xtest];
    if (standardise_x)
      X = standardize(X,1,[],[]);
      xtest = X(Ntrain-Ntest+1:end,:);
    end
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if (test_prediction)
      Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
      Ymean = zeros(1,3); Ystd = zeros(1,3);
      [Y(1:Ntrain-Ntest,1), Ymean(1), Ystd(1)] = standardize(log(Y(1:Ntrain-Ntest,1)),1,[],[]);
      [Y(:,2:3), Ymean(2:3), Ystd(2:3)] = standardize(log(Y(:,2:3)),1,[],[]);
    else
      Y = [Y; ytest];
      [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
    end
    params.X = X(1:Ntrain,:);
    params.Y = Y(1:Ntrain,:);
    save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
    
end

% pre-process x
if ~strcmp(dataset, 'jura')
if (standardise_x)
  [X, Xmean, Xstd] = standardize(X,1,[],[]);
  xtest = standardize(xtest,1,Xmean,Xstd);
end
% pre-process y
[Y,Ymean,Ystd] = standardize(Y,1,[],[]);
params.X = X; params.Y = Y;
save([dataset '.mat'], 'Ymean', 'Ystd'); % need this for prediction
end

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
hyp0 = feval(func_hypinit, D);

D = Ntrain*num_latents*(num_outputs+1);
theta0 = randn(num_modes, D+1);
theta0(:,D+1) = log(1);

my_npv_run(theta0,hyp0(1:end-1),hyp0(end),params,maxIters,optimise_sf,optimise_sw,lambda,optimise_hyp);
end

function my_npv_run(theta,hyp,lsy,params,nIter,optimise_sf,optimise_sw,lambda,optimise_hyp)
fid = fopen(['time' num2str(tic) '_modes' num2str(params.num_modes) '.txt'],'w');
tol = 0.00001; F = zeros(nIter,1);
[K D] = size(theta); D = D - 1;
opts = struct('Display','off','Method','lbfgs','MaxIter',200,...
  'MaxFunEvals',200,'DerivativeCheck','off'); 

X = params.X; Dx = size(X,2); Q = params.num_latents;
% pre-compute Lf, Lw 
if ~optimise_sf,  hyp(Dx+1) = log(1);  end
if ~optimise_sw,  hyp(end) = log(1);   end
Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
tbegin = tic;
for iter = 1:nIter
  F(iter) = -exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,0);
  if iter > 1 && abs(F(iter)-F(iter-1)) < tol; break; end
  % optimise component mean
  for k = 1:K
    func = @(x) exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,1,k,x);
    theta(k,1:D) = minFunc(func,theta(k,1:D)',opts);
  end
  % optimise component variance log \sigma_k^2
  func = @(x) exact_nELBO(theta(:,1:D),x,lsy,Lf,Lw,params,2);
  theta(:,D+1) = minFunc(func,theta(:,D+1),opts);
  % optimise hyp of covariance function
  if optimise_hyp
    func = @(x) nbound_ltheta_regularise(x,theta(:,1:D),theta(:,D+1),lsy,params,...
      optimise_sf,optimise_sw,lambda);
    hyp = minFunc(func, hyp, opts);
    if ~optimise_sf,  hyp(Dx+1) = log(1);  end
    if ~optimise_sw,  hyp(end) = log(1);   end
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
  end
  
  % optimise noise
  func = @(x) nbound_lsy(x,theta(:,1:D),theta(:,D+1),[],Lf,Lw,params);
  lsy = minFunc(func,lsy,opts);
end
elapse = toc(tbegin);
fprintf(fid,'concrete3\n');
fprintf(fid,'total training time %.2f(s)\n', elapse);
fprintf(fid,'iterations %d\n', iter);
fprintf(fid,'average training per iterations: %.2f(s)\n', elapse/iter);
fclose(fid);    

end

function [nf ng] = nbound_ltheta(ltheta,Mu,lsk,lsy,params,optimise_sf,optimise_sw)
nf = 0; ng = zeros(size(ltheta));
X = params.X; D = size(X,2);
if ~optimise_sf,   ltheta(D+1) = log(1); end
if ~optimise_sw,   ltheta(end) = log(1); end
Kf = feval(params.covfunc_f, ltheta(1:D+1), X);
Kw = feval(params.covfunc_w, ltheta(D+2:end), X);
[Lf, ~] = jit_chol(Kf); [Lw, ~] = jit_chol(Kw);
Lf = Lf'; Lw = Lw';
for n=1:params.num_modes
  if nargout > 1
    [f, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,...
      optimise_sf,optimise_sw);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,[]);
  end
  nf = nf + f;
end  
end

function [nf ng] = nbound_ltheta_regularise(ltheta,Mu,lsk,lsy,params,...
  optimise_sf,optimise_sw,lambda)
[nf ng] = nbound_ltheta(ltheta,Mu,lsk,lsy,params, optimise_sf,optimise_sw);
%adding regularisation
X = params.X; D = size(X,2);
ltheta_f = ltheta(1:D);
ltheta_w = ltheta(D+2:end-1);
nf = nf + lambda*sum(exp(2*ltheta_w)) + lambda*sum(exp(2*ltheta_f));
ng(1:D) = ng(1:D) + lambda*2*exp(2*ltheta_f);
ng(D+2:end-1) = ng(D+2:end-1) + lambda*2*exp(2*ltheta_w);
end

function [nf ng] = nbound_lsy(lsy,Mu,lsk,ltheta,Lf,Lw,params)
nf = 0; ng = 0; K = params.num_modes;
for n=1:K
  if nargout > 1
    [f, ~, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
  end
  nf = nf + f;
end
end

