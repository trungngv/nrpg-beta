function [mae smse hyp F hyp0] = gprn_npv_run_regulariser(dataset,num_modes,test_prediction,...
  standardise_x,log_scale,maxIters,func_hypinit,optimise_sf,optimise_sw,lambda,optimise_hyp)
% Demonstration of GPRN with nonparametric variational inference

% init data set
[params, xtest, ytest] = prepare_data(2, dataset,test_prediction, num_modes,...
  standardise_x,log_scale);
[Ntrain, D] = size(params.X);
num_latents = params.num_latents;
num_outputs = params.num_outputs;

% parameters
hyp0 = feval(func_hypinit, D);
D = Ntrain*num_latents*(num_outputs+1);
theta0 = randn(num_modes, D+1);
theta0(:,D+1) = log(1);

[F mu s2 hyp lsy] = my_npv_run(theta0,hyp0(1:end-1),hyp0(end),params,maxIters,...
  optimise_sf,optimise_sw,lambda,optimise_hyp);

disp('learned hyp vs. init hyp');
disp([exp([hyp(:); lsy]), exp(hyp0(:))])
disp('s2')
disp(s2)
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

%% prediction
mu(1:num_modes,1:10)
[mae smse]= gprn_prediction(xtest,ytest,mu,hyp,params);
hyp = [hyp; lsy];
end

function [F mu s2 hyp lsy] = my_npv_run(theta,hyp,lsy,params,nIter,...
  optimise_sf,optimise_sw,lambda,optimise_hyp)
tol = 0.00001; F = zeros(nIter,1);
[K D] = size(theta); D = D - 1;
opts = struct('Display','off','Method','lbfgs','MaxIter',200,...
  'MaxFunEvals',200,'DerivativeCheck','off'); 

X = params.X; Dx = size(X,2); Q = params.num_latents;
% pre-compute Lf, Lw 
if ~optimise_sf,  hyp(Dx+1) = log(1);  end
if ~optimise_sw,  hyp(end) = log(1);   end
Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
tbegin = tic;
for iter = 1:nIter
  disp(['iteration: ',num2str(iter)]);
  F(iter) = -exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,0);
  fprintf('ELBO %.5f\n', F(iter));
  if iter > 1 && abs(F(iter)-F(iter-1)) < tol; break; end
  
  tstart = tic;
  % optimise component mean
  for k = 1:K
    func = @(x) exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,1,k,x);
    theta(k,1:D) = minFunc(func,theta(k,1:D)',opts);
  end
  disp(['optimising mu takes ', num2str(toc(tstart))])

  % optimise component variance log \sigma_k^2
  tstart = tic;
  func = @(x) exact_nELBO(theta(:,1:D),x,lsy,Lf,Lw,params,2);
  theta(:,D+1) = minFunc(func,theta(:,D+1),opts);
  disp(['optimising sigma takes ', num2str(toc(tstart))])
  fprintf('\nsigma = %.4f', exp(theta(:,D+1)));
  fprintf('\n');
  
  % optimise hyp of covariance function
  if optimise_hyp
    tstart = tic;
    func = @(x) nbound_ltheta_regularise(x,theta(:,1:D),theta(:,D+1),lsy,params,...
      optimise_sf,optimise_sw,lambda);
    hyp = minFunc(func, hyp, opts);
    disp(['optimising log hyp takes ', num2str(toc(tstart))])
    if ~optimise_sf,  hyp(Dx+1) = log(1);  end
    if ~optimise_sw,  hyp(end) = log(1);   end
    disp(['hyp = ', num2str(exp([hyp' lsy]))])
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
  end
  
  % optimise noise
  tstart = tic;
  func = @(x) nbound_lsy(x,theta(:,1:D),theta(:,D+1),[],Lf,Lw,params);
  lsy = minFunc(func,lsy,opts);
  disp(['optimising noise takes ', num2str(toc(tstart))]);

end
fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
mu = theta(:,1:D);
s2 = exp(theta(:,end));
end

function [nf ng] = nbound_ltheta(ltheta,Mu,lsk,lsy,params,optimise_sf,optimise_sw)
nf = 0; ng = zeros(size(ltheta));
X = params.X; D = size(X,2);
if ~optimise_sf,   ltheta(D+1) = log(1); end
if ~optimise_sw,   ltheta(end) = log(1); end
Kf = feval(params.covfunc_f, ltheta(1:D+1), X);
Kw = feval(params.covfunc_w, ltheta(D+2:end), X);
[Lf, ~] = jit_chol(Kf); [Lw, ~] = jit_chol(Kw);
Lf = Lf'; Lw = Lw';
for n=1:params.num_modes
  if nargout > 1
    [f, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,...
      optimise_sf,optimise_sw);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,[]);
  end
  nf = nf + f;
end  
end

function [nf ng] = nbound_ltheta_regularise(ltheta,Mu,lsk,lsy,params,...
  optimise_sf,optimise_sw,lambda)
[nf ng] = nbound_ltheta(ltheta,Mu,lsk,lsy,params, optimise_sf,optimise_sw);
%adding regularisation
X = params.X; D = size(X,2);
ltheta_f = ltheta(1:D);
ltheta_w = ltheta(D+2:end-1);
nf = nf + lambda*sum(exp(2*ltheta_w)) + lambda*sum(exp(2*ltheta_f));
ng(1:D) = ng(1:D) + lambda*2*exp(2*ltheta_f);
ng(D+2:end-1) = ng(D+2:end-1) + lambda*2*exp(2*ltheta_w);
end

function [nf ng] = nbound_lsy(lsy,Mu,lsk,ltheta,Lf,Lw,params)
nf = 0; ng = 0; K = params.num_modes;
for n=1:K
  if nargout > 1
    [f, ~, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
  end
  nf = nf + f;
end
end

