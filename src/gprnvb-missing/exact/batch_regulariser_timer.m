clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'concrete3';
num_outputs = 3;
maxIters = 200;
predict_test = true;
standardise_x = true;
optimise_sf = false;
optimise_sw = true;
lambda = 0.0;
executor = @npv_run_regulariser_timer;
func_hypinit = 'hyp_init_standard';
modes = [3];
for num_modes=modes
  cnt = 1; iters = 3;
  while cnt <= iters
    try
      executor(dataset,num_modes,predict_test, standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,lambda,true);
      cnt = cnt + 1;
   catch err
      disp(err)
    end
  end  
end

