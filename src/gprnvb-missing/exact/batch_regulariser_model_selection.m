clear;
%rngs = [101 23 393 123 9793 13547 1361237 100889 332310 734];
rngno = 23;
rng(rngno, 'twister');
dataset = 'concrete3';
num_outputs = 3;
predict_test = true;
standardise_x = true;
optimise_sf = false;
optimise_sw = true;
lambda = 0.1;
executor = @gprn_npv_run_regulariser;
modes=[1,4];
for num_modes=modes
func_hypinit = 'hyp_init_standard';
timestamp = num2str(tic);
logfilename = ['output/npvgprn/' dataset '/selection' func2str(executor) ...
    '_modes' num2str(num_modes) '_' timestamp '.log'];
maxItersHypOpt = 100; maxIters = 500;
 
% model selection for hyper-parameters
cnt = 1; iters = 10;
elbos = zeros(iters,1);
learned_hyp = [];
fid = fopen(logfilename, 'w');
mae = zeros(iters,num_outputs);
mae_pseudo = mae; smse = mae; smse_pseudo = mae;
while cnt <= iters
 try
    [mae(cnt,:),smse(cnt,:), ~, hyp, elbo, mae_pseudo(cnt,:), smse_pseudo(cnt,:) hyp0] = executor(...
    dataset, num_modes, predict_test,standardise_x,maxItersHypOpt,func_hypinit,optimise_sf,optimise_sw,lambda,true);
    elbo = elbo(elbo ~= 0);
    fprintf(fid, '====================\nRun %d:\n',cnt);
    fprintf(fid, 'Init hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp0));
    fprintf(fid, '\nOptimised hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp));
    fprintf(fid, '\n\tELBO = ');
    fprintf(fid, '\n\t\t%.4f', elbo);
    fprintf(fid, '\n\tmae \t\t\t\t =');
    fprintf(fid, '%.5f\t', mae(cnt,:));
    fprintf(fid, '\n\tmae (pseudo) =');
    fprintf(fid, '%.5f\t', mae_pseudo(cnt,:));
    fprintf(fid, '\n\tsmse \t\t\t\t =');
    fprintf(fid, '%.5f\t', smse(cnt,:));
    fprintf(fid, '\n\tsmse (pseudo) =');
    fprintf(fid, '%.5f\t', smse_pseudo(cnt,:));
    fprintf(fid, '\n');
    assert(elbo(end) < -500);
    elbos(cnt) = elbo(end);
    if isempty(learned_hyp)
      learn_hyp = zeros(numel(hyp0),iters);
    end
    learned_hyp(:,cnt) = hyp(:);
    cnt = cnt + 1;
 catch err
    disp(err)
  end
end  

fclose(fid);
copyfile(logfilename, '/home/trung/Dropbox/experimentslog/');
% save prediction error to file
save([logfilename '.mat'], 'mae', 'mae_pseudo', 'smse', 'smse_pseudo');
  
% record settings
filename = ['output/npvgprn/' dataset  '/selection' func2str(executor) ...
    '_modes' num2str(num_modes) '_' timestamp '.txt'];
fid = fopen(filename, 'w');
fprintf(fid, 'Runner: %s\n', func2str(executor));
fprintf(fid, 'Date time: %s\n', datestr(now));
fprintf(fid, 'Rngno:\t %d\n', rngno);
fprintf(fid, 'Dataset:\t %s\n', dataset);
fprintf(fid, 'Num outputs:\t %d\n', num_outputs);
fprintf(fid, 'Num modes:\t %d\n', num_modes);
fprintf(fid, 'Max iters hyp optimisation: %d\n', maxItersHypOpt);
fprintf(fid, 'Max iters: %d\n', maxIters);
fprintf(fid, 'Standardised X:\t %d\n', standardise_x);
fprintf(fid, 'Predict on new data:\t %d\n', predict_test);
fprintf(fid, 'Optimise latent signal variance: %d\n', optimise_sf);
fprintf(fid, 'Optimise weight signal variance: %d\n', optimise_sw);
fprintf(fid, 'Lambda: %.4f\n', lambda);

cnt = 1; iters = 10;
mae = zeros(iters,num_outputs);
mae_pseudo = mae; smse = mae; smse_pseudo = mae;
ymean = zeros(iters,num_outputs);
[~, idx_max] = max(elbos);
func_hypinit = @(D) (learned_hyp(:,idx_max));
while cnt <= iters
  try
    [mae(cnt,:), smse(cnt,:), ymean(cnt,:) hyp lowerbound mae_pseudo(cnt,:), smse_pseudo(cnt,:) hyp0] = executor(dataset,...
      num_modes, predict_test, standardise_x, maxIters, func_hypinit,optimise_sf,optimise_sw,lambda,true);
    fprintf(fid, '====================\nRun %d:\n',cnt);
    fprintf(fid, 'Init hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp0));
    fprintf(fid, '\nOptimised hyp: ');
    fprintf(fid, '%.4f\t', exp(hyp));
    fprintf(fid, '\n\tELBO = ');
    fprintf(fid, '\n\t\t%.4f', lowerbound(lowerbound~=0));
    fprintf(fid, '\n\tmae \t\t\t\t =');
    fprintf(fid, '%.5f\t', mae(cnt,:));
    fprintf(fid, '\n\tmae (pseudo) =');
    fprintf(fid, '%.5f\t', mae_pseudo(cnt,:));
    fprintf(fid, '\n\tsmse \t\t\t\t =');
    fprintf(fid, '%.5f\t', smse(cnt,:));
    fprintf(fid, '\n\tsmse (pseudo) =');
    fprintf(fid, '%.5f\t', smse_pseudo(cnt,:));
    fprintf(fid, '\n');
    assert(elbo(end) < -500);
    cnt = cnt + 1;
    disp(mae)
  catch err
    disp(err)
  end
end  

% save prediction error to file
save([filename '.mat'], 'mae', 'mae_pseudo', 'smse', 'smse_pseudo');
% for the first output
mae = mae(:,1); ymean=ymean(:,1); mae_pseudo = mae_pseudo(:,1);
smse = smse(:,1); smse_pseudo = smse_pseudo(:,1);
  
fprintf(fid, '\nMAE:\n');
fprintf(fid, '%.5f\n', mae);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae), std(mae));

fprintf(fid, '\nTrue MAE:\n');
fprintf(fid, '%.5f\n', mae_pseudo);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(mae_pseudo), std(mae_pseudo));
  
fprintf(fid, '\nSMSE:\n');
fprintf(fid, '%.5f\n', smse);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(smse), std(smse));

fprintf(fid, '\nTrue SMSE:\n');
fprintf(fid, '%.5f\n', smse_pseudo);
fprintf(fid, 'avg: %.5f, std: %.5f\n', mean(smse_pseudo), std(smse_pseudo));

fclose(fid);
disp(['output written to ' filename])
copyfile(filename, '/home/trung/Dropbox/experimentslog/');
end

