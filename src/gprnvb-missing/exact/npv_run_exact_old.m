function [mae Ymean hyp F mae_pseudo hyp0] = npv_run_exact_v1(dataset, num_modes, test_prediction, ...
  standardise_x, maxIters, func_hypinit, optimise_sf, optimise_sw,optimise_hyp)
% Demonstration of GPRN with nonparametric variational inference
% Version that learn length-scales of covariance functions.
% All latent functions share the same hyperparameters.

%% init data set
switch (dataset)
  case 'synthetic'
    Ntrain = 30; Ntest = 20;
    num_latents = 3; num_outputs = 3; D = 2;
    sigma_f = 0.000; sigma_y = 0.1;
    theta_f = log(rand(D+1,1));
    theta_w = log(rand(D+1,1));
    theta_f(end) = 0.5+theta_f(end);
    theta_w(end) = 0.5+theta_w(end);
    a = ones(num_latents,2)*theta_f(end);
    [params, xtest, ytest] = test_data(Ntrain, Ntest, D, num_latents, num_outputs, num_modes,...
        theta_f, theta_w, a, sigma_f, sigma_y,true);
    [params.Y Ymean Ystd] = standardize(params.Y,1,[],[]);
    save('temp.mat', 'Ymean', 'Ystd');
    % prediction on new/test data or missing data
    if test_prediction
      ind_n = randperm(Ntrain,ceil(0.1*Ntrain));
      params.Y(ind_n,1) = nan;
    else
      xtest = params.X(1:Ntest,:);
      ytest = params.Y(1:Ntest,:);
    end
  case 'jura' % jura with standard gp -- 1 output
    Ntrain = 359; Ntest = 100;
    D = 2; num_latents = 2; num_outputs = 3; % jura
    params = test_data(Ntrain,Ntest,D,num_latents,num_outputs,num_modes,[],[],[],[],[],false);
    [X, Y, xtest, ytest] = load_data('data/jura', 'juraCd');
    % pre-process x
    X = [X; xtest];
    if (standardise_x)
      X = standardize(X,1,[],[]);
      xtest = X(Ntrain-Ntest+1:end,:);
    end
    % pre-process y
    Y = Y(:,1:num_outputs);
    ytest = ytest(:,1:num_outputs);
    if (test_prediction)
      Y = [Y; [nan(Ntest,1), ytest(:,2:3)]];
      Ymean = zeros(1,3); Ystd = zeros(1,3);
      [Y(1:Ntrain-Ntest,1), Ymean(1), Ystd(1)] = standardize(log(Y(1:Ntrain-Ntest,1)),1,[],[]);
      [Y(:,2:3), Ymean(2:3), Ystd(2:3)] = standardize(log(Y(:,2:3)),1,[],[]);
    else
      Y = [Y; ytest];
      [Y Ymean Ystd] = standardize(log(Y),1,[],[]);
    end
    params.X = X(1:Ntrain,:);
    params.Y = Y(1:Ntrain,:);
    save('temp.mat', 'Ymean', 'Ystd'); % need this for prediction
end

%% for missing data
params.Y_isnan = isnan(params.Y);
params.Wcell_isnan = cell(num_outputs, num_latents);
for i=1:num_outputs
  for j=1:num_latents
    params.Wcell_isnan{i,j} = false(size(params.Y,1),1);
    params.Wcell_isnan{i,j}(params.Y_isnan(:,i)) = true;
  end
end
params.W_isnan = cell2mat(params.Wcell_isnan);
params.w_isnan = params.W_isnan(:);

%% parameters
hyp0 = feval(func_hypinit, D);

D = Ntrain*num_latents*(num_outputs+1);
theta0 = randn(num_modes, D+1);
theta0(:,D+1) = 1;

[F mu s2 hyp lsy] = my_npv_run(theta0,hyp0(1:end-1),hyp0(end),params,maxIters,...
  optimise_sf,optimise_sw,optimise_hyp);

disp('learned hyp vs. init hyp');
disp([exp([hyp(:); lsy]), exp(hyp0(:))])
disp('s2')
disp(s2)
% figure;
% plot(F,'-o','LineWidth',2);
% xlabel('Iteration');
% ylabel('ELBO');

%% prediction
mu(1:num_modes,1:10)
mae = gprn_prediction(xtest,ytest,mu,hyp,params,false);
mae_pseudo = gprn_prediction(xtest,ytest,mu,hyp,params,true);

hyp = [hyp; lsy];
end

function [F mu s2 hyp lsy] = my_npv_run(theta,hyp,lsy,params,nIter,...
  optimise_sf,optimise_sw,optimise_hyp)
tol = 0.0001; F = zeros(nIter,1);
[K D] = size(theta); D = D - 1;
opts = struct('Display','off','Method','lbfgs','MaxIter',200,...
  'MaxFunEvals',200,'DerivativeCheck','off'); 

X = params.X; Dx = size(X,2); Q = params.num_latents;
tbegin = tic;
for iter = 1:nIter
  disp(['iteration: ',num2str(iter)]);
  if iter == 1 || optimise_hyp
    if ~optimise_sf,  hyp(Dx+1) = log(1);  end
    if ~optimise_sw,  hyp(end) = log(1);   end
    disp(['hyp = ', num2str(exp([hyp' lsy]))])
    Kf = feval(params.covfunc_f, hyp(1:Dx+1), X);
    Kw = feval(params.covfunc_w, hyp(Dx+2:end), X);
    Lf = jit_chol(Kf)'; Lw = jit_chol(Kw)';
  end  
  F(iter) = -exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,0);
%   elbo_after = F(iter);
%   if iter > 1, fprintf('\tafter - before = %.10f\n', elbo_after - elbo_before); end
  fprintf('ELBO %.5f\n', F(iter));
  if iter > 1 && abs(F(iter)-F(iter-1)) < tol; break; end
  
  tstart = tic;
  % optimise component mean
  for k = 1:K
    func = @(x) exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,1,k,x);
    theta(k,1:D) = minFunc(func,theta(k,1:D)',opts);
  end
  disp(['optimising mu takes ', num2str(toc(tstart))])
        
  % optimise component variance log \sigma_k^2
  tstart = tic;
  func = @(x) exact_nELBO(theta(:,1:D),x,lsy,Lf,Lw,params,2);
  theta(:,D+1) = minFunc(func,theta(:,D+1),opts);
  disp(['optimising sigma takes ', num2str(toc(tstart))])
  disp(['sk2 = ', num2str(exp(2*theta(:,D+1)))]);
  
  % optimise noise
  tstart = tic;
  func = @(x) nbound_lsy(x,theta(:,1:D),theta(:,D+1),[],Lf,Lw,params);
  lsy = minFunc(func,lsy,opts);
  disp(['optimising noise takes ', num2str(toc(tstart))]);
  
  % optimise hyp of covariance function
  if optimise_hyp
    tstart = tic;
    func = @(x) nbound_ltheta(x,theta(:,1:D),theta(:,D+1),lsy,params,optimise_sf,optimise_sw);
    %elbo_before = -exact_nELBO(theta(:,1:D),theta(:,D+1),lsy,Lf,Lw,params,0);
    hyp = minFunc(func, hyp, opts);
    disp(['optimising log hyp takes ', num2str(toc(tstart))])
  end  
end
fprintf('\n\nnpv learned completed in %.2f(s)\n', toc(tbegin));
    
mu = theta(:,1:D);
s2 = exp(theta(:,end));
end

function [nf ng] = nbound_ltheta(ltheta,Mu,lsk,lsy,params,optimise_sf,optimise_sw)
nf = 0; ng = zeros(size(ltheta));
X = params.X; D = size(X,2);
if ~optimise_sf,   ltheta(D+1) = log(1); end
if ~optimise_sw,   ltheta(end) = log(1); end
Kf = feval(params.covfunc_f, ltheta(1:D+1), X);
Kw = feval(params.covfunc_w, ltheta(D+2:end), X);
[Lf, ~] = jit_chol(Kf); [Lw, ~] = jit_chol(Kw);
Lf = Lf'; Lw = Lw';
for n=1:params.num_modes
  if nargout > 1
    [f, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,...
      optimise_sf,optimise_sw);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,3,[]);
  end
  nf = nf + f;
end  
end

function [nf ng] = nbound_lsy(lsy,Mu,lsk,ltheta,Lf,Lw,params)
nf = 0; ng = 0; K = params.num_modes;
for n=1:K
  if nargout > 1
    [f, ~, ~, ~, g] = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
    ng = ng + g;
  else
    f = exact_nlogjoint(Mu(n,:),lsk(n),ltheta,lsy,Lf,Lw,params,4);
  end
  nf = nf + f;
end
end

