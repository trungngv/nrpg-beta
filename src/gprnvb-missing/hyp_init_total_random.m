function hyp = hyp_init_total_random(D)
%HYP_INIT_TOTAL_RANDOM hyp = hyp_init_total_random()
% 
thetaf = log(rand(D+1,1));
thetaw = rand(D+1,1);
hyp = [thetaf; thetaw; log(1)];
end
