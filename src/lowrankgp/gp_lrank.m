function [varargout] = gp_lrank(hyp, inf, mean, cov, lik, x, y, xs, ys)
% Gaussian Process prediction using low-rank approximation. It returns 
% the test set predictive probabilities (mean and covariance).
%
% Usage:
%
%     [ymu ys2 fmu fs2   ] = gp(hyp, mean, cov, lik, x, y, xs);
% or: [ymu ys2 fmu fs2 lp] = gp(hyp, mean, cov, lik, x, y, xs, ys);
%
% where:
%
%   hyp      column vector of hyperparameters
%   inf      inference function (only infLA eligible)
%   cov      prior covariance function (see below)
%   mean     prior mean function
%   lik      likelihood function
%   x        n by D matrix of training inputs
%   y        column vector of length n of training targets
%   xs       ns by D matrix of test inputs
%   ys       column vector of length nn of test targets
%
%   ymu      column vector (of length ns) of predictive output means
%   ys2      column vector (of length ns) of predictive output variances
%   fmu      column vector (of length ns) of predictive latent means
%   fs2      column vector (of length ns) of predictive latent variances
%   lp       column vector (of length ns) of log predictive probabilities
%
%   post     structure containing Kinv = (K + sn2 * In)^{-1} and alpha =
%            Kinv * y
% 
% See also covFunctions.m, infMethods.m, likFunctions.m, meanFunctions.m.
%
% Author: Trung V. Nguyen
% Last update: 18/07/12
%
if nargin<7 || nargin>9
  disp('Usage: [ymu ys2 fmu fs2   ] = gp(hyp, mean, cov, lik, x, y, xs);')
  disp('   or: [ymu ys2 fmu fs2 lp] = gp(hyp, mean, cov, lik, x, y, xs, ys);')
  return
end
if isempty(inf),  inf = @infLA; else                        % set default inf
  if iscell(inf), inf = inf{1}; end                      % cell input is allowed
  if ischar(inf), inf = str2func(inf); end        % convert into function handle
end
if isempty(mean), mean = {@meanZero}; end                     % set default mean
if ischar(mean) || isa(mean, 'function_handle'), mean = {mean}; end  % make cell
if isempty(cov), error('Covariance function cannot be empty'); end  % no default
if ischar(cov)  || isa(cov,  'function_handle'), cov  = {cov};  end  % make cell
cov1 = cov{1}; if isa(cov1, 'function_handle'), cov1 = func2str(cov1); end
if strcmp(cov1,'covFITC'); inf = @infFITC; end       % only one possible inf alg
if isempty(lik),  lik = @likGauss; else                        % set default lik
  if iscell(lik), lik = lik{1}; end                      % cell input is allowed
  if ischar(lik), lik = str2func(lik); end        % convert into function handle
end
D = size(x,2);

if ~isfield(hyp,'mean'), hyp.mean = []; end        % check the hyp specification
if eval(feval(mean{:})) ~= numel(hyp.mean)
  error('Number of mean function hyperparameters disagree with mean function')
end
if ~isfield(hyp,'cov'), hyp.cov = []; end
if eval(feval(cov{:})) ~= numel(hyp.cov)
  error('Number of cov function hyperparameters disagree with cov function')
end
if ~isfield(hyp,'lik'), hyp.lik = []; end
if eval(feval(lik)) ~= numel(hyp.lik)
  error('Number of lik function hyperparameters disagree with lik function')
end

try                                                  % call the inference method
  post = inf(hyp, mean, cov, lik, x, y);
catch
  msgstr = lasterr;
  if nargin > 6, error('Inference method failed [%s]', msgstr); else 
    warning('Inference method failed [%s] .. attempting to continue',msgstr)
    dnlZ = struct('cov',0*hyp.cov, 'mean',0*hyp.mean, 'lik',0*hyp.lik);
    varargout = {NaN, dnlZ}; return                    % continue with a warning
  end
end

alpha = post.alpha;
Kinv = post.Kinv;
if issparse(alpha)                  % handle things for sparse representations
  nz = alpha ~= 0;                                 % determine nonzero indices
else
  nz = true(size(alpha)); % non-sparse representations
end

ns = size(xs,1);                                       % number of data points
nperbatch = 1000;                       % number of data points per mini batch
nact = 0;                       % number of already processed test data points
ymu = zeros(ns,1); ys2 = ymu; fmu = ymu; fs2 = ymu; lp = ymu;   % allocate mem
while nact<ns               % process minibatches of test cases to save memory
  id = (nact+1):min(nact+nperbatch,ns);               % data points to process
  kss = feval(cov{:}, hyp.cov, xs(id,:));              % test set covariance
  Ks  = feval(cov{:}, hyp.cov, x(nz,:), xs(id,:));         % cross-covariances
  ms = feval(mean{:}, hyp.mean, xs(id,:));
  fmu(id) = ms + Ks'*alpha;
  % TODO probably there is more efficient way of doing this (because
  % cross-covariances of test points have no meaning)
  fs2(id) = diag(kss - Ks'*Kinv*Ks);
  fs2(id) = max(fs2(id),0);   % remove numerical noise i.e. negative variances
  if nargin<9
    [lp(id) ymu(id) ys2(id)] = lik(hyp.lik, [], fmu(id), fs2(id));
  else
    [lp(id) ymu(id) ys2(id)] = lik(hyp.lik, ys(id), fmu(id), fs2(id));
  end
  nact = id(end);          % set counter to index of last processed data point
end
if nargin<9
  varargout = {ymu, ys2, fmu, fs2, [], post};        % assign output arguments
else
  varargout = {ymu, ys2, fmu, fs2, lp, post};
end
end
