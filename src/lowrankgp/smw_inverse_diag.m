function R = smw_inverse_diag(A, U, C, V)
%SMW_INVERSE R = smw_inverse_diag(A, U, C, V)
%
% Computes the inverse of (A + UCV)^{-1} where A is a diagonal matrix using
% the Sherman-Morrison-Woodbury formula:
% (A + UCV)^{-1} = A^{-1} - A^{-1}U(C^{-1} + VA^{-1}U)^{-1}VA^{-1}
%
% Author: Trung V. Nguyen
% Last update: 16/07/12
n = size(A, 1);
k = size(U, 2);
assert(isequal([n, n], size(A)), 'A must be square');
assert(isequal([n, k], size(U)), 'U must have same rows as A');
assert(isequal([k, k], size(C)), 'C must be square');
assert(isequal([k, n], size(V)), 'V must have same columns as A');
A_inv = diag(1 ./ diag(A));
R = A_inv - A_inv * U * inv(inv(C) + V * A_inv * U) * V * A_inv;
end
