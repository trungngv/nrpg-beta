function U = approx_kern_multipass(K, npoints, lrank, opt_params, npass)
%APPROXIMATE_KERN U = approx_kern_multipass(K, npoints, lrank, opt_params, npass)
% 
% Approximate the kernel matrix K = UU' based only on a subset of its
% points where U has lower rank than K. User can specify the indice
% of elements in the matrix K (by row_ind and col_ind). Alternatively, if
% row_ind and col_ind are not provided, the method randomly selects npoints
% of K to use.
%
% INPUT
%   - K : a covariance matrix
%   - npoints : number of points to used (empty [] if row_ind and col_ind is
%   provided)
%   - lrank : the low rank dimension
%   - opt_params : (struct) optimization parameters to be used for the internal
%   stochastic gradient descent algorithm
%
% OUTPUT
%   - U : the low-rank approximation
%
% Author: Trung V. Nguyen
% Last update: 17/07/12
%
N = size(K, 1);
if isempty(opt_params)
  opt_params = struct('alpha', 0.1, 'lambda', 0.01, 'max_iters', 200, 'epsilon', 1e-05);
end
% first pass to get some U
for i=1:npass
  % random a new subset for each pass
  [~, row_ind, col_ind] = subset(K, npoints, true, []);
  row_ind = [row_ind; (1:N)'];
  col_ind = [col_ind; (1:N)'];
  if i == 1
    U = lrank_approx_sym(K, [], row_ind, col_ind, lrank, opt_params);
  else
    U = lrank_approx_sym(K, U, row_ind, col_ind, lrank, opt_params);
  end  
end
Kapprox = U * U';
disp('Kapprox ./ K')
Kapprox(1:10, 1:10) ./ K(1:10, 1:10)
end

