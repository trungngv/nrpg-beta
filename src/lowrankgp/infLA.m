function [post nlZ dnlZ] = infLA(hyp, mean, cov, lik, x, y)

% Low-rank approximation for a GP with Gaussian likelihood. Compute 
% the negative log marginal likelihood and its derivatives
% w.r.t. the hyperparameters.

% Note that this method does not return the posterior parameterization
% used in this framework as we want to avoid doing the Cholesky decomposition.
% User must therefore define their own function to make predictions.
%
% See also "help infMethods".
%
% Author: Trung V. Nguyen
% Last update: 17/07/12
%
% See also INFMETHODS.M.
likstr = lik; if ~ischar(lik), likstr = func2str(lik); end 
if ~strcmp(likstr,'likGauss')               % NOTE: no explicit call to likGauss
  error('LA inference only possible with Gaussian likelihood');
end
 
[n, D] = size(x);
K = feval(cov{:}, hyp.cov, x);                      % evaluate covariance matrix
m = feval(mean{:}, hyp.mean, x);                          % evaluate mean vector

sn2 = exp(2*hyp.lik);                               % sigma_n^2 : noise variance of likGauss
lrank = 150;
npoints = floor(0.25 * n * n);
npass = 1;
%npoints = n * lrank;
%alpha = 0.2 lambda = 0.8 lrank = 150 npoints = 0.25 is currently the best
%(for n = 2000)
opt_params = struct('alpha', 1, 'lambda', 50, 'max_iters', 20, 'epsilon', 1e-07);
%P = approx_kern(K, [], [], npoints, lrank, opt_params);
P = approx_kern_multipass(K, npoints, lrank, opt_params, npass);
Isn = sn2*eye(n);
Ir = eye(lrank);
%Kinv = smw_inverse_diag(Isn, P, Ir, P');
Kinv = eye(n)/sn2 - (P/(Ir+P'*P/sn2)*P')/(sn2^2);
if n <= 500
  Kinv2 = inv(K + sn2 * eye(n));
  disp('Kinv approximation ./ Kinv real')
  Kinv(1:10,1:10) ./ Kinv2(1:10,1:10)
end  
alpha = Kinv*(y-m);

% save structure for prediction
post.alpha = alpha;
post.Kinv = Kinv;

if nargout>1                               % do we want the marginal likelihood?
  nlZ = (y-m)'*alpha/2 + n*log(sn2) + log(det(Ir+P'*P/sn2)) + n*log(2*pi)/2;  % -log marg lik
  if nargout>2                                         % do we want derivatives?
    dnlZ = hyp;                                 % allocate space for derivatives
    Q = Kinv - alpha*alpha';    % precompute for convenience
    for i = 1:numel(hyp.cov)
      % dK/dtheta = feval(cov{:}, hyp.cov, x, [], i)
      % tr(Q dK/dTheta) = tr(Q dk/dTheta') = tr(Q' dk/dTheta) =
      % sum(sum(Q_{i,j} .* dK/dTheta_{i,j})
      dnlZ.cov(i) = sum(sum(Q.*feval(cov{:}, hyp.cov, x, [], i)))/2;
    end
    dnlZ.lik = sn2*trace(Q);
    for i = 1:numel(hyp.mean), 
      dnlZ.mean(i) = -feval(mean{:}, hyp.mean, x, i)'*alpha;
    end
  end
end

