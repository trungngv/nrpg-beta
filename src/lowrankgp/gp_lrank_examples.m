%GP_LRANK_EXAMPLES
% Examples using the GP with low-rank approximation
%
%% prediction with low-rank approximation using GPR datasets
dataset_dir = 'data';
dataset = 'SARCOS';
n = 2000;
[x, y, xs, ys] = load_data_gpr(dataset_dir, dataset, n);
[~, D] = size(x);

covfunc = {@covSEard}; hyp.cov = log(rand(D + 1, 1));
likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
inffunc = @infExact;
nruns = 1;
hyp = minimize(hyp, @gp, 50, @infExact, [], covfunc, likfunc, x, y);
%%
%lrank prediction
rng(1110, 'twister');
gp_lrank_regression(dataset,x,y,xs,ys, covfunc, likfunc, inffunc, ...
 hyp, false, nruns);
% exact prediction
[m s2] = gp(hyp, @infExact, [], covfunc, likfunc, x, y, xs);
mserror = mse(m, ys, mean(ys), var(ys));
fprintf('mse by exact inference: %.5f\n', mserror);
% fitc prediction
nu = fix(n/2);
ind_u = randperm(n, nu);
u = x(ind_u,:);
covfuncF = {@covFITC, {@covSEard}, u};
[mF s2F] = gp(hyp, @infFITC, [], covfuncF, likfunc, x, y, xs);
mserror = mse(mF, ys, mean(ys), var(ys));
fprintf('mse by FITC: %.5f\n', mserror);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiments with our own synthesis data sets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set parameters for mean, cov, and lik function
rng(1110, 'twister');
covfunc = {@covSEard}; ell = exp(rand); sf = 1; hyp.cov = log([ell; sf]);
likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
n = 500; d = 1;
x = -2 + 4 * rand(n, d);
%x = randn(n,d);
K = feval(covfunc{:}, hyp.cov, x);
% chol(K)' * randn(n, d) gives f ~  GP(0, K)
% y(x) = f(x) + sigma_n \esp, hence
% y ~ N(m(x) + f(x), sigma_n I_n)
y = jit_chol(K)'*randn(n, d) + exp(hyp.lik)*randn(n, d);

figure(1)
set(gca, 'FontSize', 24)
disp(' '); disp('plot(x, y, ''+'')');
plot(x, y, '+', 'MarkerSize', 12)
axis([-1.9 1.9 -4 3.9])
grid on
xlabel('input, x')
ylabel('output, y')

%% Use low-rank approximation to make predictions (with true hyperparameters)
%rng(1110, 'twister');
z = linspace(-1.9, 1.9, 100)';
m_sum = zeros(size(z));
s2_sum = zeros(size(z));
nruns = 1;
for i=1:nruns
  [m s2] = gp_lrank(hyp, @infLA, [], covfunc, likfunc, x, y, z);
  m_sum = m_sum + m;
  s2_sum = s2_sum + s2;
end
m = m_sum / nruns;
s2 = s2_sum / nruns;

figure(2)
set(gca, 'FontSize', 24);
disp(' ');
disp('f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];'); 
f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)]; 
disp('fill([z; flipdim(z,1)], f, [7 7 7]/8);');
fill([z; flipdim(z,1)], f, [7 7 7]/8);

disp('hold on; plot(z, m); plot(x, y, ''+'')')
hold on; plot(z, m, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)
axis([-1.9 1.9 min(f)-0.5 max(f)+0.5])
grid on
xlabel('input, x')
ylabel('output, y')

%% Use gp() to learn hyper-parameters with low-rank approximation.
% Use the learned hyp to make predictions
rng(1110, 'twister');
covfunc = @covSEard;
hyp2.cov = [0; 0]; hyp2.lik = log(0.1);
infFunc = @infLA;
% minimize() with 1 output gives the hyperparameters
% here -100 gives maximum allowed number of func evaluation
hyp2 = minimize(hyp2, @gp, -50, infFunc, [], covfunc, likfunc, x, y);
disp('nlml = ')
nlml = gp(hyp2, infFunc, [], covfunc, likfunc, x, y)
disp('learned hyp.cov vs. true hyp.cov')
hyp2.cov
hyp.cov
disp('learned hyp.lik vs. true hyp.lik')
hyp2.lik
hyp.lik
% prediction using infLA with the original hyper-parameters
m_sum = zeros(size(z));
s2_sum = zeros(size(z));
nruns = 10;
for i=1:nruns
  [m s2] = gp2(hyp2, infFunc, [], covfunc, likfunc, x, y, z);
  m_sum = m_sum + m;
  s2_sum = s2_sum + s2;
end
m = m_sum / nruns;
s2 = s2_sum / nruns;

figure(2)
set(gca, 'FontSize', 24);
disp(' ');
disp('f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];'); 
f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)]; 
disp('fill([z; flipdim(z,1)], f, [7 7 7]/8);');
fill([z; flipdim(z,1)], f, [7 7 7]/8);

disp('hold on; plot(z, m); plot(x, y, ''+'')')
hold on; plot(z, m, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)
axis([-1.9 1.9 min(f)-0.5 max(f)+0.5])
grid on
xlabel('input, x')
ylabel('output, y')

%% Predictions with FITC (using KNOWN hyp)
% Real use of this should be in training as that's when K^{-1} is
% approximated
nu = fix(n/2); u = linspace(-1.3,1.3,nu)';
covfunc = @covSEiso;
covfuncF = {@covFITC, {covfunc}, u};
[mF s2F] = gp(hyp, @infFITC, [], covfuncF, likfunc, x, y, z);

figure(5)
set(gca, 'FontSize', 24)
disp(' ');
disp('f = [mF+2*sqrt(s2F); flipdim(mF-2*sqrt(s2F),1)];');
f = [mF+2*sqrt(s2F); flipdim(mF-2*sqrt(s2F),1)];
disp('fill([z; flipdim(z,1)], f, [7 7 7]/8)');
fill([z; flipdim(z,1)], f, [7 7 7]/8)
disp('hold on; plot(z, mF); plot(x, y, ''+'');');
hold on; plot(z, mF, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)
disp('plot(u,1,''o'')')
plot(u,1,'ko', 'MarkerSize', 12)
grid on
xlabel('input, x')
ylabel('output, y')
axis([-1.9 1.9 min(f)-0.5 max(f)+0.5])
print -depsc f4.eps
disp(' ');
