function [P, Q] = lrank_approximation(A, obs_row_ind, obs_col_ind, ...
  lrank, alpha, lambda, max_iters, epsilon)
%LRANK_APPROXIMATION [P, Q] = lrank_approximation(A, obs_row_ind, obs_col_ind, lrank, alpha, lambda, max_iters, epsilon)
% 
% Computes a low-rank approximation for a matrix A using a subset of its
% entries (treating A as sparse). This function uses Stochastic Gradient
% Descent and Euclidean norm for the error function. The result of this
% function are P and Q such that A ~ PQ' where P and Q has lower rank than
% A.
%
% INPUT
%   - A : a matrix of size M x N
%   - (obs_row_ind, ob_col_ind) : subscripts of elements in A that is used
%   for approximation
%   - lrank : the low-rank to use
%   - alpha : learning rate
%   - lambda : regularization for the low-rank matrices norm
%   - max_iters : max iterations to run SGD (optional, default = 1000)
%   - epsilon : condition for converge (optional, default = 1.e-4)
%
% OUTPUT
%   - P : M x lrank matrix
%   - Q : N x lrank matrix
%
% Author: Trung V. Nguyen
% Last update: 16/07/12

if isempty(max_iters)
  max_iters = 100;
end
if isempty(epsilon)
  epsilon = 1.e-04;
end

[M, N] = size(A);
P = rand(M, lrank);
Q = rand(N, lrank);
n = length(obs_row_ind);
error_trace = zeros(max_iters, 1);
for iter = 1:max_iters
  last_error = 0;
  new_error = 0;
  for idx = 1:n
    u = obs_row_ind(idx);
    i = obs_col_ind(idx);
    qi = Q(i, :);
    pu = P(u, :);
    error_ui = A(u, i) - qi * pu';
    last_error = last_error + error_ui * error_ui + ...
      (lambda/n) * ((norm(qi) ^ 2) + (norm(pu) ^ 2));
    P(u, :) = pu + alpha * (error_ui * qi - lambda/n * pu);
    Q(i, :) = qi + alpha * (error_ui * pu - lambda/n * qi);
    error_ui = A(u, i) - Q(i, :) * P(u, :)';
    new_error = new_error + error_ui * error_ui + ...
      (lambda/n) * ((norm(Q(i, :)) ^ 2) + (norm(P(u, :)) ^ 2));
  end
  error_trace(iter) = new_error;
  if mod(iter, 20) == 0
    fprintf('iter %d : error %.5f\n', iter, new_error);
  end  
  if (abs(last_error - new_error) < epsilon)
    break;
  end
end

figure(1);
plot(1:length(error_trace), log(error_trace));
title('Error vs. iteration');

end

