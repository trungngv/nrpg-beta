function test_nonunif_subset()
%TEST_SUBSET test_subset()
% 
% Test for the subset() function
%
% Author : Trung V. Nguyen
% Last update: 17/07/12
A = randn(5000, 5000);
n = floor(0.1*5000*5000);
[Ahat, row_ind, col_ind] = nonunif_subset(A, n, true, 1110);
length(row_ind)
Ahat(1:10,1:10)
[row_ind(1:10) col_ind(1:10)]
assert(length(row_ind) == length(col_ind));
assert(isempty(find(row_ind > col_ind, 1)));
fprintf('test_nonunif_subset() passed\n');
end

