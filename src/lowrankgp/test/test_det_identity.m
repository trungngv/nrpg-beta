function test_det_identity()
%TEST_DET_IDENTITY test_det_identity()
% 
% Test the formula for computing det(K + sn2 * In) using the determinant
% identity |K + sn2 * In| = sn2^n |I_r + P'P/sn2| where K = PP'.
%
% Author: Trung V. Nguyen
% Last update: 18/07/12
n = 100;
lrank = 50;
P = rand(n, lrank);
K = P*P';
sn2 = 0.1;
% test for det
d1 = det(K + sn2 * eye(n))
d2 = sn2^n * det(eye(lrank) + P'*P/sn2)
assert(abs(d1-d2) < 1e-15)
% test for log det
logd2 = n*log(sn2) + log(det(eye(lrank) + P'*P/sn2));
log(d1)-logd2
assert(abs(log(d1)-logd2) < 1e-10)
fprintf('test_det_identity() passed\n');
end

