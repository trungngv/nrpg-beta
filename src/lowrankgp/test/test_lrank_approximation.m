function test_lrank_approximation()
%TEST_LRANK_APPROXIMATION test_lrank_approximation()
% 
% Test for lrank_approximation().
%
% Author: Trung V. Nguyen
% Last update: 17/07/12
rng(1110, 'twister');
N = 1000;
lrank = 30;
P = rand(N, lrank);
Q = rand(N, lrank);
A = P * Q';
n = floor(0.01 * N * N);
[row_ind, col_ind] = subset(A, n, false);
lrank = 10;
alpha = 0.01;
lambda = 0.05;
max_iters = 500;
tolerance = 1e-07;
[Pt, Qt] = lrank_approximation2(A, row_ind, col_ind, ...
  lrank, alpha, lambda, max_iters, tolerance);
At = Pt * Qt';
fprintf('norm(A - At) = %.4f\n', norm(A - At));
At(1:10, 1:10) ./ A(1:10, 1:10)
end

