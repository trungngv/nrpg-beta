function test_subset()
%TEST_SUBSET test_subset()
% 
% Test for the subset() function
%
% Author : Trung V. Nguyen
% Last update: 17/07/12
A = rand(5, 5);
n = 10;
[~, row_ind, col_ind] = subset(A, n, true, 1110);
assert(length(row_ind) == n);
assert(length(col_ind) == n);
assert(isempty(find(row_ind > col_ind, 1)));
fprintf('test_subset() passed\n');
end

