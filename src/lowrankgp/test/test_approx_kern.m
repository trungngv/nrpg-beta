function test_approx_kern()
%TEST_APPROXIMATE_KERN test_approx_kern()
% 
% Test for the approximate_kern() function.
%
% Author: Trung V. Nguyen
% Last update: 17/07/12
%
rng(1110, 'twister');
USERS_FEATURES = 'data/sushi3/sushi3.udata';
% possible issue: initialization of P, Q is not close enough to Kt and the
% error goes to infinity!
t = read_users_features(USERS_FEATURES);
t = t(1:100, :);
[N, D] = size(t);
Kt = covSEard(rand(D + 1, 1), t);
lrank = 30;
alpha = 0.01;
lambda = 0.001;
max_iters = 300;
epsilon = 1e-05;
n = floor(0.1 * N * N);
opt_params = struct('alpha', alpha, 'lambda', lambda, 'max_iters', max_iters, 'epsilon', epsilon);
U = approx_kern(Kt, [], [], n, lrank, opt_params);
K_approx = U * U';
fprintf('norm(Kt - K_approx) = %.4f\n', norm(Kt - K_approx));
end

