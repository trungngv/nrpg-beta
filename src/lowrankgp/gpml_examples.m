%GPML_EXAMPLES
% Examples using the GPML package
%

%% Set parameters for mean, cov, and lik function
% Set parameters according to the mean, cov, and lik function
% parameterization. Here parameters are fixed (no learning of hyp yet)
%rng(1110, 'twister');
d = 1;
meanfunc = {@meanConst};
hyp.mean = 0;

covfunc = {@covSEard};
ell = 1/4;
sf = 1;
hyp.cov = log([ell; sf]);

likfunc = @likGauss;
sn = 0.1;
hyp.lik = log(sn);

n = 1000;
x = randn(n, d);
K = feval(covfunc{:}, hyp.cov, x);
mu = feval(meanfunc{:}, hyp.mean, x);
% chol(K)' * randn(n, d) gives f ~  GP(0, K)
% y(x) = f(x) + sigma_n \esp, hence
% y ~ N(m(x) + f(x), sigma_n I_n)
y = jit_chol(K)'*randn(n, d) + mu + exp(hyp.lik)*randn(n, d);

%% Use gp() to get nlml and partial derivatives
% call gp() without test points return nlml
% specify the function @infExact that computes posterior and likelihood
nlml = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y)

%% Use gp() to make predictions
z = linspace(-1.9, 1.9, 101)';

% specify @infExact as the function that computes prediction
% call gp() with test points return mean (m) and sigma (s2)
% can specify 2 more arg: fmu and fs2 to get posterior dist of f
[m s2] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y, z);

%% Use gp() to learn hyper-parameters. Use the learned hyp to make predictions
rng(1110, 'twister');
covfunc = @covSEiso;
hyp2.cov = [2; 2]; hyp2.lik = log(0.1);
infFunc = @infExact;
% minimize() with 1 output gives the hyperparameters
% here -100 gives maximum allowed number of func evaluation
hyp2 = minimize(hyp2, @gp, -100, infFunc, [], covfunc, likfunc, x, y);
nlml2 = gp(hyp2, infFunc, [], covfunc, likfunc, x, y)
[m s2] = gp(hyp2, infFunc, [], covfunc, likfunc, x, y, z);

%% Predictions with FITC (using KNOWN hyp)
% Real use of this should be in training as that's when K^{-1} is
% approximated
nu = fix(n/2); u = linspace(-1.3,1.3,nu)';
covfunc = @covSEiso;
covfuncF = {@covFITC, {covfunc}, u};
[mF s2F] = gp(hyp, @infFITC, meanfunc, covfuncF, likfunc, x, y, z);
