%GP_LRANK_REGRESSION gp_lrank_regression(data_set, x, y, xs, ys, covfunc, likfunc, ...
%  inffunc, hyp, learn_hyp, nruns)
% 
% Run gp_lrank for GP regpression on a number of data sets.
%
% INPUT
%   - data_set : name of the dataset
%   - x, y, xs, ys
%   - covfunc, likfunc, inffunc
%   - hyp : hyperparameters struct
%   - learn_hyp : should we learn hyp?
%   - nruns : average results over nruns
%
% Trung V. Nguyen
% 20/07/12
%
function gp_lrank_regression(data_set,x, y, xs, ys, covfunc, likfunc, ...
  inffunc, hyp, learn_hyp, nruns)
if (learn_hyp)
  hyp = minimize(hyp, @gp, -100, inffunc, [], covfunc, likfunc, x, y);
end
m_sum = zeros(size(xs,1),1);
s2_sum = zeros(size(xs,1),1);
predstart = tic;
for i=1:nruns
  [m s2] = gp_lrank(hyp, @infLA, [], covfunc, likfunc, x, y, xs);
  m_sum = m_sum + m;
  s2_sum = s2_sum + s2;
end
pred_time = toc(predstart)/nruns
m = m_sum/nruns;
s2 = s2_sum/nruns;
m20=m(1:20)';
s20=s2(1:20)';
m20
s20
mserror = mse(m, ys, mean(ys), var(ys));
fprintf('normalized mean square error = %.5f\n', mserror);
save(['output/lrankapprox/tmp/' data_set '.mat'], 'hyp', ...
  'xs', 'm', 'ys', 's2', 'mserror', 'pred_time');
disp(['saved results to output/lrankapprox/tmp/' data_set '.mat'])

% plot for 50 points only
nplots = 50;
z=(1:nplots)';
ys = ys(1:nplots,:);
m = m(1:nplots,:);
s2 = s2(1:nplots,:);
figure(2)
set(gca, 'FontSize', 24);
disp(' ');
disp('f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];'); 
f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];
disp('fill([z; flipdim(z,1)], f, [7 7 7]/8);');
fill([z; flipdim(z,1)], f, [7 7 7]/8);

hold on; plot(z, m, 'ro', 'MarkerSize', 5); plot(z, ys, 'b+', 'MarkerSize', 5)
axis([1 length(z) -2.5 2.5])
grid on
xlabel('input, x')
ylabel('output, y')
title(['low-rank prediction for the ' data_set ' data set (avg of ' num2str(nruns) ' runs']);
