function P = lrank_approx_sym(A, P, obs_row_ind, obs_col_ind, ...
  lrank, opt_params)
%LRANK_APPROXIMATION P = lrank_approx_sym(A, obs_row_ind, obs_col_ind, lrank, opt_params)
% 
% Computes a low-rank approximation for a SYMMETRIC matrix A using a subset
% of its entries (treating A as sparse). This function uses Stochastic Gradient
% Descent and Euclidean norm for the error function. The result of this
% function is P such that A ~ PP' with P has lower rank than A.
%
% INPUT
%   - A : a symmetric matrix of size N x N
%   - P : initial low-rank approximation matrix (optional: [])
%   - (obs_row_ind, ob_col_ind) : subscripts of elements in the upper
%   triangular of A that are used for approximation
%   - lrank : the low-rank to use
%   - opt_params : a struct containing configuration for optimization
%       opt_params.alpha : learning rate
%       opt_params.lambda : regularization for the low-rank matrices norm
%       opt_params.max_iters : max iterations to run SGD (optional, default = 1000)
%       opt_params.epsilon : condition for converge (optional, default = 1.e-4)
%
% OUTPUT
%   - P : N x lrank matrix
%
% Author: Trung V. Nguyen
% Last update: 17/07/12
alpha = opt_params.alpha;
lambda = opt_params.lambda;
max_iters = opt_params.max_iters;
epsilon = opt_params.epsilon;

N = size(A, 1);
if isempty(P)
  P = sqrt(max(max(A))/lrank) * rand(N, lrank);
  %P = 0.1*rand(N, lrank);
end  
n = length(obs_row_ind);
error_trace = zeros(max_iters, 1);
for iter = 1:max_iters
  last_error = 0;
  new_error = 0;
  perm = randperm(n);
  % notice that only 2 vectors are changed in each step -> opportunity for
  % parellelisation
  for idx = 1:n
    i = obs_row_ind(perm(idx));
    j = obs_col_ind(perm(idx));
    pi = P(i, :);
    pj = P(j, :);
    error_ij = A(i, j) - pj * pi';
    last_error = last_error + error_ij * error_ij + ...
      lambda/n * ((norm(pj)^2) + (norm(pi)^2));
    P(i, :) = pi + alpha*(error_ij*pj - lambda/n * pi);
    P(j, :) = pj + alpha*(error_ij*pi - lambda/n * pj);
    error_ij = A(i, j) - P(j, :) * P(i, :)';
    new_error = new_error + error_ij*error_ij + ...
      lambda/n * ((norm(P(j, :))^2) + (norm(P(i, :))^2));
  end
  error_trace(iter) = new_error;
  if mod(iter, 10) == 0 || iter == 1
    fprintf('iter %d : error %.5f\n', iter, new_error);
  end
  if (abs(last_error - new_error) < epsilon)
    break;
  end
end

figure(100);
plot(1:length(error_trace), log(error_trace));
title('Error vs. iteration');

end

