function [A, row_ind, col_ind] = subset(A, n, issym, seed)
%SUBSET [A, row_ind, col_ind] = subset(A, n)
% 
% Select n random entries from a matrix A.
%
% INPUT
%   - A : the matrix
%   - n : number of entries to select
%   - issym : true if A is a symmetric matrix in which case only elements
%   in the upper triangular is selected
%   - seed : use this to get repeatable subset of entries
%
% OUTPUT
%   - (row_ind, col_ind)
%
% Author : Trung V. Nguyen
% Last update: 16/07/12
if ~isempty(seed)
  rng(seed, 'twister');
end
[M, N] = size(A);
if issym
  rows = repmat((1:M)', 1, N);
  rows = rows(:);
  cols = repmat(1:N, M, 1);
  cols = cols(:);
  % select only elements in the upper triangular
  valid_ind = rows <= cols;
  rows = rows(valid_ind);
  cols = cols(valid_ind);
  selected_ind = randperm(length(rows), n);
  row_ind = rows(selected_ind);
  col_ind = cols(selected_ind);
else
  row_ind = randsample(M, n, true);
  col_ind = randsample(N, n, true);
end
end

