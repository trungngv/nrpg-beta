function [Ahat, row_ind, col_ind] = nonunif_subset(A, n, issym, seed)
%SUBSET_WITH_PROB Summary of this function goes here
%   Detailed explanation goes here
if ~isempty(seed)
  rng(seed, 'twister');
end
[M, N] = size(A);
b = max(max(abs(A)));
retain = n/N/N;
tau = retain*(A/b).^2;
%p = max(tau, sqrt(tau .* (8*log(N))^4/N));
rand_p = rand(size(tau));
selection = rand_p < tau; % select with probability p_ij
Ahat = zeros(size(A));
Ahat(selection) = A(selection) ./ tau(selection);
[row_ind, col_ind] = find(Ahat);
if issym
  valid_ind = row_ind <= col_ind;
  row_ind = row_ind(valid_ind);
  col_ind = col_ind(valid_ind);
end
end

