%% assume mae, smse are loaded into the workspace
disp('mean(mae) = ')
disp(mean(mae))
disp('std(mae0 = ')
disp(std(mae))
disp('mean(smse) = ')
disp(mean(smse))
disp('std(smse) = ')
disp(std(smse))

%% assume mae, smse are loaded into the workspace
disp('mean(mae) = ')
disp(mean(mae_all))
disp('std(mae0 = ')
disp(std(mae_all))
disp('mean(smse) = ')
disp(mean(smse_all))
disp('std(smse) = ')
disp(std(smse_all))

%% assume mae_pseduo are loaded into the workspace
disp('mean(mae) = ')
disp(mean(mae_pseudo))
disp('std(mae) = ')
disp(std(mae_pseudo))
