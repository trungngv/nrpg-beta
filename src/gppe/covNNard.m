function [A, B] = covNNard(logtheta, x, z)

% Neural network covariance function with Automatic Relevance Determination
% (ARD) for the distance measures. The covariance function is parameterized as:
%
% k(x^p,x^q) = sf2 * asin(x^p'*inv(P)*x^q / sqrt[(1+x^p'*inv(P)*x^p)*(1+x^q'*inv(P)*x^q)])
%
% where the x^p and x^q vectors on the right hand side have an added extra bias
% entry with unit value. P is a diagonal with ARD parameters
% ell_1^2,...,ell_D^2, where D is the dimension of the input space and sf2
% is the signal variance. The hyperparameters are:
%
% loghyper = [ log(ell_1)
%              log(ell_2)
%               .
%              log(ell_D)  
%              log(sqrt(sf2) ]
%
% For more help on design of covariance functions, try "help covFunctions".
%
% Trung V. Nguyen
% 31/07/12
if nargin == 0, A = '(D+1)'; return; end              % report number of parameters
persistent Q
[n D] = size(x);
ell = exp(logtheta(1:D));
x = x*diag(1./ell);
sf2 = exp(2*logtheta(end));
if nargin == 2                                             % compute covariance
  Q = x*x';
  % the denominator is a matrix M where M_pq = u(p)*u(q) where u is
  % sqrt(1+diag(Q))
  K = Q./(sqrt(1+diag(Q))*sqrt(1+diag(Q)'));
  A = sf2*asin(K);                 
elseif nargout == 2                              % compute test set covariances
  z = z*diag(1./ell); 
  Qz = z*z';
  A = sf2*asin(Qz./(sqrt(1+diag(Qz))*sqrt(1+diag(Qz)')));
  B = sf2*asin((x*z')./sqrt((1+diag(Q))*(1+diag(Qz)')));
else                                                % compute derivative matrix
  error('Derivative computation for covNNard not supported');
end
