function [mean_error, errors] =  gppe_make_prediction_users(covfunc_t, covfunc_x, theta, train_t, x, ...
    train_pairs, idx_global, idx_global_1, idx_global_2, ind_t, ind_x, test_t, idx_pairs, ftrue, ytrue)
%  fstar =  gppe_make_prediction_new_users(covfunc_t, covfunc_x, theta, train_t, x, ...
%     train_pairs, idx_global, idx_global_1, idx_global_2, ind_t, ind_x, test_t, idx_pairs, ftrue, ytrue)
%
% Makes predictions for users (either unseen users) or seen users with
% unseen/test data.
%
% INPUT:
%   - covfunc_t: Covariance function on user space
%   - covfunc_x: Covariance function on item space
%   - theta = [theta_t; theta_x; theta_sigma]: vector of hyperparameters
%       theta_t and theta_x are the hyperparameters of the covariences. 
%       theta_sigma = log (sigma).
%   - train_t: Users' features
%   - x: Items' features
%   - train_pairs: Cell array of M elements. Each element is a O_m x 2 matrix 
%       where O_m is the number of preferences observed for the corresponding
%       user. Each row all_pairs{m} contains a preference relation 
%       of the form train_pairs{m}(1) > train_pairs{m}(2)     
%   - idx_global: The unique global indices of the observed preferences
%   - idx_global_1: The global indices of the first objects in the preferences
%   - idx_global_2: The gobal indices of the second objects in the preferences
%   - ind_t: Indices of seen tasks
%   - ind_x: Indices of seen items
%   - test_t: Matrix of test users features 
%   - idx_pairs: The matrix of all pairwise item comparisons:
%           idx_pairs(i,1) > idx_pairs(i,2) 
%   - ftrue: The true value of utility function of test user (used for
%       evaluation)
%   - ytrue: Binary matrix indicating if the corresponding 
%       item comparisons hold for the test users (one column for each user)
% 
% OUTPUT:
%   - vector of prediction errors for each test user
%
% trung v. nguyen (trung.ngvan@gmail.com)
% last update: 27 june 2012

N      = size(x,1);
Mtrain = size(train_t,1);
[f Kx, Kinv, W, L]  = approx_gppe_laplace_fast(covfunc_t, covfunc_x, ...
    theta, train_t, x, train_pairs, idx_global, idx_global_1, idx_global_2, ind_t, ind_x, Mtrain, N);

ntest = size(test_t, 1); all_errors = []; errors = zeros(ntest, 1);
%ferrors = zeros(ntest, 1);
disp('making prediction for users: (user, #predictions)');
for ti = 1:ntest
  Npairs = size(idx_pairs,1);
  Fstar  = NaN(N,Npairs);
  P = zeros(Npairs,1);
  for i = 1:Npairs
    pair = idx_pairs(i,:);
    [p mustar] = predict_gppe_laplace(covfunc_t, covfunc_x, theta, f, Kx, ...
        Kinv, W, L, train_t, x, idx_global, ind_t, ind_x, test_t(ti, :), pair);
    P(i) = p;    
    Fstar([pair(1), pair(2)],i) = mustar;
  end
  % pairwise prediction
  fstar = mynanmean(Fstar, 2);
  ypred = P > 0.5;
  test_idx = find(ytrue(:,ti) ~= -1);
  test_idx = prune_low_utility_items(test_idx, idx_pairs, fstar, 0.5);
  if ~isempty(test_idx)
    errors(ti) = sum(ytrue(test_idx, ti) ~= ypred(test_idx),1)/ length(test_idx);
    all_errors = [all_errors; ytrue(test_idx, ti) ~= ypred(test_idx)];
  else
    errors(ti) = -1;
  end
  fprintf('%d (%d), ', ti, length(test_idx));
  if mod(ti, 10) == 0
    fprintf('\n');
  end
  
  % utility prediction: bad idea; utility values learned here are meant to
  % be comparative only -> futile to evaluate against ratings
%   if ~isempty(ftrue)
%     fstar = mynanmean(Fstar, 2);
%     fstar = fstar / max(fstar); % standardize (normalize)
%     observed_idx = find(ftrue(:, ti) ~= 0);
%     ferrors(ti) = rms(fstar(observed_idx) - ftrue(observed_idx, ti));
%   end  
end
fprintf('\n');
mean_error = mean(all_errors);
fprintf('errors (aggregated from all users)  = %.4f (%d instances)\n',...
  mean_error, length(all_errors));

% ystar = ( fstar(idx_pairs(:,1)) - fstar(idx_pairs(:,2)) ) > EPSILON; 
%fprintf('root mean squared error: mean = %.3f, std = %.3f\n', mean(ferrors), std(ferrors));


return;

% remove all pairs where both items have low utilities
function retain_idx = prune_low_utility_items(idx, idx_pairs, fstar, topk)
  [dummy sorted_idx] = sort(fstar, 'descend');
  topk_idx = sorted_idx(1:floor(topk*length(fstar)));
  retain_idx = [];
  for i=1:length(idx)
    pair_idx = idx(i);
    pair = idx_pairs(pair_idx,:);
    if ismember(pair(1),topk_idx) || ismember(pair(2),topk_idx)
      retain_idx = [retain_idx; pair_idx];
    end
  end
return;