function [nl grad_theta] = learn_gppe_nonstationary(theta, covfunc_t, covfunc_x, t, x, all_pairs, ...
        idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N)
% [nl grad_theta] = learn_gppe(theta, covfunc_t, covfunc_x, t, x, all_pairs, ...
%        idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N)
%
% Computes the negative marginal likelihood and its gradients wrt to the
% hyper-paramters using non-stationary kernel. This can be used to learn
% a gppe model with Carl Rasmussen's minimize function.
%
% INPUT:
%   - theta = [theta_t; theta_x; theta_sigma]: vector of hyperparameters
%       theta_t and theta_x are the hyperparameters of the covariences. 
%       theta_sigma = log (sigma)
%   - covfunc_t: Covariance function on user space
%   - covfunc_x: Covariance function on item space 
%   - t: Users' features
%   - x: Items' features
%   - all_pairs: Cell array of M elements. Each element is a O_m x 2 matrix 
%       where O_m is the number of preferences observed for the corresponding
%       user. Each row all_pairs{m} contains a preference relation 
%       of the form all_pairs{m}(1) > all_pairs{m}(2)     
%   - idx_global: The unique global indices of the observed preferences
%   - idx_global_1: The global indices of the first objects in the preferences
%   - idx_global_2: The gobal indices of the second objects in the preferences
%   - ind_t: Indices of seen tasks
%   - ind_x: Indices of seen items
%   - M: The number of users
%   - N: The number of items
%
% OUTPUT:
%   - nl: The negative marginal log likelihood
%   - grad_theta: The gradients of the negative marginal log likelihood wrt
%       to the hyper-parameters

% trung v. nguyen  (trung.ngvan@gmail.com)
% last update: 27 june 2012

% TODO(trung):
% - implement the non-stationary covariance function that conforms to the
% convetion/signature in the gpml library so that other code still works. 
%
% theta_x, theta_sigma are different for each covariance function so if the
% new implementation fails to abide to the gpml interface, then would also
% need to change get_gppe_parameters (and probably some others).

global all_diag_idx;
n = M*N;
all_diag_idx = sub2ind([n,n],1 : n, 1 :n);
nl = nmll(theta, covfunc_t, covfunc_x, t, x, all_pairs, ...
        idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N);
% TODO(trung): return func val at theta so as to save one computation for
% nl (although grad3 does not have that)
grad_theta = numeric_grad3(@nmll, theta, covfunc_t, covfunc_x, t, x, ...
  all_pairs, idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N);
fprintf('nmll=%.2f\n', nl);
%fprintf('theta= %.2f', theta);
%fprintf('\n');

end

% evaluate the negative marginal log-likelihood at theta
function nl = nmll(theta, covfunc_t, covfunc_x, t, x, all_pairs, ...
        idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N)
theta_sigma = theta(end);
sigma = exp(theta_sigma); 
[fhat Kx, Kinv, W, L] = approx_gppe_laplace_fast(covfunc_t, covfunc_x, theta, t, ...
    x, all_pairs, idx_global, idx_global_1, idx_global_2, ind_t, ind_x, M, N);
fvis = fhat(idx_global); % f visible
cond_loglike = log_likelihood_gppe(fhat, sigma, all_pairs, idx_global_1, idx_global_2, M, N);
margl = -0.5*( - log(det(Kinv)) + 2*sum(log(diag(L))) ) - 0.5*fvis'*Kinv*fvis ...
    + cond_loglike;
nl = - margl;
end