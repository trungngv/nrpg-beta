% Gaussian Process Preference Elicitation (GPPE)

%%
% The workflow of this package is as follows:
%
% 1) Create new functions for communication between the package and a new
% dataset
%
% 2) Learn the hyperparameters with one of the two minimization procedures:
% learn_gppe_with_minimize or learn_gppe_with_netlab.
%
% Within learn_gppe_with_minimize, a function that computes the objective
% function and gradients is required (as usual in gradient-based
% optimization method). That function in this package is learn_gppe.
%
% 3) Use the learned hyperparameters to make predictions with the function
% make_predictions_new_user.
% Within make_predictions_new_user:
% - posterior distribution is computed with approx_gppe_laplace_fast
% - utility prediction is made with predict_gppe_laplace
% - pairwise preference is made (decision-theorectic) based on the
% likelihood model
% - evaluation can be done with the true utility function (if known) and
% the true pairwise preference (usually known in preference setting)

%%
% VARIABLES AND DATA STRUCTURES

% Problem Variables
% - M : int, number of users/tasks
% - N : int, number of items
% - D_t : int, dimension of user features
% - D_x : int, dimension of item features

% Indexing of the Joint User-Item Space
% In multi-output settings, we deal with the joint space of user-item and
% so there is a need to jointly index user and item. In this package
% all pairs of users and items are assigned an index from 1 to NxM
% with the help of this auxiliary matrix IND (size N x M):
% IND = [ 1 N+1 .. (M-1)N + 1
%         2 N+2 .. (M-1)N + 2 
%         . .   .. .
%         N N+N .. (M-1)N + N]

% With IND, the following are equivalent ways of identifying a pair (j,i)
% of user j and item i:
% - IND(i, j) 
% - (j - 1) * N + idx (this will be convenient for indexing a set of items
% for a particular user j)
% - sub2ind(size(IND), i, j)
%
% The observed data points (pairs of users and items) are only SUBSET of IND
% and are indexed by:
% - (idx_global_1, idx_global_2) : all observed pairwise preferences.
% a pair of elements from idx_global_1 and idx_global_2 such as k1, k2
% can be decoded to get user idx (1..M) and item idx (1..N). 
% Basically it is vec(IND) after removing points that are not observed.
% - idx_global: unique indice of all observed pairs
% - ind_t : indice of seen tasks/users
% - ind_x : indice of seen items
% Note that [ind_t ind_x] = ind2sub([N M], idx_global) due to the way we
% index pairs of users and items. These are needed because the kernel is a
% factorized kernel and so covariance of users and items are computed
% separately. 
%
% Aside note: mtgp does this indexing in the opposite direction. It first
% creates the indice of all tasks/users and items.
% v = repmat(1:M,N,1)
% ind_kt = v(:)
% v = repmat((1:N)',1,M)
% ind_kx = v(:)
%
% Then it selects some of the indices for training.
% rnd_idx = randperm(N)
% train_idx = rnd_idx(1:floor(0.1*N*M))
% ind_t = ind_kt(train_idx)
% ind_x = ind_kx(train_idx)
% 

% Feature Matrices
% - train_t : M x D_t matrix, each row is the feature vector t^(j)' of user j
% - x : N x D_x matrix, each row is the feature vector x^(i)' of item i
% - test_t : 1 x D_t matrix, features of test users (similar to train_t)
% - test_idx : indice of test users

% Preference Pairs
% - train_pairs : M x 1 cell, where each cell train_pairs{j} is a matrix of
%                 observed pairwise preference for user j.
% - idx_pairs : C(N,2) x 2 matrix, all possible pairs of items (obtained by
%                 combnk(1:N, 2)

% Utility Functions
% F : N x M matrix, where each column is f^(j), the utilities of all items
% for user j
% f = vec(F) = F(:), the vector form of F, hence f ~ N(0, kron(K_t, K_x))
% Y : C(N,2) x M, pairwise preference of all users for all possible pairs
% (in idx_pairs)
% ftrue (or ftruetest) : true utilities of the test users (sub-matrix of F)
% ytrue (or ytruetest) : true pairwise preference of the test users
% (sub-matrix of Y)

% Hyperparameters
% theta = [theta_t; theta_x; theta_sigma]: vector of hyperparameters
%       theta_t and theta_x are the hyperparameters of the covariences. 
%       theta_sigma = log (sigma).

% TODO: understand/explain the communication between dataset and the
% prediction function

function gppe_prediction()
% A toy example demonstrating the use of the gppe package to verify the
% effect of using non-stationary utility function with the sushi dataset.
% The most important blocks in the code are elicit_gppe which carries out
% the elicitation process and the blocks corresponding to hyper-parameter
% learning.
%
% For a new dataset it is necessary to create the corresponding data
% structures (see elicit_gppe) and, more importantly, create new functions 
% for the pointers ptr_query_func and ptr_loss_func. These functions make 
% a query on a test user and compute the corresponding loss of making a
% particular recommendation respectively. See make_query_toydata.m and
% loss_query_toydata.m for more details on these functions.
% 

% trung v. nguyen (trung.ngvan@gmail.com)
% last update: 30/07/2012
%
p = genpath('external'); addpath(p);
p = genpath('scripts');  addpath(p);
p = genpath('utils');  addpath(p);

ITEMS_FEATURES = 'data/sushi3/sushi3.idata';
USERS_FEATURES = 'data/sushi3/sushi3.udata';
USERS_ORDERS = 'data/sushi3/sushi3a.5000.10.order';

dataset = 'sushi';
cov_t = 'covSEard';
cov_x = 'covSEard';
covfunc_t      = check_covariance(cov_t);
covfunc_x      = check_covariance(cov_x);
Ntrain = 10; Mtrain = 30;

filename = ['output/gppe/' dataset '-N' num2str(Ntrain) '-M' num2str(Mtrain) '-' cov_t '-' cov_x '.txt3'];
fid = fopen(filename, 'w+');
fprintf(fid, 'Covariance functions: users=%s, items=%s\n', cov_t, cov_x);
fprintf(fid, 'Ntrain = %d, Mtrain = %d\n', Ntrain, Mtrain);
fprintf(fid, 'Training pairs / All pairs = %.2f\n', 0.3);
fprintf(fid, 'Evaluation: topk = 0.5\n');

rngs = [1, 12, 239, 200, 100, 199, 166, 799, 3533, 5333, 779313];
niters=10;
iter_errors = zeros(niters,1);
for i=1:niters
rng(rngs(i), 'twister');
fprintf(fid, '==============================================\n');
fprintf(fid, 'RUN %d\n', i);
fprintf(fid, '==============================================\n');

fprintf('running iteration %d\n', i);
[idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] = ...
    generate_sushi_data(USERS_FEATURES, ITEMS_FEATURES, USERS_ORDERS, Ntrain, Mtrain);
% [idx_pairs, train_pairs, train_t, x, test_t, test_idx, ftrue_test, ytrue_test] = ...
%     generate_mvlens_data();

% init model
N = size(x, 1);
[covfunc_t, covfunc_x, theta] = init_data(train_t, x, covfunc_t, covfunc_x);
M           = length(train_pairs); % Update number of users to training users
[idx_global_1, idx_global_2] = compute_global_index(train_pairs, N);
idx_global    = unique([idx_global_1; idx_global_2]);
[ind_x ind_t] = ind2sub([N M], idx_global); % idx of seen points and tasks

% learn hyp
tic;
theta0 = rand(size(theta));
try
theta_learned = learn_gppe_with_minimize(covfunc_t, covfunc_x, ...
      theta0, train_t, x, train_pairs, idx_global, ...
      idx_global_1, idx_global_2, ind_t, ind_x, M, N);
disp('learned theta:')
disp(theta_learned')

% make prediction after learning hyp
[mean_error errors] = gppe_make_prediction_users(covfunc_t, covfunc_x, ...
  theta_learned, train_t, x, train_pairs, idx_global, idx_global_1, idx_global_2, ...
  ind_t, ind_x, test_t, idx_pairs, ftrue_test, ytrue_test);
iter_errors(i) = mean_error;  
fprintf(fid, 'mean error: %.4f\n', mean_error);
fprintf(fid, 'errors for each test user:\n');
fprintf(fid, '%.4f ', errors);
fprintf(fid, '\nexecution time %.2f (secs)\n', toc);
fprintf(fid, '\n');

fprintf('\nmean error: %.4f\n', mean_error);
fprintf('execution time %.2f (secs)\n', toc);
catch err
  % ignore error to continue running the next experiment
end

end

iter_errors = nonzeros(iter_errors);
fprintf(fid, '\nIterations without error: %d', length(iter_errors));
fprintf(fid, '\nERROR: AVG = %.4f, STD = %.4f\n', mean(iter_errors), std(iter_errors));
fclose(fid);

return;

%% Init data for the model
function [covfunc_t, covfunc_x, theta] = init_data(t, x, covfunc_t, covfunc_x)
%theta is [hyp(covfunc_t); hyp(covfunc_x); sigma]
D = size(t,2);   ntheta_t = eval(feval(covfunc_t{:}));
D = size(x,2);   ntheta_x = eval(feval(covfunc_x{:}));
theta = zeros(ntheta_t + ntheta_x + 1, 1);

return;

    