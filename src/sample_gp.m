% Generate sample functions from a Gaussian Process
function sample_gp()

  % plot the input points x versus output points y
  function plotResults(x, y, color)
    figure(1); hold on;
    plot(x, y, color);
    %plot(x, y);
  end

  % sample from a multivariate normal distribution N(mu, K)
  function f = sampleGaussian(mu, K, n)
    % A function f is represented by its values at the input points X.
    % By GP property, f = (f(x1), f(x2), ..., f(xN)) ~ N(mean(X), K)

    % 1. Sample n i.i.d points from the standard normal N(0, 1)
    u = randn(n, 1); % u ~ N(0, I)

    % 2. Find a matrix A such that K = AA'
    %A = chol(K)'; % K = AA'
    % svd decomposition as K might not be positive definite, K = ASB'
    [A, S, B] = svd(K);

    % 3. By the affine property of gaussian distribution:
    % Au + mean(X) ~ N(mean(X), AIA') = N(mean(X), K)
    % f = A * u
    f = mu + A * sqrt(S) * u; % use A * S^0.5 as svd decomposition is being used
  end

  % Some sample kernels (for 1-D input dimension)
  linearKern = @(x, y) x'*y;
  % if x and y are not scalar, use positive diagonal matrix L instead of scalar
  l = 0.7;
  gaussKern = @(x, y) exp(-0.5*(x-y).'*(1/l)*(x-y));
  % non-stationary kernel 1: x'y (x and y are column vectors)
  kern1 = @(x, y, p) (x * y)^p;
  kern2 = @(x, y) exp(1 - x - y);
  
  % 1. Generate input points and initialize
  %x = linspace(0, 2, 200)';
  x = (0:0.01:2)';
  n = length(x);

  % 2. Specify mean (=0), kernel functions and compute the cov matrix
  % using the kernel function
  kernel = kern1;
  C = zeros(length(x), length(x));
  for i=1:length(x)
    for j=1:length(x)
      C(i, j) = kernel(x(i), x(j), 2);
    end
  end
  
  % 3. Sample function f from the GP.
  %rng(1000, 'twister');
  f = mvnrnd(zeros(1,n), covLINard([log(1)], x));
  %f = mvnrnd(zeros(1,n), C);
  %f = sampleGaussian(zeros(n, 1), C, n);
  plotResults(x, f, 'b');
  
end
