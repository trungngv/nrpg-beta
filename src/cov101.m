% kernel (covariance) functions
kernel = 6;
switch kernel
    case 1; k = @(x,y) 1*x'*y; % linear
    case 2; k = @(x,y) 1 * min(x, y); % brownian
    case 3; k = @(x,y) exp(-100*(x-y)'*(x-y)); % squared exponential
    case 4; k = @(x,y) exp(-1*sqrt((x-y)'*(x-y))); % ornstein-uhlenbeck
    case 5; k = @(x,y) exp(-1*sin(5*pi*(x-y))^2);% periodic
    case 6; k = @(x,y) exp(-100*min(abs(x-y),abs(x+y))); % symmetric
end

x = (0:0.005:1);
y = (0:0.005:1);
n = length(x);
C = zeros(n, n);
for i=1:n
  for j=1:n
    C(i, j) = k(x(i), x(j));
  end
end

mesh(x, y, C);