function test_mc_approximation(nsamples)
%TEST_MC_APPROXIMATION Test the mc_approximation function
%
% Author: Trung V. Nguyen
% Last update: 04 Jul 2012

% We use f(x) = x^2 and X ~ N(mu, sigma^2) so that
% E[f(x)] = E[X^2] = var(X) + E[X]^2 = sigma^2 + mu^2.
% 
mu = 5;
sigma2 = 1;
ef = mc_approximation(@f, @sample_pdf, nsamples, mu, sigma2);
ef_true = sigma2 + mu ^ 2;
fprintf('true value: %.5f, approximation: %.5f, absolute error: %.5f', ...
  ef_true, ef, abs(ef_true - ef));
end

function val = f(x, varargin)
val = x ^ 2;
end

function s = sample_pdf(mu, sigma2)
s = mu + sqrt(sigma2) * randn;
end