function theta = gprn_put_parameters(theta_f, theta_w, sigma_f, sigma_y)
%GPRN_PUT_PARAMETERS theta = gprn_put_parameters(theta, theta_f, theta_w, sigma_f, sigma_y)
%   Compose the vector containing all hyperparameters in the gprn model
%   from the respective hyperparameters of different components.
%
% INPUT: 
%   theta_f, theta_w : hyperparameters of the covariance functions for f and w
%   sigma_f : the noise associating with latent functions
%   sigma_y : output noise
%
% OUTPUT:
%   theta : vector containing all hyperparameters
%
% See also: gprn_get_parameters.m
% 
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 03 Jul 2012
%
theta_f = reshape(theta_f, length(theta_f), 1);
theta_w = reshape(theta_w, length(theta_w), 1);
theta = [theta_f; theta_w; sigma_f; sigma_y];

end
