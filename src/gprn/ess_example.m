% example usage of elliptical slice sampling
function ess_example()
x = (-pi:0.26:pi)';
y1 = sin(x) .^ 2 + 0.01 * randn();
y2 = cos(x) .^ 2 + 0.01 * randn();

% compute chol(Sigma) -- the cholesky decomposition of the covariance
% matrix of prior with hyperparameter \sigma_f = 1, ell_1 = 1
Sigma1 = covSEard([0; 0], x); % block for f1
Sigma2 = covSEard([0; 0], x); % block for f2
Sigma = blkdiag(Sigma1, Sigma2); % (block diagonal blockdiag(Sigma1, Sigma2))
prior = chol(Sigma);

% get samples from p(f|D,hyp)
nsamples = 100;
burnins = 100;
% initial assignment of the chain (simply choose 0)
N = size(x, 1);
sample = repmat(0, 2 * N, 1);
% fsamples(:, i) is the ith sample (first half is f1, second half is f2)
% IMPORTANT: we are trying to sample for the parameters (latent functions) 
% f1, f2 EVALUATED at training points (hence dimension is quite large)
fsamples = zeros(2 * N, nsamples);
log_likelihood = zeros(burnins + nsamples, 1);
for i = 1:burnins + nsamples
  [sample, log_likelihood(i)] = elliptical_slice(sample, prior, @loglikelihood, [], [], y1, y2)
  if i > burnins
    fsamples(:, i - burnins) = sample;
  end
  size(sample)
end

% compute the average and display f1, f2
fPosterior = mean(fsamples, 2)
figure(1); hold on;
plot(x, fPosterior(1:N), 'm');
plot(x, fPosterior(N+1:end), 'c');
plot(x, sin(x) .^ 2, 'g');
plot(x, cos(x) .^ 2, 'b');
legend('f1', 'f2', 'y1', 'y2');
title('approximation with elliptical slice sampling');

figure(2); hold on;
plot(1:burnins + nsamples, log_likelihood);
title('log likelihood');

% the log likelihood function
% f is the current assignment to the posterior p(f|D,hyp)
% in this example, f = [f1; f2] where f_i = [f_i(x1), ..., f_i(xN)]
function llh = loglikelihood(f, y1, y2)
  assert(all(size(y1) == size(y2)), 'y1 and y2 must have the same size');
  noiseVariance = [0.01, 0; 0, 0.01];
  N = size(y1, 1);
  f1 = f(1:N);
  f2 = f((N + 1):end);
  f = [f1, f2];
  % product of a gaussian dist for each data point in [y1, y2]
  mvnpdfs = mvnpdf([y1, y2], f, noiseVariance);
  llh = log(prod(mvnpdfs));
end

end