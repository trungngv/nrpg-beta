function ystar = mean_prediction2(xstar, theta, Usamples, params)
%MEAN_PREDICTION mean_ystar = mean_prediction2(xstar, theta, Usamples, params)
%
% Computes the predictive mean E[ystar|D,theta,xstar] for a test point.
% This can ONLY be used for models where ystar directly relates to the outputs
% rather than be comprised of Wf like GPRN. In such model, this function
% should be adapted to such setting.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/2012
%
covfunc_f = params.covfunc_f;
covfunc_w = params.covfunc_w;
X = params.X;
num_outputs = params.num_outputs;
num_latents = params.num_latents;
[theta_f, theta_w, ~, ~] = gprn_get_parameters(theta, params);
Kf_inv = inv(params.Kf);
Kw_inv = inv(params.Kw);
Kinv = blkcov(Kf_inv, Kw_inv, params);

[~, kf_s] = feval(covfunc_f, theta_f, X, xstar);
[~, kw_s] = feval(covfunc_w, theta_w, X, xstar);
Ks = blkcov(kf_s, kw_s, params);
alpha = Ks' * Kinv;
% do this for every test point xstar
sum_ustar = 0;
nsamples = size(Usamples,2);
for i = 1 : nsamples
  sum_ustar = sum_ustar + alpha * Usamples(:,i);
end
ustar_mean = sum_ustar / nsamples;
fstar = ustar_mean(1:num_latents);
wstar = ustar_mean(num_latents + 1 : end);
Wstar = reshape(wstar, num_outputs, num_latents);
ystar = Wstar * fstar;
end

