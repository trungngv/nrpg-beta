% Generate data (utility functions) using the GPRN model
x = linspace(0, 2, 200)'; % N = 100
input_dimensions = 1; % D = 1
num_outputs = 4; % p
num_latent_funcs = 2; % q
sigma_f = 0.005; % latent func noise
sigma_y = 0.005; % output nosie
theta_w = log(0.2); % D x 1 -- log(ell^2)
theta_f = [log(1); log(0.5); log(1); log(0.5)]; % q x D -- each row has same role as theta_w
%theta_f = [log(4.5); log(0.3)]; % exp4
%theta_f = [log(1); log(0.5)]; %exp3
% real-correlations experiments: seed = 110, 11, 1110
rng(110, 'twister');
[X, Y, f, W] = gprn_synthetic(theta_f(1:num_latent_funcs), theta_w, sigma_f, sigma_y, x, num_outputs);
%[X, Y, f, W] = gprn_utility_funcs(theta_f(1:num_latent_funcs), theta_w, sigma_f, sigma_y, x, num_outputs);

% plot the samples
figure(1); hold on;
colors = ['b', 'g', 'm', 'r', 'c'];
for i = 1:size(f,1)
  plot(x, f(i, :)', colors(i));
end
legend(num2str((1:num_latent_funcs)'));
title('GPRN -- weight and latent functions')

figure(2); hold on;

for i = 1:num_outputs
  plot(x, Y(:, i), colors(i));
end
title('GPRN -- utility functions');
legend(num2str((1:num_outputs)'));

% save data
N = size(X, 1);
testIdx = rnsubset(0.2*N,N);
%testIdx = find((X > 0.4 & X < 0.7) | (X > 1.0 & X < 1.4));
trainingIdx = setdiff(1:N, testIdx);
save_data(X(trainingIdx,:), Y(trainingIdx,:)', X(testIdx,:), Y(testIdx,:)', 'data', 'gprnx');
