function [Fhat, W] = sample_posterior(theta, params, nsamples, burnin)
%SAMPLE_LATENT_POSTERIOR [fhat, w] = sample_posterior(theta, params, nsamples, burnin)
% 
% Samples the latent and weight functions from its posterior p(f, w | D, theta).
%
% INPUT
%   - theta : the learned hyperparameters
%   - params : the model's configurations
%   - nsamples : number of samples to take
%   - burnin : burn-in iterations before collecting samples
%
% OUTPUT
%   - Fhat : a matrix where each column is one sample of the latent functions
%   - W : a matrix where each column is one sample of the weight functions
%
% See also gprn_data_structures.m to see how f and w is represented.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 09 Jul 2012
%
if nargin == 3 || isempty(burnin)
  burnin = 1000;
end
[fhat0, w0, Kfhat_chol, Kw_chol] = sample_prior(params.X, theta, params, [], []);
u = [fhat0; w0];
Fhat = zeros(size(fhat0, 1), nsamples);
W = zeros(size(w0, 1), nsamples);
[~, ~, ~, sigma_y] = gprn_get_parameters(theta, params);
sigma_y=exp(sigma_y);
cur_log_likelihood = loglikelihood(u, sigma_y, params);
fprintf('sampling posterior...\n');
tic;
for i = 1:burnin + nsamples
  [fhat0, w0, ~, ~] = sample_prior(params.X, theta, params, Kfhat_chol, Kw_chol);
  u0 = [fhat0; w0];
  [u, cur_log_likelihood] = elliptical_slice(u, u0, @loglikelihood, ...
    cur_log_likelihood, [], sigma_y, params);
%  u = elliptical_slice(rand(size(u0)), u0, @loglikelihood, [], [], sigma_y, params);
  if i > burnin
    [fhat, w] = u_to_fhat_w(u, params);
    Fhat(:, i - burnin) = fhat;
    W(:, i - burnin) = w;
  end
  if i == burnin
    fprintf('\tburned in\n');
  end
end
fprintf('done (%.fs)\n', toc);

end

  function llh = loglikelihood(u, sigma_y, params)
    [fhat, w] = u_to_fhat_w(u, params);
    llh = gprn_loglikelihood(fhat, w, sigma_y, params);
  end

