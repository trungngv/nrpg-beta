% function [X, Ytrue, Ynoise, f, W] = gprn_utility_funcs(theta_f, theta_w, sigma_f, sigma_y, X, num_outputs, standardized)
%
% Generates outputs from the GPRN model (see Gaussian Process Regression Networks) paper for details of the model.
% All kernels used in this function is ARD squared exponential.
%
% NOTE: the hyperparameters for kernel functions are in the same form as
% that being used in the gpml-matlab library. In other words, theta_w described below
% (and theta_f(j, :) has size D x 1 and
% theta_w = [ log(ell_1)
%             log(ell_2)
%             .
%             log(ell_D)]
% and the squared exponential kernel is k(x,s) = exp(-(x-s)'*inv(P)*(x-s))
% where P = diag(ell_1^2,..., ell_D^2).
%
% Parameters
%   theta_f : hyperparameters of the kernels for latent nodes (functions) f_j.
%             It must be a matrix of size q x (D +1) where q is the number of latent functions and
%             D is the input dimension.
%   theta_w : hyperparameters of the kernels for weight functions w_ij (all weight functions
%             share the same set of parameters) (size (D + 1)x 1).
%   sigma_f : the standard deviation of latent function noise (i.e., \epsilon ~ N(0, sigma_f^2))
%   sigma_y : the standard deviation of the output noise (i.e., \z ~ N(0,
%             sigma_y^2)
%   X       : N x D matrix of input data where N is the number of data
%             points and D is the input dimension.
%   num_outputs: the output dimension
%
% Returns
%   X : N x D feature matrix
%   Ytrue : N x p output matrix (without noise)
%   Ynoise: N x p output matrix (with niose)
%   f : q x N matrix (latent functions)
%   W : Np x q matrix (weight functions)
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 11/07/2012
%
function [X, Ytrue, Ynoise, f, W] = gprn_utility_funcs(theta_f, theta_w, sigma_f, ...
  sigma_y, X, num_outputs, standardized)
  [N, D] = size(X);
  theta_w = reshape(theta_w, D + 1, 1);
  num_latent_funcs = size(theta_f, 1);
  if (size(theta_f, 2) ~= D + 1)
    error('theta_f must have size q x (D + 1). See documentation for details');
  end
  
  % generate all latent node functions
  % f is q x N and each row is f_j = f(j, :) = [f_j(x1); f_j(x2); ...; f_j(xN)]
  f = zeros(num_latent_funcs, N);
  for j=1:num_latent_funcs
    Kf = covSEard(theta_f(j, :)', X);
    Kfhat = Kf + (sigma_f^2) * eye(N);
    f(j, :) = mvnrnd(zeros(1,N), Kfhat);
  end
  
  % generate all weight functions
  % W{i,j} is W_ij, the weight functions associated with output i and latent function j
  W = cell(num_outputs, num_latent_funcs);
  Kw = covSEard(theta_w, X);
  for i=1:num_outputs
    for j=1:num_latent_funcs
      W{i,j} = mvnrnd(zeros(N, 1), Kw)'; % mvnrnd returns 1xN
    end
  end
  % cell2mat will convert the cell into a block matrix and so W_i(xn) =
  % [W_i1(xn), ..., W_iq(xn)] = W(N*(i-1) + xn, :)
  W = cell2mat(W);
  
  % generate the outputs
  Y = zeros(N, num_outputs);
  outputIdx = (1:num_outputs)';
  for n=1:N
    Y(n, outputIdx) = W(N*(outputIdx-1)+n,:)*f(:,n); % y(x) = W(x)fhat(x)
  end
  
  % independent noises
  Ytrue = Y;
  Ynoise = Y + sigma_y*randn(size(Y));
  
  n = min(size(X,1),5);
  disp('Kfhat(1:5,1:5)')
  disp(Kfhat(1:n,1:n))
  disp('Kw(1:5,1:5)')
  disp(Kw(1:n,1:n))
  disp('[Ytrue(1:5,:), Ynoise(1:5,:)]')
  disp([Ytrue(1:n,:), Ynoise(1:n,:)])
end