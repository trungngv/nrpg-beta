function p_ustar = gprn_predictive(theta, ustar, xstar, Usamples, Kinv, params)
%GPRN_PREDICTION p_ustar = gprn_prediction(theta, ustar, xstar, U_samples, Kinv, params)
% 
% Compute the predictive distribution p(ustar | D, gamma) with Monte Carlo
% approximation.
%
% INPUT
%   - ustar : predictive value
%   - xstar : a test instance
%   - U_samples : samples from the posterior distribution (think of it as
%   the posterior distribution itself)
%   - Kinv : Kyy^{-1}
%   - params
%
% OUTPUT
%   - p_ustar : the approximation p(ustar|D, gamma)
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 10/07/2012
%
nsamples = size(Usamples, 2);
X = params.X;
covfunc_f = params.covfunc_f;
covfunc_w = params.covfunc_w;
num_outputs = params.num_outputs;
num_latents = params.num_latents;
[theta_f, theta_w, ~, ~] = gprn_get_parameters(theta, params);
sum_p = 0;
for i = 1 : nsamples
  [kf_ss, kf_s] = feval(covfunc_f, theta_f, X, xstar);
  [kw_ss, kw_s] = feval(covfunc_w, theta_w, X, xstar);
  Ks = blkcov(kf_s, kw_s, params);
  Kss = blkdiag(kf_ss * eye(num_latents), kw_ss * eye(num_outputs * num_latents));
  tmp = Ks' * Kinv;
  mean_s = tmp * Usamples(:, i);
  cov_s = Kss - tmp * Ks + 0.001 * eye(length(mean_s)); % add jitter
  sum_p = sum_p + mvnpdf(ustar, mean_s, cov_s);
end
p_ustar = sum_p / nsamples;
fprintf('gprn_predictive p(u*|D, theta) = %.10f\n', p_ustar);
end

