function logl = gprn_loglikelihood(fhat, w, lsy, params)
%GPRN_NMLL logl = gprn_loglikelihood(fhat, w, lsy, params)
%
% Computes the log likelihood log p(Y | fhat, w, sigma_y).
%
% INPUT
%   fhat : latent functions
%   w : weight functions
%   lsy : the output noise parameter log(sigma_y)
%   params : a (global) struct setting of the gprn model
%
% OUTPUT
%   the likelihood value
% 
% See also gprn_data_structures.m for convention of how data is
% represented.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 17/08/2012
%

Y = params.Y; N = size(Y, 1);
P = params.num_outputs;
Q = params.num_latents;
Wmat = reshape(w, N*P, Q);
Fhat = reshape(fhat, N, Q);
sy2 = exp(2*lsy) + 1e-15;

Wxn_ind = N*(0:P-1)';
W_ind = bsxfun(@plus,Wxn_ind,1:N);
Wblk = Wmat(W_ind(:),:);
tt = repmat(1:N,P,1);
Fblk = Fhat(tt(:),:);
Ymean = reshape(sum(Wblk.*Fblk,2),P,N)';
logl = -0.5*sum(sum((Y-Ymean).^2))/sy2;
logl = logl - N*0.5*P*log(2*pi*sy2);

end

