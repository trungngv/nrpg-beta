function test_gprn_learn_em()
%TEST_GPRN_LEARN test_gprn_learn_em()
%   
% Test for the gprn_learn_em.m function.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 12/07/2012
%
rng(23, 'twister');
N = 10; D = 2; num_outputs = 3; num_latents = 2;
covfunc_f = 'covSEard'; covfunc_w = 'covSEard';
sigma_f = 0.0001; sigma_y = 0.1;
theta_f = [log(rand(D, 1)); log(0.1+rand/2)];
theta_w = [log(rand(D, 1)); log(0.1+rand/2)];
theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
% gprn_utility_funcs requires theta_f as a matrix
theta_ff = repmat(theta_f', num_latents, 1);
X = rand(N, D);
[~, Ytrue, Ynoise, Ftrue, Wtrue] = gprn_utility_funcs(theta_ff, theta_w,...
  sigma_f, sigma_y, X, num_outputs, true);
Y = Ynoise(1:N, :);

params = struct('covfunc_f', covfunc_f, 'covfunc_w', covfunc_w, ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Y);
nsamples = 5000; burnin = 0; MAXITER = 10; EM_ITERS = 5;
theta_old = [log(rand(size(theta, 1) - 2, 1)); log(0.2); log(0.2)];
theta0 = theta_old;
qtheta_all = [];
qtheta_msteps = zeros(EM_ITERS, 1);
for i = 1:EM_ITERS
  tminimize = tic;
  % E-step: compute the posterior p(u|theta_old,...)
  [Fhat, W] = sample_posterior(theta_old, params, nsamples, burnin);
  posterior_samples = [Fhat; W];
  % M-step: min - Q(theta, theta_old)
  % can start with theta_old or just some random theta
  [theta_old, qtheta, ~] = minimize(theta_old, @gprn_learn_em, MAXITER, ...
    posterior_samples, params);
  qtheta_all = [qtheta_all; qtheta];
  qtheta_msteps(i) = qtheta(end);
  fprintf('\nEM run %d finished (%.2fs): qtheta = %.4f\n', i, ...
    toc(tminimize), qtheta_msteps(i));
  disp('current theta')
  disp(exp(theta_old'))
end

figure(1); hold on;
plot(1:length(qtheta_all), qtheta_all, 'g');
legend('qtheta for all');
title('-Q(theta, theta_old) all linesearches');

figure(2); hold on;
plot(1:length(qtheta_msteps), qtheta_msteps, 'b');
legend('qtheta');
title('-Q(theta, theta_old)  at end of M-steps');

disp('theta, theta_old, theta0')
disp([exp(theta), exp(theta_old), exp(theta0)])

end

