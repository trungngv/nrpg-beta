function test_mean_prediction()
%TEST_MEAN_PREDICTION test_mean_prediction()
%
% Test for mean_prediction().
% 
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/12
%
% N=50;% D=2;ntest=5;num_outputs=1;num_latents=2;
%rng(111003,'twister');sigma_f=0.0001;sigma_y=0.2;
rng(23, 'twister');
N = 10; D = 2; ntest = 5;
num_outputs = 3; num_latents = 2; covfunc_f = 'covSEard'; covfunc_w = 'covSEard';
sigma_f = 0.0001; sigma_y = 0.01;
theta_f = log(rand(D+1,1)); theta_w = log(rand(D+1,1));
theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
% gprn_utility_funcs requires theta_f as a matrix
theta_ff = repmat(theta_f', num_latents, 1);
X = rand(N+ntest, D);
[~, Ytrue, Ynoise, Ftrue, Wtrue] = gprn_utility_funcs(theta_ff, theta_w,...
  sigma_f, sigma_y, X, num_outputs, false);
[Ynoise, Ymean, Ystd] = standardize(Ynoise,1,[],[]);
Ytrue = standardize(Ytrue,1,Ymean,Ystd);

xtest = X(N+1:N+ntest,:);
ytest = Ynoise(N+1:N+ntest,:);
X = X(1:N,:);
Y = Ynoise(1:N,:);

params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard', ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Y);
burnin = 000; nsamples = 10;
Kf = feval(covfunc_f, theta_f, X) + (sigma_f ^ 2)*eye(N);
Kw = feval(covfunc_w, theta_w, X);
params.Kf = Kf; params.Kw = Kw;
[Fhat, W] = sample_posterior(theta, params, nsamples, burnin);
Usamples = [Fhat; W];
fprintf('[\tymean\t ytest\t ytrue (no noise)]\n');
ydiff = zeros(ntest,params.num_outputs);
for i=1:ntest
  ymean = mean_prediction3(xtest(i,:), theta, Usamples, params);
  ydiff(i,:) = ymean - ytest(i,:)';
  %ymean = (ymean(:)-Ymean(:))./Ystd(:);
  disp([ymean, ytest(i,:)', Ytrue(N+i,:)'])
end

fprintf('smse: %.4f\t', mean(ydiff.^2));
end

