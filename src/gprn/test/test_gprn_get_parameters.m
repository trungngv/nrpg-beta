function test_gprn_get_parameters()
%TEST_GPRN_GET_PARAMETERS test_gprn_get_parameters()
% Test the gprn_get_parameters function.
theta = [1 2 3 4 5 6]';
params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard', 'X', zeros(1, 1));
[theta_f, theta_w, sigma_f, sigma_y] = gprn_get_parameters(theta, params);
assert(isequal(theta_f, theta(1:2)), num2str(theta_f));
assert(isequal(theta_w, theta(3:4)), num2str(theta_w));
assert(sigma_f == 5, num2str(sigma_f));
assert(sigma_y == 6, num2str(sigma_y));
fprintf('\ntest_gprn_get_parameters passed\n');
end

