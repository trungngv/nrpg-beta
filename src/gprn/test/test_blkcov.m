function test_blkcov()
%TEST_BLKCOV test_blkcov()
% 
% Test for the blkcov() function.
params = struct('num_outputs', 3, 'num_latents', 2);
Kf = [1 2];
Kw = [3 4];
K_expected = blkdiag([1 2], [1 2], [3 4], [3 4], [3 4], [3 4], [3 4], [3 4]);
K = blkcov(Kf, Kw, params);
assert(isequal(K, K_expected), 'test_blkcov() failed\n');
fprintf('\ntest_blkcov() passed\n');
end

