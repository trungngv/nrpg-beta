function test_gprn_learn()
%TEST_GPRN_LEARN test_gprn_learn()
%   
% Test for the gprn_learn.m function.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 07 Jul 2012
N = 5; D = 2; num_outputs = 3; num_latents = 2;
covfunc_f = 'covSEard'; covfunc_w = 'covSEard';

sigma_f = 0.01; sigma_y = 0.2;
theta_f = [log(rand(D, 1)); log(0.5 + rand)]
theta_w = [log(rand(D, 1)); log(0.5 + rand)]
theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
% gprn_utility_funcs requires theta_f as a matrix
theta_ff = repmat(theta_f', num_latents, 1);
X = rand(N, D);
[~, Ytrue, Ynoise, Ftrue, Wtrue] = gprn_utility_funcs(theta_ff, theta_w,...
  sigma_f, sigma_y, X, num_outputs);

params = struct('covfunc_f', covfunc_f, 'covfunc_w', covfunc_w, ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Ynoise);

nsamples = 5000;
MAXITER = 5;
theta0 = [log(rand(size(theta, 1) - 2, 1)); log(1); log(1)];
%theta0 = zeros(size(theta));
tminimize = tic;
[theta_learned, nlml, ~] = minimize(theta0, @gprn_learn, MAXITER, ...
  nsamples, params);
fprintf('\nminimize finished (%.2fs)', toc(tminimize));
min_nlml = gprn_nlml(theta, nsamples, params);
fprintf('\nnlml of true theta: %.2f\n', min_nlml);
disp([theta, theta_learned, theta0])

figure(1); hold on;
plot(1:length(nlml), nlml);
title('Negative marginal log likelihood');

end

