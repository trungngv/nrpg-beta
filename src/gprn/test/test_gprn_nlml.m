function test_gprn_nlml()
%TEST_GPRN_NLML test_gprn_nlml()
% Test the gprn_nlml function.
% 
num_outputs = 3; num_latents = 2; sigma_f = 0.01; sigma_y = 0.2;
N = 5; D = 2;
rng(1100, 'twister');
X = rand(N, D);
theta_f = [log(rand(D, 1)); log(0.5 + rand)];
theta_w = [log(rand(D, 1)); log(0.5 + rand)];
theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
% gprn_utility_funcs requires theta_f as a matrix
theta_ff = repmat(theta_f', num_latents, 1);
X = rand(N, D);
[~, ~, Ynoise, ~, ~] = gprn_utility_funcs(theta_ff, theta_w,...
  sigma_f, sigma_y, X, num_outputs);
params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard', ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Ynoise);
theta_rand = rand(size(theta));

nsamples = 50000;
fprintf('nmll with true hyperparameters\n');
[nlml ~] = gprn_nlml(theta, nsamples, params);
fprintf('nmll = %.5f\n', nlml);


fprintf('nmll with random hyperparameters\n');
[nlml ~] = gprn_nlml(theta_rand, nsamples, params);
fprintf('nmll = %.5f\n', nlml);

end

