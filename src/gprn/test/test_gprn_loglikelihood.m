function test_gprn_likelihood()
%TEST_GPRN_LIKELIHOOD test_gprn_likelihood()
%   Test for the gprn_likelihood() function.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 05 Jul 2012
%
num_outputs = 2; num_latents = 2;
N = 2;
Y = zeros(N, num_outputs);
params = struct('num_outputs', num_outputs, 'num_latents', num_latents, 'Y', Y);
fhat = zeros(N, num_latents);
w = zeros(N * num_latents * num_outputs, 1);
sigma_y = 1;
logl = gprn_loglikelihood(fhat, w, log(sigma_y), params);
% N(0|0, I) = (2pi)^(-Np/2)
expected_logl = log((2 * pi)^(-N*num_outputs/2));
assert((abs(logl - expected_logl) < 1e-4));
fprintf('\ntest gprn_loglikelihood passed');
fprintf('\nlog likelihood: %.5f', logl);
fprintf('\nexpected log likelihood: %.5f\n', expected_logl);

N = 100; num_outputs = 3; num_latents = 2;
Y = rand(N, num_outputs);
rng(11, 'twister');
params = struct('num_outputs', num_outputs, 'num_latents', num_latents, 'Y', Y);
fhat = rand(N, num_latents);
w = rand(N * num_latents * num_outputs, 1);
sigma_y = 0.5;
logl = gprn_loglikelihood(fhat, w, log(sigma_y), params);
fprintf('log likelihood for a large test: %.15f\n', logl);
end

