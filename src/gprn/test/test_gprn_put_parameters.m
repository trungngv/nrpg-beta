function test_gprn_put_parameters()
%TEST_GPRN_PUT_PARAMETERS test_gprn_put_parameters()
% Test the gprn_put_parameters function.
theta_f = [1 2 3]';
theta_w = [4 5 6]';
sigma_f = 7;
sigma_y = 8;
theta_test = gprn_put_parameters(theta_f, theta_w, sigma_f, sigma_y);
assert(isequal(theta_test, (1:8)'), 'theta_test should be 1:8\n');
fprintf('test_gprn_put_parameters() passed\n');
end

