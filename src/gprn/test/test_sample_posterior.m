function test_sample_posterior()
%TEST_SAMPLE_POSTERIOR test_sample_posterior()
%   
% Test for the sample_posterior() function.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 09 Jul 2012
%

N = 10; D = 2; num_outputs = 2; num_latents = 2;
rng(11100, 'twister');
sigma_f = 0.01; sigma_y = 0.3;
theta_f = [log(rand(D, 1)); log(1 + rand)];
theta_w = [log(rand(D, 1)); log(1 + rand)];
theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
% gprn_utility_funcs requires theta_f as a matrix
theta_ff = repmat(theta_f', num_latents, 1);
X = rand(N, D) + rand(N, D);
[~, Ytrue, Y, Ftrue, Wtrue] = gprn_utility_funcs(theta_ff, theta_w,...
  sigma_f, sigma_y, X, num_outputs);

params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard', ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Y);
nsamples = 50;
burnin = 100;

[F, W] = sample_posterior(theta, params, nsamples, burnin);
Fmean = mean(F, 2);
Wmean = mean(W, 2);
Ftran = Ftrue';
% Fmean and Wmean is not weighted so this is not be close to the true
% values
[Ftran(:), Fmean]
[Wtrue(:), Wmean]

end

