function [X, Y, theta ] = randomize(N, D, num_outputs)
%RANDOMIZE [X, Y, theta] = randomize(N, D, num_outputs, num_latents)
% Generate a completely random data set and hyperparameters for
% test functions. This function assumes that both of the covariance
% functions for latent function and weight functions are squared
% exponential.
% 
% Author : Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 04 Jul 2012
%
X = rand(N, D);
Y = rand(N, num_outputs);
theta_f = rand(D + 1, 1);
theta_w = rand(D + 1, 1);
sigma_f = rand;
sigma_y = rand;
theta = gprn_put_parameters(theta_f, theta_w, sigma_f, sigma_y);

end

