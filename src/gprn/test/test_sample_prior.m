function test_sample_prior()
%TEST_SAMPLE_PRIOR test_sample_prior()
%
% Test for the sample_prior() function.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/12
%

rng(1100, 'twister');
N = 5;
D = 3;
theta_f = rand(D + 1, 1);
theta_w = rand(D + 1, 1);
sigma_f = 0.0001;
sigma_y = 0.0005;
theta = gprn_put_parameters(theta_f, theta_w, sigma_f, sigma_y);
X = rand(N, D);
num_outputs = 3;
num_latents = 2;
params = struct('covfunc_f', 'covSEard', 'covfunc_w', 'covSEard', ...
  'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X);
[fhat, w] = sample_prior(X, theta, params, [], []);
assert(isequal(size(fhat), [N * num_latents, 1]));
assert(isequal(size(w), [N * num_outputs * num_latents, 1]));
fprintf('test sample_prior passed\n');
end

