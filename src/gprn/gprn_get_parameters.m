function [ theta_f, theta_w, sigma_f, sigma_y ] = gprn_get_parameters(theta, params)
%GPRN_GET_PARAMETERS [ theta_f, theta_w, sigma_f, sigma_y ] = gprn_get_parameters(theta, params)
%   Decompose the vector containing all hyperparameters in the gprn model
%   to the respective hyperparameters for different components.
%
% INPUT: 
%   params : a (global) struct containing the model's configurations
%   theta : vector containing all hyperparameters
%
% OUTPUT:
%   theta_f : hyperparameters of the covraince function for latent
%     functions
%   theta_w : hyperparameters of the covariance function for weights
%   sigma_f : additive latent function noise
%   sigma_y : additive output noise
%
% See also: gprn_put_parameters.m
% 
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 03 Jul 2012
%
X = params.X;
D = size(X, 2); % used by gpml package
ntheta_f = eval(feval(params.covfunc_f));
ntheta_w = eval(feval(params.covfunc_w));
assert(ntheta_f + ntheta_w + 1 + 1 == length(theta), 'BUG: not enough hyperparameters\n');

theta_f = theta(1:ntheta_f);
theta_w = theta(ntheta_f + 1 : ntheta_f + ntheta_w);
sigma_f = theta(end - 1);
sigma_y = theta(end);

end
