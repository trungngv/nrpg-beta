function ystar = mean_prediction3(xstar, theta, Usamples, params)
%MEAN_PREDICTION mean_ystar = mean_prediction3(xstar, theta, Usamples, params)
%
% Computes the predictive mean E[ystar|D,theta,xstar] for a test point.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/2012
%
covfunc_f = params.covfunc_f;
covfunc_w = params.covfunc_w;
X = params.X;
N = size(X, 1);
num_outputs = params.num_outputs;
num_latents = params.num_latents;
[theta_f, theta_w, ~, ~] = gprn_get_parameters(theta, params);
Kf_inv = inv(params.Kf);
Kw_inv = inv(params.Kw);
[~, Kf_inv_blk, Kw_inv_blk] = blkcov(Kf_inv, Kw_inv, params);

% do this for every test point xstar
[~, kf_s] = feval(covfunc_f, theta_f, X, xstar);
[~, kw_s] = feval(covfunc_w, theta_w, X, xstar);
[~, Kf_star_blk, Kw_star_blk] = blkcov(kf_s, kw_s, params);
alpha_f = Kf_star_blk' * Kf_inv_blk;
alpha_w = Kw_star_blk' * Kw_inv_blk;
sum_ystar = 0;
nsamples = size(Usamples,2);
for i = 1:nsamples
  sample = Usamples(:,i);
  fi = sample(1:N*num_latents);
  wi = sample(length(fi) + 1 : end);
  alpha_W = reshape(alpha_w * wi, num_outputs, num_latents);
  sum_ystar = sum_ystar + alpha_W * alpha_f * fi;
end
ystar = sum_ystar / nsamples;
end

