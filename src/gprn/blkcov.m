function [K_blk, Kf_blk, Kw_blk] = blkcov(Kf, Kw, params)
%BLKDIAG_FROM_KFKW [K_blk, Kf_blk, Kw_blk] = blkcov(Kf, Kw, params)
%
% Constructs the block covariance matrices from Kf and Kw.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/12
%

num_outputs = params.num_outputs;
num_latents = params.num_latents;
Kf_blk = [];
for q = 1:num_latents
  Kf_blk = blkdiag(Kf_blk, Kf);
end
Kw_blk = [];
for pq = 1:(num_outputs * num_latents)
  Kw_blk = blkdiag(Kw_blk, Kw);
end
K_blk = blkdiag(Kf_blk, Kw_blk);

end

