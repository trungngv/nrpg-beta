function log_p = log_prior(f, w, Lf, Lw, params)
%LOG_PRIOR log_p = log_prior(f, w, Lf, Lw, params)
% 
% Computes the log prior probability ln p(u | theta) for an input point.
%
% INPUT
%   - f : the latent functions associated with the input
%   - w : the weight functions associated with the input
%   - Lf : the Cholesky decomposition (chol(Kf)') of the covariance matrix of latent functions
%   - Lw : the Cholesky decomposition (chol(Kw)') of the covariance matrix of weight functions
%   - params
%
% OUTPUT
%   - log_p : p(f, w | theta)
%
% Author : Trung V. Nguyen (trung.ngvan@gmail.com)
% 18/08/2012
%
N = size(params.X,1); P = params.num_outputs; Q = params.num_latents;
F = reshape(f,N,Q); W = mat2cell(reshape(w,N*P,Q), N*ones(1,P), ones(1,Q));
log_p = -0.5*N*Q*(P+1)*log(2*pi) -Q*sum(log(diag(Lf))) - P*Q*sum(log(diag(Lw)));
for j=1:Q
  %log_p = log_p - 0.5*F(:,j)'*Kf_inv*F(:,j);
  alpha = Lf\F(:,j);
  log_p = log_p - 0.5*(alpha')*alpha;
  for i=1:P
    alpha = Lw\W{i,j};
    %log_p = log_p - 0.5*W{i,j}'*Kw_inv*W{i,j};
    log_p = log_p - 0.5*(alpha')*alpha;
  end
end

end

