function [nl grad_theta] = gprn_learn(theta, nsamples, params)
%GPRN_LEARN [nl grad_theta] = gprn_learn(theta, nsamples, params)
%
% Computes the negative log marginal likelihood and its gradient wrt the
% hyperparameters. This can be used to learn a gprn model with Carl
% Rasmussen's minimize function.
% 
% INPUT
%   - theta : the point at which gradients are evaluated
%   - params : a (global) struct containing the model's setting
%
% OUTPUT
%   - nl : the negative log marginal likelihood
%   - grad_theta : its gradient wrt to the hyperparameters
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 05 Jul 2012
%
tic;
nl = gprn_nlml(theta, nsamples, params);
grad_theta = numeric_grad3('gprn_nlml', theta, nsamples, params);
fprintf('gprn_learn nlml = %.3f (%.2fs)\n', nl, toc);
end

