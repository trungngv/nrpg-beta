function ystar = mean_prediction(xstar, theta, nsamples_prior, nsamples_posterior, params)
%MEAN_PREDICTION mean_ystar = mean_prediction(xstar, theta, nsamples_prior, nsamples_posterior, params)
%
% Deprecated.
% Computes the predictive mean E[ystar|D,theta,xstar] for a test point using
% samples from the prior.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 11/07/12
%
covfunc_f = params.covfunc_f;
covfunc_w = params.covfunc_w;
num_outputs = params.num_outputs;
num_latents = params.num_latents;
[theta_f, theta_w, sigma_f, ~] = gprn_get_parameters(theta, params);
Kf_inv = inv(params.Kf);
Kw_inv = inv(params.Kw);
Kinv = blkcov(Kf_inv, Kw_inv, params);
[Fhat, W] = sample_posterior(theta, params, nsamples_posterior);
U_samples = [Fhat; W];

% do this for every test point xstar
Kf_star = feval(covfunc_f, theta_f, xstar);
Kfhat_star = Kf_star + sigma_f ^ 2;
[Kfhat_tmp ~] = jit_chol(Kfhat_star);
Kfhat_chol = Kfhat_tmp';
Kw_star = feval(covfunc_w, theta_w, xstar);
[Kw_tmp ~] = jit_chol(Kw_star);
Kw_chol = Kw_tmp';

ustar_sum = 0;
for i = 1:nsamples_prior
  % sample ustar from prior
  [fhat_star, w_star, Kfhat_chol, Kw_chol] = sample_prior(xstar, theta, params, Kfhat_chol, Kw_chol);
  ustar = [fhat_star; w_star];
  % compute the predictive probability p(u*|u, gamma) here
  ustar_sum = ustar_sum + ustar * gprn_predictive(theta, ustar, xstar, ...
    U_samples, Kinv, params) / exp(log_prior(fhat_star, w_star, Kf_star, Kw_star, params));
end
ustar_mean = ustar_sum / nsamples_prior;
fstar = ustar_mean(1:num_latents);
wstar = ustar_mean(num_latents + 1 : end);
Wstar = reshape(wstar, num_outputs, num_latents);
ystar = Wstar * fstar;
end

