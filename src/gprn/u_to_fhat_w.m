function [fhat,w] = u_to_fhat_w(u, params)
%U_TO_FHAT_W [fhat,w] = u_to_fhat_w(u, params)
%
%
u = u(:);
N = size(params.X, 1);
fhat = u(1:(N*params.num_latents));
w = u(length(fhat)+1:end);
end

