function [nlml, accept_rate] = gprn_nlml(theta, nsamples, params)
%GPRN_NLML nlml = gprn_nlml(theta, nsamples, params)
% Computes the negative log marginal likelihood - log p(Y|theta). This can be
% used as the objective function that we want to minimize with respect to
% the hyperparameters theta.
% 
% INPUT
%   theta : vector of hyperparameters
%   nsamples : number of samples used to approximate nlml
%   params : a (global) struct containing the model's configurations
%
% OUTPUT
%   the negative log marginal likelihood - log p(Y|theta)
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 04 Jul 2012

% p(D|theta) = \int p(D, fhat, W, theta) dfhat dW
%   = \int p(D|fhat, W, theta) p(fhat, W | theta) dfhat dW
% so we use monte carlo approximiation to approximate p(D|theta).
[~, ~, ~, sigma_y] = gprn_get_parameters(theta, params);
sigma_y=exp(sigma_y);
% It is CRITICAL to use the same number generator to make the approximation
% stable/consistent for the same set of inputs (function parameters)
rng(1110, 'twister');
sum_likelihood = 0;
Kfhat_chol = [];
Kw_chol = [];
for i = 1 : nsamples
  [fhat, w, Kfhat_chol, Kw_chol] = feval('sample_prior', params.X, ...
    theta, params, Kfhat_chol, Kw_chol);
  likelihood = feval('gprn_likelihood', fhat, w, sigma_y, params);
  sum_likelihood = sum_likelihood + likelihood;
end

marginal_likelihood = sum_likelihood / nsamples;
nlml = -log(marginal_likelihood);
accept_rate = 1.0;

end

