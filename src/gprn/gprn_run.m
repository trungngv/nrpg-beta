function gprn_run()
%GPRN_RUN gprn_run()
% 
% Starting place for the GPRN model.
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last updated: 26/07/2012
%
% TODO(trung): careful with how parameters that are in log space
% in numerical differentiation (use the proper scale)
%
% can use different rngs
rng(11100, 'twister');
start_time = tic;

% Read data
[X, Y, ~, ~] = load_data('data/jura', 'juraCd');
[Y, Ymean, Ystd]=standardize(Y,1,[],[]);
%[X, Xmean, Xstd]=standardize(X,2,[],[]);
[N_all, D] = size(X);
N = 100; ntest = 10;
xtest = X(N_all-ntest:N_all,:);
ytest = Y(N_all-ntest:N_all,:);
train_ind = randperm(N_all, N);
X = X(train_ind,:);
Y = Y(train_ind,:);

% Setting model configuration
num_outputs = size(Y, 2); num_latents = 2;
covfunc_f = 'covSEard'; covfunc_w = 'covSEard';
params = struct('covfunc_f', covfunc_f, 'covfunc_w', covfunc_w, ...
    'num_outputs', num_outputs, 'num_latents', num_latents, 'X', X, 'Y', Y);

theta = learn_hyperparameters(D, params);
%theta = grid_search_hyperparameters(D, params);
make_prediction(theta, params, xtest, ytest);

fprintf('Total running time: %.2fs\n', toc(start_time));

end

% search the grid of hyper-parameters for the argmax p(D|theta)
% not working
function theta_best = grid_search_hyperparameters(D, params)
sigma_f = 0.001; sigma_y = 0.25; % fix sigma_f0 and sigma_y
%grid = linspace(-1,1,10);
%ells = grid(1)*ones(2*D,1);
qtheta_best = Inf; theta_best = []; qtheta_all = [];
burnin = 0; nsamples = 5000; valid_cnt = 0;
for d = 1:2*D
  for i = 1:50
    %ells(d) = grid(i);
    ells = rand(2*D+2, 1); % take random hyperparameters first
    theta_f = log(ells(1:D+1)); theta_w = log(ells(D+2:end));
    theta = gprn_put_parameters(theta_f, theta_w, log(sigma_f), log(sigma_y));
    try
      [Fhat, W] = sample_posterior(theta, params, nsamples, burnin);
      posterior_samples = [Fhat; W];
      [qtheta ~] = gprn_qtheta(theta, posterior_samples, params);
      qtheta_all = [qtheta_all; qtheta];    
      if (qtheta_best > qtheta)
        qtheta_best = qtheta;
        theta_best = theta;
        disp('best theta so far')
        disp(exp(theta_best'))
      end
      valid_cnt = valid_cnt + 1;
    catch err
      disp(err)
    end
  end
end

figure(1); hold on;
plot(1:length(qtheta_all), qtheta_all, 'g');
legend('qtheta for all');
title('-Q(theta) all grid points');
fprintf('\nvalid_cnt = %d\n', valid_cnt);

end

% learn hyper-parameters with the EM algorithm
function theta = learn_hyperparameters(D, params)
nsamples = 10000; burnin = 0; LS_ITER = 10; EM_ITERS = 10;
theta_f0 = log(rand(D+1,1)); theta_w0 = log(rand(D+1,1));
sigma_f0 = 0.001; sigma_y0 = 00.5;
theta0 = gprn_put_parameters(theta_f0, theta_w0, log(sigma_f0), log(sigma_y0));
theta_old = theta0;
qtheta_all = [];
qtheta_msteps = zeros(EM_ITERS, 1);

% EM for learning the hyperparameters
for i = 1:EM_ITERS
  tminimize = tic;
  % E-step: compute the posterior p(u|...)
  [Fhat, W] = sample_posterior(theta_old, params, nsamples, burnin);
  posterior_samples = [Fhat; W];
  % M-step: min - Q(theta, theta_old)
  % can start with theta_old or just some random theta
  [theta_old, qtheta, ~] = minimize(theta_old, @gprn_learn_em, LS_ITER, ...
    posterior_samples, params);
  qtheta_all = [qtheta_all; qtheta];
  qtheta_msteps(i) = qtheta(end);
  fprintf('\nEM run %d finished (%.2fs): qtheta = %.4f\n', i, ...
    toc(tminimize), qtheta_msteps(i));
  disp('current theta')
  disp(exp(theta_old'))
end

figure(1); hold on;
plot(1:length(qtheta_all), qtheta_all, 'g');
legend('qtheta for all');
title('-Q(theta) all linesearches');

figure(2); hold on;
plot(1:length(qtheta_msteps), qtheta_msteps, 'b');
legend('qtheta');
title('-Q(theta)  at end of M-steps');

disp('theta0, \t theta_learned')
disp([exp(theta0), exp(theta_old)])

theta = theta_old;
end

% make prediction using the learnd parameters
function make_prediction(theta, params, xtest, ytest)
burnin = 0; nsamples = 10000;
%burnin = 5; nsamples = 10;
[theta_f, theta_w, sigma_f, ~] = gprn_get_parameters(theta, params);
sigma_f=exp(sigma_f);
X = params.X; N = size(X, 1);
Kf = feval(params.covfunc_f, theta_f, params.X) + (sigma_f^2) * eye(N);
Kw = feval(params.covfunc_w, theta_w, params.X);
params.Kf = Kf;
params.Kw = Kw;
[Fhat, W] = sample_posterior(theta, params, nsamples, burnin);
Usamples = [Fhat; W];
fprintf('[\tymean\t ytest\t]\n');
ntest=size(xtest,1);
ydiff = zeros(ntest,params.num_outputs);
for i=1:ntest
  ymean = mean_prediction4(xtest(i,:), theta, Usamples, params);
  ydiff(i,:) = ymean - ytest(i,:)';
  disp([ymean, ytest(i,:)'])
end
fprintf('smse: %.4f\t', mean(ydiff.^2));
end

