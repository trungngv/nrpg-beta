function [fhat, w, Kfhat_chol, Kw_chol] = sample_prior(X, theta, params, Kfhat_chol, Kw_chol)
%SAMPLE_PRIOR [fhat, w, Kfhat_chol, Kw_chol] = sample_prior(X, theta, params, Kfhat_chol, Kw_chol)
% Sample from the prior distribution p(u | \gamma) where u =
%[fhat; w] and theta is the hyperparameters of the covariance functions and
% the addictive noises.
% 
% INPUT
%   - X
%   - theta : vector of hyperparameters
%   - params : a (global) struct containing setting for the gprn model
%   - Kfhat_chol : optional cholesky decomposition of Kfhat
%   - Kw_chol : optional cholesky decomposition of Kw_chol
%
% OUTPUT
%   - fhat : latent functions
%   - w : weight functions
%   - Kfhat_chol : the cholesky decomposition of Kfhat, chol(Kfhat)'
%   - Kw_chol : the cholesky decomposition of Kw, chol(Kw)'
%
% Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 09 Jul 2012
%
covfunc_f = params.covfunc_f;
covfunc_w = params.covfunc_w;
num_outputs=  params.num_outputs;
num_latents = params.num_latents;
N = size(X, 1);
[theta_f, theta_w, sigma_f, ~] = gprn_get_parameters(theta, params);
sigma_f=exp(sigma_f);

if nargin == 2 || (isempty(Kfhat_chol) && isempty(Kw_chol))
  Kf = feval(covfunc_f, theta_f, X);
  Kfhat = Kf + (sigma_f^2) * eye(N); % add noise
  [Kfhat_tmp ~] = jit_chol(Kfhat);
  Kfhat_chol = Kfhat_tmp';
  Kw = feval(covfunc_w, theta_w, X);
  [Kw_tmp ~] = jit_chol(Kw);
  Kw_chol = Kw_tmp';
end

% sample Fhat = [fhat1 fhat2 ... fhatq]
% Z: N x q (each column zq ~ N(0, In) for the latent function q)
Z = randn(N, num_latents);
Fhat = Kfhat_chol * Z;
fhat = Fhat(:);

% sample w = [W11; W21; ... Wp1; ... W1q; W2q; ...; Wpq]
% Wij ~ GP(0, Kw) i.i.d
Wcell = cell(num_outputs, num_latents);
for i = 1:num_outputs
  for j = 1:num_latents
    Wcell{i,j} = Kw_chol*randn(N, 1);
  end
end
Wmat = cell2mat(Wcell);
w = Wmat(:);
end

