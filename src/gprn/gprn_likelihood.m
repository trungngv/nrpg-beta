function likelihood = gprn_likelihood(fhat, w, sigma_y, params)
%GPRN_NMLL likelihood = gprn_likelihood(fhat, w, sigma_y, params)
%
% Deprecated: should always use log for computation.
% 
% Computes the likelihood value p(Y | fhat, w, sigma_y).
%
% INPUT
%   fhat : latent functions
%   w : weight functions
%   sigma_y : the output noise parameter
%   params : a (global) struct setting of the gprn model
%
% OUTPUT
%   the likelihood value
% 
% See also gprn_data_structures.m for convention of how data is
% represented.
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 14/08/2012
%
%
Y = params.Y;
N = size(Y, 1);
num_outputs = params.num_outputs;
num_latents = params.num_latents;
Wmat = reshape(w, N*num_outputs, num_latents);
Fhat = reshape(fhat, N, num_latents);
logl = 0; sy2 = sigma_y^2;
% the p-th output starts at row (p - 1) * N + 1 
output_base_ind = N*(0:num_outputs-1)';
for i=1:N
  % y(xi) ~ N(ymean(xi), sigma_y^2 Ip)
  ymean = Wmat(output_base_ind+i, :)*Fhat(i,:)';
  %logsum = logsum + log(mvnpdf(Y(i, :), ymean', Sigma));
  logl = logl - 0.5*params.num_outputs*log(2*pi*sy2) - 0.5*(Y(i,:)-ymean')*(Y(i,:)'-ymean);
end

likelihood = exp(logl);
fprintf('likelihood = %.5f\n', likelihood);
end

