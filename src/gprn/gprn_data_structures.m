% The data structures used in the GPRN package.
% 
%% Model configuration parameters: params : struct
% params = struct('covfunc_f', .., 'covfunc_w', .., 'num_outputs', ..,
% 'num_latents', .., 'X', .., 'Y', ..)
% with:
% - covfunc_f : the covariance function for latent functions (string)
% - covfunc_w : the covariance function for weight functions (string)
% - num_outputs : the number of ouputs/tasks/users (scalar), i.e., p
% - num_latents : the number of latent functions f (scalar), i.e., q
% - X : the training input features matrix (N x D)
% - Y : the training outputs (N x p)
%
%% Input Features Matrix: X : N x D
% X = [x1' x2' ... xN']
%where each xi = [xi_1
%                  .
%                  .
%                 xi_D]
% is the feature vector of one input xi.
%
%% Output Matrix: Y : N x p
% Y = [y1 y2 ... yp] = [y(x1)'; ...; y(xN)']
% where each yi = [yi(x1)
%                   .
%                   .
%                  yi(xN)]
% is the outputs of the ith task/user.
% Hence y = Y(:) is Np x 1 matrix
% y = [y1(x1)
%      y1(x2)
%      .
%      .
%      y1(xN)
%      .
%      .
%      .
%      yp(x1)
%      yp(x2)
%      .
%      yp(xN)]
%
% Doing reshape(y, N, p) gives us back Y.
%
%% Latent Functions f and fhat
% F : N x q latent function values, similar to Y
% F = [f1 f2 ... fq] = [f(x1)'; ...; f(xN)']
% where each fi = [fi(x1)
%                   .
%                   .
%                  fi(xN)]
% Hence f = F(:) is Nq x 1 matrix
%
% f = [f1(x1)
%      f1(x2)
%      .
%      .
%      f1(xN)
%      .
%      .
%      .
%      fq(x1)
%      fq(x2)
%      .
%      fq(xN)]
% 
% fhat is simply the latent functions f with noise,
% fhat = [f1(x1) + sigma_f esp(x1)
%         f1(x2) + sigma_f esp(x2)
%         .
%         .
%         f1(xN) + sigma_f esp(xN)
%         .
%         .
%         .
%         fq(x1) + sigma_f esp(x1)
%         fq(x2) + sigma_f esp(x2)
%         .
%         fq(xN) + sigma_f esp(xN)]
%
% Doing reshape(f, N, q) gives us back the original F.
%
%% Weight Functions W
% There are p x q weight functions, each Wij = [Wij(x1) ... Wij(xN)]'.
% The weight functions evaluated at all inputs x1, x2, ..., xN is
% w = [W11
%      W21
%      .
%      Wp1
%      .
%      .
%      W1q
%      .
%      Wpq]
% which is a Npq x 1 matrix
% - If we use W{i, j} = Wij then:
% Wmat = cell2mat(W)
% w = Wmat(:)
% and 
% Wmat = reshape(w, N * p, q)
% W = mat2cell(Wmat, N * ones(1, p), ones(1, q)).
%% Combination of latent and weight functions
% u = [f
%      w]
% is the vector of latent and weight functions.
%
%% Covariance Functions: Ku, Kf, Kw
% Ku : Nq(p+1) x Nq(p+1) block diagonal matrix where each block is a N x N
% covariance matrix. We use the same covariance function for all weight
% functions and the same covariance function for all latent functions.
% Hence there are only two covariance matrices that compose Ku.
% Ku = [Kf
%         . 
%           .
%            Kf
%              Kw
%                .
%                  .
%                   Kw]
% where there are q blocks of Kf and p x q blocks of Kw.
% Here Kf = covfunc_f(X) and Kw = covfunc_w(X).
% 