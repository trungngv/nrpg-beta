function [qtheta grad_theta] = gprn_learn_em(theta, posterior_samples, params)
%GPRN_LEARN_EM [nl grad_theta] = gprn_learn_em(theta, theta_old, params)
%
% Computes -Q(theta, theta_old), the negative expected log complete-data
% likelihood and its gradient wrt the hyperparameters. We do this by Monte
% Carlo approximation so this contains the E-step in standard EM as well.
% Carl Rasmussen's minimize function can be used to do maximization in the
% M step.
% 
% INPUT
%   - theta : the point at which gradients are evaluated
%   - posterior_samples : samples from the posterior
%   - params : a (global) struct containing the model's setting
%
% OUTPUT
%   - qtheta : the expected log complete-data likelihood Q(theta, theta_old)
%   - grad_theta : its gradient wrt to the hyperparameters
%
% Author: Trung V. Nguyen (trung.ngvan@gmail.com)
% Last update: 05 Jul 2012
%
tic;
qtheta = gprn_qtheta(theta, posterior_samples, params);
grad_theta = numeric_grad3('gprn_qtheta', theta, posterior_samples, params);
fprintf('gprn_learn -Q(theta, theta_old) = %.3f (%.2fs)\n', qtheta, toc);
end

