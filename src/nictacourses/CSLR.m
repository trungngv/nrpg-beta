% Trung Nguyen VanTrung.Nguyen@nicta.com.au

%% Data preparation
close all;
clear;
clc;
load('AR_165x120_Infer_Sunglass.mat')
img_size = [165 120];
faces = fea(:,1:10);
figure;
for i = 1:10
  face = reshape(faces(:,i), img_size);
  subplot(2,5,i);
  imshow(face);
end

disp('Press any key')
pause

%% Eigenfaces
num_eigs = 16;
dfea = double(fea);
mean_face = mean(dfea, 2);
normalized_face = dfea-repmat(mean_face,1,size(dfea,2));
[U,~,~] = svds(normalized_face,num_eigs);
figure('Name','Eigen faces');
for i=1:num_eigs
    subplot(4,4,i);
    imshow(mat2gray(reshape(U(:,i),img_size)));
end
disp('Press any key')
pause

% Reconstruction error
mean_value = mean_face(330);
face = dfea(:,330);

figure('Name','Orginal face');
imshow(mat2gray(reshape(face,img_size)));

proj = U'*(face+mean_value);
reconstructed = U*proj + mean_value;
figure('Name','Reconstructed face');
imshow(mat2gray(reshape(reconstructed,img_size)));

disp('Press any key')
pause

gface = mat2gray(face);
greconstruct = mat2gray(reconstructed);
error = (gface-greconstruct)' * (gface-greconstruct);
fprintf('Squared error in soft thresholding: %.4f\n', error);

%% Low-rank submatrix
n = 600;
iters = 15;
p_truncate = 0.05;
faces = reshape(fea(:,1:n), img_size(1), img_size(2)*n);
dfaces = cast(faces, 'double');
B = sign(dfaces - mean(dfaces(:)));

for i = 1:iters
    Zrow = diag(B*(B')*B*(B'));
    [values, ind] = sort(Zrow);
    numRemove = round(numel(Zrow)*p_truncate);
    B(ind(1:numRemove),:) = [];
    faces(ind(1:numRemove),:) = [];
end

figure();
for i = 1:10
    subplot(2,5,i), imshow(faces(:, (img_size(2)*(i-1)+1):(img_size(2)*i)));
end

