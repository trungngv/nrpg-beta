load AR_165x120_Infer_Sunglass

%%plot first 10 faces in dataset
figure('Name','First 10 faces','position',[100 100 600 330]);
title('first 10 faces');
for i=1:10
    x = reshape(fea(:,i),165,120);
    subplot(2,5,i);
    imshow(x)
end

%%Problem 1
num_eigenfaces = 20;
mean_face = mean(double(fea), 2);
normalized_face = double(fea) - repmat(mean_face, 1, size(fea,2));
[U,S,V] = svds(double(normalized_face),num_eigenfaces);
figure('Name','Top 20 Eigen faces','position',[100 100 600 660]);

%Visualize the Eigenfaces
for i=1:num_eigenfaces
    subplot(4,5,i);
    imshow(mat2gray(reshape(U(:,i),165,120)));
end

%Visualize the reconstruction Error for first image
mean_value = mean_face(330); 
face = double(fea(:,330));
figure('Name','Orginal Image');
imshow(mat2gray(reshape(face,165,120)));
Projection = U' * (face+mean_value);
reconstructed = U * Projection + mean_value;
figure('Name','Reconstructed Image');
imshow(mat2gray(reshape(reconstructed,165,120)));
error = calculateError(mat2gray(face),mat2gray(reconstructed));
str = sprintf( 'Squared Error in soft thresholding : %f ', error);
disp(str);

%%Problem 2
%%Using soft thresholding
num_of_coeff = 4;
lambda = 100;
while true
    x = pinv(U) * face;
    indexG = find(x > (0.5*lambda));
    indexL = find(x < (-0.5*lambda));
    if((length(indexG) + length(indexL)) > num_of_coeff )
        lambda = lambda *1.5;
    elseif((length(indexG) + length(indexL)) < num_of_coeff )
        lambda = lambda * 0.5;
    else
        break
    end
            
end
xx = zeros(size(x));
xx(indexG) = x(indexG) - (lambda*0.5);
xx(indexL) = x(indexL) + (lambda*0.5);
reconstructed = U * xx;
error = calculateError(mat2gray(face),mat2gray(reconstructed));
str = sprintf( 'Squared Error in soft thresholding : %f ', error);
disp(str);
figure('Name','Reconstructed Image Using Soft Threasholding');
imshow(mat2gray(reshape(reconstructed,165,120)));




