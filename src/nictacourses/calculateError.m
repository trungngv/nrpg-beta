function error = calculateError(x,y)
    error = (x-y)' * (x-y);
end