Submission by Trung V. Nguyen
VanTrung.Nguyen@nicta.com.au

To run the code, simply place the file containing the data set 'AR_165x120_Infer_Sunglass.mat' into a the current path of Matlab.

The eigenfaces all have appearance of ghosly blurry face and can be useful in recognising new faces. All faces wear glasses which are a submatrix in the original faces and can be removed by detecting the low rank submatrix. This is helpful to make prediction on faces that do not wear glasses.
