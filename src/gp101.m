% Generate utility functions using the preference kernel in Edwin's paper.
function mtgp()

  % construct a covariance matrix given a kernel function and a set of
  % points
  function C = constructCovMatrix(kernelFunc, L, x)
    C = zeros(length(x), length(x));
    for i=1:length(x)
      for j=1:length(x)
        C(i, j) = kernelFunc(x(i), x(j), L);
      end
    end
  end

  % construct the covariance matrix for the convolved GP process by the
  % r_th latent function
  function C = constructConvolvedCovMatrix(r)
    % S : the Q x 1 matrix, cofficients of the smoothing kernels k_qr
    % [S_1r; S_2r; ...; S_Qr]
    % L : Q x D x D cell where each cell is the scales of the smoothing kernels k_qr
    % (D = % dimension of input, i.e., cardinality of feature vector of an item x)
    % L_r : the D x D matrix, scales of the kernel of the latent functions
    C = cell(Q);
    for q=1:Q
      for s=1:Q
        C{q, s} = zeros(N, N);
        P = pinv(LQR{q, r}) + pinv(LQR{s, r}) + pinv(L(r));
        partition = S(q, r) * S(s, r) * (det(L(r))^-0.5) / sqrt(det(P));
        Pinv = pinv(P);
        for i=1:N
          for j=1:N
            C{q, s}(i, j) = partition * exp(-0.5 * (items(i,:) - items(j,:)) * Pinv * (items(i,:)-items(j,:))');
          end
        end
      end
    end
  end

  % preferene kernel k = k_f(q, s) * k_x(x, y) by edwin
  function k = prefKern(q, x, s, y, kf, kx)
    
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % Define some kernel functions
  linearKern = @(x, y, L) x'*y;
  gaussKern = @(x, y, L) exp(-0.5*(x-y).'*L*(x-y));
  %kx = gaussKern;
  %kf = gaussKern;
  %prefKern = @(q, x, s, y) % kernel in the Multi-task pref paper

  % Construct the covariance matrix K between all points
  D = 1; % input dimensions = 1
  R = 1; % latent functions = 1
  Q = 4; % number of users
  N = 300; % number of items
  
  % Set S = Q x R for each of the smoothing kernel k_qr
  S = 10 .* abs(randn(Q, R)); % each S_qr ~ N(0, 10)
  % Set S same as in the paper
  S(1, 1) = 1;
  S(2, 1) = 2;
  S(3, 1) = 5;
  S(4, 1) = 5;
  
  % Set L{qr} for each smoothing kernel function k_qr
  LQR = cell(Q, R, D, D);
  for q=1:Q
    for r=1:R
      % Trung: LQR{q, r} must be symmetric positive definite
      LQR{q, r} = 100 .* abs(randn(D, D));
    end
  end
  
  % Same setting as in the paper
  LQR{1, 1} = 50;
  LQR{2, 1} = 50;
  LQR{3, 1} = 300;
  LQR{4, 1} = 200;
  
  % Set the length-scales L_r for covariance functions of the latent
  % processes.
  % Setting same length-scale across all input dimension
  commonLr = 100 * eye(D);
  
  % Currently setting same L_r for al latent processes
  L = repmat(commonLr, R, 1);
  
  % Generate random item features (input points)
  items = zeros(N, D);
  for itemIdx=1:N
    items(itemIdx, :) = rand(1, D); % each feature ~ N(0, 1)
  end
  
  % initialize the cumulative covariance matrix
  cumMatrix = cell(Q);
  for q1=1:Q
    for q2=1:Q
      cumMatrix{q1, q2} = zeros(N, N);
    end
  end
  
  % Compute covariance for each latent process and sum them up
  for r=1:R
    covr = constructConvolvedCovMatrix(r);
    for q1=1:Q
      for q2=1:Q
        cumMatrix{q1, q2} = cumMatrix{q1, q2} + covr{q1, q2};
      end
    end
  end
  
  % The covariance matrix K(q,x, q', x') of size QN * QN
  % where first row is [q=1,x=1,q=1,x=1 ... q=1,x=1,q=1,x=N, q=1,x=1,q=2,x=1, ...,
  % q=1,x=1,q=2,x=N, ..., q=1,x=1,q=Q,x=1, ... q=1,x=1,q=Q,x=N] (Q*N
  % elements)!
  
  % Sampling from Gaussian N(0, K) gives an utility function 
  K = cell2mat(cumMatrix);
  fqx = gsamp(zeros(size(K, 1), 1), K, 1); % fqx has size 1xQN (1 row)
  
  % Reshape fqx to get Q utility functions for Q users
  fq = zeros(Q, N);
  for q=1:Q
    fq(q, :) = fqx((q-1)*N+1:q*N);
  end
  
  % plot the utility functions of all Q users
  figure(1); hold on;
  color = ['m', 'b', 'y', 'r'];
  for q=1:Q
    plot(items, fq(q, :), ['.' color(q)]);
  end
  axis([0,1,-20,20]);
  
  % 3. Sample function f from the GP.
  %f = sampleFromGP(cumMatrix, n);
  %plotResults(x, f, 'b');

end
