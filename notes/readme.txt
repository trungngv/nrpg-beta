Some implementation notes:
- Be sure to use the same random number generator (rng) with same seed for
repeated sequences of random numbers
- The covariance involving a pair of variable such as (q,x) has some
equivalent forms:
* K = K(f(q,x),f(s,x')) = kron(Kf, Kx) if the kernel is factorized
* K = K(f(q,x),f(s,x')) = cell2mat(C)
  where C{q,s} = Kx(q,s) for a pair of q and s
* K = cov(f, f) where f = [f1; f2; ...; fQ] and fq=[fq(x1); fq(x2); ...; fq(xN)].
In other words, f is the stack of column vectors of outputs for each task.
Visually:
f =
  [q1(x1)
   q1(x2)
   .
   q1(xN)
   .
   .
   qQ(x1)
   .
   qQ(xN)]

* The indexing of q and x for corresponding elements in K is:
repmat((1:Q),N,1)(:) because
repmat((1:Q),N,1) = 
[1 2 ... Q
 1 2 .. Q,
 ...
 1 2 .. Q]
(N rows)
and its stacked columns is
[1
 .
 .
 1
 .
 .
 .
 Q
 .
 .
 Q]
where each 1 and Q appears N times.

- A random vector fqx sampled from N(0, K) is a matrix of size 1 x QN
where the fq = reshape(fqx, N, Q)' gives samples of the utility functions.
fqx ~ N(0, K)
fq = reshape(fqx, N, Q)'
Now fq(q,:), the q_th row, is the values at different inputs x of the q_th task

