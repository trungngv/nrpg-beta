function predJura()
% function predJura()
% Using the multi-task gp to predict data for the Jura data set
% 1. Generates sample from true MTGP model 
% 2. Selects data-points for training
% 3. Plots training data
% 4. Sets parameters for learning (including initialization) 
% 5. Learns hyperparameters with minimize function
% 6. Makes predictions at all points on all tasks
% 7. Plots the predictions

rand('state',18);
randn('state',20);
M = 3;    % 3 tasks, one primary and two secondary
D = 2;    % input space = spatial coordinates of locations (x, y)
covfunc_x = 'covSEard'; % Covariance function on input space

% Load data from Jura swiss data set
[XTemp, yTemp, XTestTemp, yTestTemp] = mapLoadData(['juraData' 'Cu']);
x = XTemp{1}; % x = training data
%x = XTemp{2}; % test using all available input for the model

[N D]= size(x); % N = number of sample per tasks
n = N*M;        % n = N * M = total number of output points

% N x M matrix where each row is the output of M tasks

%Y = [[yTemp{1}; yTestTemp{1}], yTemp{2}(1:N), yTemp{3}(1:N)];
Y = [yTemp{1}(1:N), yTemp{2}(1:N), yTemp{3}(1:N)];
y = Y(:);
v = repmat((1:M),N,1); % why M x N? probably for Kf x Kx
ind_kf = v(:); % indices to tasks
v = repmat((1:N)',1,M);
ind_kx = v(:); % indices to input space data-points

% 2. Set up data points for training
% Actually the data loaded is for training data; here we are mapping to the
% variables in this model
% ntrain = n in our case
nx = ones(n,1); % Number of observations on each task-input point
ytrain = y;
xtrain = x;
ind_kx_train = ind_kx;
ind_kf_train = ind_kf;

% 3. Plotting all data and training data here
plot3(x(:,1),x(:,2),Y,'ko','markersize',7);
hold on;
for j = 1 : M 
  idx = find(ind_kf_train == j);
  plot3(x(ind_kx_train(idx),1),x(ind_kx_train(idx),2),...
        Y(ind_kx_train(idx),j),'k.','markersize',18);
  hold on;
end

% 4. Settings for learning
irank = 2;                         % Full rank

% Notice that Kf is a positive semi-definite matrix that specifies the
% inter-task similaries. Here Kf is parameterized with M*(M+1)/2
% parameters. Had we use the same kernel function, such as gaussian, with
% same scale parameter then number of parameter would be 1. On the other
% hand, if use different scales for each pair of fq, fq' then we need the
% same numbers to parametrize Kf; hence can learn the elements in Kf
% directly rather than learning the scales.

nlf = irank*(2*M - irank +1)/2;    % Number of parameters for Lf
theta_lf0 = init_Kf(M,irank);      % Parameter initialization 
theta_kx0 =  init_kx(D);           %
theta_sigma0 = init_sigma(M);      %
logtheta_all0 = [theta_lf0; theta_kx0; theta_sigma0];


% 5. We learn the hyper-parameters here
%    Optimize wrt all parameters except signal variance as full variance
%    is explained by Kf
deriv_range = ([1:nlf,nlf+2:length(logtheta_all0)])'; 
logtheta0 = logtheta_all0(deriv_range);
niter = 500; % setting for minimize function: number of function evaluations
[logtheta nl] = minimize(logtheta0,'learn_mtgp',niter, logtheta_all0, ...
			 covfunc_x, xtrain, ytrain,...
			 M, irank, nx, ind_kf_train, ind_kx_train, deriv_range);
%
% Update whole vector of parameters with learned ones
logtheta_all0(deriv_range) = logtheta;
theta_kf_learnt = logtheta_all0(1:nlf);
Ltheta_x = eval(feval('covSEard')); % Number of parameters of theta_x
theta_kx_learnt = logtheta_all0(nlf+1 : nlf+Ltheta_x);
theta_sigma_learnt = logtheta_all0(nlf+Ltheta_x+1:end);

% 6. Making predictions at validation points on each task
for task=1:M
  xtest = XTestTemp{task};
  Ntest = size(xtest,1);
  [alpha, Kf, L, Kxstar] = alpha_mtgp(logtheta_all0, covfunc_x, xtrain, ytrain,...
				    M, irank, nx, ind_kf_train, ...
				    ind_kx_train, xtest);
  all_Kxstar = Kxstar(ind_kx_train,:);
  Kf_task = Kf(ind_kf_train,task);
  Ypred = (repmat(Kf_task,1,Ntest).*all_Kxstar)'*alpha;

  disp(['Mean absoluate error for task ' num2str(task)])
  mean(abs((yTestTemp{task} - Ypred)))
end

% 7. Plotting the negative marginal log-likelihood
hold off;
figure(task), plot(nl); title('Negative Marginal log-likelihood');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function theta_kx0 = init_kx(D)
theta_kx0 = [log(1); log(rand(D,1))];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function theta_lf0 = init_Kf(M,irank)
Kf0 = eye(M);                 % Init to diagonal matrix (No task correlations)
Lf0 = chol(Kf0)';
theta_lf0 = lowtri2vec_inchol(Lf0,M,irank);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function theta_sigma0 = init_sigma(M)
theta_sigma0 =  (1e-7)*rand(M,1);  

