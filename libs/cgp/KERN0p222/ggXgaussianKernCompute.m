function [K, Linv, Ankinv, Bkinv, kBase, ...
    factorKern1y, factorKern1u ] = ggXgaussianKernCompute(ggKern, gaussianKern, x, x2)

% GGXGAUSSIANKERNCOMPUTE Compute a cross kernel between the GG and GAUSSIAN kernels.
%
%	Description:
%
%	K = GGXGAUSSIANKERNCOMPUTE(GGKERN, GAUSSIANKERN, X) computes cross
%	kernel terms between GG and GAUSSIAN kernels for the multiple output
%	kernel.
%	 Returns:
%	  K - block of values from kernel matrix.
%	 Arguments:
%	  GGKERN - the kernel structure associated with the GG kernel.
%	  GAUSSIANKERN - the kernel structure associated with the GAUSSIAN
%	   kernel.
%	  X - inputs for which kernel is to be computed.
%
%	K = GGXGAUSSIANKERNCOMPUTE(GGKERN, KERNEL., X, X2) computes cross
%	kernel terms between GG and GAUSSIAN kernels for the multiple output
%	kernel.
%	 Returns:
%	  K - block of values from kernel matrix.
%	 Arguments:
%	  GGKERN - the kernel structure associated with the GG kernel.
%	  KERNEL. - % ARG gaussianKern the kernel structure associated with
%	   the GAUSSIAN kernel.
%	  X - row inputs for which kernel is to be computed.
%	  X2 - column inputs for which kernel is to be computed.
%	
%	
%
%	See also
%	MULTIKERNPARAMINIT, MULTIKERNCOMPUTE, GGKERNPARAMINIT, GAUSSIANKERNPARAMINIT


%	Copyright (c) 2008 Mauricio A. Alvarez and Neil D. Lawrence
% 	ggXgaussianKernCompute.m SVN version 383
% 	last update 2009-06-04T08:29:43.974856Z
  
if nargin < 4
  x2 = x;
end

cond1 = isfield(ggKern, 'isNormalised') && ~isempty(ggKern.isNormalised);
cond2 = isfield(gaussianKern, 'isNormalised') && ~isempty(gaussianKern.isNormalised);
if cond1 == cond2
    if  ggKern.isNormalised ~= gaussianKern.isNormalised
        error('Both kernels should be normalised or unnormalised')
    end
else
    error('Both kernels should have flags for normalisation')
end

Ank = ggKern.precision_y;
mu_n = ggKern.translation;
Bk =  gaussianKern.precision_u;
Ankinv = 1./Ank;
Bkinv = 1./Bk;
P = Ankinv + Bkinv;
ldet = prod(P);
Linv = sqrt(1./P);
Linvx = (x- repmat(mu_n',size(x,1),1))*diag(Linv);
Linvx2 = x2*diag(Linv);
n2 = dist2(Linvx, Linvx2);

if cond1
    if ggKern.isNormalised
        preFactor = 1;
    else
        preFactor = prod(Bkinv);
    end
else
    preFactor = prod(Bkinv);
end

kBase = exp(-0.5*n2);

K = ggKern.sigma2_y*gaussianKern.sigma2_u*sqrt((preFactor)/ldet)*kBase;

if nargout > 1
    factorKern1y = ggKern.sigma2_u*sqrt((preFactor)/ldet);
    factorKern1u = ggKern.sigma2_y*sqrt((preFactor)/ldet);
end


