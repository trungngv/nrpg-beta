function options = lfmOptions

% LFMOPTIONS Creates a set of default options for a LFM model.
%
%	Description:
%
%	OPTIONS = LFMOPTIONS returns a default options stucture for a LFM
%	model.
%	 Returns:
%	  OPTIONS - the options structure.
%	
%
%	See also
%	LFMCREATE


%	Copyright (c) 2007 Neil D. Lawrence
% 	lfmOptions.m SVN version 20
% 	last update 2009-03-04T16:40:09.440520Z

options.optimiser = 'conjgrad';
