function [K, Linv, Ankinv, Amkinv, Bkinv, kBase, factorKern1y, ...
    factorKern2y, factorKern1u ] = ggXggKernCompute(ggKern1, ggKern2, x, x2)

% GGXGGKERNCOMPUTE Compute a cross kernel between two GG kernels.
%
%	Description:
%
%	K = GGXGGKERNCOMPUTE(GGKERN1, GGKERN2, X) computes cross kernel
%	terms between two GG kernels for the multiple output kernel.
%	 Returns:
%	  K - block of values from kernel matrix.
%	 Arguments:
%	  GGKERN1 - the kernel structure associated with the first GG
%	  GGKERN2 - the kernel structure associated with the second GG
%	   kernel.
%	  X - inputs for which kernel is to be computed.
%	DESC computes cross
%	kernel terms between two GG kernels for the multiple output kernel.
%	RETURN K :  block of values from kernel matrix.
%	ARG ggkern1 : the kernel structure associated with the first GG kernel.
%	ARG ggkern2 : the kernel structure associated with the second GG kernel.
%	ARG x : row inputs for which kernel is to be computed.
%	ARG x2 : column inputs for which kernel is to be computed.
%	
%	
%	
%
%	See also
%	MULTIKERNPARAMINIT, MULTIKERNCOMPUTE, GGKERNPARAMINIT


%	Copyright (c) 2006 Neil D. Lawrence


%	With modifications by Mauricio A. Alvarez 2008, 2009
% 	ggXggKernCompute.m SVN version 383
% 	last update 2009-06-04T08:29:44.626823Z

if nargin < 4
  x2 = x;
end
Ank = ggKern1.precision_y;
Amk = ggKern2.precision_y;
Bk = ggKern1.precision_u;
mu_n = ggKern1.translation;
mu_m = ggKern2.translation;
% If Ank, Amk is diagonal then 1./Ank, 1./Amk would be invalid.
% Hence Ank, Amk, Bk are only symmetric positive definite
% Then why does they compute 1./Ank as Ankinv??
% It seems that this kernel is only applicable to the case of 1D input!!
% Anyway can replace the 1./ thing with pinv to work with multidimensional
Ankinv = 1./Ank; % inv(L_qr)
Amkinv = 1./Amk; % inv(L_sr)
Bkinv = 1./Bk; % inv(L_r)
P = Ankinv + Amkinv + Bkinv; % inv(L_qr) + inv(L_sr) + inv(L_r)
ldet = prod(P); % if they store the diagonal matrix as vector ...
Linv = sqrt(1./P);
% Specific technique for computing the kernel of x and x2 (both are matrix)

% Say k(i, j) = (x(i) - x2(j))'C(x(i)-x2(j))
% Since L is symmetric positive definite, C = LL' (cholesky decomposition)
% (x(i)-x2(j))'LL'(x(i)-x2(j)) = [L'(x(i)-x2(j))]'[L'(x(i)-x2(j))]
% = norm(L'(x(i)-x2(j))]^2 = norm(L'(x(i)) - L'(x(j)))^2

% Hence k(i, j) = norm(L'(x(i)) - L'(x2(j)))^2, in other words, the
% distance b.w the 2 vectors L'x(i) and L'x2(j)

% Now X = [x(1)'; ...; x(n)'] and X2 = [x2(1)'; ...; x2(m)'] therefore
% X * L = [x(1)'L; ...; x(n)'L] and X2 * L = [x2(1)'L; ...; x2(m)'L]
% Using the dist2 function we would have:
% C = dist2(X*L, X2*L)
% where C(i, j) = dist2(row_i(X*L), row_j(X2*L)) = norm(row_i(X*L),
% row_j(X2*L))^2 = norm(x(i)'L - xx2(j)'L) = k(i, j).

% This allows us to use matrix operation for computing the kernel matrix
% rather than looping over each pair of elements.

Linvx = (x- repmat(mu_n',size(x,1),1))*diag(Linv); % why only take diagonal?
Linvx2 = (x2- repmat(mu_m',size(x2,1),1))*diag(Linv);
n2 = dist2(Linvx, Linvx2);
kBase = exp(-0.5*n2);
cond1 = isfield(ggKern1, 'isNormalised') && ~isempty(ggKern1.isNormalised);
cond2 = isfield(ggKern2, 'isNormalised') && ~isempty(ggKern2.isNormalised);
if cond1 == cond2
    if  ggKern1.isNormalised ~= ggKern2.isNormalised
        error('Both kernels should be normalised or unnormalised')
    end
else
    error('Both kernels should have flags for normalisation')
end

if cond1
    if ggKern1.isNormalised
        preFactor = 1;
    else
        detBkinv = prod(Bkinv);
        preFactor = detBkinv;
    end
else
   detBkinv = prod(Bkinv);
   preFactor = detBkinv; 
end

% term on the first line is common to all elements of K
K = ggKern1.sigma2_y*ggKern2.sigma2_y*ggKern1.sigma2_u*sqrt((preFactor)/ldet)...
    *kBase;

if nargout > 1
    factorKern1y = ggKern2.sigma2_y*ggKern1.sigma2_u*sqrt((preFactor)/ldet);
    factorKern2y = ggKern1.sigma2_y*ggKern1.sigma2_u*sqrt((preFactor)/ldet);
    factorKern1u = ggKern1.sigma2_y*ggKern2.sigma2_y*sqrt((preFactor)/ldet);
end
