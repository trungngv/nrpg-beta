function H = gpsimMapFunctionalLogLikeHessian(model)

% GPSIMMAPFUNCTIONALLOGLIKEHESSIAN Compute the functional Hessian for GPSIMMAP.
%
%	Description:
%
%	H = GPSIMMAPFUNCTIONALLOGLIKEHESSIAN(MODEL) computes the functional
%	Hessian of the log likelihood for use in the MAP approximation to
%	the GPSIM posterior solution.
%	 Returns:
%	  H - the Hessian of the log likelihood with respect to the points
%	   of the function.
%	 Arguments:
%	  MODEL - the model for which the Hessian is to be computed.
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPFUNCTIONALUPDATEW, GPSIMMAPUPDATEKERNELS


%	Copyright (c) 2006 Magnus Rattray and Neil D. Lawrence
% 	gpsimMapFunctionalLogLikeHessian.m CVS version 1.2
% 	gpsimMapFunctionalLogLikeHessian.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

H = - model.W - model.invK;
