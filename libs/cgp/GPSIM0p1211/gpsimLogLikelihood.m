function ll = gpsimLogLikelihood(model)

% GPSIMLOGLIKELIHOOD Compute the log likelihood of a GPSIM model.
%
%	Description:
%
%	LL = GPSIMLOGLIKELIHOOD(MODEL) computes the log likelihood of the
%	given Gaussian process for use in a single input motif protein
%	network.
%	 Returns:
%	  LL - the log likelihood of the data set.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%	
%
%	See also
%	GPSIMCREATE, GPSIMLOGLIKEGRADIENT, GPSIMOBJECTIVE


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimLogLikelihood.m CVS version 1.4
% 	gpsimLogLikelihood.m SVN version 7
% 	last update 2008-02-18T18:46:39.000000Z


dim = size(model.y, 1);
ll = -dim*log(2*pi) - model.logDetK - model.m'*model.invK*model.m;
ll = ll*0.5;

% In case we need priors in.
if isfield(model, 'bprior'),
  ll = ll + kernPriorLogProb(model.kern);
  ll = ll + priorLogProb(model.bprior, model.B);
end

