function ll = cgpsimLogLikelihood(model)

% CGPSIMLOGLIKELIHOOD Compound GPSIM model's log likelihood.
%
%	Description:
%
%	LL = CGPSIMLOGLIKELIHOOD(MODEL) this is just a wrapper for learning
%	three GP models simultaneously.
%	 Returns:
%	  LL - the log likelihood.
%	 Arguments:
%	  MODEL - input model for which gradients of the likelihood will be
%	   computed.
%	
%
%	See also
%	GPSIMLOGLIKELIHOOD


%	Copyright (c) 2006 Neil D. Lawrence
% 	cgpsimLogLikelihood.m CVS version 1.1
% 	cgpsimLogLikelihood.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

ll = 0;
for i = 1:length(model.comp)
  ll = ll + gpsimLogLikelihood(model.comp{i});
end