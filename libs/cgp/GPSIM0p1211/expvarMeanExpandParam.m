function model = expvarMeanExpandParam(model, params)

% EXPVARMEANEXPANDPARAM Update expvarMean mapping with new vector of parameters.
%
%	Description:
%
%	MODEL = EXPVARMEANEXPANDPARAM(MODEL, PARAMS) takes a vector of
%	EXPVARMEAN parameters and places them in their respective positions
%	in the EXPVARMEAN model. The function is a wrapper for the mlpunpa
%	command.
%	 Returns:
%	  MODEL - the model with the weights distributed in the correct
%	   places.
%	 Arguments:
%	  MODEL - the model in which the weights are to be placed.
%	  PARAMS - a vector of the weights to be placed in the model.
%	
%
%	See also
%	EXPVARMEANCREATE, EXPVARMEANEXTRACTPARAM, KERNEXPANDPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	expvarMeanExpandParam.m CVS version 1.2
% 	expvarMeanExpandParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

% simply update the parameters of the kernel.
model.kern = kernExpandParam(model.kern, params);