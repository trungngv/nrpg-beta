function f = gpsimArtificialProtein(t, alpha, mu, sigma);

% GPSIMARTIFICIALPROTEIN Generate an artifical protein sequence.
%
%	Description:
%	f = gpsimArtificialProtein(t, alpha, mu, sigma);
%% 	gpsimArtificialProtein.m CVS version 1.1
% 	gpsimArtificialProtein.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

f = zeros(size(t));
for i = 1:length(alpha)
  difft = (t - mu(i))/sigma(i);
  f = f + alpha(i)*exp(-difft.*difft);
end