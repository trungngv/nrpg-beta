function model = gpdisimExpandParam(model, params)

% GPDISIMEXPANDPARAM Expand the given parameters into a GPDISIM structure.
%
%	Description:
%
%	MODEL = GPDISIMEXPANDPARAM(MODEL, PARAMS) takes the given vector of
%	parameters and places them in the model structure, it then updates
%	any stored representations that are dependent on those parameters,
%	for example kernel matrices etc..
%	 Returns:
%	  MODEL - a returned model structure containing the updated
%	   parameters.
%	 Arguments:
%	  MODEL - the model structure for which parameters are to be
%	   updated.
%	  PARAMS - a vector of parameters for placing in the model
%	   structure.
%	
%
%	See also
%	GPSIMCREATE, GPSIMEXTRACTPARAM, MODELEXTRACTPARAM, GPSIMUPDATEKERNELS


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpdisimExpandParam.m CVS version 1.1
% 	gpdisimExpandParam.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

params = real(params);
if isfield(model, 'fix')
  for i = 1:length(model.fix)
    params(model.fix(i).index) = model.fix(i).value;
  end
end

if length(params) ~= model.numParams
  error('Parameter vector is incorrect length');
end
startVal = 1;
endVal = model.kern.nParams;
model.kern = kernExpandParam(model.kern, params(startVal:endVal));

fhandle = str2func([model.bTransform 'Transform']);
model.B = fhandle(params(endVal+1:end), 'atox');

% The decays and sensitivities are actually stored in the kernel.
% We'll put them here as well for convenience.
model.delta = model.kern.comp{2}.di_decay;
model.sigma = sqrt(model.kern.comp{2}.di_variance);
for i = 2:model.kern.numBlocks
  model.D(i-1) = model.kern.comp{i}.decay;
  model.S(i-1) = sqrt(model.kern.comp{i}.variance);
end
model.mu = model.B./model.D;

model = gpsimUpdateKernels(model);
lengthObs = size(model.t, 1);
ind = 1:lengthObs;
model.m(ind) = model.y(ind);
ind = ind + lengthObs;
for i = 1:model.numGenes
  model.m(ind) = model.y(ind) - model.mu(i)*ones(lengthObs, 1);
  ind = ind + lengthObs;
end
