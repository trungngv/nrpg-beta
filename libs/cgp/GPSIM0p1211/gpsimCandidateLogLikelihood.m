function ll = gpsimCandidateLogLikelihood(model)

% GPSIMCANDIDATELOGLIKELIHOOD Compute the log likelihood of a gene.
%
%	Description:
%
%	LL = GPSIMCANDIDATELOGLIKELIHOOD(MODEL) computes the log likelihood
%	of one gene given the existing genes in the GPSIM model. For use
%	when ranking other prospective candidate genes.
%	 Returns:
%	  LL - the log likelihood of the new gene.
%	 Arguments:
%	  MODEL - the model containing the existing genes for which the log
%	   likelihood is computed.
%	gpsimCandidateObjective
%	
%
%	See also
%	GPSIMCREATE, GPSIMADDCANDIDATE, GPSIMCANDIDATELOGLIKELIHOOD, 


%	Copyright (c) 2007 Neil D. Lawrence
% 	gpsimCandidateLogLikelihood.m CVS version 1.4
% 	gpsimCandidateLogLikelihood.m SVN version 7
% 	last update 2008-02-18T18:44:47.000000Z


dim = size(model.y, 1);

invK_ffm = model.candidate.invK*model.candidate.m;
K_ufinvK_ffm = model.candidate.K_uf*invK_ffm;

ll = -0.5*(dim*log(2*pi) + model.candidate.logDetK ...
           + model.candidate.logDetA ...
           - model.logDetK ...
           + sum(sum(invK_ffm.*model.candidate.m))...
           - sum(sum((model.candidate.Ainv*K_ufinvK_ffm).*K_ufinvK_ffm)));

% In case we need priors in.
if isfield(model.candidate, 'bprior'),
  ll = ll + kernPriorLogProb(model.candidate.kern);
  ll = ll + priorLogProb(model.candidate.bprior, model.B);
end

