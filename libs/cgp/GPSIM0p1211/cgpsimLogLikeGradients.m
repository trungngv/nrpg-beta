function g = cgpsimLogLikeGradients(model)

% CGPSIMLOGLIKEGRADIENTS Compound GPSIM model's gradients.
%
%	Description:
%
%	G = CGPSIMLOGLIKEGRADIENTS(MODEL) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  G - gradients of the log likelihood with repsect to the
%	   parameters.
%	 Arguments:
%	  MODEL - input model for which gradients of the likelihood will be
%	   computed.
%	
%
%	See also
%	GPSIMLOGLIKEGRADIENTS


%	Copyright (c) 2006 Neil D. Lawrence
% 	cgpsimLogLikeGradients.m CVS version 1.1
% 	cgpsimLogLikeGradients.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

g = gpsimLogLikeGradients(model.comp{1});
for i = 2:length(model.comp)
  g = g + gpsimLogLikeGradients(model.comp{i});
end