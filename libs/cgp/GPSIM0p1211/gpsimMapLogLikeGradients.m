function g = gpsimMapLogLikeGradients(model, gfdata)

% GPSIMMAPLOGLIKEGRADIENTS Compute the gradients of the log likelihood of a GPSIMMAP model.
%
%	Description:
%
%	G = GPSIMMAPLOGLIKEGRADIENTS(MODEL) computes the gradients of the
%	log likelihood of the given Gaussian process for use in a single
%	input motif protein network.
%	 Returns:
%	  G - the gradients of the parameters of the model.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%
%	G = GPSIMMAPLOGLIKEGRADIENTS(MODEL, GFDATA) computes the gradients
%	of the log likelihood of the given Gaussian process for use in a
%	single input motif protein network.
%	 Returns:
%	  G - the gradients of the parameters of the model.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%	  GFDATA - the functional gradient of the data portion of the log
%	   likelihood
%	gpsimMapGradient, gpsimMapFunctionalLogLikeGradients
%	
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPLOGLIKELIHOOD, 


%	Copyright (c) 2006 Magnus Rattray and Neil D. Lawrence


%	With modifications by Pei Gao 2008
% 	gpsimMapLogLikeGradients.m CVS version 1.5
% 	gpsimMapLogLikeGradients.m SVN version 7
% 	last update 2008-02-18T13:00:19.000000Z

% kernel parameters
dataRespond = false;
if dataRespond & nargin < 2
  [void, gfdata] = gpsimMapFunctionalLogLikeGradients(model);
end
covGrad = model.invK*(model.f*model.f'+model.covf)*model.invK  ...
           - model.invK;
covGrad = covGrad*0.5;
if dataRespond
  covGrad = covGrad - model.invK*model.covf*diag(model.covf* ...
                                                 model.W)*gfdata;
end
gkern = kernGradient(model.kern, model.mapt, covGrad);

% model parameters
gmodel = gpsimMapMarginalLikeliGradient(model);

g = [gkern gmodel];

% Add term arising from data likelihood (pg 125 in Rasmussen & Williams).
if strcmp(model.nonLinearity, 'linear')
  g1 = 0;
else
%  g1 = 0;
  g1 = gpsimMapLikeGradientImplicit(model);
end

g = g + g1;

if isfield(model, 'fix')
  for i = 1:length(model.fix)
    g(model.fix(i).index) = 0;
  end
end
