function ll = cgpdisimLogLikelihood(model)

% CGPDISIMLOGLIKELIHOOD Compound GPDISIM model's log likelihood.
%
%	Description:
%
%	LL = CGPDISIMLOGLIKELIHOOD(MODEL) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  LL - the log likelihood.
%	 Arguments:
%	  MODEL - input model for which gradients of the likelihood will be
%	   computed.
%	
%	
%
%	See also
%	GPDISIMLOGLIKELIHOOD


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	cgpdisimLogLikelihood.m CVS version 1.1
% 	cgpdisimLogLikelihood.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

ll = 0;
for i = 1:length(model.comp)
  ll = ll + gpdisimLogLikelihood(model.comp{i});
end
