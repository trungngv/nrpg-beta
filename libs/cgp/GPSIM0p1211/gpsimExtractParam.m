function [param, names] = gpsimExtractParam(model)

% GPSIMEXTRACTPARAM Extract the parameters of a GPSIM model.
%
%	Description:
%
%	PARAMS = GPSIMEXTRACTPARAM(MODEL) extracts the model parameters from
%	a structure containing the information about a Gaussian process for
%	single input motif modelling.
%	 Returns:
%	  PARAMS - a vector of parameters from the model.
%	 Arguments:
%	  MODEL - the model structure containing the information about the
%	   model.
%	
%
%	See also
%	GPSIMCREATE, GPSIMEXPANDPARAM, MODELEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimExtractParam.m CVS version 1.3
% 	gpsimExtractParam.m SVN version 7
% 	last update 2008-02-18T11:12:31.000000Z

if nargout>1
  [param, names] = kernExtractParam(model.kern);
  for i=1:length(model.mu);
    names{end+1}=['Basal transcription ' num2str(i)];
  end
else
  param = kernExtractParam(model.kern);
end
fhandle = str2func([model.bTransform 'Transform']);
param = [param fhandle(model.B, 'xtoa')];

if isfield(model, 'fix')
  for i = 1:length(model.fix)
    param(model.fix(i).index) = model.fix(i).value;
  end
end
param = real(param);