function model = gpsimOptimise(model, display, iters);

% GPSIMOPTIMISE Optimise the GPSIM model.
%
%	Description:
%
%	MODEL = GPSIMOPTIMISE(MODEL, DISPLAY, ITERS) optimises the Gaussian
%	process single input motif model for a given number of iterations.
%	 Returns:
%	  MODEL - the optimised model.
%	 Arguments:
%	  MODEL - the model to be optimised.
%	  DISPLAY - whether or not to display while optimisation proceeds,
%	   set to 2 for the most verbose and 0 for the least verbose.
%	  ITERS - number of iterations for the optimisation.
%	
%
%	See also
%	SCG, CONJGRAD, GPSIMCREATE, GPSIMGRADIENT, GPSIMOBJECTIVE


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimOptimise.m CVS version 1.1
% 	gpsimOptimise.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z


if nargin < 3
  iters = 2000;
  if nargin < 2
    display = 1;
  end
end


params = gpsimExtractParam(model);

options = optOptions;
if display
  options(1) = 1;
  if length(params) <= 100
    options(9) = 1;
  end
end
options(14) = iters;

if isfield(model, 'optimiser')
  optim = str2func(model.optimiser);
else
  optim = str2func('conjgrad');
end

params = optim('gpsimObjective', params,  options, ...
               'gpsimGradient', model);

model = gpsimExpandParam(model, params);
