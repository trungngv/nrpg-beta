function g = gpsimMapGradients(param, model)

% GPSIMMAPGRADIENTS Compute the gradients of the log likelihood of a GPSIMMAP model.
%
%	Description:
%
%	G = GPSIMMAPGRADIENTS(MODEL) computes the gradients of the log
%	likelihood of the given Gaussian process for use in a single input
%	motif protein network.
%	 Returns:
%	  G - the gradients of the parameters of the model.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%	gpsimMapGradient, gpsimMapFunctionalLogLikeGradients
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPLOGLIKELIHOOD, 


%	Copyright (c) Magnus Rattray and Neil D. Lawrence, 2008 Pei Gao
% 	gpsimMapGradients.m CVS version 1.1
% 	gpsimMapGradients.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z
  
  
g = zeros(1,length(param));
Nrep = length(model.comp);
   
for rep = 1:Nrep   % Work out likelihood gradient for each replicate
  options = defaultOptions;
  model.comp{rep} = gpsimMapExpandParam(model.comp{rep}, param);
  model.comp{rep} = gpsimMapUpdateF(model.comp{rep}, options);

  dg{rep} = gpsimMapLogLikeGradients(model.comp{rep});
  g = g - dg{rep};
end


