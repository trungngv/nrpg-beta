function g = gpdisimGradient(params, model)

% GPDISIMGRADIENT Gradient wrapper for a GPDISIM model.
%
%	Description:
%
%	G = GPDISIMGRADIENT(PARAMS, MODEL) wraps the log likelihood gradient
%	function to return the gradient of the negative of the log
%	likelihood. This can then be used in, for example, NETLAB,
%	minimisation tools.
%	 Returns:
%	  G - the returned gradient of the negative log likelihood for the
%	   given parameters.
%	 Arguments:
%	  PARAMS - the parameters of the model.
%	  MODEL - the model for which gradients will be computed.
%	
%	
%
%	See also
%	SCG, CONJGRAD, GPSIMCREATE, GPSIMOBJECTIVE, GPSIMLOGLIKEGRADIENT, GPSIMOPTIMISE


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	gpdisimGradient.m CVS version 1.1
% 	gpdisimGradient.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

model = gpdisimExpandParam(model, params);
g = - gpdisimLogLikeGradients(model);
