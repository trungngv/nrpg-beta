% GPSIM toolbox
% Version 0.1211		19-May-2009
% Copyright (c) 2009, Neil D. Lawrence
% 
, Neil D. Lawrence
% GPSIMXGRADIENT ...
% DISTFIT Fit a distribution to given parameter percentiles
% GPSIMLOADBARENCOTESTDATA Load in Martino Barenco's test data as processed by mmgMOS.
% DEMBARENCOVARIATIONAL1 Optimise model using variational approximation with RBF kernel and exponential response.
% GPSIMMAPECOLIRESULTS Plot the results from the MAP script.
% GPSIMLOADBARENCOPUMADATA Load in Martino Barenco's data as re-processed by mmgMOS.
% DEMBARENCORANK Do ranking experiments on data from Barenco et al in Genome Biology.
% CGPSIMEXPANDPARAM Expand params into model structure.
% GPSIMTOOLBOXES Toolboxes for the GPSIM software.
% GPSIMTEST Test the gradients of the GPSIM model.
% GPSIMMAPUPDATEYPRED Update the stored numerical prediction for y.
% GPDISIMEXTRACTPARAM Extract the parameters of a GPDISIM model.
% GPSIMMAPFUNCTIONALLOGLIKELIHOOD Compute the log likelihood of a GPSIMMAP model.
% GPDISIMLOGLIKELIHOOD Compute the log likelihood of a GPDISIM model.
% GPSIMMODELFUNCTIONS Update the nonlinear transformation of f.
% GPSIMMAPUPDATEPOSTERIORCOVARIANCE update the posterior covariance of f.
% GPSIMARTIFICIALPROTEIN Generate an artifical protein sequence.
% DEMECOLIMAP1 Optimise model using MAP approximation with MLP kernel and multiple repression response, using SCG optimisation and Ecoli data set.
% GPSIMADDCANDIDATE Add candidate genes to a GPSIM model.
% DEMBARENCOMAP1 Optimise model using MAP approximation with MLP kernel and MM activation response, using SCG optimisation and new PUMA processed data.
% GPSIMCANDIDATELOGLIKEGRADIENTS Compute the gradients for the parameters of candidate genes.
% EXPVARMEANOUT Output of an EXPVARMEAN model.
% CGPDISIMEXPANDPARAM Expand params into model structure.
% GPSIMMAPWGRADIENT Compute the gradients of W with respect to the parameters of the k-th gene.
% DEMTOYPROBLEM3 Generate an artifical data set and solve with GPSIM.
% GPDISIMLOGLIKEGRADIENTS Compute the gradients of the log likelihood of a GPDISIM model.
% GPSIMMAPGRADIENTS Compute the gradients of the log likelihood of a GPSIMMAP model.
% GPSIMMAPUPDATEG Update the nonlinear transformation of f.
% GPSIMMAPOBJECTIVE Compute the objective function  of the GPSIMMAP model.
% GPSIMSAMPLETEST Test the single input motif code.
% GPSIMMAPUPDATEYPREDVAR Update the variance for y.
% GPSIMMAPFUNCTIONALWGRADIENT computes the implicit part of the gradient
% GPSIMCANDIDATELOGLIKELIHOOD Compute the log likelihood of a gene.
% GPSIMSAMPLEHMC Do HMC sampling for the GPSIM model.
% GPSIMMAPUPDATEF Update posterior mean of f.
% CGPSIMLOGLIKEGRADIENTS Compound GPSIM model's gradients.
% DEMBARENCOMAP3 Optimise model using MAP approximation with MLP kernel and linear response, using SCG optimisation and new PUMA processed data with noise variance estimation.
% GPSIMLOGLIKEGRADIENTS Compute the gradients of the log likelihood of a GPSIM model.
% GPSIMMAPUPDATEKERNELS Updates the kernel representations in the GPSIMMAP structure.
% GPDISIMOPTIMISE Optimise the GPSIM model.
% DROSPLOTEXPPCTS Plot expression values
% GPSIMMAPTEST Tests the gradients of the GPSIMMAP model.
% GPSIMCANDIDATECOVGRADS Sparse objective function gradients wrt Covariance function.
% GPSIMDISPLAY Display a Gaussian process model.
% GPSIMMAPEXTRACTPARAM Extract the parameters of a GPSIMMAP model.
% GPSIMMAPFUNCTIONALEXTRACTPARAM Extract the function values from a GPSIMMAP model.
% GPSIMMAPFUNCTIONALUPDATEW Update the data component of the Hessian.
% GPSIMCANDIDATEOPTIMISE Optimise the GPSIM model for candidate genes.
% GPSIMLOADBARENCODATA Load in Martino Barenco's data as processed by mmgMOS.
% GPSIMARTIFICIALGENES Give artifical genes from given parameters.
% GPSIMCANDIDATEEXPANDPARAM Expand the given parameters for a candidate gene.
% DEMBARENCO1 Run experiments on data from Barenco et al in Genome Biology.
% DEMGPDISIMMEF2 Run experiments on Mef2 data. The raw data is pre-processed by the PUMA package.
% DEMBARENCOMAP2 Optimise model using MAP approximation with MLP kernel and exp response, using SCG optimisation and new PUMA processed data.
% EXPVARMEANCREATE Creates an the mean function for the EXP kernel.
% GPSIMMAPLOGLIKELIHOOD Compute the log likelihood of a GPSIMMAP model.
% CGPSIMLOGLIKELIHOOD Compound GPSIM model's log likelihood.
% CGPDISIMLOGLIKELIHOOD Compound GPDISIM model's log likelihood.
% GPSIMMAPLOGLIKEGRADIENTS Compute the gradients of the log likelihood of a GPSIMMAP model.
% DEMBARENCO2 Run experiments on data from Barenco et al in Genome Biology.
% GPSIMMAPMARGINALLIKELIGRADIENT Compute the gradients of the log marginal likelihood of a GPSIMMAP model with respect to the model parameters.
% CGPSIMEXTRACTPARAM Extract parameters from compound GPSIM model.
% GPSIMMAPGRADFUNCWRAPPER wraps the log-likelihood function and the gradient function 
% CGPDISIMLOGLIKEGRADIENTS Compound GPDISIM model's gradients.
% DEMTOYPROBLEM1 Generate an artifical data set and solve with GPSIM.
% GPSIMCREATE Create a GPSIM model.
% GPSIMOPTIMISE Optimise the GPSIM model.
% GPSIMGRADIENT Gradient wrapper for a GPSIM model.
% EXPVARMEANEXPANDPARAM Update expvarMean mapping with new vector of parameters.
% GPSIMMAPINITPARAM Creates a set of options for a GPSIMMAP model as the initial parameters.
% EXPVARMEANEXTRACTPARAM Extract weights and biases from an EXPVARMEAN mapping.
% DEMTOYPROBLEM2 Display results from toy problem point at a time.
% GPDISIMGRADIENT Gradient wrapper for a GPDISIM model.
% GPDISIMEXPANDPARAM Expand the given parameters into a GPDISIM structure.
% DEMTOYPROBLEM5 Generate an artifical data set and solve with GPSIM.
% DROSFINDTARGETS returns a sorted list of targets
% DROSGETGENEINDS returns indices of given genes in the given data
% GPSIMMAPFUNCTIONALLOGLIKEGRADIENTS Compute the functional gradient for GPSIMMAP.
% GPSIMMAPBARENCORESULTS Plot the results from the MAP script.
% GPSIMMAPFUNCTIONALLOGLIKEHESSIAN Compute the functional Hessian for GPSIMMAP.
% GPDISIMCREATE Create a GPDISIM model.
% GPSIMCANDIDATEOBJECTIVE Wrapper function for GPSIM candidate gene objective.
% GPSIMLOADECOLIFULLDATA Load in E. coli full data for the repression case.
% GPSIMMAPCREATE Create a GPSIMMAP model.
% GPSIMUPDATEKERNELS Updates the kernel representations in the GPSIM structure.
% GPSIMOBJECTIVE Wrapper function for GPSIM objective.
% GPSIMMAPLIKEGRADIENTIMPLICIT computes the implicit part of the gradient
% GPSIMLOADBARENCOMASDATA Load in Martino Barenco's data as processed by MAS5.
% CGPDISIMEXTRACTPARAM Extract parameters from compound GPDISIM model.
% GPSIMEXPANDPARAM Expand the given parameters into a GPSIM structure.
% GPSIMEXTRACTPARAM Extract the parameters of a GPSIM model.
% GPDISIMGETDROSDATA Get Drosophila data as processed by mmgMOS.
% GPSIMCANDIDATEUPDATEKERNELS Updates the kernel representations in the GPSIM candidate structure.
% GPSIMOPTIONS Creates a set of default options for a GPSIM model.
% GPSIMLOADECOLIDATA Load in E. coli data for the represion case.
% GPSIMMAPFUNCTIONALEXPANDPARAM Expand the given function values into a GPSIMMAP structure.
% GPDISIMOBJECTIVE Wrapper function for GPDISIM objective.
% GPSIMMAPEXPANDPARAM Expand the given parameters into a GPSIMMAP structure.
% GPSIMLOGLIKELIHOOD Compute the log likelihood of a GPSIM model.
% GPSIMCANDIDATEEXTRACTPARAM Extract the parameters of a GPSIM model.
% GPSIMMAPOPTIONS Creates a set of default options for a GPSIMMAP model.
% GPSIMMAPFUNCTIONALLIKEGRAD2 Compute the functional gradient for GPSIMMAP.
% DROSFINDSIMTARGETS returns a list of targets only controlled by given tf
