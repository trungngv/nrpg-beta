function f = gpsimObjective(params, model)

% GPSIMOBJECTIVE Wrapper function for GPSIM objective.
%
%	Description:
%
%	F = GPSIMOBJECTIVE(PARAMS, MODEL) returns the negative log
%	likelihood of a Gaussian process model for single input motifs given
%	the model structure and a vector parameters. This allows the use of
%	NETLAB minimisation functions to find the model parameters.
%	 Returns:
%	  F - the negative log likelihood of the GPSIM model.
%	 Arguments:
%	  PARAMS - the parameters of the model for which the objective will
%	   be evaluated.
%	  MODEL - the model structure for which the objective will be
%	   evaluated.
%	
%
%	See also
%	SCG, CONJGRAD, GPSIMCREATE, GPSIMGRADIENT, GPSIMLOGLIKELIHOOD, GPSIMOPTIMISE


%	Copyright (c) 2005, 2006 Neil D. Lawrence
% 	gpsimObjective.m CVS version 1.1
% 	gpsimObjective.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model = gpsimExpandParam(model, params);
f = - gpsimLogLikelihood(model);
