function Y = expvarMeanOut(model, X);

% EXPVARMEANOUT Output of an EXPVARMEAN model.
%
%	Description:
%
%	Y = EXPVARMEANOUT(MODEL, X) gives the output of the mean of a
%	variational approximation to a given kernel. This is for use with
%	the exponentiated kernel when making a variational approximation to
%	an exponentiated Gaussian process.
%	 Returns:
%	  Y - the output.
%	 Arguments:
%	  MODEL - the model for which the output is required.
%	  X - the input data for which the output is required.
%	
%	
%
%	See also
%	MODELOUT, EXPVARMEANCREATE


%	Copyright (c) 2006 Neil D. Lawrence
% 	expvarMeanOut.m CVS version 1.1
% 	expvarMeanOut.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

Y = exp(0.5*kernDiagCompute(model.kern.argument, X));