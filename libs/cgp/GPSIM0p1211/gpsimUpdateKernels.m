function model = gpsimUpdateKernels(model)

% GPSIMUPDATEKERNELS Updates the kernel representations in the GPSIM structure.
%
%	Description:
%
%	MODEL = GPSIMUPDATEKERNELS(MODEL) updates any representations of the
%	kernel in the model structure, such as invK, logDetK or K.
%	 Returns:
%	  MODEL - the model structure with the kernels updated.
%	 Arguments:
%	  MODEL - the model structure for which kernels are being updated.
%	
%	
%
%	See also
%	% SEEALSO GPSIMEXPANDPARAM, GPSIMCREATE


%	Copyright (c) 2006 % COPYRIGHT Neil D. Lawrence


%	With modifications by Pei Gao 2008
% 	gpsimUpdateKernels.m CVS version 1.6
% 	gpsimUpdateKernels.m SVN version 7
% 	last update 2008-02-18T11:15:52.000000Z
  
eps = 1e-6;                             % minimum noise variance for the
                                        % RBF kernel of TF.

if isfield(model, 'proteinPrior') && isfield(model, 'timesCell')
  k = real(kernCompute(model.kern, model.timesCell));
  if model.includeNoise
    noiseVar = [zeros(model.kern.comp{1}.diagBlockDim{1}, 1); model.yvar];
  else
    noiseVar = [eps*ones(model.kern.diagBlockDim{1}, 1); model.yvar];
  end
else
  k = real(kernCompute(model.kern, model.t));
  noiseVar = model.yvar;
end

model.K = k + diag(noiseVar);
[model.invK, U, jitter] = pdinv(model.K);
if jitter>1e-4
  fprintf('Warning: gpsimUpdateKernels added jitter of %2.4f\n', jitter)
end
model.logDetK = logdet(model.K, U);