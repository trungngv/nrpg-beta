function genes = drosFindSIMTargets(droschip, drosTF, tf),

% DROSFINDSIMTARGETS returns a list of targets only controlled by given tf
%
%	Description:
%	(according to the given ChIP-chip data)
%	
%	Usage:
%	genes = drosFindSIMTargets(droschip, drosTF, tf)
%	where tf is one of 'bap', 'bin', 'mef2', 'tin', 'twi'
%	


%	Copyright (c) 2007 Antti Honkela
% 	drosFindSIMTargets.m CVS version 1.1
% 	drosFindSIMTargets.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

I = strcmp(tf, drosTF.names);
tflabel = drosTF.labels(I);
inds = drosTF.chipinds{I};

J = find(sum(droschip.data(:, setdiff(1:15, inds))') == 0);
[foo, I] = sort(-sum(droschip.data(J, :) ~= 0, 2));
genes = droschip.genes(J(I));
