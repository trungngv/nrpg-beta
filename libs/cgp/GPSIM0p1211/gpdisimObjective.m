function f = gpdisimObjective(params, model)

% GPDISIMOBJECTIVE Wrapper function for GPDISIM objective.
%
%	Description:
%
%	F = GPDISIMOBJECTIVE(PARAMS, MODEL) returns the negative log
%	likelihood of a Gaussian process model for single input motifs given
%	the model structure and a vector parameters. This allows the use of
%	NETLAB minimisation functions to find the model parameters.
%	 Returns:
%	  F - the negative log likelihood of the GPDISIM model.
%	 Arguments:
%	  PARAMS - the parameters of the model for which the objective will
%	   be evaluated.
%	  MODEL - the model structure for which the objective will be
%	   evaluated.
%	
%	
%
%	See also
%	SCG, CONJGRAD, GPDISIMCREATE, GPDISIMGRADIENT, GPDISIMLOGLIKELIHOOD, GPDISIMOPTIMISE


%	Copyright (c) 2005, 2006 Neil D. Lawrence


%	With modifications by Antti Honkela 2007
% 	gpdisimObjective.m CVS version 1.1
% 	gpdisimObjective.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

model = gpdisimExpandParam(model, params);
f = - gpdisimLogLikelihood(model);
