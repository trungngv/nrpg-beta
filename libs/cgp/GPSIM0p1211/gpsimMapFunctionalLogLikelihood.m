function ll = gpsimMapFunctionalLogLikelihood(model)

% GPSIMMAPFUNCTIONALLOGLIKELIHOOD Compute the log likelihood of a GPSIMMAP model.
%
%	Description:
%
%	LL = GPSIMMAPFUNCTIONALLOGLIKELIHOOD(MODEL) computes the log
%	likelihood of the given Gaussian process for use in a single input
%	motif protein network. Acts as a wrapper for gpsimMapLogLikelihood.
%	 Returns:
%	  LL - the log likelihood of the data set.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%	
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPLOGLIKELIHOOD, GPSIMMAPFUNCTIONALLOGLIKEGRADIENTS


%	Copyright (c) 2006 Neil D. Lawrence


%	With modifications by Pei Gao 2008
% 	gpsimMapFunctionalLogLikelihood.m CVS version 1.4
% 	gpsimMapFunctionalLogLikelihood.m SVN version 7
% 	last update 2008-03-01T07:20:23.000000Z

ll = gpsimMapLogLikelihood(model)-0.5*model.logDetCovf;

% Add constraints
if isfield(model, 'priorProtein') && ~isempty(model.priorProtein)
  nCons = length(model.priorProtein);
  for i=1:nCons
    ftimeIndex = find((model.priorProteinTimes(i)-model.mapt)==0);
    ll = ll - 0.5*model.consLambda*(model.f(ftimeIndex) - model.priorProtein(i))^2;
  end
end
