function f = gpsimMapFunctionalExtractParam(model)

% GPSIMMAPFUNCTIONALEXTRACTPARAM Extract the function values from a GPSIMMAP model.
%
%	Description:
%
%	PARAMS = GPSIMMAPFUNCTIONALEXTRACTPARAM(MODEL) extracts the function
%	values from a structure containing the information about a Gaussian
%	process for single input motif modelling.
%	 Returns:
%	  PARAMS - a row vector of the function parameters from the model.
%	 Arguments:
%	  MODEL - the model structure containing the information about the
%	   model.
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPFUNCTIONALEXPANDPARAM, MODELEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimMapFunctionalExtractParam.m CVS version 1.2
% 	gpsimMapFunctionalExtractParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

f = model.f';
