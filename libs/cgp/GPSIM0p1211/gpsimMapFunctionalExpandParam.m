function model = gpsimMapFunctionalExpandParam(model, f)

% GPSIMMAPFUNCTIONALEXPANDPARAM Expand the given function values into a GPSIMMAP structure.
%
%	Description:
%
%	MODEL = GPSIMMAPFUNCTIONALEXPANDPARAM(MODEL, VALS) takes the given
%	vector of function values and places them in the model structure. It
%	then updates any stored representations that are dependent on those
%	function values, for example the Hessian, etc..
%	 Returns:
%	  MODEL - a returned model structure containing the updated function
%	   values.
%	 Arguments:
%	  MODEL - the model structure for which parameters are to be
%	   updated.
%	  VALS - a vector of function values for placing in the model
%	   structure.
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPFUNCTIONALEXTRACTPARAM, MODELEXTRACTPARAM, GPSIMMAPUPDATEKERNELS


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimMapFunctionalExpandParam.m CVS version 1.1
% 	gpsimMapFunctionalExpandParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model.f = f';
model = gpsimMapUpdateG(model);
model = gpsimMapUpdateYpred(model);
model = gpsimMapFunctionalUpdateW(model);
model = gpsimMapUpdatePosteriorCovariance(model);
