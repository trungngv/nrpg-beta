function gpsimDisplay(model, spaceNum)

% GPSIMDISPLAY Display a Gaussian process model.
%
%	Description:
%
%	GPSIMDISPLAY(MODEL, SPACENUM) displays in human readable form the
%	contents of the GPSIM model.
%	 Arguments:
%	  MODEL - the model structure to be displaced.
%	  SPACENUM - number of spaces to place before displaying model
%	   structure.
%	
%
%	See also
%	GPSIMCREATE, MODELDISPLAY.


%	Copyright (c) 2005, 2006 Neil D. Lawrence
% 	gpsimDisplay.m CVS version 1.1
% 	gpsimDisplay.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

if nargin > 1
  spacing = repmat(32, 1, spaceNum);
else
  spaceNum = 0;
  spacing = [];
end
spacing = char(spacing);
fprintf(spacing);
fprintf('Gaussian process single input motif model:\n')
fprintf(spacing);
fprintf('  Number of time points: %d\n', size(model.t, 1));
fprintf(spacing);
fprintf('  Number of genes: %d\n', model.numGenes);

if any(model.mu~=0)
  fprintf(spacing);
  fprintf('  Output biases:\n');
  for i = 1:length(model.mu)
    fprintf(spacing);
    fprintf('    Basal transcription gene %d: %2.4f\n', i, model.B(i));
  end
end

fprintf(spacing);
fprintf('  Kernel:\n')
kernDisplay(model.kern, 4+spaceNum);

