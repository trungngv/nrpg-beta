function I = drosGetGeneinds(drosdata, genes),

% DROSGETGENEINDS returns indices of given genes in the given data
%
%	Description:
%	
%	Usage:
%	I = drosGetGeneinds(drosdata, genes)


%	Copyright (c) 2007 Antti Honkela
% 	drosGetGeneinds.m CVS version 1.1
% 	drosGetGeneinds.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

I = [];

for k=1:length(genes),
  I = [I, find(strcmp(genes{k}, drosdata.genes))];
end
