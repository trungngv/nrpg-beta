function model = gpsimCreate(numGenes, numProteins, times, geneVals, ...
                             geneVars, options, annotation)

% GPSIMCREATE Create a GPSIM model.
%
%	Description:
%	The GPSIM model is a model for estimating the protein
%	concentration in a small gene network where several genes are
%	governed by one protein. The model is based on Gaussian processes
%	and simple linear differential equations of the form
%	
%	dx(t)/dt = B + Cf(t) - Dx(t)
%	
%	where x(t) is a given genes concentration and f(t) is the protein
%	concentration.
%	
%
%	MODEL = GPSIMCREATE(NUMGENES, NUMPROTEINS, TIMES, GENEVALS,
%	GENEVARS, OPTIONS, ANNOTATION) creates a model for single input
%	motifs with Gaussian processes.
%	 Returns:
%	  MODEL - model structure containing default parameterisation.
%	 Arguments:
%	  NUMGENES - number of genes to be modelled in the system.
%	  NUMPROTEINS - number of proteins to be modelled in the system.
%	  TIMES - the time points where the data is to be modelled.
%	  GENEVALS - the values of each gene at the different time points.
%	  GENEVARS - the varuabces of each gene at the different time
%	   points.
%	  OPTIONS - options structure, the default options can be generated
%	   using gpsimOptions.
%	  ANNOTATION - annotation for the data (gene names, etc.) that is
%	   stored with the model. (Optional)
%	
%	
%
%	See also
%	MODELCREATE, GPSIMOPTIONS


%	Copyright (c) 2006, 2007 Neil D. Lawrence


%	With modifications by Pei Gao 2008


%	With modifications by Antti Honkela 2008
% 	gpsimCreate.m CVS version 1.6
% 	gpsimCreate.m SVN version 137
% 	last update 2008-11-05T12:09:05.000000Z

if any(size(geneVars)~=size(geneVals))
  error('The gene variances have a different size matrix to the gene values.');
end

if(numGenes ~= size(geneVals, 2))
  error('The number of genes given does not match the dimension of the gene values given.')
end

if(size(times, 1) ~= size(geneVals, 1))
  error('The number of time points given does not match the number of gene values given')
end

model.type = 'gpsim';

kernType1{1} = 'multi';

if isfield(options, 'proteinPrior') && ~isempty(options.proteinPrior)
  model.proteinPrior = options.proteinPrior;
  kernType1{2} = 'rbf';
  if isfield(options, 'proteinPriorTimes')
    timesCell{1} = options.proteinPriorTimes;
  else
    timesCell{1} = times;
  end
  tieParam{1} = [1];                    % RBF kernel parameters: inverse
                                        % widths and variance.
  for i = 1:numGenes
    kernType1{i+2} = 'sim';
    timesCell{i+1} = times; 
    tieParam{1} = [tieParam{1} tieParam{1}(end)+3];
  end  
  model.timesCell = timesCell;
else
  timesCell = times;                     % Non-cell structure in this case
  tieParam{1} = [2]; % These are the indices of the inverse widths which
                % need to be constrained to be equal.
  for i = 1:numGenes
    kernType1{i+1} = 'sim';
    if i>1
      tieParam{1} = [tieParam{1} tieParam{1}(end)+3];
    end
  end
end

model.y = geneVals(:);

model.includeNoise = options.includeNoise;

% if model.includeNoise
%   model.yvar = zeros(size(geneVars(:)));
% else
  model.yvar = geneVars(:);
% end

% Check if we have a noise term.
if model.includeNoise
  % Create a new multi kernel to contain the noise term.
  kernType2{1} = 'multi';
  % NEIL: Need to set up tie param to hold the variances of the white kernels
  % the same ... perhaps have an option that determines to do this or not.
  % tieParam{2} = INDEX OF FIRST NOISE VARIANCE; % These are the indices of the variances.

  % Set the new multi kernel to just contain 'white' kernels.
  if isfield(model, 'proteinPrior') && ~isempty(model.proteinPrior)
    kernType2{2}='whitefixed';
    for i = 2:(numGenes+1)
      kernType2{i+1} = 'white';
    end    
  else
    for i = 1:numGenes
      kernType2{i+1} = 'white';
    end
  end
  if isfield(options, 'singleNoise') & options.singleNoise
    % NEIL Again, need to get the right indices on tie param if the
    tieParam{2} = 2+3*numGenes + 1;
    for i = 1:numGenes
      if i>1
        tieParam{2} = [tieParam{2} tieParam{2}(end)+1];
      end
    end
    
  end
            
  
  % Now create model with a 'cmpnd' (compound) kernel build from two
  % multi-kernels. The first multi-kernel is the sim-sim one the next
  % multi-kernel is the white-white one. 
  model.kern = kernCreate(timesCell, {'cmpnd', kernType1, kernType2});
else
  model.kern = kernCreate(timesCell, kernType1);
end

% This is if we need to place priors on parameters ...
if isfield(options, 'addPriors') && options.addPriors,
  for i = 1:length(model.kern.numBlocks)
    % Priors on the sim kernels.
    model.kern.comp{i}.priors = priorCreate('gamma');
    model.kern.comp{i}.priors.a = 1;
    model.kern.comp{i}.priors.b = 1;
    if i == 1
      % For first kernel place prior on inverse width.
      model.kern.comp{i}.priors.index = [1 2 3];
    else
      % For other kernels don't place prior on inverse width --- as
      % they are all tied together and it will be counted multiple
      % times.
      model.kern.comp{i}.priors.index = [1 3];
    end
  end

  % Prior on the b values.
  model.bprior = priorCreate('gamma');
  model.bprior.a = 1;
  model.bprior.b = 1;
end

model.kern = modelTieParam(model.kern, tieParam);
model.kern.comp{2}.comp{1}.variance = 1e-6;

% The decays and sensitivities are actually stored in the kernel.
% We'll put them here as well for convenience.
if isfield(model, 'proteinPrior') && ~isempty(model.proteinPrior)
  for i = 2:model.kern.numBlocks
    if model.includeNoise
      model.D(i-1) = model.kern.comp{1}.comp{i}.decay;
      model.S(i-1) = sqrt(model.kern.comp{1}.comp{i}.variance);
    else
      model.D(i-1) = model.kern.comp{i}.decay;
      model.S(i-1) = sqrt(model.kern.comp{i}.variance);
    end
  end  
else
  for i = 1:model.kern.numBlocks
    if model.includeNoise
      model.D(i) = model.kern.comp{1}.comp{i}.decay;
      model.S(i) = sqrt(model.kern.comp{1}.comp{i}.variance);
    else
      model.D(i) = model.kern.comp{i}.decay;
      model.S(i) = sqrt(model.kern.comp{i}.variance);
    end
  end 
end

model.numParams = numGenes + model.kern.nParams;
model.numGenes = numGenes;
model.mu = mean(geneVals);
model.B = model.D.*model.mu;

if isfield(model, 'proteinPrior') && ~isempty(model.proteinPrior)
  dim = size(model.proteinPrior, 1) + size(model.y, 1);
  model.m = [model.proteinPrior; model.y];
else
  model.m = model.y;
  model.t = times;
end

model.optimiser = options.optimiser;

if isfield(options, 'fix')
  model.fix = options.fix;
end

% The basal transcriptions rates must be postitive.
model.bTransform = optimiDefaultConstraint('positive');

if nargin > 6,
  model.annotation = annotation;
end

% This forces kernel compute.
params = gpsimExtractParam(model);
model = gpsimExpandParam(model, params);

