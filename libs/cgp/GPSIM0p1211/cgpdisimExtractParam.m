function [param, names] = cgpdisimExtractParam(model)

% CGPDISIMEXTRACTPARAM Extract parameters from compound GPDISIM model.
%
%	Description:
%
%	PARAM = CGPDISIMEXTRACTPARAM(MODEL) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  PARAM - parameters extracted from the model.
%	 Arguments:
%	  MODEL - input model from which parameters will be extracted.
%	
%	
%
%	See also
%	GPDISIMEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	cgpdisimExtractParam.m CVS version 1.1
% 	cgpdisimExtractParam.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

if nargout == 1,
  param = gpdisimExtractParam(model.comp{1});
else
  [param, names] = gpdisimExtractParam(model.comp{1});
end
