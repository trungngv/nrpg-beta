function f = gpsimMapObjective(param, model)

% GPSIMMAPOBJECTIVE Compute the objective function  of the GPSIMMAP model.
%
%	Description:
%
%	F = GPSIMMAPOBJECTIVE(MODEL) computes the objective function of the
%	model given Gaussian process for use in a single input motif protein
%	network.
%	 Returns:
%	  F - the objective function of the model.
%	 Arguments:
%	  MODEL - the model for which the objective function is computed.
%	gpsimMapGradient, gpsimMapFunctionalLogLikeGradients
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPLOGLIKELIHOOD, 


%	Copyright (c) Magnus Rattray and Neil D. Lawrence, 2008 Pei Gao
% 	gpsimMapObjective.m CVS version 1.1
% 	gpsimMapObjective.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z
  

if isfield(model, 'comp')
    Nrep = length(model.comp);
    for rep=1:Nrep  %Work out likelihood gradient for each replicate
        options = defaultOptions;
        model.comp{rep} = gpsimMapExpandParam(model.comp{rep}, param);
%         f = gpsimMapFunctionalExtractParam(model.comp{rep});
%         model.comp{rep} = gpsimMapFunctionalExpandParam(model.comp{rep}, f);        
%        options(1) = 1;
        model.comp{rep} = gpsimMapUpdateF(model.comp{rep}, options);
        ll(rep) = gpsimMapLogLikelihood(model.comp{rep});
    end
end

f = - sum(ll);

