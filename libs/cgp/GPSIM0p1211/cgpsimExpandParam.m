function model = cgpsimExpandParam(model, params)

% CGPSIMEXPANDPARAM Expand params into model structure.
%
%	Description:
%
%	MODEL = CGPSIMEXPANDPARAM(MODEL, PARAMS) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  MODEL - model filled with the given parameters.
%	 Arguments:
%	  MODEL - input model from which parameters will be extracted.
%	  PARAMS - parameters to place in the model.
%	
%
%	See also
%	GPSIMEXPANDPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	cgpsimExpandParam.m CVS version 1.1
% 	cgpsimExpandParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

for i = 1:length(model.comp)
  model.comp{i} = gpsimExpandParam(model.comp{i}, params);
end