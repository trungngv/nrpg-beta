function genes = drosFindTargets(droschip),

% DROSFINDTARGETS returns a sorted list of targets
%
%	Description:
%	
%	Usage:
%	genes = drosFindTargets(droschip)


%	Copyright (c) 2008 Antti Honkela
% 	drosFindTargets.m CVS version 1.1
% 	drosFindTargets.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

[foo, I] = sort(-sum(droschip.data ~= 0, 2));
genes = droschip.genes(I);
