function options = gpsimOptions

% GPSIMOPTIONS Creates a set of default options for a GPSIM model.
%
%	Description:
%
%	OPTIONS = GPSIMOPTIONS returns a default options stucture for a
%	GPSIM model.
%	 Returns:
%	  OPTIONS - the options structure.
%	
%	
%
%	See also
%	GPSIMCREATE


%	Copyright (c) 2006, 2008 Neil D. Lawrence


%	With modifications by Pei Gao 2008
% 	gpsimOptions.m CVS version 1.1
% 	gpsimOptions.m SVN version 7
% 	last update 2008-04-11T10:09:39.000000Z

options.optimiser = 'conjgrad';
options.includeNoise = 0;
options.singleNoise = false;