function options = gpsimMapOptions(numGenes)

% GPSIMMAPOPTIONS Creates a set of default options for a GPSIMMAP model.
%
%	Description:
%
%	OPTIONS = GPSIMMAPOPTIONS(NUMGENES) returns a default options
%	stucture for a GPSIMMAP model.
%	 Returns:
%	  OPTIONS - the options structure.
%	 Arguments:
%	  NUMGENES - the number of Genes that the model will consider.
%	
%
%	See also
%	GPSIMMAPCREATE


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimMapOptions.m CVS version 1.1
% 	gpsimMapOptions.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

options = gpsimOptions;

% Number of points for the numerical path integration (complexity
% is cubic in this value).
options.intPoints = 100;
options.kern = 'rbf';
options.nonLinearity = 'exp';

if nargin < 1
  options.B = [];
  options.D = [];
  options.S = [];
else
  options.B = ones(1, numGenes);
  options.D = ones(1, numGenes);
  options.S = ones(1, numGenes);
end