function samples = gpsimSampleHMC(model, display, iters);

% GPSIMSAMPLEHMC Do HMC sampling for the GPSIM model.
%
%	Description:
%
%	SAMPLES = GPSIMSAMPLEHMC(MODEL, DISPLAY, ITERS) performs HMC
%	sampling for the Gaussian process single input motif model for a
%	given number of iterations.
%	 Returns:
%	  SAMPLES - the samples.
%	 Arguments:
%	  MODEL - the model to be optimised.
%	  DISPLAY - whether or not to display while optimisation proceeds,
%	   set to 2 for the most verbose and 0 for the least verbose.
%	  ITERS - number of samples to return.
%	
%	
%
%	See also
%	HMC, GPSIMCREATE, GPSIMGRADIENT, GPSIMOBJECTIVE


%	Copyright (c) 2006 Neil D. Lawrence


%	With modifications by Antti Honkela 2007
% 	gpsimSampleHMC.m CVS version 1.1
% 	gpsimSampleHMC.m SVN version 7
% 	last update 2008-02-18T18:18:08.000000Z


if nargin < 3
  iters = 2000;
  if nargin < 2
    display = 1;
  end
end


params = modelExtractParam(model);

options = optOptions;
if display
  options(1) = display;
  if length(params) <= 100
    options(9) = 1;
  end
end
% Momentum persistence
options(5) = 1;
% Leapfrog steps
options(7) = 10;
options(18) = .01;
% Number of samples to return
options(14) = iters;

samples = hmc('modelObjective', params,  options, ...
	      'modelGradient', model);
