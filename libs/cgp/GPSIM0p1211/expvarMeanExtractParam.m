function [params, names] = expvarMeanExtractParam(model)

% EXPVARMEANEXTRACTPARAM Extract weights and biases from an EXPVARMEAN mapping.
%
%	Description:
%
%	[PARAMS, NAMES] = EXPVARMEANEXTRACTPARAM(MODEL) returns a vector of
%	all the parameters from the mapping associated with the mean
%	function of a exponential variational approximation.
%	 Returns:
%	  PARAMS - vector of all the parameters.
%	  NAMES - optional additional returned cell array of the names of
%	   the parameters.
%	 Arguments:
%	  MODEL - the mappings from which we wish to extract the parameters.
%	   and biases.
%	
%
%	See also
%	EXPVARMEANCREATE, EXPVARMEANEXPANDPARAM, MODELEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	expvarMeanExtractParam.m CVS version 1.2
% 	expvarMeanExtractParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

if nargout > 1
  [params, names] = kernExtractParam(model.kern);
else
  params = kernExtractParam(model.kern);
end
