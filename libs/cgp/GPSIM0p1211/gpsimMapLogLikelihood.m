function ll = gpsimMapLogLikelihood(model, df)

% GPSIMMAPLOGLIKELIHOOD Compute the log likelihood of a GPSIMMAP model.
%
%	Description:
%
%	LL = GPSIMMAPLOGLIKELIHOOD(MODEL) computes the log likelihood of the
%	given Gaussian process for use in a single input motif protein
%	network.
%	 Returns:
%	  LL - the log likelihood of the data set.
%	 Arguments:
%	  MODEL - the model for which the log likelihood is computed.
%	
%	
%
%	See also
%	GPSIMMAPCREATE, GPSIMMAPLOGLIKEGRADIENT, GPSIMMAPOBJECTIVE


%	Copyright (c) 2006 Neil D. Lawrence


%	With modifications by Pei Gao 2008
% 	gpsimMapLogLikelihood.m CVS version 1.5
% 	gpsimMapLogLikelihood.m SVN version 7
% 	last update 2008-03-01T07:19:40.000000Z

numData = length(model.t);

% ll = model.f'*model.invK*model.f ...
%       + log(det(eye(size(model.W))+model.K*model.W));
ll = model.f'*model.invK*model.f ...
    - model.logDetCovf + model.logDetK;

if isfield(model, 'includeNoise') && model.includeNoise
  noiseMat = ones(length(model.t), 1)*model.noiseVar;
  yvar = model.yvar + noiseMat;
else
  yvar = model.yvar;
end

for i = 1:numData
  for j = 1:model.numGenes
    ind = i + (j-1)*numData;
    beta_ij = 1/yvar(ind);
    factor = (model.ypred(model.times_index(i), j)...
              - model.y(ind));
    ll = ll + factor*factor*beta_ij - log(beta_ij);
  end
end

ll = ll + numData*model.numGenes*log(2*pi);

ll = -.5*ll;
