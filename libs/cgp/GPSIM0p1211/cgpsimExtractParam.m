function [param, names] = cgpsimExtractParam(model)

% CGPSIMEXTRACTPARAM Extract parameters from compound GPSIM model.
%
%	Description:
%
%	PARAM = CGPSIMEXTRACTPARAM(MODEL) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  PARAM - parameters extracted from the model.
%	 Arguments:
%	  MODEL - input model from which parameters will be extracted.
%	
%
%	See also
%	GPSIMEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
% 	cgpsimExtractParam.m CVS version 1.2
% 	cgpsimExtractParam.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

if nargout == 1,
  param = gpsimExtractParam(model.comp{1});
else
  [param, names] = gpsimExtractParam(model.comp{1});
end
