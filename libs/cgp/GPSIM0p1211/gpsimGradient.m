function g = gpsimGradient(params, model)

% GPSIMGRADIENT Gradient wrapper for a GPSIM model.
%
%	Description:
%
%	G = GPSIMGRADIENT(PARAMS, MODEL) wraps the log likelihood gradient
%	function to return the gradient of the negative of the log
%	likelihood. This can then be used in, for example, NETLAB,
%	minimisation tools.
%	 Returns:
%	  G - the returned gradient of the negative log likelihood for the
%	   given parameters.
%	 Arguments:
%	  PARAMS - the parameters of the model.
%	  MODEL - the model for which gradients will be computed.
%	
%
%	See also
%	SCG, CONJGRAD, GPSIMCREATE, GPSIMOBJECTIVE, GPSIMLOGLIKEGRADIENT, GPSIMOPTIMISE


%	Copyright (c) 2006 Neil D. Lawrence
% 	gpsimGradient.m CVS version 1.1
% 	gpsimGradient.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model = gpsimExpandParam(model, params);
g = - gpsimLogLikeGradients(model);
