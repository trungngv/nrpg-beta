function model = expvarMeanCreate(inputDim, outputDim, options)

% EXPVARMEANCREATE Creates an the mean function for the EXP kernel.
%
%	Description:
%
%	MODEL = EXPVARMEANCREATE(INPUTDIMENSION, OUTPUTDIM, OPTIONS) creates
%	a model for returning the first moment of an 'exponentiated Gaussian
%	process'. If the output of a Gaussian process is exponentiated the
%	resulting process is no longer Gaussian. However, it can be
%	approximated by a Gaussian process by matching its first and second
%	moments. This function returns the mean of that approximating
%	process for a given kernel (specified in the options vector). It
%	should be used in tandem with the EXP kernel for approximating these
%	Gaussian processes.
%	 Returns:
%	  MODEL - model structure containing the mapping.
%	 Arguments:
%	  INPUTDIMENSION - dimension of input to function.
%	  OUTPUTDIM - dimension of output from mean function data.
%	  OPTIONS - options structure. The structure contains the type of
%	   kernel that the function is based on. A set of default options are
%	   given by the file expvarMeanOptions.
%	
%
%	See also
%	EXPVARMEANOPTIONS


%	Copyright (c) 2005, 2006 Neil D. Lawrence
% 	expvarMeanCreate.m CVS version 1.1
% 	expvarMeanCreate.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model.type = 'expvarMean';
if isstruct(options.kern) 
  model.kern = options.kern;
else
  model.kern = kernCreate(inputDim, options.kern);
end
model.q = inputDim;
model.d = outputDim;
model.numParams = model.kern.nParams;
