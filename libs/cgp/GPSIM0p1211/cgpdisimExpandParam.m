function model = cgpdisimExpandParam(model, params)

% CGPDISIMEXPANDPARAM Expand params into model structure.
%
%	Description:
%
%	MODEL = CGPDISIMEXPANDPARAM(MODEL, PARAMS) this is just a wrapper
%	for learning three GP models simultaneously.
%	 Returns:
%	  MODEL - model filled with the given parameters.
%	 Arguments:
%	  MODEL - input model from which parameters will be extracted.
%	  PARAMS - parameters to place in the model.
%	
%	
%
%	See also
%	GPDISIMEXPANDPARAM


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	cgpdisimExpandParam.m CVS version 1.1
% 	cgpdisimExpandParam.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

for i = 1:length(model.comp)
  model.comp{i} = gpdisimExpandParam(model.comp{i}, params);
end
