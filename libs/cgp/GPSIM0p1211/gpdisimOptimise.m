function model = gpdisimOptimise(model, display, iters);

% GPDISIMOPTIMISE Optimise the GPSIM model.
%
%	Description:
%
%	MODEL = GPDISIMOPTIMISE(MODEL, DISPLAY, ITERS) optimises the
%	Gaussian process single input motif model for a given number of
%	iterations.
%	 Returns:
%	  MODEL - the optimised model.
%	 Arguments:
%	  MODEL - the model to be optimised.
%	  DISPLAY - whether or not to display while optimisation proceeds,
%	   set to 2 for the most verbose and 0 for the least verbose.
%	  ITERS - number of iterations for the optimisation.
%	
%	
%
%	See also
%	SCG, CONJGRAD, GPDISIMCREATE, GPDISIMGRADIENT, GPDISIMOBJECTIVE


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	gpdisimOptimise.m CVS version 1.1
% 	gpdisimOptimise.m SVN version 7
% 	last update 2008-02-18T18:18:23.000000Z


if nargin < 3
  iters = 2000;
  if nargin < 2
    display = 1;
  end
end


params = gpdisimExtractParam(model);

options = optOptions;
if display
  options(1) = 1;
  if length(params) <= 100
    options(9) = 1;
  end
end
options(14) = iters;

if isfield(model, 'optimiser')
  optim = str2func(model.optimiser);
else
  optim = str2func('conjgrad');
end

params = optim('gpdisimObjective', params,  options, ...
               'gpdisimGradient', model);

model = gpdisimExpandParam(model, params);
