function f = gpsimCandidateObjective(params, model)

% GPSIMCANDIDATEOBJECTIVE Wrapper function for GPSIM candidate gene objective.
%
%	Description:
%
%	F = GPSIMCANDIDATEOBJECTIVE(PARAMS, MODEL) returns the negative log
%	likelihood of candidate genes given a Gaussian process  model for
%	single input motifs given the model structure and a vector
%	parameters. This allows the use of NETLAB minimisation functions to
%	find the model parameters.
%	 Returns:
%	  F - the negative log likelihood of the candidate genes given the
%	   GPSIM model.
%	 Arguments:
%	  PARAMS - the parameters of the model for which the objective will
%	   be evaluated.
%	  MODEL - the model structure for which the objective will be
%	   evaluated.
%	
%
%	See also
%	SCG, CONJGRAD, GPSIMCREATE, GPSIMADDCANDIDATE, GPSIMCANDIDATEGRADIENT, GPSIMCANDIDATELOGLIKELIHOOD, GPSIMCANDIDATEOPTIMISE


%	Copyright (c) 2007 Neil D. Lawrence
% 	gpsimCandidateObjective.m CVS version 1.1
% 	gpsimCandidateObjective.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model = gpsimCandidateExpandParam(model, params);
f = - gpsimCandidateLogLikelihood(model);
