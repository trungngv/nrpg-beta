function g = cgpdisimLogLikeGradients(model)

% CGPDISIMLOGLIKEGRADIENTS Compound GPDISIM model's gradients.
%
%	Description:
%
%	G = CGPDISIMLOGLIKEGRADIENTS(MODEL) this is just a wrapper for
%	learning three GP models simultaneously.
%	 Returns:
%	  G - gradients of the log likelihood with repsect to the
%	   parameters.
%	 Arguments:
%	  MODEL - input model for which gradients of the likelihood will be
%	   computed.
%	
%	
%
%	See also
%	GPDISIMLOGLIKEGRADIENTS


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	cgpdisimLogLikeGradients.m CVS version 1.1
% 	cgpdisimLogLikeGradients.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

g = gpdisimLogLikeGradients(model.comp{1});
for i = 2:length(model.comp)
  g = g + gpdisimLogLikeGradients(model.comp{i});
end
