function [model, ll] = gpsimMapUpdateKernels(model)

% GPSIMMAPUPDATEKERNELS Updates the kernel representations in the GPSIMMAP structure.
%
%	Description:
%
%	[MODEL, LL] = GPSIMMAPUPDATEKERNELS(MODEL) updates any
%	representations of the kernel in the model structure, such as invK,
%	logDetK or K.
%	 Returns:
%	  MODEL - the model structure with the kernels updated.
%	  LL - the log likelihood of the model after update.
%	 Arguments:
%	  MODEL - the model structure for which kernels are being updated.
%	
%
%	See also
%	% SEEALSO GPSIMMAPEXPANDPARAM, GPSIMMAPCREATE


%	Copyright (c) 2006 % COPYRIGHT Neil D. Lawrence
% 	gpsimMapUpdateKernels.m CVS version 1.2
% 	gpsimMapUpdateKernels.m SVN version 7
% 	last update 2008-02-18T10:37:35.000000Z

model.K = kernCompute(model.kern, model.mapt);
model.K = model.K + eye(size(model.K, 1))*1e-6;
% Add jitter.
[model.invK, U] = pdinv(model.K);
%if jitter>1e-4
%  fprintf('Warning: gpsimMapUpdateKernels added jitter of %2.4f\n', jitter)
%end
model.logDetK = logdet(model.K, U);
model = gpsimMapUpdatePosteriorCovariance(model);

