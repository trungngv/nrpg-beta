function [param, names] = gpdisimExtractParam(model)

% GPDISIMEXTRACTPARAM Extract the parameters of a GPDISIM model.
%
%	Description:
%
%	PARAMS = GPDISIMEXTRACTPARAM(MODEL) extracts the model parameters
%	from a structure containing the information about a Gaussian process
%	for single input motif modelling.
%	 Returns:
%	  PARAMS - a vector of parameters from the model.
%	 Arguments:
%	  MODEL - the model structure containing the information about the
%	   model.
%	
%	
%
%	See also
%	GPDISIMCREATE, GPDISIMEXPANDPARAM, MODELEXTRACTPARAM


%	Copyright (c) 2006 Neil D. Lawrence
%	Copyright (c) 2007 Antti Honkela
% 	gpdisimExtractParam.m CVS version 1.1
% 	gpdisimExtractParam.m SVN version 7
% 	last update 2008-02-18T12:13:42.000000Z

if nargout>1
  [param, names] = kernExtractParam(model.kern);
  for i=1:length(model.mu);
    names{end+1}=['Basal transcription ' num2str(i)];
  end
else
  param = kernExtractParam(model.kern);
end
fhandle = str2func([model.bTransform 'Transform']);
param = [param fhandle(model.B, 'xtoa')];


if isfield(model, 'fix')
  for i = 1:length(model.fix)
    param(model.fix(i).index) = model.fix(i).value;
  end
end
param = real(param);
