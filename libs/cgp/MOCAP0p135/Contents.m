% MOCAP toolbox
% Version 0.135		21-Oct-2008
% Copyright (c) 2008, Neil D. Lawrence
% 
, Neil D. Lawrence
% XYZHUMANEVAJOINT2POS
% BVHMODIFY Helper code for visualisation of bvh data.
% XYZANKURVISUALISE
% XYZHUMANEVADRAW
% STICKVISUALISE For drawing a stick representation of 3-D data.
% ROTATIONMATRIX Compute the rotation matrix for an angle in each direction.
% BVHREADFILE Reads a bvh file into a tree structure.
% XYZHUMANEVAVISUALISE3D
% SKELPLAYDATA Play skel motion capture data.
% XYZHUMANEVAHEADINGANGLE 
% XYZHUMANEVAANIM
% MOCAPTOOLBOXES Toolboxes required by the MOCAP toolbox.
% STICKMODIFY Helper code for visualisation of a stick man.
% XYZANKURVISUALISE2
% ACCLAIMREADSKEL Reads an ASF file into a skeleton structure.
% XYZHUMANEVAGENERATEMOVIE
% ACCLAIMPLAYFILE Play motion capture data from a asf and amc file.
% BVHVISUALISE For updating a bvh representation of 3-D data.
% SKEL2XYZ Compute XYZ values given skeleton structure and channels.
% BVHPLAYFILE Play motion capture data from a bvh file.
% SKELREVERSELOOKUP Return the number associated with the joint name.
% XYZHUMANEVAMODIFY2
% XYZHUMANEVAVISUALISE2
% SKELVISUALISE For drawing a skel representation of 3-D data.
% SKELMODIFY Update visualisation of skeleton data.
% BVHWRITEFILE Write a bvh file from a given structure and channels.
% SKELCONNECTIONMATRIX Compute the connection matrix for the structure.
% XYZHUMANEVAVISUALISE
% MOCAPRESULTSCPPBVH Load results from cpp file and visualise as a bvh format.
% XYZHUMANEVAREMOVEPART
% BVHCONNECTIONMATRIX Compute the connection matrix for the structure.
% XYZANKURANIM
% XYZANKURMODIFY
% XYZHUMANEVAMODIFY
% XYZANKUR2JOINT
% XYZANKURDRAW
% ACCLAIMLOADCHANNELS Load the channels from an AMC file.
% BVHPLAYDATA Play bvh motion capture data.
% XYZHUMANEVA2JOINT
% SMOOTHANGLECHANNELS Try and remove artificial discontinuities associated with angles.
% BVH2XYZ Compute XYZ values given structure and channels.
% ACCLAIM2XYZ Compute XYZ values given skeleton structure and channels.
% XYZHUMANEVAVISUALISEMODES
% XYZHUMANEVAALIGN
