function handle = vectorModify(handle, values)

% VECTORMODIFY Helper code for visualisation of vectorial data.
%
%	Description:
%	handle = vectorModify(handle, values)
%% 	vectorModify.m CVS version 1.2
% 	vectorModify.m SVN version 24
% 	last update 2007-03-27T18:20:37.409099Z

set(handle, 'YData', values);
