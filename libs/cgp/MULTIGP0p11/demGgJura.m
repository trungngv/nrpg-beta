
% DEMGGJURA Demonstrate multigp convolution model on JURA data using
%
%	Description:
%	the FULL covariance matrix.
% 	demGgJura.m SVN version 312
% 	last update 2009-04-08T10:39:37.000000Z

rand('twister', 1e6);
randn('state', 1e6);

dataSetName = 'juraData';
experimentNo = 1;
file = 'Cd';

% XTemp, yTemp = training
% XTestTemp, yTestTemp = validation
% think of them as X, y, XTest, yTest
[XTemp, yTemp, XTestTemp, yTestTemp] = mapLoadData([dataSetName file])

scaleVal = zeros(1,size(yTemp, 2));
biasVal = zeros(1,size(yTemp, 2));
for k =1:size(yTemp, 2),
    biasVal(k) = mean(yTemp{k});
    scaleVal(k) = sqrt(var(yTemp{k}));
end

% ftc = full training condition
options = multigpOptions('ftc');
options.kernType = 'gg';
options.optimiser = 'scg';
options.nlf = 1;
options.beta = ones(1, size(yTemp, 2));
options.bias =  [zeros(1, options.nlf) biasVal];
options.scale = [zeros(1, options.nlf) scaleVal];

q = 2; % input dimension (spatical coordinates)
d = size(yTemp, 2) + options.nlf; % 4 = #number of outputs + #latent funcs

% containing data from both the latent functions and training/observed data
X = cell(size(yTemp, 2)+options.nlf,1);
y = cell(size(yTemp, 2)+options.nlf,1);

% from 1 to options.nlf are indice of latent functions data
for j=1:options.nlf
    y{j} = [];
    X{j} = zeros(1, q); % no inducing value (for ftc)
end
% from nlf to last are indice of observed data
for i = 1:size(yTemp, 2)
    y{i+options.nlf} = yTemp{i};
    X{i+options.nlf} = XTemp{i};
end

% Similarly for testing data
XTest = cell(size(yTemp, 2)+options.nlf,1);

% all one for the latent functions
for j=1:options.nlf
    XTest{j} = ones(1, q);
end
for i = 1:size(yTemp, 2)
    XTest{i+options.nlf} = XTestTemp{i};
end

% Creates the model
model = multigpCreate(q, d, X, y, options);

display = 1;
iters = 50;
% learn model parameters from the training data (X, y)
model = multigpOptimise(model, display, iters);

% Prediction
mu = multigpPosteriorMeanVar(model, XTest);
% the error is due to NoneDiagkernel returning inappropriate size..
% measure the prediction for yTestTemp{1} as this is the primary location
% that we want to predict

% yTestTemp{2}, yTestTemp{3} would contain prediction for the other 2
% secondary locations

% however the high mean error is baffling
% probably because secondary locations explain primary location but not
% other way around
maerror = mean(abs((yTestTemp{1} - mu{model.nlf + 1})))
maerror = mean(abs((yTestTemp{2} - mu{model.nlf + 2})))
maerror = mean(abs((yTestTemp{3} - mu{model.nlf + 3})))

