
% MULTIGPTOOLBOXES Load in the relevant toolboxes for MULTIGP.
%
%	Description:
%	% 	multigpToolboxes.m SVN version 402
% 	last update 2009-06-10T21:09:16.000000Z
importLatest('netlab')
importLatest('optimi')
importLatest('mltools')
importLatest('kern')
importLatest('ndlutil')
importLatest('gp')
importLatest('mocap')
importLatest('datasets')
importLatest('voicebox')