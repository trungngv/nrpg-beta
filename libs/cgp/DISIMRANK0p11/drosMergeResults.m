function r = drosMergeResults(rr),

% DROSMERGERESULTS Merge split results
%
%	Description:
%
%	R = DROSMERGERESULTS(RR) Merge split results
%	 Returns:
%	  R - one merged result structure
%	 Arguments:
%	  RR - cell array of result structures
%	
%
%	See also
%	DROSSCORETFTARGETLIST, DROSSCORETFTARGETLISTMT


%	Copyright (c) 2009 Antti Honkela
% 	drosMergeResults.m SVN version 613
% 	last update 2009-11-22T11:00:39.000000Z

N = length(rr);

r = rr{1};
I = ~(r.ll == 0);
r.ll(isnan(r.ll)) = -Inf;

for k=2:N,
  rr{k}.ll(isnan(rr{k}.ll)) = -Inf;
  J = ~(rr{k}.ll == 0);
  L = max(length(I), length(J));
  I(end+1:L) = false;
  J(end+1:L) = false;
  if any(r.ll(I & J) ~= rr{k}.ll(I & J)) || ~all(strcmp(r.targets, rr{k}.targets)),
    error('drosMergeResults: Components are inconsistent');
  end
  r.ll(J) = rr{k}.ll(J);
  rr{k}.params(length(rr{k}.params)+1:length(J)) = {[]};
  r.params(J) = rr{k}.params(J);
  I = ~(r.ll == 0);
end
