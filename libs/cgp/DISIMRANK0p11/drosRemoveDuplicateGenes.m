function r = drosRemoveDuplicateGenes(drosexp, r0),

% DROSREMOVEDUPLICATEGENES Remove duplicate probes from ranking.
%
%	Description:
%
%	RANKING = DROSREMOVEDUPLICATEGENES(DROSEXP, R0) Remove duplicate
%	probes from ranking.
%	 Returns:
%	  RANKING - cleaned ranking
%	 Arguments:
%	  DROSEXP - the drosexp data structure from drosLoadData
%	  R0 - the initial ranking
%	
%
%	See also
%	DROSLOADDATA, DROSSCORETFTARGETLIST


%	Copyright (c) 2009 Antti Honkela
% 	drosRemoveDuplicateGenes.m SVN version 623
% 	last update 2009-12-02T18:31:15.000000Z

[B, I, J] = unique(drosexp.genes(r0), 'first');
r = r0(sort(I));

J = strcmp(drosexp.genes(r), 'NA');
r = r(~J);
