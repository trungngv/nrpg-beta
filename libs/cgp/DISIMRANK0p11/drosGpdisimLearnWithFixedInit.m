function model = drosGpdisimLearnWithFixedInit(drosexp, drosTF, tf, targets, initparams, fixcomps, randomize),

% DROSGPDISIMLEARNWITHFIXEDINIT Learns a GPDISIM model for given genes.
%
%	Description:
%
%	MODEL = DROSGPDISIMLEARNWITHFIXEDINIT(DROSEXP, DROSTF, TF, TARGETS,
%	INITPARAMS, FIXCOMPS, RANDOMIZE) Learns a GPDISIM model for given
%	genes while fixing given parameters.
%	 Returns:
%	  MODEL - the learned GPDISIM model.
%	 Arguments:
%	  DROSEXP - the drosexp data structure from drosLoadData
%	  DROSTF - the drosTF data structure from drosLoadData
%	  TF - TF symbol, should be in {'bap', 'bin', 'mef2', 'tin', 'twi'}
%	  TARGETS - a cell array of target gene probe names
%	  INITPARAMS - initial parameters to fix (use NaN for free
%	   parameters)
%	  FIXCOMPS - indices of components whos parameters are known to be
%	   fixed (speeds up learning by not re-evaluating the kernel between
%	   these)
%	  RANDOMIZE - flag if random initialisation should be used
%	   (optional, default = false)
%	
%
%	See also
%	DROSLOADDATA, DROSSCORETFTARGETLISTMT


%	Copyright (c) 2009 Antti Honkela
% 	drosGpdisimLearnWithFixedInit.m SVN version 646
% 	last update 2009-12-11T01:45:59.000000Z

if nargin < 7,
  randomize = 0;
end

tfprobe = drosTF.probes.(tf);

if ~iscell(targets),
  targets = {targets};
end

genes = [tfprobe, targets(:)'];

genenames = genes;

[y, yvar, gene, times, scale, rawExp, rawVar] = gpdisimGetDrosData(drosexp, genes);

% Get the default options structure.
options = gpsimOptions;
options.includeNoise = 1;

options.fix(1).index = 4;
options.fix(1).value = expTransform(1, 'xtoa');

% Fix first output sensitivity to fix the scaling
I = find(~isnan(initparams));
for k=1:length(I),
  options.fix(k+1).index = I(k);
  options.fix(k+1).value = initparams(I(k));
end

options.fixBlocks = fixcomps;
% initialise the model.
model.type = 'cgpdisim';  % This new model type is a hack to run
                        % the model in a hierarchical manner.
                        % need to do this more elegantly later.
for i =1:3          %% 3 original
  model.comp{i} = gpdisimCreate(length(targets), 1, times, y{i}, yvar{i}, options, struct('tf', {tf}, 'genes', {genes}));
end

if randomize,
  a = modelExtractParam(model);
  a = randn(size(a));
  model = modelExpandParam(model, a);
end

% Learn the model.
model = modelOptimise(model, [], [], 1, 3000);
