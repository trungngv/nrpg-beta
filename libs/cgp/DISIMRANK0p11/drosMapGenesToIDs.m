function ids = drosMapGenesToIDs(genes),

% DROSMAPGENESTOIDS Map Drosophila FBgn gene identifiers to integers
%
%	Description:
%
%	IDS = DROSMAPGENESTOIDS(GENES) Map Drosophila FBgn gene identifiers
%	to integers by dropping the FBgn prefix, i.e. "FBgn0012345" -> 12345
%	 Returns:
%	  IDS - An array of corresponding integer identifiers
%	 Arguments:
%	  GENES - A cell array of gene identifiers to map


%	Copyright (c) 2009 Antti Honkela
% 	drosMapGenesToIDs.m SVN version 613
% 	last update 2009-11-22T11:00:39.000000Z

ids = zeros(size(genes));
for k=1:length(genes),
  if strcmp(genes{k}, 'NA'),
    ids(k) = NaN;
  else
    ids(k) = sscanf(genes{k}(5:end), '%d');
  end
end
