% DISIMRANK toolbox
% Version 0.11		15-Apr-2010
% Copyright (c) 2010, Neil D. Lawrence
% 
, Neil D. Lawrence
% DISIMRANKTOOLBOXES Toolboxes for the DISIMRANK software.
% DROSFINDGENEINDS Find indices of given genes in a data set.
% DROSCORRELATIONRANK Rank targets by expression profile correlation.
% DROSRUNODERANKINGS Runs the baseline quadrature ranking presented in the paper
% DROSGPDISIMLEARN Learns a GPDISIM model for given genes.
% DROSODERANK Rank targets by fitting an ODE to raw expression data.
% DEMRUNRANKINGS Runs the rankings presented in the paper
% DEMPLOTMEF2MODELS Plot Mef2 sample model figures appearing in the paper
% DROSINSITUPOSITIVES Filter genes by looking for positive in-situ annotations.
% DROSSCORETFTARGETLISTMT Score a list of candidate targets using multiple-target models.
% DROSGPDISIMLEARNWITHFIXEDINIT Learns a GPDISIM model for given genes.
% DROSBOOTSTRAPEVALUATION Performs bootstrap sampling of rankings
% DROSMERGERESULTS Merge split results
% DROSDOBOOTSTRAP Perform bootstrap sampling to assess significance of differences in rankings
% DEMPLOTMODELS Plot sample model figures appearing in the paper
% DROSREMOVEDUPLICATEGENES Remove duplicate probes from ranking.
% DROSMAPGENESTOIDS Map Drosophila FBgn gene identifiers to integers
% DROSPLOTCHIPDISTANCES Plot the ChIP binding site distance figures appearing in the paper
% DROSPRINTBOOTSTRAPMATRICES Print bootstrap sampling results as a LaTeX table
% DROSPLOTACCURACYBARS Plot accuracies of alternative ranking methods.
% DROSGETDATA Get Drosophila data as processed by mmgMOS.
% DROSCOMPUTECHIPDISTANCECURVE Compute the curve of distances to ChIP binding sites
% DROSLOADDATA Loads the Drosophila data from the current directory
% DROSSCORETFTARGETLIST Score a list of candidate targets.
% DROSPLOTEVALUATION Plot the accuracy figures appearing in the paper
% DROSPLOT Plot a GPDISIM model/models
