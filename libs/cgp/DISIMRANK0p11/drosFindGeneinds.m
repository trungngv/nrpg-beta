function I = drosFindGeneinds(drosdata, genes, incnulls, doprobes),

% DROSFINDGENEINDS Find indices of given genes in a data set.
%
%	Description:
%
%	INDICES = DROSFINDGENEINDS(DROSDATA, GENES, INCNULLS, DOPROBES) Find
%	indices of given genes in a data set.
%	 Returns:
%	  INDICES - the requested indices
%	 Arguments:
%	  DROSDATA - any Drosophila data structure
%	  GENES - cell array of genes to find
%	  INCNULLS - false)
%	  DOPROBES - false)
%	
%
%	See also
%	DROSLOADDATA


%	Copyright (c) 2007-2009 Antti Honkela
% 	drosFindGeneinds.m SVN version 701
% 	last update 2010-02-22T11:27:40.000000Z

if nargin < 3,
  incnulls = 0;
end

if nargin < 4,
  doprobes = 0;
end

I = [];

if ~iscell(genes) && isfield(drosdata, 'geneids'),
  if incnulls,
    I = zeros(size(genes))';
    for k=1:length(genes),
      J = find(genes(k) == drosdata.geneids);
      if isempty(J)
	I(k) = 0;
      else
	I(k) = J;
      end
    end
  else
    for k=1:length(genes),
      J = find(genes(k) == drosdata.geneids);
      if ~isempty(J),
	I = [I, J];
      end
    end
  end
else
  if ~iscell(genes),
    genes = {genes};
  end

  if doprobes,
    myfield = 'probes';
  else
    myfield = 'genes';
  end

  for k=1:length(genes),
    J = find(strcmp(genes{k}, drosdata.(myfield)));
    if incnulls,
      if isempty(J),
	J = 0;
      end
    end
    if ~isempty(J),
      I = [I, J'];
    end
  end
end
