function r = drosCorrelationRank(drosexp, drosTF, tf),

% DROSCORRELATIONRANK Rank targets by expression profile correlation.
%
%	Description:
%
%	RANKING = DROSCORRELATIONRANK(DROSEXP, DROSTF, TF) Rank targets by
%	expression profile correlation.
%	 Returns:
%	  RANKING - indeces of genes in drosexp in ranked order
%	 Arguments:
%	  DROSEXP - the drosexp data structure from drosLoadData
%	  DROSTF - the drosTF data structure from drosLoadData
%	  TF - TF symbol, should be in {'bap', 'bin', 'mef2', 'tin', 'twi'}
%	
%
%	See also
%	DROSLOADDATA, DROSSCORETFTARGETLIST


%	Copyright (c) 2009 Antti Honkela
% 	drosCorrelationRank.m SVN version 623
% 	last update 2009-12-02T18:31:15.000000Z

N = length(drosexp.probes);

medprofiles = mean(reshape(drosexp.pctiles(:, :, 3), [N, 12, 3]), 3);
I = strcmp(drosTF.probes.(tf), drosexp.probes);

sigma = zeros(1, N);
for k=1:N,
  v = corrcoef(medprofiles(I, :)', medprofiles(k, :)');
  sigma(k) = v(1, 2);
end

[foo, J] = sort(sigma, 'descend');
r = J(2:end);
