function v = drosInsituPositives(ids, drosinsitu, drosexp),

% DROSINSITUPOSITIVES Filter genes by looking for positive in-situ annotations.
%
%	Description:
%
%	V = DROSINSITUPOSITIVES(IDS, DROSINSITU, DROSEXP) Filter a set of
%	genes by retaining ones with positive in-situ annotations, keeping
%	the order of the original list.
%	 Returns:
%	  V - the filtered indices
%	 Arguments:
%	  IDS - Genes to look, expressed as indices in drosexp
%	  DROSINSITU - drosinsitu data set returned by drosLoadData
%	  DROSEXP - drosexp data set returned by drosLoadData
%	
%
%	See also
%	DROSLOADDATA


%	Copyright (c) 2009 Antti Honkela
% 	drosInsituPositives.m SVN version 707
% 	last update 2010-02-23T00:25:27.000000Z

isgenes = drosinsitu.genes(find(sum(drosinsitu.data, 2)));
I = drosFindGeneinds(drosexp, isgenes, 0);

[C, IA, IB] = intersect(ids, I);

v = ids(sort(IA));
