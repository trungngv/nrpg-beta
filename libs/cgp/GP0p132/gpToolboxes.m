
% GPTOOLBOXES Load in the relevant toolboxes for GP.
%
%	Description:
%	% 	gpToolboxes.m CVS version 1.4
% 	gpToolboxes.m SVN version 327
% 	last update 2009-04-17T21:53:14.000000Z
importLatest('netlab');
importLatest('mocap');
importLatest('ndlutil');
importLatest('prior');
importLatest('mltools');
importLatest('optimi');
importLatest('datasets');
importLatest('kern');
importLatest('noise'); % Only needed for C++ load ins.