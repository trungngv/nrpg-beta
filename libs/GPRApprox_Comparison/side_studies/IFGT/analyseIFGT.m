addpath(genpath('../../code'));
addpath(genpath('../../evaluations'));

FS = 16;
LW = 1.5;
MS = 10;
paper = [0 0 12 10]
DATASETS={'SYNTH2', 'SYNTH8', 'CHEM', 'SARCOS'}
lines = {'k-x', 'k--x', 'k:o', 'k--o'};


set(0, 'DefaultLineLineWidth', LW);
set(0, 'DefaultLineMarkerSize', MS);
set(0, 'DefaultFigurePaperUnits', 'centimeter');
set(0, 'DefaultFigurePaperPosition', paper);

for dset=1:4
  errs=[];
  times=[];
  DATASET=DATASETS{dset}
  EXPERIMENT.DATASET=DATASET;
  loadData;
  covfunc = {'covSum', {'covSEiso', 'covNoise'}};

  %---------------------------------------------------------------------------------
  % Test direct/approximate MVMs for different bandwidths. Ensure the training set 
  % is small enough (<5000) so a direct comparison with exact results is possible.
  %---------------------------------------------------------------------------------
  Nsmall = 5000;
  Niters = 5;
  errBd=1e-3;
  bandwidths=10.^[-3:1:3];
  ids = randperm(Nsmall);
  try
    trainX = trainX(ids(1:Nsmall),:);
    trainY = trainY(ids(1:Nsmall),:);
    meanMatrix = repmat(mean(trainX), n, 1);
    trainYMean = mean(trainY);
    trainYStd  = std(trainY);
    stdMatrix  = repmat(std(trainX), n, 1);
    trainX = (trainX - meanMatrix);
    trainX = trainX./stdMatrix;
    trainY = (trainY - trainYMean)./trainYStd;
  catch
    Nsmall = size(trainX,1);
  end

  i=0;
  bs = rand(Nsmall,Niters);
  for l=bandwidths
    disp(sprintf('IFGT time/error analysis. Dataset %s, bandwidth %f', DATASET, l));
    i = i+1;
    errs{i}=[];
    times{i}=[];
    plots = [];
    for j=1:Niters
      b = bs(:,j);
      hyp = [log(l), log(1), log(1e-6)];
      % Hyperparameter conversion gpml code --> IFGT code.
      amp = exp(2*hyp(2));
      h   = sqrt(2)*exp(hyp(1));
      noise = exp(2*hyp(3));
      % Define the matrix vector multiplication. The last argument indicates:
      % 0 -- use direct evaluation
      % 1 -- use IFGT
      % 2 -- use kd trees
      % 3 -- use kd trees + IFGT
      % 4 -- automatically choose the best method 
      % (see the docs for figtree for an explanation).
      mvm = @(y,err)(amp*figtree(trainX', h, y, trainX', err, 1) + noise*y);
      exactTic=tic;
      K = feval(covfunc{:}, hyp, trainX);
      exactSol = K*b;
      exactTime=toc(exactTic);
      apprTic=tic;
      apprSol1 = mvm(b,errBd);
      apprTime1=toc(apprTic);
      mvm = @(y,err)(amp*figtree(trainX', h, y, trainX', err, 4) + noise*y);
      appTic=tic;
      apprSol2 = mvm(b,errBd);
      apprTime2=toc(apprTic);
      mvm = @(y,err)(amp*figtree(trainX', h, y, trainX', err, 0) + noise*y);
      exact2Tic=tic;
      exactSol2 = mvm(b,errBd);
      exactTime2=toc(apprTic);
      errs{i} = [errs{i} [mean(abs(exactSol - apprSol1)); mean(abs(exactSol - apprSol2))]];
      times{i} = [times{i} [exactTime; apprTime1; apprTime2; exactTime2]];
    end
  end
  % Make time and error plots for this dataset.
  meansAppr1=zeros(1,length(bandwidths));
  varsAppr1=zeros(1,length(bandwidths));
  meansAppr2=zeros(1,length(bandwidths));
  varsAppr2=zeros(1,length(bandwidths));
  timesMean1=zeros(1,length(bandwidths));
  timesMean2=zeros(1,length(bandwidths));
  timesMean3=zeros(1,length(bandwidths));
  timesMean4=zeros(1,length(bandwidths));
  for i=1:length(bandwidths)
    mErr=mean(errs{i},2);
    vErr=var(errs{i},1,2);
    tMean=mean(times{i},2);
    tVar =var(times{i},1,2);
    meansAppr1(i) = mErr(1);
    meansAppr2(i) = mErr(2);
    varsAppr1(i) = vErr(1);
    varsAppr2(i) = vErr(2);
    timesMean1(i) = tMean(1);
    timesMean2(i) = tMean(2);
    timesMean3(i) = tMean(3);
    timesMean4(i) = tMean(4);
    timesVar1(i) = tVar(1);
    timesVar2(i) = tVar(2);
    timesVar3(i) = tVar(3);
    timesVar4(i) = tVar(4);
  end
  if 0
    set(gcf, 'PaperPosition', paper, 'PaperUnits', 'centimeter');
    clf; 
    hold on;
    set(gcf, 'PaperPosition', paper, 'PaperUnits', 'centimeter');
    set(gca, 'XScale', 'log');
    set(gca, 'YScale', 'log');
    set(gca, 'FontSize', FS);
    plots{1} = errorbar(bandwidths, meansAppr1, sqrt(varsAppr1),...
                                        lines{1}, 'LineWidth', LW);
    plots{2} = errorbar(bandwidths, meansAppr2, sqrt(varsAppr2),...
                                        lines{2}, 'LineWidth', LW);
    xlabel('Lengthscale');
    ylabel('Absolute error (data var. = 1)');
    h_leg=legend(cell2mat(plots), 'IFGT', 'Auto', 'Location', 'Best');
    set(h_leg, 'FontSize', FS-2);
    saveas(gca, sprintf('Errors_%s', DATASET), 'png');
    saveas(gca, sprintf('Errors_%s', DATASET), 'fig');
  end

  if 1
    clf; hold on;
    set(gcf, 'PaperPosition', paper, 'PaperUnits', 'centimeter');
    set(gca, 'XScale', 'log');
    set(gca, 'YScale', 'log');
    set(gca, 'FontSize', FS);
    plots{1} = errorbar(bandwidths, timesMean1, sqrt(timesVar1),...
                                        lines{1}, 'LineWidth', LW);
    plots{2} = errorbar(bandwidths, timesMean2, sqrt(timesVar2),...
                                        lines{2}, 'LineWidth', LW);
    plots{3} = errorbar(bandwidths, timesMean3, sqrt(timesVar3),...
                                        lines{3}, 'LineWidth', LW);
    %plots{4} = errorbar(bandwidths, timesMean4, sqrt(timesVar4),...
    %           lines{4}, 'LineWidth', LW);
    xlabel('Lengthscale');
    ylabel(sprintf('MVM time [s] (N=%d)', Nsmall));
    h_leg=legend(cell2mat(plots), 'Exact Matlab', 'IFGT', 'Auto',  'Location', 'SouthWest');
    set(h_leg, 'FontSize', FS-2);
    saveas(gca, sprintf('Times2_%s', DATASET), 'png');
    saveas(gca, sprintf('Times2_%s', DATASET), 'fig');
  end
end
