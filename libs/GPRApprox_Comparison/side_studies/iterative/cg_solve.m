function [yy, iters, checks, all_yy] = cg_solve(yy, A, bb, max_iters, tol_cg)
% function [yy, iters, checks, all_yy] = cg_solve(yy, A, bb, max_iters, tol_cg)
%
% Pretty standard conjugate gradients for solving linear systems to some
% tolerance. This routine does one simple thing to detect problems caused by
% round-off errors. But no guarantees.
%
% Finds (approximately) yy = inv(A)*bb = A\bb
%
% Other inputs:
%              yy Nx1 Initial condition for optimization, e.g. zeros(N, 1)
%               A NxN symmetric positive definite matrix, or a function that
%                     will multiply a vector by such a matrix. Hopefully more
%                     efficiently than the explicit O(n^2) multiplication.
%              bb Nx1 
%       max_iters 1x1 
%          tol_cg 1x1 Wait until the residuals are this fraction of the original
%                     residual. Rule of thumb, if the A function is approximate:
%                     make this bigger than the possible fractional error of
%                     your gradients or you'll be in trouble.
%
% Outputs:
%              yy Nx1 (Approximate) solution to linear system
%           iters 1x1 Number of iterations used
%          checks 1x1 Number of times checked residual to avoid premature termination
%
%          all_yy Nx? Contains yy after each iter. For debugging/
%                     exploratory purposes only. Do not request if not needed.

% Iain Murray, May 2006, January 2008, August 2008

if ~isa(A, 'function_handle')
    A = @(x) A*x;
end

store_all = (nargout == 4);
if store_all
    yy_storage = 10;
    all_yy = zeros(length(yy), yy_storage);
end


iters = 0;
checks = 0;
residual = bb - A(yy);
direction = residual;
delta_new = residual'*residual;
delta_init = delta_new;
tol2 = tol_cg*tol_cg;
iters_to_hack = 20;

while (iters<max_iters) && (delta_new>(tol2*delta_init))
    qq = A(direction);
    alpha = delta_new/(direction'*qq);
    yy = yy + alpha*direction;
    if ~mod(iters, iters_to_hack)
        residual = bb - A(yy);
    else
        residual = residual - alpha*qq;
    end
    delta_old = delta_new;
    delta_new = residual'*residual;
    if delta_new<(tol2*delta_init)
        % An attempt to check termination isn't spurious
        residual = bb - A(yy);
        delta_new = residual'*residual;
        checks = checks + 1;
    end
    beta = delta_new/delta_old;
    direction = residual + beta*direction;
    iters = iters + 1;

    if store_all
        if iters > yy_storage
            all_yy = [all_yy, zeros(length(yy), yy_storage)];
            yy_storage = yy_storage * 2;
        end
        all_yy(:, iters) = yy;
    end
end

if store_all
    all_yy = all_yy(:, 1:iters);
end

