function [x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = get_sarcos(subset_size)
%GET_SARCOS return SARCOS data set with some standard pre-processing and corresponding ML hyper-parameters
%
% A number of experiments have been run on SARCOS with the exact pre-processing
% below and the corresponding hyper-parameters. When using subsets, it is
% recommended to simply take the first m points. The data has already been
% randomly permuted, and using the same subsets helps aid comparison.
%
% The hyper-parameters were found by ML fitting on a subset of size 4096. Better
% test performance can often be obtained with different values for different
% dataset sizes and when using approximations that use small sets of inducing
% points.
%
% Inputs:
%    subset_size    1 x 1 Optional. Reduce n, training set size to subset_size
%
% Outputs:
%        x_train    D x n
%        y_train    n x 1
%         x_test    D x n_*
%         y_test  n_* x 1
%        lengths    D x 1 (for covariance that has 0.5 inside exponential)
%                         "bandwidth", h = length*sqrt(2)
%      noise_var    1 x 1
%        sig_var    1 x 1
%
% Number of training points, n = 444484 (or subset_size)
%   Number of test points, n_* = 4449
%      Input dimensionality, D = 21



% sarcos hypers, instructions from ed:
% copied file from ~snelson/collaborations/sarcos
% load ss10_res4096.mat resX
% hyp_init = resX(6,:)';
% My notes from what he said: hypers are in Carl's format. Last one is
% log(sqrt(noise variance)). Rest are log(lengthscale) not log(lengthscale^2). And
% lengthscale is for kernel that has a 0.5 inside the exponential.
% Actually not quite: the second last one is the log(sqrt(fn_var))

load 'sarcos_inv';
load 'sarcos_inv_test';
x_train = sarcos_inv(:,1:21);
y_train = sarcos_inv(:,22);
x_test = sarcos_inv_test(:,1:21);
y_test = sarcos_inv_test(:,22);
clear sarcos_inv sarcos_inv_test

[N, inDim] = size(x_train);
Nt = length(y_test);

% do scaling for zero-mean unit var on x's
muw = mean(x_train); sw = std(x_train,1);
x_train = x_train - repmat(muw,N,1);
x_train = x_train ./ repmat(sw,N,1);
x_test = x_test - repmat(muw,Nt,1);
x_test = x_test ./ repmat(sw,Nt,1);

% shift y's to zero mean but don't rescale
me_y = mean(y_train);
y_train = y_train - me_y;
y_test = y_test - me_y;

% The data were permuted using this random permutation in Matlab.
% rand('state', 0);
% perm = randperm(N);
% To allow using Matlab clones, and to prevent messing with the RNG state,
% the permutation was saved out like this:
% save('sarcos_perm.mat', 'perm');
% So now it is loaded from the saved file instead:
load('sarcos_perm.mat');

if ~exist('subset_size')
    subset_size = N;
end
x_train = x_train(perm(1:subset_size), :);
y_train = y_train(perm(1:subset_size));

% Swap parity. GPML code uses NxD throughout, but DxN is arguably a more
% sensible layout in memory, and is more common in "fast" GP codes.
x_train = x_train';
x_test = x_test';

% use ckiw's hypers -- these are in GPML format
% These assume the above transformations were done
load 'ss10_res4096.mat' resX
X = resX(6,:)';
noise_var = exp(2*X(end));
lengths = exp(X(1:inDim));
sig_var = exp(2*X(inDim+1));

