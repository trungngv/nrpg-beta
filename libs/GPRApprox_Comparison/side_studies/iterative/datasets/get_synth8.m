function [x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = get_synth8(subset_size)
%GET_SYNTH8 return synth8 data set with true hyper-parameters
%
% Inputs:
%    subset_size    1 x 1 Optional. Reduce n, training set size to subset_size
%
% Outputs:
%        x_train    D x n
%        y_train    n x 1
%         x_test    D x n_*
%         y_test  n_* x 1
%        lengths    D x 1 (for covariance that has 0.5 inside exponential)
%                         "bandwidth", h = length*sqrt(2)
%      noise_var    1 x 1
%        sig_var    1 x 1
%
% Number of training points, n = 444484 (or subset_size)
%   Number of test points, n_* = 4449
%      Input dimensionality, D = 8


load('T08');
load('T00'); % Gaussian noise.
[Nboth, inDim] = size(x);

% True hypers:
noise_var = 0.001;
lengths = ones(inDim, 1);
sig_var = 1;

N = Nboth / 2;
x_train = x(1:N, :);
y_train = y(1:N);
x_test = x((N+1):end, :);
y_test = y((N+1):end);
y_train = y_train + sqrt(noise_var)*noise(1:N); % Add noise to the data. 
y_test = y_test + sqrt(noise_var)*noise((N+1):end);
Nt = length(y_test);
clear x y

if ~exist('subset_size')
    subset_size = N;
end
x_train = x_train(1:subset_size, :);
y_train = y_train(1:subset_size);

% Swap parity. GPML code uses NxD throughout, but DxN is arguably a more
% sensible layout in memory, and is more common in "fast" GP codes.
x_train = x_train';
x_test = x_test';

