function iterative_runtime(dataset)
% In iterative progress we looked at how the iterative methods perform over
% their iterations. We can't get good times from that, as normally we wouldn't
% be having to save out all the intermediate results.
%
% Here we time some normal runs with fixed numbers of iterations.


%subset_size = 128;
%subset_size = 8192;
subset_size = 16384;

get_fn = eval(['@get_', dataset]);

[x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = ...
        get_fn(subset_size);
tic
[M, yy, test_fn] = gp_se_mean_setup(x_train, y_train, x_test, y_test, noise_var, lengths, sig_var);
setup_time = toc

% So methods hit max_iters rather than returning earlier:
tol = 0;

% CG
max_iters = 100;
init = zeros(size(yy));
tic
[cg100_alpha, iters, checks] = cg_solve(init, M, yy, max_iters, tol);
cg100_time = toc

% backfit
max_iters = 100;
num_clusters = 16;
warn = false;
sets = make_subsets(subset_size, num_clusters);
tic
[bf100_alpha, iters, bf_alphas] = backfit_solve(sets, M, yy, tol, max_iters, warn);
bf100_time = toc

% CG
max_iters = 1000;
init = zeros(size(yy));
tic
[cg1000_alpha, iters, checks] = cg_solve(init, M, yy, max_iters, tol);
cg1000_time = toc

% backfit
max_iters = 1000;
num_clusters = 16;
warn = false;
sets = make_subsets(subset_size, num_clusters);
tic
[bf1000_alpha, iters, bf_alphas] = backfit_solve(sets, M, yy, tol, max_iters, warn);
bf1000_time = toc


save(['results/iterative_runtime_', dataset], ...
        'subset_size', ...
        'setup_time', ...
        'cg100_alpha', 'cg100_time', ...
        'cg1000_alpha', 'cg1000_time', ...
        'bf100_alpha', 'bf100_time', ...
        'bf1000_alpha', 'bf1000_time');

