function iterative_sod_compare(dataset)
% Compare the progress of the CG and the backfitting (or block Gauss--Seidel,
% whatever) solvers

load(['results/iterative_progress_', dataset]);
get_fn = eval(['@get_',dataset]);

%%%%%%%%

half_size = subset_size / 2;

[x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = ...
        get_fn(half_size);

tic
[M, yy, test_fn] = gp_se_mean_setup(x_train, y_train, x_test, y_test, noise_var, lengths, sig_var);
half_setup_time = toc;

tic
alpha = solve_chol(chol(M), yy); % M\yy
half_chol_time = toc;
tic
half_full_smse = test_fn(alpha);
half_test_time = toc;

clear M yy
%%%%%%%%%

quart_size = subset_size / 4;

[x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = ...
        get_fn(quart_size);

tic
[M, yy, test_fn] = gp_se_mean_setup(x_train, y_train, x_test, y_test, noise_var, lengths, sig_var);
quart_setup_time = toc;

tic
alpha = solve_chol(chol(M), yy); % M\yy
quart_chol_time = toc;
tic
quart_full_smse = test_fn(alpha);
quart_test_time = toc;

%%%%%%%%%

save(['results/iterative_sod_compare_', dataset], ...
    'half_chol_time', 'half_full_smse', 'half_setup_time', 'half_size', ...
    'quart_chol_time', 'quart_full_smse', 'quart_setup_time', 'quart_size', ...
    'half_test_time', 'quart_test_time');

