function [M, yy, test_fn] = gp_se_mean_setup(x_train, y_train, x_test, y_test, noise_var, lengths, sig_var)
% This function is for studying just the solving of the Gaussian Process mean
% for the squared-exponential kernel. This is a very limited application of GPs
% and is not recommended!
%
% It rescales the problem so that kernel computations are efficient.
% It then returns an `M' matrix (K+\sigma_n^2) and yy.
%
% The user should solve alpha = M\yy using whatever linear system solver is being studied,
% and then call mu_fn(alpha) to make predictions at x_test in original scaling.
%
% The point of this function is that computing M is pretty expensive and maybe
% the user wants to try out lots of different linear system solvers with all the
% GP details abstracted away.
%
%      lengths Dx1 Lengthscales, as in l in exp(-0.5*(x_i-x_j)^2/l^2)
%              Note l=h/sqrt(2). lengths may be 1x1 specifying a single
%              lengthscale.

% Iain Murray, August 2008

[D, N] = size(x_train);
yy = y_train(:);
assert(length(yy) == N);

xscaling = (nargin > 5);
if xscaling
    x_train = bsxfun(@rdivide, x_train, lengths(:)*sqrt(2));
    x_test = bsxfun(@rdivide, x_test, lengths(:)*sqrt(2));
end

yscaling = (nargin > 6) && (sig_var ~= 1);
if yscaling
    yy = yy/sqrt(sig_var);
    noise_var = noise_var/sig_var;
end

M = plus_diag(ugauss_Knm(x_train), noise_var);
Kstar = ugauss_Knm(x_train, x_test);
test_fn = @(alpha) test_fn_stub(alpha, Kstar, yscaling, sig_var, y_test);


function [smse, mse, mu] = test_fn_stub(alpha, Kstar, yscaling, sig_var, y_test)
mu = Kstar'*alpha;
if yscaling
    mu = mu*sqrt(sig_var);
end
mse = mean((y_test - mu).^2);
smse = mse/var(y_test);

