function idx = random_binary_splits_max_size(xx, max_size)
%RANDOM_BINARY_SPLITS_MAX_SIZE cluster ids based on hierarchical splits
%
%Split directions chosen by picking random pairs of points.
%
%          xx DxN input points
%    max_size 1x1 Maximum size of a cluster (will split if bigger)
%  
%         idx 1xN labels from 1:K
%
% K is determined at run-time and depends on positions of xx and randomness

[D, N] = size(xx);

sq_dist = @(x1, x2) ...
    bsxfun(@plus, bsxfun(@minus, sum(x1.*x1,1)', x1'*(2*x2)), sum(x2.*x2,1));

perm = randperm(N);
direction = xx(:, perm(1)) - xx(:, perm(2));
proj_pos = direction'*xx;
idx = ones(size(proj_pos));
idx(proj_pos > median(proj_pos)) = 2;

% This code is a bit wasteful, because I just copy-pasted it from something else
% Should work

update_sets = {find(idx==1), find(idx==2)};

num_clusters = 0;
for update_num = 1:2
    uidx = update_sets{update_num};
    if length(uidx) > max_size
        idx(uidx) = random_binary_splits_max_size(xx(:, uidx), max_size);
    else
        idx(uidx) = 1;
    end
    idx(uidx) = idx(uidx) + num_clusters;
    num_clusters = num_clusters + max(idx(uidx));
end

