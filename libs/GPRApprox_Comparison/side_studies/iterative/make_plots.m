function make_plots(dataset)

load(['results/iterative_sod_compare_', dataset]);
load(['results/iterative_runtime_', dataset]);
load(['results/iterative_progress_', dataset]);

FS = 16;
LW = 1.5;
MS = 10;
paper = [0 0 12 10];

set(0, 'DefaultLineLineWidth', LW);
set(0, 'DefaultLineMarkerSize', MS);
set(0, 'DefaultFigurePaperUnits', 'centimeter');
set(0, 'DefaultFigurePaperPosition', paper);


PPscale = 0.5;


figure(1); clf; hold on;
plot(1:max_iters, cg_rel_res, 'b');
plot(1:max_iters, cg2_rel_res, 'm--');
plot(1:max_iters, bfc_rel_res, 'g-.');
plot(1:max_iters, bf_rel_res, 'r:');
set(gca, 'XScale', 'log', 'YScale', 'log');
ylabel('Relative residual', 'FontSize', FS);
xlabel('Iteration number', 'FontSize', FS);
set(gca, 'Box', 'on');
set(gca, 'FontSize', FS);
axis tight
h_leg = legend('CG', 'CG-init', 'DD-RPC', 'DD', 'Location', 'SouthWest');
set(h_leg, 'FontSize', FS - 2);

filename = ['results/iter_rel_res_', dataset, '.eps'];
print(filename, '-depsc');
system(['epstopdf ', filename]);


figure(2); clf; hold on;
plot([1 max_iters], [full_smse full_smse], 'k');
plot([1 max_iters], [half_full_smse half_full_smse], 'k--');
plot([1 max_iters], [quart_full_smse quart_full_smse], 'k:');
plot(1:max_iters, cg_smse, 'b');
plot(1:max_iters, cg2_smse, 'm--');
plot(1:max_iters, bfc_smse, 'g-.');
plot(1:max_iters, bf_smse, 'r:');
set(gca, 'XScale', 'log', 'YScale', 'log');
ylabel('SMSE', 'FontSize', FS);
xlabel('Iteration number', 'FontSize', FS);
set(gca, 'Box', 'on');
set(gca, 'FontSize', FS);
axis tight
ax = axis;
ax(3) = 0.01;
axis(ax);

%legend('Full GP', 'SoD', 'CG', 'CG init', 'GS cluster', 'GS', 'Location', 'Best');
% Rely on legend in first plot

filename = ['results/iter_smse_', dataset, '.eps'];
print(filename, '-depsc');
system(['epstopdf ', filename]);


figure(3); clf; hold on;
plot([1 max_iters], [full_smse full_smse], 'k');
plot([1 max_iters], [half_full_smse half_full_smse], 'k--');
plot([1 max_iters], [quart_full_smse quart_full_smse], 'k:');
plot(1:max_iters, cg2_smse, 'm--');
plot(1:max_iters, bfc_smse, 'g-.');
plot(1:max_iters, bf_smse, 'r:');
plot(1:max_iters, cg_smse, 'b');
%set(gca, 'XScale', 'log', 'YScale', 'log');
set(gca, 'XScale', 'log');
ylabel('SMSE', 'FontSize', FS);
xlabel('Iteration number', 'FontSize', FS);
set(gca, 'Box', 'on');
set(gca, 'FontSize', FS);
% HACK
if isequal(dataset, 'sarcos')
    axis([10 1e3 0.012 0.022]);
else
    % TODO
    %axis([10 1e3 0.012 0.022]);
end

%legend('Full GP', 'SoD', 'CG', 'CG init', 'GS cluster', 'GS', 'Location', 'Best');
% Rely on legend in first plot

filename = ['results/iter_smse_detail_', dataset, '.eps'];
print(filename, '-depsc');
system(['epstopdf ', filename]);




cg_times = setup_time + cg1000_time*(1:max_iters)/1000;
bf_times = setup_time + bf1000_time*(1:max_iters)/1000;

% TODO FIXME including test time in chol_time but not in cg_times. D'oh.
full_time = setup_time + chol_time;
half_time = half_setup_time + half_chol_time;
quart_time = quart_setup_time + quart_chol_time;

min_time = min([quart_time, cg_times(1), bf_times(1)]);
max_time = max([full_time, cg_times(end), bf_times(end)]);

figure(4); clf; hold on;
plot(bf_times, bfc_smse, 'g-.');
plot(bf_times, bf_smse, 'r:');
plot(cg_times, cg_smse, 'b');
plot(cg_times, cg2_smse, 'm--');
plot([min_time max_time], [full_smse full_smse], 'k');
plot([min_time max_time], [half_full_smse half_full_smse], 'k--');
plot([min_time max_time], [quart_full_smse quart_full_smse], 'k:');
plot(full_time, full_smse, 'kx', 'LineWidth', 2);
plot(half_time, half_full_smse, 'kx', 'LineWidth', 2);
plot(quart_time, quart_full_smse, 'kx', 'LineWidth', 2);
set(gca, 'XScale', 'log', 'YScale', 'log');
ylabel('SMSE', 'FontSize', FS);
xlabel('Time / s', 'FontSize', FS);
set(gca, 'Box', 'on');
set(gca, 'FontSize', FS);
%axis([min_time 900 0.012 0.025]);
%legend('CG', 'CG init', 'GS cluster', 'GS', 'Location', 'SouthWest');
if isequal(dataset, 'sarcos')
    axis([5.5 1.3e3 0.012 0.022]);
    set(gca, 'XTick', [10 100 1000]); % Not sure why all but one Tick disappeared without this.
else
    % TODO
    %axis tight
    axis([12 425 0.15 0.48]);
end

filename = ['results/smse_time_', dataset, '.eps'];
print(filename, '-depsc');
system(['epstopdf ', filename]);

