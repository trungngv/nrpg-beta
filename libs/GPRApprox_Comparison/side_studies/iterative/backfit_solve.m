function [alpha, iters, alphas] = backfit_solve(sets, A, yy, tol, max_iters, warn)
%BACKFIT_SOLVE solves p.d. linear system with a block Gauss-Siedel variant
%
% This function should be equivalent to
%     alpha = A\yy    (that is, alpha = inv(A)*yy)
% where A is positive-definite. When tol>0 the system is solved approximately.
%
% Inputs:
%          sets  cel  array of index sets. Its union should be 1:N.
%             A  NxN  Unlike CG code this must be an actual matrix (not a
%                     function that will apply one to a vector).
%            yy  Nx1
%           tol  1x1  Allowable RMSE on fit of A*alpha to target yy
%     max_iters  1x1  Optional. Default 1000.
%          warn  1x1  Optional. Default true. Warn of failure to converge?
%
% Outputs:
%         alpha  Nx1  A*alpha = yy (approximately)
%         iters  1x1  Number of iterations until convergence (or hit max_iters)
%
%        alphas  Nx?  Contains alpha after each iter. For debugging/
%                     exploratory purposes only. Do not request if not needed.

% Reference:
%     Large-scale RLSC learning without agony.
%     Li, Wenye, Lee, Kin-Hong and Leung, Kwong-Sak,
%     Proceedings of the 24th international conference on Machine learning,
%     pp529-536. ACM Press New York, NY, USA, 2007.

% Iain Murray, July 2008, August 2008

if nargin < 5; max_iters = 1000; end
if nargin < 6; warn = true; end

N = length(yy);
mm = length(sets);
iters = 0;
alpha = zeros(N, 1);
tol2 = (tol*tol) * N;

store_all = (nargout == 3);
if store_all
    alphas_storage = 10;
    alphas = zeros(N, alphas_storage);
end

% Precomputation
Am = cell(mm, 1);
cholAm = cell(mm, 1);
idxm = cell(mm, 1);
for m = 1:mm
    idx = sets{m};
    Am{m} = A(:, idx); % Might use more memory, but saves time indexing later
    cholAm{m} = chol(A(idx, idx));
end

while 1
    iters = iters + 1;

    %for m = 1:mm
    for m = randperm(mm)
        alpha_m = solve_chol(cholAm{m}, yy(sets{m}));
        % The following line needn't compute yy(sets{m}), explicitly as we know
        % the answer will be zero. However, the additional indexing would be a
        % pain and would be slow.
        yy = yy - Am{m}*alpha_m;
        alpha(sets{m}) = alpha(sets{m}) + alpha_m;
    end

    if store_all
        if iters > alphas_storage
            alphas = [alphas, zeros(N, alphas_storage)];
            alphas_storage = alphas_storage * 2;
        end
        alphas(:, iters) = alpha;
    end

    % Convergence
    if (yy' * yy) < tol2
        break;
    end

    if iters >= max_iters
        if warn
            warning(sprintf( ...
                'backfit_solve failed to converge in max_iters = %d', max_iters));
        end
        break
    end
end

if store_all
    alphas = alphas(:, 1:iters);
end

