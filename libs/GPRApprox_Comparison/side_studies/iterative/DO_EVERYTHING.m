% Takes a long time

% Make sure to set Matlab to use only one processor (version dependent)

for dataset = {'sarcos', 'synth8'}
    x = dataset{:}
    iterative_progress(dataset{:});
    iterative_runtime(dataset{:});
    iterative_sod_compare(dataset{:});

    make_plots(dataset{:});
end
