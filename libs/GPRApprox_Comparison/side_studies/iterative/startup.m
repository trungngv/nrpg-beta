% run usual startup script
startups = which('startup', '-ALL');
if length(startups) > 1
    run(startups{2})
end

addpath('datasets');

clear startups ans
