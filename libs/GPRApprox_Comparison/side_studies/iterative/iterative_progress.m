function iterative_progress(dataset)
% Compare the progress of the CG and the backfitting (or block Gauss--Seidel,
% whatever) solvers

%subset_size = 128;
%subset_size = 8192;
subset_size = 16384;

get_fn = eval(['@get_',dataset]);
[x_train, y_train, x_test, y_test, lengths, noise_var, sig_var] = ...
        get_fn(subset_size);

tic
[M, yy, test_fn] = gp_se_mean_setup(x_train, y_train, x_test, y_test, noise_var, lengths, sig_var);
setup_time = toc;

tic
alpha = solve_chol(chol(M), yy); % M\yy
chol_time = toc;
tic
full_smse = test_fn(alpha);
test_time = toc;

max_iters = 1000;
tol = 0;

% CG
init = zeros(size(yy));
[dummy, iters, checks, cg_alphas] = cg_solve(init, M, yy, max_iters, tol);

% backfit
num_clusters = 16;
warn = false;
sets = make_subsets(subset_size, num_clusters);
[dummy, iters, bf_alphas] = backfit_solve(sets, M, yy, tol, max_iters, warn);

% CG inited with 1 iteration of backfit
[dummy, iters, checks, cg2_alphas] = cg_solve(bf_alphas(:, 1), M, yy, max_iters, tol);

% backfit with clustering
cluster_idx = random_binary_splits_max_size(x_train, ceil(subset_size/num_clusters));
sets = cell(num_clusters, 1);
for cc = 1:num_clusters
    sets{cc} = find(cluster_idx == cc);
end
[dummy, iters, bfc_alphas] = backfit_solve(sets, M, yy, tol, max_iters, warn);

% Compare performance and asymptotic convergence

cg_smse = zeros(1, max_iters);
cg2_smse = zeros(1, max_iters);
bf_smse = zeros(1, max_iters);
bfc_smse = zeros(1, max_iters);
for ii = 1:max_iters
    cg_smse(ii) = test_fn(cg_alphas(:, ii));
    cg2_smse(ii) = test_fn(cg2_alphas(:, ii));
    bf_smse(ii) = test_fn(bf_alphas(:, ii));
    bfc_smse(ii) = test_fn(bfc_alphas(:, ii));
end

cg_rel_res = zeros(1, max_iters);
cg2_rel_res = zeros(1, max_iters);
bf_rel_res = zeros(1, max_iters);
bfc_rel_res = zeros(1, max_iters);
len2 = @(x) x(:)'*x(:);
for ii = 1:max_iters
    cg_rel_res(ii) = len2(yy - M*cg_alphas(:, ii))/len2(yy);
    cg2_rel_res(ii) = len2(yy - M*cg2_alphas(:, ii))/len2(yy);
    bf_rel_res(ii) = len2(yy - M*bf_alphas(:, ii))/len2(yy);
    bfc_rel_res(ii) = len2(yy - M*bfc_alphas(:, ii))/len2(yy);
end

save(['results/iterative_progress_',dataset], ...
    'bf_rel_res', 'bf_smse', 'bfc_rel_res', 'bfc_smse',...
    'cg_rel_res', 'cg_smse', 'cg2_rel_res', 'cg2_smse',...
    'chol_time', 'full_smse', 'max_iters', 'setup_time', 'subset_size');

