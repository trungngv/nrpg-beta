function idx = make_subsets(N, max_size)
%MAKE_SUBSETS creates cell array of sets no larger than max_size, partitioning 1:N
%
% Use to create batches when N/max_size isn't an integer.
% The set sizes differ by at most one. To achieve this, they might all be
% smaller than max_size.

% There's probably a vectorized one- or two-liner for this.
% This is dumb code I rattled off quickly.

% Scale down max_size until all subsets can have size in {max_size, max_size-1}
while true
    num_chunks = ceil(N/max_size);
    excess = num_chunks*max_size - N;
    if excess > num_chunks
        max_size = max_size - 1;
    else
        break;
    end
end

idx = cell(num_chunks, 1);
start = 1;
for i = 1:(num_chunks - excess)
    % Chunks of maximum size
    idx{i} = start:(start + max_size - 1);
    start = start + max_size;
end
for i = (num_chunks - excess + 1):num_chunks
    % Chunks that are smaller than max size by 1
    idx{i} = start:(start + max_size - 2);
    start = start + max_size - 1;
end

