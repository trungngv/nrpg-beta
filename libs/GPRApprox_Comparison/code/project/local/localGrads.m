function [l, g] = localGrads(logtheta, covfunc, meanfunc, likfunc, x, y, ci, varargin)
  cn = length(unique(ci));
  % Compute the log marginal likelihood and gradients.
  % In the local approximation, compute the sum of likelihoods of the 
  % clusters (and its gradient).
  %
  % Add an additional varargin (e.g. 1) to return cluster logliks.
  %
  % Krzysztof Chalupka, University of Edinburgh 2011
  [l,g] = gp(logtheta, @infExact, meanfunc, covfunc, likfunc, x(ci==0,:), y(ci==0));
  fields = fieldnames(g);
  ls = [l];
  for i=1:cn-1
    [lnew,gnew] = gp(logtheta, @infExact, meanfunc, covfunc, likfunc, x(ci==i,:), y(ci==i));
    for iField = 1:length(fields)
        g.(fields{iField}) = g.(fields{iField}) + gnew.(fields{iField});
    end
    
    if (size(varargin,2) == 0)
      l = l + lnew;
    else 
      ls = [ls lnew];
    end
  end
  
  if (size(varargin,2) == 1)
    l = ls;
  end

end
