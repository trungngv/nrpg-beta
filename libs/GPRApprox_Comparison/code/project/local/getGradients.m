function [lik, grad] = getGradients(logtheta, covfunc, x, y, ci)
  % Helper function for gp_local
  % Krzysztof Chalupka, University of Edinburgh 2011.
  cn = length(unique(ci));

  % Compute the log marginal likelihood and gradients.
  % In the local approximation, compute the sum of likelihoods of the 
  % clusters (and its gradient).
  lik = zeros(1,cn);
  grad= zeros(length(logtheta),cn);

  for i=1:cn
    [l,g] = gpr(logtheta, covfunc, x(ci==i,:), y(ci==i));
    lik(i) = l;
    grad(:,i) = g;
  end

  lik = sum(lik,2);
  grad = sum(grad,2);
end
