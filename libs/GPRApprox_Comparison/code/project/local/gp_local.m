function [out1, out2] = gp_local(logtheta, covfunc, meanfunc, ...
				 likfunc,x, y, ci, splits, splitAxes, xstar)
%function [out1, out2] = gp_local(logtheta, covfunc, meanfunc, likfunc,x, y, ci, splits, splitAxes, xstar)

% gpr_local - Gaussian Process regression, using a local approximation. 
% The algorithm clusters the training points first, then runs independent
% GP regression on each cluster.
%
% usage: loghyper = gp_local(logtheta, covfunc, x, y, ci, method)
%    or: [mu S2]  = gp_local(logtheta, covfunc, x, y, ci, cc, xstar)
%
% where:
%
%   logtheta is a (column) vector of log hyperparameters common for all the
%             clusters or a matrix with colums specifying different hyperpara-
%             meters for each clusater.
%   covfunc  is the covariance function
%   x        is a n by D matrix of training inputs
%   y        is a (column) vector (of size n) of targets
%   ci       is a (row) vector of indices of the cluster indices
%             each point in x is assigned to
%   method   if set to 'joint' then find one set of hyperparameters for
%             all the clusters. If 'separate' then return a matrix of 
%             hyperparameter sets, one for each cluster.
%   cc       is a  D by cn vector of cluster centers (only necessary for
%                regression tasks)
%   xstar    is an nn by D matrix of test points (only for regression)
%   loghyper is a vector of optimized hyperparameters
%   mu       is the mean prediction vector
%   S2       is the predictive variance
% 
%   Krzysztof Chalupka, University of Edinburgh 2011.

global testTime;
global testTimeBig;
testTimeBig = 0;
meanfunc = [];
if (nargout == 1) 
  if strcmp(splits, 'joint')
    % Optimize the hyperparameters.
    out1 = minimize(logtheta, 'localGrads', -100, covfunc, meanfunc, likfunc, x, y, ci);
  elseif strcmp(splits, 'separate')
    % Optimize the hyperparameters separately for each cluster.
    out1 = [];
    for i = 0:(size(unique(ci),2)-1)
        disp(sprintf('Optimizing hyperparameters for cluster %d of size %d', i, length(find(ci==i))));
        out1 = [ out1 minimize(logtheta, @gp, -100, @infExact, meanfunc, covfunc, likfunc, x(ci==i,:), y(ci==i)) ];
    end
  end
else
  % Do the actual regression.
  out1 = zeros(size(xstar,1),1);
  out2 = out1;

  clustTime = tic;
  % Find the cluster center closest
  % to each test point.
  % testCi = allDists(cs', xstar, size(x,2));
  % For each test point, traverse the clustering's 
  % projection-median-split tree to find the 
  % volume the test point belongs to in the Recursive Projection Clustering.
  if 1
	  treePositions = ones(1, size(xstar,1));
	  treeLvl = 0;
	  maxTreeLvl = floor(log2(length(splits)));
	  while treeLvl < maxTreeLvl
	    coeffs = dot(splitAxes(:,treePositions), xstar');
	    children = coeffs < splits(treePositions); % 0 means left child.
	    treeIncrmts = treePositions-2^treeLvl;
	    treeLvl = treeLvl+1;
	    treePositions = 2^treeLvl+2*treeIncrmts+children;
          end
          testCi = treePositions-2^treeLvl;%2*treeIncrmts+children;
  end
  % Clusters are numbered from 0.
  testCi = testCi - 1;
  if size(logtheta, 2) == 1
    joint = 1;
  else
    joint = 0;
  end
  testTimeBig = toc(clustTime);

  for i=unique(testCi)
    % If the cluster is empty or there are no test points
    % close to it, skip it.
    xlocal = x(ci==i,:);
    ylocal = y(ci==i,:);
    xslocal = xstar(testCi==i,:);
    if isempty(xlocal) || isempty(ylocal) || isempty(xslocal)
      continue;
    end

    if joint
      [testYs, vars] = gp(logtheta, @infExact, meanfunc, covfunc, likfunc, xlocal, ylocal, xslocal);
    else
      [testYs, vars] = gp(logtheta(:,i+1), @infExact, meanfunc, covfunc, likfunc, xlocal, ylocal, xslocal);
    end
    testTimeBig = testTimeBig + testTime;

    out1(testCi==i) = testYs;
    out2(testCi==i) = vars;
  end
end

