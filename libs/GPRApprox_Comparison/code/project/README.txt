This is mostly my work, but based on GPML code wherever appropriate, to keep common coding style.

inducing/ directory contains inducing points choice and clustering code. I copied figtree-0.9.3's 
clustering code into inducing/ for convenience. As a result, KCenterClustering.* and the mex files
were not created by me (see ../figtree-0.9.3/ for more details.)

Krzysztof Chalupka, 2011-10-07
