% ANALYZE_DATA: Plot standardized mean squared error (SMSE) and 
% mean standardized log loss (MSLL) as a function of time for all 
% given dataset/method.
% 
% This script should be run after RUN_EXPERIMENTS completes.
% See RUN_EXPERIMENTS for an explanation of the data format etc. 
% The evaluations/data/ directory contains by default the datafiles 
% used in our JMLR publication [1]. An easy way to compare your 
% approximation's performance with our results is to run RUN_EXPERIMENTS
% with your method, and place the results in the same directory. Then,
% simply run this script with obvious modifications (see code below).
%
% Our own results are attached, for comparison. See datafiles under
% ./Chalupka_Williams_Murray/results[METHOD]_[DATASET].m.
%
% NOTE: This provides only basic plotting, you'll probably need to set
% xlims, and ylims manually to get reasonable plots.
%
% [1] K. J. Chalupka, C. K. I. Williams, I. Murray, "A Framework for 
% Evaluating Approximation Methods for Gaussian Process Regression", 
% JMLR 2012 (submitted)
%
% Krzysztof Chalupka, University of Edinburgh 
% and California Institute of Technology, 2012
% kjchalup@caltech.edu

RESULTS_DIR = './Chalupka_Williams_Murray_Results/'; %
ANALYSIS_DIR = './plots/'; % Ready plots go here.
DATASETS = {'SYNTH2', 'SYNTH8', 'CHEM', 'SARCOS'}; % Plot data for these datasets only.
METHODS = {'SoD', 'FITC', 'Local'}; % Plot data for these methods only.

plot_colors = {'r', 'g', 'b', 'k'}; % At least as many colors as methods 
                                    % plotted. 
for dset_id = 1:length(DATASETS)
    dataset = DATASETS{dset_id};
   
    %----------------------------------------
    % Plot MSLL vs hyper-time.
    %----------------------------------------
    figure;
    for method_id = 1:length(METHODS)
        method = METHODS{method_id};
        % Load data.
        load(sprintf('%sresults%s_%s', RESULTS_DIR, method, dataset));
        results = eval(['results' method]);
        hold on;
        plot(results.hyp_time, results.msll, '.', 'Color', plot_colors{method_id});
        plots{method_id} = plot(mean(results.hyp_time), mean(results.msll), '-', 'Color', plot_colors{method_id});
        xlabel('Hyperparameter training time [s]');
        ylabel('MSLL');
        set(gca, 'xscale', 'log');
    end
    legend(cell2mat(plots), METHODS);
    print('-dpdf', [ANALYSIS_DIR dataset '_' method '_hyp_MSLL']);
                    
    %----------------------------------------
    % Plot MSLL vs test time per datapoint.
    %----------------------------------------
    figure;
    for method_id = 1:length(METHODS)
        method = METHODS{method_id};
        % Load data.
        load(sprintf('%sresults%s_%s', RESULTS_DIR, method, dataset));
        results = eval(['results' method]);
        plot(results.test_time/results.N_test, results.msll, '.', 'Color', plot_colors{method_id});
        hold on;
        plots{method_id} = plot(mean(results.test_time)/results.N_test, mean(results.msll), '-', 'Color', plot_colors{method_id});
        xlabel('Test time per datapoint [s]');
        ylabel('MSLL');
        set(gca, 'xscale', 'log');
    end
    legend(cell2mat(plots), METHODS);
    print('-dpdf', [ANALYSIS_DIR dataset '_' method '_test_MSLL']);
    
    %----------------------------------------
    % Plot SMSE vs hyper-time.
    %----------------------------------------
    figure;    
    for method_id = 1:length(METHODS)
        method = METHODS{method_id};
        % Load data.
        load(sprintf('%sresults%s_%s', RESULTS_DIR, method, dataset));
        results = eval(['results' method]);                        
        plot(results.hyp_time, results.mse, '.', 'Color', plot_colors{method_id});
        hold on;
        plots{method_id} = plot(mean(results.hyp_time), mean(results.mse), '-', 'Color', plot_colors{method_id});
        xlabel('Hyperparameter training time [s]');
        ylabel('SMSE');
        set(gca, 'xscale', 'log');
        set(gca, 'yscale', 'log');
    end
    legend(cell2mat(plots), METHODS);
    print('-dpdf', [ANALYSIS_DIR dataset '_' method '_hyp_SMSE']);
    
    %----------------------------------------
    % Plot SMSE vs test time per datapoint.
    %----------------------------------------
    figure;    
    for method_id = 1:length(METHODS)
        method = METHODS{method_id};
        % Load data.
        load(sprintf('%sresults%s_%s', RESULTS_DIR, method, dataset));
        results = eval(['results' method]);                        
        plot(results.test_time/results.N_test, results.mse, '.', 'Color', plot_colors{method_id});
        hold on;
        plots{method_id} = plot(mean(results.test_time)/results.N_test, mean(results.mse), '-', 'Color', plot_colors{method_id});
        xlabel('Test time per datapoint [s]');
        ylabel('SMSE');
        set(gca, 'xscale', 'log');
        set(gca, 'yscale', 'log');
    end
    legend(cell2mat(plots), METHODS);
    print('-dpdf', [ANALYSIS_DIR dataset '_' method '_test_SMSE']);
end
